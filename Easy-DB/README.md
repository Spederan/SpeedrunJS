# Easy-DB
Easy-DB is a NodeJS database creation and management application for beginners and individuals who do not want to deal with asynchronous code, function callbacks, or added complexity, and just want a simple synchronous database that works. This database also has built-in AES256 encryption, and there are built in functions for the secure cryptographic hashing, storage, and management of user passwords.

<sub> *Note: If you use encryption then you will want some system or strategy to manage your master password so it does not stay in memory. Otherwise the encryption would be next to pointless. For this reason it is disabled by default.* </sub>

---


To use Easy-DB download it and then require it:

```
const Database = require('./easy-db');
```

Now you will want to instantiate a new database:

```
let database = new Database();
```

This will give you a simple, unencrypted database. Alternatively, you can set your own parameters here:

   <sub> **- param1 is whether or not your database is ENCRYPTED (false by default).** </sub>
   
   <sub> **- param2 is your PASSWORD. If your database is encrypted then choose a strong one (false by default).** </sub>
   
   <sub> **- param3 is the NAME of your database ("database" by default).** </sub>
   
   <sub> **- param4 is whether or not you want UNIX timestamps (true by default).** </sub>

   <sub> **- param5 is whether or not you want password entries by default (false by default).** </sub>

   <sub> **- param6 is the number of BCrypt rounds you want on each hashed password (10 by default).** </sub>

   <sub> **- param7 is the number of PBKDF2 rounds you want on your database master password (100000 by default).** </sub>
   
   
    
```
let database = new Database(param1, param2, param3, param4, param5, param6, param7);
```

# How to use the database:

---

### Add something to the database:

  <sub> **- param1: A javascript object** </sub>
  

```
database.insert(param1)
```

Example usage:

```
database.insert({helloWorld: "test123"});
```


### Read the entire database:


```
let data = database.read()
console.log(data);
```


### Create an array of values based on a keyword and an object:

  <sub> **- param1: A string representing the key of a key-value pair stored in your database** </sub>

  <sub> **- param2: A javascript object, or one of the EasyDB functions that return a javascript object** </sub>
  

```
database.list(param1, param2)
```

Example usage:

```
database.insert({helloWorld: "test123"});
let data = database.list("helloWorld", this.read());
console.log(data);
```


### Create an array of objects that contain a specified key

  <sub> **- param1: A string representing the key of a key-value pair stored in your database** </sub>


```
database.queryAny(param1)
```

Example usage:

```
database.insert({helloWorld: "test123"});
let data = database.queryAny("helloWorld");
console.log(data);
```

### Create an array of objects that contain a specified key and hold a specified value

  <sub> **- param1: A string representing the key of a key-value pair stored in your database** </sub>
  
  <sub> **- param2: A string or number representing the value of a key-value pair stored in your database** </sub>


```
database.query(param1, param2)
```

Example usage:

```
database.insert({helloWorld: "test123"});
let data = database.query("helloWorld", "test123");
console.log(data);
```

### Create an array of objects that contain a specified key and hold a value greater than some specified value

  <sub> **- Note: Any string that is a valid number will be interpreted as a number, otherwise as a string. Sorting based on numeric values and alphabetical values both work.** </sub>

  <sub> **- param1: A string representing the key of a key-value pair stored in your database** </sub>
  
  <sub> **- param2: A string or number representing the minimum value of a key-value pair stored in your database** </sub>


```
database.queryGT(param1, param2);
```

Example usage:

```
database.insert({helloWorld: "test123"});
let data = database.queryGT("helloWorld", "g");
console.log(data);
```

*For simplicity's sake, there are three more functions that work exactly like this one but for examining greater-than-or-equals-to, less-then, etc...*

```
database.queryGTE(param1, param2);
database.queryLT(param1, param2);
database.queryLTE(param1, param2);
```

### Create an array of objects that contain a specified key and hold a value between two specified values

  <sub> **- Note: Works mostly the same as queryGTE. Also, for your convenience, you can insert the two latter parameters in either order and it will produce the same result.** </sub>

  <sub> **- param1: A string representing the key of a key-value pair stored in your database** </sub>
  
  <sub> **- param2: A string or number representing the minimum value of a key-value pair stored in your database** </sub>
  
  <sub> **- param3: A string or number representing the maximum value of a key-value pair stored in your database** </sub>


```
database.queryRange(param1, param2, param3);
```

Example usage:

```
database.insert({helloWorld: "test123"});
let data = database.queryRange("helloWorld", "a", "z");
console.log(data);
```

### Create an array of all used keys


```
database.queryKeys();
```

Example usage:

```
database.insert({helloWorld: "test123"});
let data = database.queryKeys();
console.log(data);
```

### Remove a list of entries from the database if that they match the given criteria

  <sub> **- Note: this requires rewriting the entire database, so it's recommended to do this as little as possible, or to use this.read() -> edit values in memory -> then use overwriteDatabase() to write the database all at once. It is up to you as the developer to decide how computationally efficient you wish to be.** </sub>

  <sub> **- param1: A string representing the key of a key-value pair stored in your database** </sub>
  
  <sub> **- param2: A string representing the value of a key-value pair stored in your database** </sub>


```
database.remove(param1, param2);
```

Example usage:

```
database.insert({helloWorld: "test123"});
let data = database.remove("helloWorld", "test123");
console.log(data);
```

*For simplicity's sake, there is a family of remove functions that mirror the query functions, and they behave exactly as you'd expect them to.*

```
database.removeGT(param1, param2);
database.removeGTE(param1, param2);
database.removeLT(param1, param2);
database.removeLTE(param1, param2);
database.removeRange(param1, param2, param3);
```

### Edit a value in the database in all entries with matching criteria

  <sub> **- Note: this requires rewriting the entire database, so it's recommended to do this as little as possible, or to use this.read() -> edit values in memory -> then use overwriteDatabase() to write the database all at once. It is up to you as the developer to decide how computationally efficient you wish to be.** </sub>

  <sub> **- param1: A string representing the key you are searching for, of a key-value pair stored in your database** </sub>
  
  <sub> **- param2: A string representing the value you are searching for, of a key-value pair stored in your database** </sub>
  
  <sub> **- param3: A string representing the key whose value you plan to modify, of a key-value pair stored in your database** </sub>
  
  <sub> **- param4: A string representing the value that you plan to use to overwrite the previous value, of a key-value pair stored in your database** </sub>


```
database.editValue(param1, param2, param3, param4);
```

Example usage:

```
database.insert({helloWorld: "test123", id: "abc12345"});
let data = database.editValue("id", "abc12345", "helloWorld", "test456");
console.log(data);
```

### Add a value field in the database to all entries with matching criteria

  <sub> **- Note: this requires rewriting the entire database, so it's recommended to do this as little as possible, or to use this.read() -> edit values in memory -> then use overwriteDatabase() to write the database all at once. It is up to you as the developer to decide how computationally efficient you wish to be.** </sub>

  <sub> **- param1: A string representing the key you are searching for, of a key-value pair stored in your database** </sub>
  
  <sub> **- param2: A string representing the value you are searching for, of a key-value pair stored in your database** </sub>
  
  <sub> **- param3: A string representing the key you plan to add, of a key-value pair stored in your database** </sub>
  
  <sub> **- param4: A string representing the value that you plan to add, of a key-value pair stored in your database** </sub>


```
database.addField(param1, param2, param3, param4);
```

Example usage:

```
database.insert({helloWorld: "test123"});
let data = database.addField("helloWorld", "test123", "myFavoritePie", "Cherry Pie");
console.log(data);
```

### Add a value field to all entries in your database

  <sub> **- param1: A string representing the key you plan to add** </sub>
  
  <sub> **- param2: A string representing the value that you plan to add** </sub>


```
database.addFields(param1, param2);
```

Example usage:

```
database.insert({helloWorld: "test123"});
let data = database.addFields("myFavoritePie", "Cherry Pie");
console.log(data);
```

### Edits a key in all entries of a database and renames them to something else

  <sub> **- param1: A string representing the key you plan to change** </sub>
  
  <sub> **- param2: A string representing the new key that you plan to change the old key to** </sub>


```
database.editKey(param1, param2);
```

Example usage:

```
database.insert({helloWorld: "test123"});
let data = database.editKey("helloWorld", "goodbyeWorld");
console.log(data);
```

### Add or update a cryptographic hash of a user's password (using BCrypt)
  
  <sub> **- param1: A string representing the the value of "_id".** </sub>
  
  <sub> **- param2: A string representing the password you are updating a user's password to.** </sub>


  <sub> **- Important note: This function can only query users by "_id". This is to protect you from accidentally querying multiple users at once.** </sub>
  
  <sub> **- Note2: With the default settings of 10 rounds of bcrypt hashing, you may experience around 50 ms of downtime to hash a single password. This is configurable, it is a feature of BCrypt security, and it makes it hard for attackers to brute force weak passwords in a stolen database scenario. It is highly recommended to not lower this value at all, but consider raising it if hardware allows. It is also recommended to use asynchronous code or multithreading so that bcrypt hashing doesn't block your main event loop in your server, but it's not immediately necessary if you are just learning how to create a simple application as a beginner.** </sub>


```
database.updateUserPassword(param1, param2);
```


### Check to see if a user's password is valid cryptographically

  
  <sub> **- param1: A string representing the the value in a key-value pair you are searching for. For example, the value of a "_id".** </sub>
  
  <sub> **- param2: A string representing the password you are checking.** </sub>
  
  <sub> **- param3 (optional, false by default): A boolean value. If true then the act of you checking will not change the number of failed login attempts. If false then it will automatically increment a counter stored in the user's entry if a login attempt is failed, and will reset the counter to zero if the login attempt is successful.** </sub>
  


```
database.checkUserPassword(param1, param2);
database.checkUserPassword(param1, param2, param3);
```


### Check to see how many login attempts a user has

  
  <sub> **- param1: A string representing the the value of a "_id".** </sub>
  


```
database.checkUserPasswordAttempts(param1);
```


### Backup the database with a second copy

```
database.backupDatabase();
```

### Delete all information in the database

```
database.deleteDatabase();
```

### Overwrite a database with a javascript object containing a modified version of the database

  <sub> **- param1: A javascript object representing a modified version of the database (called using read()) you'd like to overwrite the original database with.** </sub>

```
database.overwriteDatabase(param1);
```

Example usage:

```
database.insert({helloWorld: "test123"});
let obj = database.read();
obj[0]["helloWorld"] = "test456";
database.overwriteDatabase(obj);
console.log(database.read())
```

### Saves a decrypted version of your database if it is encrypted and returns the name of the new file

```
database.saveDecrypted();
```


### Change your password used if your database is encrypted, and re-encrypt all entries
  <sub> **- param1: Your new password. You don't need to input your old password since it should already be in memory, unless you intentionally removed it, then you will need to use addPassword() first.** </sub>

```
database.changePassword(param1);
```


### Add encryption to your database if it is not encrypted
  <sub> **- param1: Your new password.** </sub>

```
database.addEncryption(param1);
```

### Removes encryption from your database if it is encrypted

```
database.removeEncryption();
```

### Delete your password from memory

  <sub> **- Note: if you do this, 90% of EasyDB will no longer be functional. You will have to add your password back using addPassword().** </sub>

```
database.deletePassword();
```

### Add your password back to memory

  <sub> **- Param1 (optional): A string representing your password. If you leave this blank, then your terminal will ask you for user input, and wait for you to type your password in. If you put in the wrong password then you will get an error message.** </sub>

```
database.addPassword();
database.addPassword(param1);
```

## Wrapping up

And that is all a user of Easy-DB needs to know. There are a few more functions in DatabaseUtilities.js, but it's not assumed that you as the developer will have use for any of these. The different cryptographic primitives and helper functions live there. To reiterate, it is highly recommended not to mess with cryptographic primitives unless you know what you are doing, as doing so can compromise the security of your application.

If you have any suggestions for features to add to Easy-DB or if you find any bugs, please let me know.

And I hope your user experience with EasyDB is smooth, and easy to switch to NeDB or MongoDB later if you so choose.





