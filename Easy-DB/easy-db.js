const DatabaseUtilities = require('./databaseutilities.js');
const fs = require('fs');

class Database extends DatabaseUtilities{
    constructor(encryptedByDefault = false, password = true, name = "database", unixTimeStamps = true, defaultPasses = false, bcryptRounds = 10, pbkdf2Rounds = 100000){
        super();
        this.password = password;
        this.encryptedByDefault = encryptedByDefault;
        this.file = name;
        this.unixTimeStamps = unixTimeStamps;
        this.defaultPasses = defaultPasses;
        this.bcryptRounds = bcryptRounds;
        this.pbkdf2Rounds = pbkdf2Rounds;
        this.memory = false;
        fs.appendFileSync(`${this.file}.db`, ""); 
        let data = fs.readFileSync(`./${this.file}.db`).toString();
        if (data == ""){
            this.id = super.addRandomID();
            this.unix = super.addTimestamp();
            this.password = super.PBKDF2(this.password, this.id, this.pbkdf2Rounds);
            this.hash = super.generateHash(this.password, this.bcryptRounds);
            fs.writeFileSync(`${this.file}.db`, super.prefix());
        } else {
            let str = JSON.parse(`[${data}]`.replace(",]","]"));
            switch(str[0].encrypted){
                case "true": this.encryptedByDefault = true; break;
                case "false": this.encryptedByDefault = false; break;
            };
            this.unix = str[0]["UNIX"];
            this.id = str[0]["_id"];
            this.password = super.PBKDF2(this.password, this.id, this.pbkdf2Rounds);
            this.hash = str[0].keyCheck;
            if (!super.checkHash(this.password, this.hash)) super.badPassword();
        };
    };
    //Insert any value into the database in the form of an object
    insert(obj){
        if (!this.password) return;
        if (this.unixTimeStamps) obj["UNIX"] = super.addTimestamp();
        if (this.defaultPasses){
            obj["pass"] = false;
            obj["passAttempts"] = 0;
        };
        obj["_id"] = super.addRandomID();
        let data = [obj];
        data = super.encryptList(data);
        super.appendData(data);
    };
    //Return the entire database an an array of unnamed objects
    read(){
        if (!this.password) return;
        this.file = super.truncate(); 
        let data;
        if (!this.memory){
            data = fs.readFileSync(`./${this.file}.db`).toString();
        } else {
            return this.memory;
        };
        data.replaceAll(",,", ",");
        if (data.substring(data.length-1, data.length) === ",") data = data.substring(0, data.length-1);
        let obj = JSON.parse(`[${data}]`);
        obj.shift();
        if (obj) obj = super.decryptList(obj);
        this.memory = obj;
        return obj;
    };
    //Description: Create an array of values based on the value associated with a key in a database
    list(key, data){
        if (!this.password) return;
        let list = [];
        let c = 0;
        for (let i in data){
            if (data[i][key] || data[i][key] === 0) list[c++] = data[i][key];
        };
        return list;
    };
    //Description: Create an array of object entries based on a key in a database
    queryAny(key){
        if (!this.password) return;
        let data = this.read();
        let list = [];
        let c = 0;
        for (let i in data){
            if (data[i][key] !== undefined) list[c++] = data[i];
        };
        return list;
    };
    //Search the database and return an array of object entries meeting the criteria
    query(key, value){
        if (!this.password) return;
        value = value + "";
        let data = this.queryAny(key);
        let list = [];
        let c = 0;
        for (let i in data){
            if (data[i][key] == value) list[c++] = data[i];
        };
        return list;
    };
    //Query if data value is a number greater than the parameter value
    queryGT(key, value){
        if (!this.password) return;
        let data = this.queryAny(key);
        let list = [];
        let c = 0;
        for (let i in data){
            if (data[i][key] > value) list[c++] = data[i];
        };
        return list;
    };
    //Query if data value is a number greater than or equal to the parameter value
    queryGTE(key, value){
        if (!this.password) return;
        let data = this.queryAny(key);
        let list = [];
        let c = 0;
        for (let i in data){
            if (data[i][key] >= value) list[c++] = data[i];
        };
        return list;
    };

    //Query if data value is a number less than the parameter value
    queryLT(key, value){
        if (!this.password) return;
        let data = this.queryAny(key);
        let list = [];
        let c = 0;
        for (let i in data){
            if (data[i][key] < value) list[c++] = data[i];
        };
        return list;
    };
    //Query if data value is a number less than or equal to the parameter value
    queryLTE(key, value){
        if (!this.password) return;
        let data = this.queryAny(key);
        let list = [];
        let c = 0;
        for (let i in data){
            if (data[i][key] <= value) list[c++] = data[i];
        };
        return list;
    };
    //Query object entries if data value is between (and including) two numbers. 
    //The value parameters can be specified in either order and will return the same results.
    queryRange(key, valueGTE, valueLTE){
        if (!this.password) return;
        if (valueLTE < valueGTE){
            [valueLTE, valueGTE] = [valueGTE, valueLTE];
        };
        let data = this.queryAny(key);
        let list = [];
        let c = 0;
        for (let i in data){
            if (data[i][key] >= valueGTE && data[i][key] <= valueLTE) list[c++] = data[i];
        };
        return list;
    };
    //Search the database and return an array of all unique keys. 
    queryKeys(){
        if (!this.password) return;
        let data = this.read();
        let list = [];
        let c = 0;
        for (let i in data){
            list[c++] = Object.keys(data[i])[0];
        };
        return [...new Set(list)];
    };
    //Removes all entries meeting the following criteria
    remove(key, value){
        if (!this.password) return;
        value = value + "";
        let data = this.read();
        let list = [];
        let c = 0;
        for (let i in data){
            if ((!data[i][key] &&  data[i][key] !== 0) || typeof data[i][key] !== typeof value || data[i][key] != value) list[c++] = data[i];
        };
        
        list = super.encryptList(list);
        super.writeData(list);
    };
    //Removes all entries associated with some key greater than the following value
    removeGT(key, value){
        if (!this.password) return;
        let data = this.read();
        let list = [];
        let c = 0;
        for (let i in data){
            if ((!data[i][key] &&  data[i][key] !== 0) || typeof data[i][key] !== typeof value || data[i][key] <= value) list[c++] = data[i];
        };
        list = super.encryptList(list);
        super.writeData(list);
    };
    //Removes all entries associated with some key greater than or equal to the following value
    removeGTE(key, value){
        if (!this.password) return;
        let data = this.read();
        let list = [];
        let c = 0;
        for (let i in data){
            if ((!data[i][key] &&  data[i][key] !== 0) || typeof data[i][key] !== typeof value || data[i][key] < value) list[c++] = data[i];
        };
        list = super.encryptList(list);
        super.writeData(list);
    };
    //Removes all entries associated with some key less than the following value
    removeLT(key, value){
        if (!this.password) return;
        let data = this.read();
        let list = [];
        let c = 0;
        for (let i in data){
            if ((!data[i][key] &&  data[i][key] !== 0) || typeof data[i][key] !== typeof value || data[i][key] >= value) list[c++] = data[i];
        };
        list = super.encryptList(list);
        super.writeData(list);
    };
    //Removes all entries associated with some key less than or equal to the following value
    removeLTE(key, value){
        if (!this.password) return;
        let data = this.read();
        let list = [];
        let c = 0;
        for (let i in data){
            if ((!data[i][key] &&  data[i][key] !== 0) || typeof data[i][key] !== typeof value || data[i][key] > value) list[c++] = data[i];
        };
        list = super.encryptList(list);
        super.writeData(list);
    };
    //Removes all entries associated with some key between a range of values
    removeRange(key, valueGTE, valueLTE){
        if (!this.password) return;
        if (valueLTE < valueGTE){
            [valueLTE, valueGTE] = [valueGTE, valueLTE];
        };
        let data = this.read();
        let list = [];
        let c = 0;
        for (let i in data){
            if ((!data[i][key] &&  data[i][key] !== 0) || typeof data[i][key] !== typeof valueGTE || 
            typeof data[i][key] !== typeof valueLTE || data[i][key] < valueGTE || data[i][key] > valueLTE) 
            list[c++] = data[i];
        };
        list = super.encryptList(list);
        super.writeData(list);
    };
    //Edits the value from any key across all members
    editAny(key, value){
        if (!this.password) return;
        value = value + "";
        let data = this.read();
        for (let i in data){
            data[i][key] = value;
        };
        data = super.encryptList(data);
        super.writeData(data);
    };
    //Edits all values meeting the following criteria
    editValue(key, value, key2, newValue){
        if (!this.password) return;
        value = value + "";
        let data = this.read();
        for (let i in data){
            if (data[i][key] === value) data[i][key2] = newValue;
        };
        data = super.encryptList(data);
        super.writeData(data);
    };
    //Adds a new field to all valid entries
    addField(key, value, newKey, newValue){
        if (!this.password) return;
        let data = this.query(key, value);
        for (let i in data){
            if (data[i][key] === value) data[i][newKey] = newValue;
        };
        data = super.encryptList(data);
        super.writeData(data);
    };
    //Adds a new field to all entries
    addFields(newKey, newValue){
        if (!this.password) return;
        let data = this.read();
        for (let i in data){
            data[i][newKey] = newValue;
        };
        data = super.encryptList(data);
        super.writeData(data);
    };
    //Edits all keys meeting the following criteria
    editKey(key, key2){
        if (!this.password) return;
        if (key === "UNIX" || key === "_id" || key === "pass" || key === "passAttempts" || 
            key2 === "UNIX" || key2 === "_id" || key2 === "pass" || key2 === "passAttempts"){
            console.log("Error: You can't rename a key to that.")
            return;
        };
        let data = this.read();
        data = super.renameKey(data, key, key2);
        data = super.encryptList(data);
        super.writeData(data);
    };
    //Update or instantiate a user's password in an already existing entry
    updateUserPassword(value, userPassword){
        if (!this.password) return;
        let data = this.read();
        try {
            let user = [];
            for (let i in data){
                if (data[i]["_id"] === value) user[0] = data[i];
            };
            if (!user[0]){
                console.log(`The user ${value} does not exist.`)
                return false;
            };
            if (!user[0]["pass"]){
                this.addField("_id", value, "pass", "null");
                this.addField("_id", value, "passAttempts", "0");
            };
            let userHash = super.generateHash(userPassword, this.bcryptRounds);
            this.editValue("_id", value, "pass", userHash)
        } catch (error) {
            console.log(error);
        };
        return this.query("_id", value);
    };
    //Check a user password hash made in the database
    checkUserPassword(value, userPassword, anonymous = false){
        if (!this.password) return;
        let check = false;
        try {
            let user = this.query("_id", value);
            if (!user[0]){
                console.log(`The user ${value} does not exist.`)
                return false;
            };
            if (!user[0]["pass"]){
                console.log(`The user ${value} does not have a password.`)
                return false;
            };
            if (super.checkHash(userPassword, user[0]["pass"])) check = true;
        } catch (error) {
            
        };
        if (!anonymous){
            let user = this.query("_id", value);
            let attemptNum;
            if (!check){
                attemptNum = ++user[0].passAttempts;
            } else {
                attemptNum = 0;
            };
            if (user[0].passAttempts > 0 || attemptNum > 0) this.editValue("_id", value, "passAttempts", attemptNum)
        };
        return check;
    };
    //Checks the number of times a user has attempted their password
    checkUserPasswordAttempts(value){
        if (!this.password) return;
        try {
            let user = this.query("_id", value);
            if (!user[0]){
                console.log(`The user ${value} does not exist.`)
                return false;
            };
            if (!user[0]["pass"]){
                console.log(`The user ${value} does not have a password.`)
                return false;
            };
        } catch (error) {
        };
        let user = this.query("_id", value);
        return user[0].passAttempts;
    };
    //Backs up the working database
    backupDatabase(){
        let data = fs.readFileSync(`./${this.file}.db`).toString();
        fs.writeFileSync(`${this.file} (backup).db`, data);
    };
    //Permanently clears all data in the working database
    deleteDatabase(){
        fs.writeFileSync(`${this.file}.db`, "");
    };
    //Overwrites the working database with a new object
    //It is more computationally efficient to modify a local object generated from this.read() and overwrite it on a needed basis
    overwriteDatabase(obj, badObj){
        if (!this.password) return;
        let check = true;
        if (badObj){
            console.log("Error: You must wrap your object in brackets, and you cannot have a second parameter in this.overWriteDatabase.");
            return;
        };
        if (typeof obj === "string"){
            console.log("Error: You must wrap your object in brackets and cannot pass it as a string.");
            return;
        };
        try {
            JSON.parse(JSON.stringify(obj)); 
        } catch (error) {
            console.log(error);
            check = false;
        };
        if (check) super.writeData(super.encryptList(obj));
    };
    //Saves a decrypted version of the working database and returns the new name
    saveDecrypted(){
        if (!this.password) return;
        let data = this.read();
        let str = JSON.stringify(data);
        this.encryptedByDefaultCon = this.encryptedByDefault;
        this.encryptedByDefault = false;
        fs.writeFileSync(`decrypted_${this.file}.db`, super.prefix()+str.substring(1, str.length-1));
        this.encryptedByDefault = this.encryptedByDefaultCon;
        return `decrypted_${this.file}.db`;
    };
    //Changes the password of your local database, re-encrypting all contents
    changePassword(newPassword){
        if (!this.password) return;
        if (!this.encryptedByDefault) return;
        if (!newPassword){
            console.log("Please enter a valid password");
            return;
        };
        let data = this.read();
        this.password = super.PBKDF2(newPassword, this.id, this.pbkdf2Rounds);
        this.hash = super.generateHash(this.password, this.bcryptRounds);
        data = super.encryptList(data);
        super.writeData(data);
    };
    //Adds encryption to your local database using a password
    addEncryption(password){
        if (!this.password) return;
        let data = this.read();
        this.password = super.PBKDF2(password, this.id, this.pbkdf2Rounds);;
        this.encryptedByDefault = true;
        data = super.encryptList(data);
        super.writeData(data);
    };
    //Removes the encryption of your local database
    removeEncryption(){
        if (!this.password) return;
        if (!this.encryptedByDefault) return;
        let data = this.read();
        this.password = "";
        this.encryptedByDefault = false;
        data = super.encryptList(data);
        super.writeData(data);
    };
    //Deletes password from memory
    deletePassword(){
        this.password = false;
    };
    //Adds a password back to memory. Pass a password to do it automatically, otherwise it will wait for user input.
    addPassword(password){
        if (password){
            let pass = super.PBKDF2(password, this.id, this.pbkdf2Rounds);
            if (super.checkHash(pass, this.hash)){
                this.password = pass;
                console.clear();
            } else {
                super.badPassword();
            };
        } else {
            const readline = require('readline').createInterface({
                input: process.stdin,
                output: process.stdout
            });
            readline.question('What is your password?\n\n', pass => {
                if (pass){
                    let password = super.PBKDF2(pass, this.id, this.pbkdf2Rounds);
                    if (super.checkHash(password, this.hash)){
                        this.password = password;
                        console.clear();
                    } else {
                        super.badPassword();
                    };
                };
                readline.close();
            });
        };
    };
};

module.exports = Database;