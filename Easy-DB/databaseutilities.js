const fs = require('fs');
const crypto = require('crypto');
const bcrypt = require('../node_modules/bcrypt');
const AES256 = require('./aes256.js');
let aes256 = new AES256();

class DatabaseUtilities {
    constructor(){};
    //AES256 Encryption
    encrypt(text, salt){
        return aes256.encrypt(text, this.password+salt);
    };
    //AES256 Decryption
    decrypt(ciphertext, salt){
        let pass = true;
        let text;
        try {
            text = aes256.decrypt(ciphertext, this.password+salt);
        } catch (error) {
            pass = false;
            console.log(error);
        };
        if (pass){
            return text;
        } else {
            return ciphertext;
        };
    };
    //Generate PBKDF2 hash
    PBKDF2(password, randomBytes, pbkdf2Rounds = 100000){
        return crypto.pbkdf2Sync(password+"", randomBytes+"", pbkdf2Rounds, 100, 'sha512').toString('base64');
    };
    //Generates BCrypt hash 
    generateHash(password, bcryptRounds = 10){
        return bcrypt.hashSync(password+"", bcrypt.genSaltSync(bcryptRounds));
    };
    //Checks BCrypt hash
    checkHash(password, hash){
        return bcrypt.compareSync(password, hash);
    };
    //Warn user about an inocrrect password and block all operations
    badPassword(){
        console.log("Error: The password is incorrect and therefore no actions can be performed.");
        this.deletePassword();
    };
    //Writes the settings of the database
    prefix(){
        if (this.unixTimeStamps){
            return `{"encrypted": "${this.encryptedByDefault}", "keyCheck": "${this.hash}", "UNIX": "${this.unix}", "_id": "${this.id}"}`;
        } else {
            return `{"encrypted": "${this.encryptedByDefault}", "keyCheck": "${this.hash}", "_id": "${this.id}"}`;
        };
    };
    //Rename a key
    renameKey(obj, key, newKey){
        let str = JSON.stringify(obj);
        str = str.replaceAll(`"${key}":`, `"${newKey}":`);
        return JSON.parse(str);
    };
    //Determine if character is a number
    isNumber(s){
        s+= "";
        for (let i in s){
            if (s.substring(i,i+1) < '0' || s.substring(i,i+1) > '9') return false;
        };
        return true;
    };
    // Replace `"` and `\` with valid alernatives
    validateText(s){
        s = s + "";
        let temp1;
        let temp2;
        while (true) {
            s = s.replace("\\\\", "\\");
            s = s.replace("\\\"", "\"");
        temp1 = s;
        if (temp1 === temp2) break;
        temp2 = s;
        };
        s = s.replaceAll("\\", "\\\\");
        s = s.replaceAll("\"", "\\\"");
        return s;
    };
    //Utility for writing data to the database
    writeData(data){
        data = JSON.stringify(data);
        data = data.substring(1, data.length-1);
        if (data){
            data = this.prefix()+","+(data);
        } else {
            data = this.prefix();
        }
        fs.writeFileSync(`${this.file}.db`, data);
        this.memory = false;
    };
    //Utility for appending data to the database
    appendData(data){
        data = JSON.stringify(data);
        data = (","+data.substring(1, data.length-1));
        fs.appendFileSync(`${this.file}.db`, data);
        this.memory = false;
    };
    //A utility for encrypting a list
    encryptList(data){
        if (this.encryptedByDefault){
            let key;
            let keys;
            for (let i in data){
                keys = Object.keys(data[i]);
                for (let k in keys){
                    key = keys[k];
                    if (key === "_id" || key === "UNIX" || key === "passAttempts") continue;

                    data[i][key] = this.encrypt(data[i][key]+"", data[i]["_id"]+"");
                };
            };
        };
        return data;
    };
    //A utility for decrypting a list and parsing any numbers
    decryptList(data){
        let key;
        let keys;
        for (let i in data){
            keys = Object.keys(data[i]);
            for (let k in keys){
                key = keys[k];
                if (key === "_id" || key === "UNIX" || key === "passAttempts") continue;
                if (this.encryptedByDefault){
                    data[i][key] = this.decrypt(data[i][key], data[i]["_id"]);
                };
                if (this.isNumber(data[i][key])) data[i][key] = data[i][key]*1;
            };
        };
        return data;
    };
    //Generate a cryptographically random string of characters
    addRandomID(){
        return crypto.randomBytes(15).toString('hex');
    };
    //Adds UNIX timestamp
    addTimestamp(){
        return Date.now();;
    };
    //Generate a random number between (and including) start and stop
    random(start, stop){
        return Math.floor(Math.random() * (stop - start + 1) + start);
    };
    //Truncate the extension and require use of .db for standardization
    truncate(){
        return this.file.substring(0, (this.file+".").indexOf("."));
    };
};

module.exports = DatabaseUtilities;
