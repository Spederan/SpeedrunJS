const crypto = require('crypto');

class AES256{
    constructor(){
        /*
        Note: The password used in the encryption is the master password of the database. 
        The password is salted with separate cryptographically random data for every entry in
        the database. 
        */
    };
    encrypt(text, password){
        const key = crypto.createHash('sha256').update(password).digest('hex').substring(0,32);
        const iv =  Buffer.from(crypto.createHash('sha256').update(key).digest('hex').substring(0,16), 'utf-8')
        let mykey = crypto.createCipheriv('aes-256-cbc', key, iv);
        let ciphertext = mykey.update(text, 'utf8', 'hex')
        ciphertext += mykey.final('hex');
        return ciphertext;
    };
    decrypt(ciphertext, password){
        password = password || this.password;
        const key = crypto.createHash('sha256').update(password).digest('hex').substring(0,32);
        const iv =  Buffer.from(crypto.createHash('sha256').update(key).digest('hex').substring(0,16), 'utf-8')
        let mykey = crypto.createDecipheriv('aes-256-cbc', key, iv);
        let text = mykey.update(ciphertext, 'hex', 'utf8')
        text += mykey.final('utf8');
        return text;
    };
};

module.exports = AES256;
