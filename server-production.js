const production = (buildProduction = true, addAllFiles = false, minify = true, components = [], logs = true, logFiles = false) => {
    /* Note: At this time, "addAllFiles" excludes moneroWallet, for the reason that its not in the public directory by default.*/
    if (buildProduction){
        if (logs) console.log("Reading public files...")
        const fs = require('fs');
        var UglifyJS = require("uglify-js");
        let extraCode = "";
        let scanFiles = [`./main.js`];
        scanFiles = scanFiles.concat(components);
        const compileMain = () => {
            let fileData = '';
            for (let i in scanFiles){
                if (scanFiles[i]) fileData+= fs.readFileSync(`./public/${scanFiles[i]}`).toString() + '; ';
            };
            return fileData;
        };
        let writtenCode = UglifyJS.minify(compileMain()).code;
        let files = [`src/clientUtilities.js`, `src/objects/box.js`, `src/objects/div.js`, `src/objects/rect.js`]
        //* Text
        if (addAllFiles || (writtenCode.includes('.text') || writtenCode.includes('.input') || writtenCode.includes('.message') || 
            writtenCode.includes('.login') || writtenCode.includes('.contact') || writtenCode.includes('.mediaPlayer') || 
            writtenCode.includes('.select') || writtenCode.includes('.navbar')|| writtenCode.includes('.tab') || 
            writtenCode.includes('.graph') || writtenCode.includes('cryptoAccept') || writtenCode.includes('moneroWallet') || 
            writtenCode.includes('moneroAccept'))) files.push(`src/objects/text.js`);
        //* Images
        if (addAllFiles || (writtenCode.includes('.image') || writtenCode.includes('.select') || 
            writtenCode.includes('.mediaPlayer') || writtenCode.includes('cryptoAccept') || writtenCode.includes('moneroAccept') || 
            writtenCode.includes('moneroWallet'))) files.push(`src/objects/image.js`);
        //* QR Codes
        if (addAllFiles || (writtenCode.includes('.qrcode') || writtenCode.includes('moneroWallet') || 
            writtenCode.includes('cryptoAccept') || writtenCode.includes('moneroAccept'))){
            files.push(`src/libraries/qrcode.min.js`);
            files.push(`src/objects/qrcode.js`);
        };            
        //* Polygon / Collision
        if (addAllFiles || (writtenCode.includes('.polygon') || writtenCode.includes('.watchCollisions') || 
            writtenCode.includes('.collision') || writtenCode.includes('.showHitBoxes') || writtenCode.includes('.setHitBoxes') || 
            writtenCode.includes('.boundaries') || writtenCode.includes('.updateHitBoxes'))) 
            files.push(`src/objects/polygon.js`);
        //* Buttons
        if (addAllFiles || (writtenCode.includes('.detect') || writtenCode.includes('.drag') || writtenCode.includes('.stick') || 
            writtenCode.includes('.submit') || writtenCode.includes('.select') || writtenCode.includes('.mediaPlayer') ||
            writtenCode.includes('.login') || writtenCode.includes('.contact') || writtenCode.includes('.navbar')|| 
            writtenCode.includes('.tab') || writtenCode.includes('moneroWallet') || writtenCode.includes('cryptoAccept') || 
            writtenCode.includes('moneroAccept'))) files.push(`src/objects/button.js`);
        //* Graphs
        if (addAllFiles || (writtenCode.includes('.graph'))) files.push(...[`src/libraries/percent-round.js`, `src/objects/graph.js`]);
        //* Inputs
        if (addAllFiles || (writtenCode.includes('.input') || writtenCode.includes('.message') || writtenCode.includes('.login') || 
            writtenCode.includes('.contact') || writtenCode.includes('moneroWallet'))) files.push(`src/objects/input.js`);
        //* Messages (Text Areas)
        if (addAllFiles || (writtenCode.includes('.message') || writtenCode.includes('.contact') || 
            writtenCode.includes('moneroWallet'))) files.push(`src/objects/message.js`);
        //* Range Sliders
        if (addAllFiles || (writtenCode.includes('.slider') || writtenCode.includes('.mediaPlayer') || 
            writtenCode.includes('moneroWallet'))) files.push(`src/objects/slider.js`);
        //* Selects
        if (addAllFiles || (writtenCode.includes('.select'))) files.push(`src/objects/select.js`);
        //* Checks
        if (addAllFiles || (writtenCode.includes('.check'))) files.push(`src/objects/check.js`);
        //* Radios
        if (addAllFiles || (writtenCode.includes('.radio'))) files.push(`src/objects/radio.js`);
        //* ColorPickers
        if (addAllFiles || (writtenCode.includes('.colorPicker'))) files.push(`src/objects/colorPicker.js`);
        //* Navbars
        if (addAllFiles || (writtenCode.includes('.navbar'))) files.push(`src/objects/navbar.js`);
        //* Tabs
        if (addAllFiles || (writtenCode.includes('.tab') || writtenCode.includes('moneroWallet'))) files.push(`src/objects/tab.js`);
        //* Logins
        if (addAllFiles || (writtenCode.includes('.login'))) files.push(`src/objects/login.js`);
        //* Contacts
        if (addAllFiles || (writtenCode.includes('.contact'))) files.push(`src/objects/contact.js`);
        //* Media Players
        if (addAllFiles || (writtenCode.includes('.mediaPlayer') || writtenCode.includes('.video') || writtenCode.includes('.audio'))) 
            files.push(`src/objects/mediaPlayer.js`);
        //* Server Tools
        let includeServer = false;
        if (addAllFiles || writtenCode.includes('server') || writtenCode.includes('cryptoAccept') || 
            writtenCode.includes('moneroAccept')) includeServer = true;
        //* Cryptography Tools
        let includeCrypto = false;
        if (addAllFiles || (writtenCode.includes('server.send') || writtenCode.includes('encrypt') || writtenCode.includes('decrypt') ||
            writtenCode.includes('sha256') || writtenCode.includes('sha512') || writtenCode.includes('.login') ||
            writtenCode.includes('pbkdf2') || writtenCode.includes('deriveSecureKey') || writtenCode.includes('uniqueHash') || 
            writtenCode.includes('checkHash') || writtenCode.includes('sha512') || writtenCode.includes('SybilShield') || 
            writtenCode.includes('injectWorker') || writtenCode.includes('hashpuzzle') || 
            writtenCode.includes('cryptoAccept') || writtenCode.includes('moneroAccept'))) includeCrypto = true;
        //* Cryptography/Server Config
        let workerFiles = [`src/SybilShielder/randomness.js`, `src/SybilShielder/CryptoJS v3.1.2/core.js`, `src/SybilShielder/CryptoJS v3.1.2/x64-core.js`, 
            `src/SybilShielder/CryptoJS v3.1.2/format-hex.js`, `src/SybilShielder/CryptoJS v3.1.2/enc-base64.js`, 
            `src/SybilShielder/CryptoJS v3.1.2/md5.js`, `src/SybilShielder/CryptoJS v3.1.2/evpkdf.js`, 
            `src/SybilShielder/CryptoJS v3.1.2/cipher-core.js`, `src/SybilShielder/CryptoJS v3.1.2/sha256.js`,
            `src/SybilShielder/CryptoJS v3.1.2/sha512.js`, `src/SybilShielder/CryptoJS v3.1.2/pbkdf2.js`, 
            `src/SybilShielder/CryptoJS v3.1.2/aes.js`, `src/SybilShielder/javascript-bcrypt-master/bCrypt.js`, 
            `src/SybilShielder/cryptography.js`, `src/SybilShielder/SybilShielder.js`];
        if (includeCrypto){
            files.push(`src/cryptoWorker.js`);
        };
        if (includeServer){
            files = files.concat([`src/libraries/socketio.min.js`,`src/serverlogic.js`]);
        } else {
            extraCode = "eval(`var server = () => {}; server.send = () => {};`);";
        };
        if (includeCrypto && includeServer){
            files.push(`src/servercrypto.js`);
        };
        //* Crypto Accept
        if (addAllFiles || writtenCode.includes('cryptoAccept') || writtenCode.includes('moneroAccept')) files.push(`src/objects/cryptoAccept.js`);
        //* Monero Wallet
        if (writtenCode.includes('moneroWallet') || writtenCode.includes('new Monero') || 
            writtenCode.includes('monerojs') || writtenCode.includes('moneroJS') || writtenCode.includes('MoneroJS')){
            //? Add Monero Files
            files.push(`src/objects/cryptoWallet.js`);
            files.push(`src/objects/moneroWallet.js`);
            let moneroDist = fs.readFileSync(`./public/monero.dist.js`);
            let moneroDistMap = fs.readFileSync(`./public/monero.dist.js.map`);
            let moneroFullWasm = fs.readFileSync(`./public/monero_wallet_full.wasm`);
            let moneroKeysWasm = fs.readFileSync(`./public/monero_wallet_keys.wasm`);
            let moneroWebWorker = fs.readFileSync(`./public/monero_web_worker.js`);
            let moneroWebWorkerMap = fs.readFileSync(`./public/monero_web_worker.js.map`);
            fs.writeFileSync(`./production/monero.dist.js`, moneroDist);
            fs.writeFileSync(`./production/monero.dist.js.map`, moneroDistMap);
            fs.writeFileSync(`./production/monero_wallet_full.wasm`, moneroFullWasm);
            fs.writeFileSync(`./production/monero_wallet_keys.wasm`, moneroKeysWasm);
            fs.writeFileSync(`./production/monero_web_worker.js`, moneroWebWorker);
            fs.writeFileSync(`./production/monero_web_worker.js.map`, moneroWebWorkerMap);
        } else {
            //? Remove Monero Files
            fs.writeFileSync(`./production/monero.dist.js`, '');           //fs.unlinkSync(`./production/monero.dist.js`);
            fs.writeFileSync(`./production/monero.dist.js.map`, '');       fs.unlinkSync(`./production/monero.dist.js.map`);
            fs.writeFileSync(`./production/monero_wallet_full.wasm`, '');  fs.unlinkSync(`./production/monero_wallet_full.wasm`);
            fs.writeFileSync(`./production/monero_wallet_keys.wasm`, '');  fs.unlinkSync(`./production/monero_wallet_keys.wasm`);
            fs.writeFileSync(`./production/monero_web_worker.js`, '');     fs.unlinkSync(`./production/monero_web_worker.js`);
            fs.writeFileSync(`./production/monero_web_worker.js.map`, ''); fs.unlinkSync(`./production/monero_web_worker.js.map`);
        };
        files = files.concat([
            `./main.js`, 
            `src/loop.js`
        ]);
        files = files.concat(components);
        if (logFiles) console.log(files);
        const combineFiles = () => {
            let allFileData = '';
            for (let i in files){
                allFileData+= fs.readFileSync(`./public/${files[i]}`).toString() + '; ';
            };
            return extraCode + allFileData;
        };
        if (includeCrypto){
            const minifyCrypto = () => {
                let workerFileData = '';
                for (let i in workerFiles){
                    workerFileData+= fs.readFileSync(`./public/${workerFiles[i]}`).toString() + '; ';
                };
                let result = UglifyJS.minify(workerFileData);
                fs.writeFileSync(`./production/src/workerscripts.js`, result.code);
            };
            minifyCrypto();
        } else {
            let code = fs.readFileSync(`./public/src/SybilShielder/randomness.js`).toString() + '; ';
            let result = UglifyJS.minify(code);
            fs.writeFileSync(`./production/src/workerscripts.js`, result.code);
        };
        let combinedFiles = combineFiles();
        if (logs) console.log("Building production files...")
        /*
        We will end up with three different javascript files, one for cryptography tools required by workers, 
        a worker file, and a file for all the core library and developer code.
        */
        const minifyFileData = () => {
            let fileData = combinedFiles;
            let result = UglifyJS.minify(fileData);
            if (result.error){
                if (logs) console.log(result.error);
                if (logs) console.log("Failed to produce production build.");
                if (logs) console.log("Try switching to serving the 'public' folder to troubleshoot.");
            } else {
                
                fs.writeFileSync(`./production/src/speedrun.js`, result.code);
                if (logs) console.log("Production build is complete.")
            };
        };
        if (minify){
            minifyFileData();
        } else {
            fs.writeFileSync(`./production/src/speedrun.js`, combinedFiles);
            if (logs) console.log("Unminified production test build is complete.")
        };
    };
};

module.exports = {production};