/*
? Welcome to SpeedrunJS's server instance. In here there is boilerplate code for an Express server with
    ? socket.io server-client communication, minification, compression, and multithreading.

? Note: If you want to test out database creation, the cryptography tools, or perform any intensive task,
    ? the code should be written in server-worker.js, not here. You don't want to block the main event loop.

? If you come across an error, first try updating Node.js: 
    ? https://askubuntu.com/questions/426750/how-can-i-update-my-nodejs-to-the-latest-version
*/

//? Minify files and create production application
    const Build_Production = require('./server-production.js').production;
//? param1: Build production (default: true) ----------- //? param4: Component files (default: [])
//? param2: Add all files (default: false) ------------- //? param5: Log success/failure (default: true)
//? param3: Enable minification (default: true) -------- //? param6: Log added files (default: false)
    Build_Production(true, false, true, [], true, false);
//? Change to "public" if you want to serve the development files, or "production" otherwise.
//* To troubleshoot compilation errors, try switching to public, then production + all files added, then previous + unminified.
    let public = 'production'; 

//? Server Setup
    const express = require("express");
    const app = express();
    const PORT = process.env.PORT || 3000;
    const server = app.listen(PORT, () => {
        console.log(`My socket server is running on port ${PORT}!`);
    });

//? Use Compression (saves an additional ~75%)
    const zlib = require('constants');
    const compression = require("compression");
    app.use(compression({
        //* These are the maximum possible compression values
        level: 9,
        threshold: 0,
        memLevel: 9,
        strategy: zlib.Z_RLE,
    }));

//?Serve files
    app.use(express.static(public));

//?Use multithreading
    const {Worker} = require("worker_threads");

//?Use socketio
    const socket = require("socket.io");
    const io = socket(server);
    io.sockets.on("connection", newConnection);

//? Optional Serverside Monero App (Must be run on main thread)
/*
const Database = require('./Easy-DB/easy-db');
let database = new Database(false, true, "subaddr_index");
let moneroIndex = database.queryAny("moneroIndex")[0]?.["moneroIndex"];
if (!moneroIndex){
    database.insert({"moneroIndex": 0})
    moneroIndex = 0;
};
let prevIndex = 0;
let updateMoneroIndex = () => {
    if (prevIndex !== moneroIndex){
        database.editAny("moneroIndex", moneroIndex);
        prevIndex = moneroIndex;
    };
};
setInterval(()=>{
    updateMoneroIndex()
}, 1000)
const Monero = require('./wallets/monero/monero.js');
const monero = new Monero(undefined, moneroIndex);
let seed = null;
let restoreHeight = null;
let seedOffset = null;
let moneroWallet;
let callback = (wallet) => {
moneroWallet = wallet;
const refresh = 5000;
console.log("Wallet is loading...")
monero.sync(wallet, 
    ((progress) => {}), 
    ((metadata) => {console.log(`\nWallet is ready and contains a balance of ${metadata.balance}.\n`);}),
    ((received) => {}), 
    refresh);
};
monero.openWallet(seed, restoreHeight, seedOffset, callback, ((privateData) => console.log(privateData)));
*/

//? What to execute each client connection, separately
function newConnection(socket) {

    //* Optional Action for Sending Monero Subaddress To Client (validated with hashpuzzles for spam-resistance)
    //! If you are not using Monero, you can either delete this code or leave it be, it won't execute.
    try {
        //! Add what you want here:
            //* Callback after funds are received
            let moneroCallback = (addr, bal) => {
                console.log("\nReceived new funds!", '\nsubaddr', addr, '\nsubaddrBal:', bal)
            };
            //* Amount they must pay
            let moneroRequirement = 0.00001;
            //* When invoice times out (30 minutes by default)
            let moneroTimeLimit = 1000 * 60 * 30;
            //* How often the server checks for payments (5 seconds by default)
            let moneroCheckBalance = 1000 * 3;
        if (monero && moneroWallet){
            socket.on("monero", (data)=>{
                const worker = new Worker("./server-worker.js", {workerData: {data: data}});
                worker.once("message", workerResponse => {
                    if (workerResponse){
                        monero.subaddress(moneroWallet, (addr)=>{
                            moneroIndex++;
                            let subaddrIndex = moneroIndex;
                            let subaddrBal;
                            //? Look for payments repeatedly
                            let tempInt = setInterval(()=>{
                                (async () => {
                                    try {
                                    let rawBal = (await moneroWallet.getBalance(0, subaddrIndex));
                                    subaddrBal = parseInt(rawBal)/monero.rate;
                                    } catch (error) {
                                    console.log(error);
                                    };
                                })();
                                if (subaddrBal >= moneroRequirement){
                                    if (moneroCallback) moneroCallback(addr, subaddrBal);
                                    clearInterval(tempInt);
                                };
                            }, moneroCheckBalance);
                            setTimeout(()=>{
                                try {
                                    if (subaddrBal < moneroRequirement) clearInterval(tempInt);
                                } catch (error) {};
                            }, moneroTimeLimit)
                            //? Send subaddress
                            socket.emit("monero", addr);
                        });                 
                    };                    
                });
            });
        };
    } catch (e) {};

    //? Send data to client.
    socket.emit("data", "Hello World!");

    io.to(socket.id).emit("data", {});

    console.log(`New connection: ${socket.id}`);
    socket.on("disconnect", function () {
        console.log(`The user ${socket.id} has been disconnected!`);
    });

    //? Receive Data from client.
    socket.on("data", respond);

    //?Respond to client
    function respond(data) {
        
        //? Create new worker thread, send it a message, and execute it's logic.
        const worker = new Worker("./server-worker.js", {workerData: {data: data}});//? The nested object is necessary.

        //? Receive Message from worker.
        worker.once("message", workerResponse => {
            console.log("Worker is finished: ", workerResponse);
        });

        //? Additional, optional messages
        worker.on("error", error => {
            console.log(error);
        });
        worker.on("exit", exitCode => {
            console.log(exitCode);
        });
        
    };
};

