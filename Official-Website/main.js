/*This is speedrun.js. 
Build your website fast!*/

//? Background
box()
    .color(0,125,255,50)
    .gradient(0,0,255,30,false,180)
    .size(windowWidth, windowHeight)

//? Front Page (Home)
let page1;
{
    page1 = box()
    .div()
    .position(0,0)
    .size(windowWidth, windowHeight)
    .overflow('hidden', 'scroll')

let facilitator = box()
    .overflow('visible')
    .size(300,100)
    .containerPosition(mid, 200)
    .within(page1, false)

if (isMobile) facilitator.containerPosition(mid, 100)

let lightningBolt = box()
    .size(100)
    .position(150,0,true)
    .opacity(0.25)
    .image('favicon.ico')
    .within(facilitator, false)

//? Solar System
let planetoids = [];
let parentoids = [];
let numP = 64;
for (let i = 0; i < numP; i++){
    let acc = random(0.01,0.5)/2;
    parentoids[i] = box()
        .size(20)
        .containerPosition(150,50,true,true)
        .gradient(255,0,0,20,true)
        .spin(random(1,7)/i*10, 0.001)
        .within(facilitator, false)
    planetoids[i] = box()
        .size(6)
        .position(50 + i*random(5,10),0,true,true)
        .move(-90, acc)
        .accelerateSpeed(0.001)
        .run((a)=>{
            if (a.x < 25 - 10){
                planetoids[i].remove()
                parentoids[i].remove()
            };
        })
        .curves(100)
        .color(0,random(100,255),0,100)
        .within(parentoids[i], false)
};
let speedrunJS = box()
    .size(300, 100)
    .text("SpeedrunJS", 50)
    .nGradient([[0,0,0,255],[0,175,0,255],[0,0,0,255]],false,0,true)
    .within(facilitator, false)

let paragraphWidth = windowWidth*0.6;
let paragraphTextSize = 18;
let paragraphYPos = 300;
let paragraphHeight = 500;
if (isMobile){
    paragraphWidth = windowWidth*0.9;
    paragraphTextSize = 15;
    paragraphYPos = 250;
    paragraphHeight = 700;
};
let paragraph = box()
    .size(paragraphWidth, paragraphHeight)
    .position(mid, paragraphYPos)
    .text(`
SpeedrunJS is a quick-to-use and easy-to-learn Javascript library for designing websites, user-interfaces, animations, web games, and more.

This library is oriented towards beginners to Javascript and programming in general, but offers plenty of well-documented and advanced functionality for more experienced developers.

Gone are the days of needing complicated frameworks bloated with heavy-duty libraries and memorizing dozens of different stacks for simple tasks. SpeedrunJS is feature-rich like beginner-friendly frontend libraries that came before it (such as P5.js), but is professionalized, object-oriented, efficient in both size and execution speed, and not only promotes creative coding but also general web development.

SpeedrunJS aims to render frameworks redundant by reintroducing high-level object-oriented organization into applications, and making it easier for everyone to get into web development, leveling the playing field. No complicated tutorials or schooling is needed to "learn" SpeedrunJS, it has consistent syntax and structure interchangeable with Javascript itself, and a suite of documentation covering every facet of the library.

Virtually all UI logic has been abstracted away into a single, central class: A box. Whether you want a shape, an image, text, an input, or anything else, they are all "boxes", and to give them the attributes you want, you simply attach those methods to them.

Everything you need to begin your journey of web development is right here, so why not dive in and see what SpeedrunJS can do for you?

    `, paragraphTextSize)
    .textAlign('center', 0, 'top', 0)
    .textInjectCSS('text-align', 'center')
    .textStyle(false, true)
    .textColor(0,60,0)
    .textGradient(0,0,0,255,90)
    .within(page1, false)


let watchDemo = box()
    .size(400, 75)
    .position(mid)
    .under(paragraph)
    .text("Watch Demos:", 25)
    
    .nGradient([[0,0,0,255],[0,175,0,255],[0,0,0,255]],false,0,true)
    .textStyle(true, true)
    .within(page1)

let demo1 = box()
    .size(1200, 40)
    .position(mid)
    .under(watchDemo, 20)
    .text("Demo #1: Creating A Website", 17, 'https://www.bitchute.com/video/SCYF5WyXM1yc/')
    .textStyle(true, true, true)
    .within(page1)
let demo2 = box()
    .size(1200, 40)
    .position(mid)
    .under(demo1)
    .text("Demo #2: Creating Interactive Objects", 17, 'https://www.bitchute.com/video/ggvfxlqwUCgK/')
    .textStyle(true, true, true)
    .within(page1)
let demo3 = box()
    .size(1200, 40)
    .position(mid)
    .under(demo2)
    .text("Demo #3: Accepting Monero", 17, 'https://www.bitchute.com/video/fW5oWpo3HLhl/')
    .textStyle(true, true, true)
    .within(page1)
let demo4 = box()
    .size(1200, 40)
    .position(mid)
    .under(demo3)
    .text("Demo #4: Creating A Monero Web Wallet", 17, 'https://www.bitchute.com/video/UsEXatxW3fbE/')
    .textStyle(true, true, true)
    .within(page1)

let spacer = box()
    .position(200, 1050)
    .within(page1, false)
if (isMobile) spacer.position(200,1500);

};


    

let navLength = 100;

//? Page 2 (Editor)
let page2;
{
    let editorWidth = 615;
    let editorHeight = 315;
    let scaleDown = 1;
    let switchTextOffset = 1;
    if (isMobile){
        editorWidth = windowWidth;
        editorHeight = 200;
        scaleDown = 0.8;
        switchTextOffset = 0.8;
    };
    let code = "";
    let fontSize = 18;
    page2 = box()
        .div()
        .hide()
        .position(0,0)
        .size(windowWidth, windowHeight)
        .overflow('hidden', 'scroll')

        //? EditorBuilder
        let editorBuilder = box()
            .color(0,0,0,0)
            .size(editorWidth, editorHeight)
            .containerPosition(mid, mid)
            .within(page2)

        if (isMobile) editorBuilder.containerPosition(mid, windowHeight-editorHeight-52)
    
        //? Top of editor/builder
        let editorTitle = box()
            .color(0)
            .outline(0)
            .size(editorBuilder.w+1, 20)
            .text("Builder", 15)
            .textAlign('left', 7)
            .textColor(0,125,255)
            .containerPosition(0, -20)
            .within(editorBuilder)

        //? Editor
        let editorPane = box()
            .size(editorWidth, editorHeight-60)
            .containerPosition(0, 2)
            .color(255)
            .message((val)=>{
                if (val) code = val;
            })
            .inputFont(fontSize)
            .inputFillColor(50)
            .inputTextColor(0,255,125)
            .hide()
            .within(editorBuilder)

        //? Builder
        let builderPane = box()
            .size(editorWidth, editorHeight)
            .containerPosition(0, 2)
            .color(150)
            .outline(0)
            .within(editorBuilder)

        //? Switch To Builder/Editor
        let editorState = false;
        let switchToBuilder = box()
            .submit(()=>{
                if (editorState){
                    editorPane.hide();
                    builderPane.show();
                    editorTitle
                        .text("Builder", 15)
                        .textColor(0,125,255)
                    switchToBuilder
                        .text("Switch To Editor", 15)
                        .textColor(0,255,125)
                } else {
                    editorPane.show();
                    builderPane.hide();
                    editorTitle
                        .text("Editor", 15)
                        .textColor(0,255,125)
                    switchToBuilder
                        .text("Switch To Builder", 15)
                        .textColor(0,50,255)
                };
                editorState = !editorState;
            }, 50)
            .color(0,0,0,0)
            .outline(0,0,0,0)
            .size(200,20)
            .position(editorPane.w - 150*switchTextOffset, -1)
            .text("Switch To Editor", 15)
            .textColor(0,255,125)
            .textAlign('left', 0, 'top')
            .within(editorTitle)
    
        if (!isMobile){
            editorBuilder.draggableIcon((a)=>{
                a
                    .color(50)
                    .outline(0,255,125,175)
                    .imageColor(75, 100)
                    .within(editorTitle)
                setTimeout(() => {
                    a.position(editorBuilder.w - 20, -1)
                }, 1);
                
            }, 20);
        };
            
        
        //? Editor Console
        let errorMessage = '';
        let editorConsole = box()
            .size(editorPane.w-1, 60)
            .run((a)=>{
                a
                    .text(errorMessage, 12)
                    .textColor(255,0,0)
                    .textAlign('left', 5, 'top', 5)
    
            })
            .position(1, editorPane.h)
            .color(0)
            .outline(0,255,125,175)
            .within(editorPane)
    
        //? Mirroring console
        console.stdlog = console.log.bind(console);
        console.logs = [];
        console.log = function(){
            console.logs.push(Array.from(arguments));
            console.stdlog.apply(console, arguments);
        }
        setInterval(()=>{
            errorMessage = console.logs[console.logs.length-1];
        }, 100)
    
        //? Clearing virtual console
        const clearConsole = () => {
            errorMessage = "";
            console.logs.length = 0;
        };

        const stopCode = () => {
            clearConsole();
            editorPane.listNew((objs)=>{
                for (let i in objs){
                    objs[i].remove();
                };                    
            });
        };
        const runCode = () => {
            try {
                clearConsole();
                eval(code);
            } catch (error) {
                errorMessage = "Code was not successfully executed. One or more problems found: " + error;
                console.log(errorMessage)
            };
        };

        //? Builder Functions:
        
        let boxSelector = 0;
        let boxLatest = 0;
        let boxSelection = []; 
        let selection;
        const defineBoxSelection = (i) => {
            boxSelection[i] = {
                x: 0,
                y: 0,
                w: 100,
                h: 100,
                rotation: 0,
                r: 255,
                g: 255,
                b: 255,
                a: 255,
                gStyle: 0,
                gDir: 0,
                gr: 0,
                gg: 0,
                gb: 0,
                ga: 255,
                or: 0,
                og: 0,
                ob: 0,
                oa: 0,
                ow: 1,
                text: '',
                textSize: 12,
                tr: 0,
                tg: 0,
                tb: 0,
                ta: 255,
                curves: 0,
                polygon: ['rectangle', 'triangle', 'trapezoid', 'parallelogram','pentagon','hexagon','heptagon',
                    'octogon','nonagon','decagon','bevel','rabbet','arrow','pointer','chevron','star','cross',
                    'message', 'close', 'frame'],
                polygonIndex: 0,
                textFont: ['serif', 'sans-serif', 'monospace'],
                textFontIndex: 0,
                textStyle: ['none','italic','bold','italicbold','underline','underlineitalic','underlinebold','underlineitalicbold',
                    'overline','overlineitalic','overlinebold','overlineitalicbold','throughline','throughlineitalic','throughlinebold','throughlineitalicbold'],
                textStyleIndex: 0,
                velocity: 0,
                speed: 0,
                spin: 0,
                boundary: ['none', 'bounce', 'wrap', 'stop', 'delete'],
                boundaryIndex: 0,
                accSpeed: 0,
                accVelocity: 0,
            };
        };
        defineBoxSelection(0);
        selection = boxSelection[0];
        const virtualize = (c) => {
            editorPane.clearInput(c);
            editorPane.listNew((objs)=>{
                for (let i in objs){
                    if (i < objs.length) objs[i].remove();
                };                    
            });
            runCode();
        };
        let builderPaneUpperDiv = box()
            .div()
            .size(editorWidth, 50)
            .overflow('scroll')
            .within(builderPane)
        if (!isMobile) builderPaneUpperDiv.overflow('visible');


        let createNewBox = box()
            .submit(()=>{
                boxSelector = boxLatest + 1;
                boxLatest = boxSelector;
                defineBoxSelection(boxSelector);
                selection = boxSelection[boxSelector];
                code+= 
`
let box_${boxSelector} = box()
    /*${boxSelector}*/.color(255)
    /*${boxSelector}*/.size(100)
    /*${boxSelector}*/.position(0, 0)
`
            virtualize(code);
            })
            .size(130, 25)
            .curves(15,15,15,15,true)
            .position(25, 15)
            .text("Create New Box", 15)
            .textStyle(true, true)
            .textColor(0,75,125)
            .color(0,255,125,175)
            .within(builderPaneUpperDiv)

        let selectPrev = box()
            .submit(()=>{
                if (boxSelector > 1){
                    boxSelector--;
                    selection = boxSelection[boxSelector];
                };
            })
            .run((a)=>{
                if (boxSelector > 1){
                    a.enable();
                } else {
                    a.disable();
                };
            }, 10)
            .size(80, 25)
            .curves(15,15,15,15,true)
            .rightOf(createNewBox, 10)
            .text("Select Prev", 12)
            .textStyle(true)
            .textColor(0,75,125)
            .color(0,255,125,175)
            .within(builderPaneUpperDiv)

        let selectNext = box()
            .submit(()=>{
                if (boxSelector < boxLatest){
                    boxSelector++;
                    selection = boxSelection[boxSelector];
                };
            })
            .run((a)=>{
                if (boxSelector < boxLatest){
                    a.enable();
                } else {
                    a.disable();
                };
            }, 10)
            .size(80, 25)
            .disable()
            .curves(15,15,15,15,true)
            .rightOf(selectPrev, 10)
            .text("Select Next", 12)
            .textStyle(true)
            .textColor(0,75,125)
            .color(0,255,125,175)
            .within(builderPaneUpperDiv)

        let saveBuilder = box()
            .submit(()=>{
                setStorage("codeBuilderSavedContents", [code, boxSelection]);
            })
            .run(()=>{
                if (code && boxSelection) setStorage("codeBuilderAutosavedContents", [code, boxSelection]);
            }, 1000)
            .size(40, 25)
            .curves(15,15,15,15,true)
            .rightOf(selectNext, 60)
            .text("Save", 10)
            .textStyle(true)
            .textColor(0,75,125)
            .color(0,255,125,175)
            .within(builderPaneUpperDiv)

        let restoreBuilder = box()
            .submit(()=>{
                [code, boxSelection] = getStorage("codeBuilderSavedContents");
                editorPane.clearInput(code);
                stopCode();
                runCode();
                boxSelector = boxSelection.length-1;
                boxLatest = boxSelector;
                selection = boxSelection[boxSelector];
            })
            .size(80, 25)
            .curves(15,15,15,15,true)
            .rightOf(saveBuilder, 5)
            .text("Restore Save", 10)
            .textStyle(true)
            .textColor(0,75,125)
            .color(0,255,125,175)
            .within(builderPaneUpperDiv)

        let restoreBuilderFromAutosave = box()
            .submit(()=>{
                [code, boxSelection] = getStorage("codeBuilderAutosavedContents");
                editorPane.clearInput(code);
                stopCode();
                runCode();
                boxSelector = boxSelection.length-1;
                boxLatest = boxSelector;
                selection = boxSelection[boxSelector];
            })
            .size(80, 25)
            .curves(15,15,15,15,true)
            .rightOf(restoreBuilder, 5)
            .text("Restore Autosave", 10)
            .textStyle(true)
            .textColor(0,75,125)
            .color(0,255,125,175)
            .within(builderPaneUpperDiv)

        let attributesMenuBackdrop = box()
            .size(editorWidth-2, editorHeight-54)
            .containerPosition(-1, 52)
            .color(180)
            .outline(0)
            .within(builderPane)

        let attributesMenu = box()
            .div()
            .overflow('hidden', 'scroll')
            .size(editorWidth-2, editorHeight-54)
            .containerPosition(-1, 52)
            .within(builderPane)

        const injectEditor = (identifier, params) => {
            code = deleteLine(code, `/*${boxSelector}*/.${identifier}`, 1, true);
            if (params){
                code = deleteLine(code, `let box_${boxSelector} = box()`, 1, false, 
                `let box_${boxSelector} = box()\n    /*${boxSelector}*/.${identifier}(${params})`);
            };
            virtualize(code);
        };
        let initialOffset = 2100;
        let positionX = box()
            .prototype('builderTools', (a, b)=>{
                a.size(1)
                a.bottomOf(b.prev,0,0.25)
                if (!b.titleWidth) b.titleWidth = 35;
                if (b.initialValue == null) b.initialValue = 0;
                if (b.enter == null) b.enter = true;
                if (b.sliderStart == null) b.sliderStart = 0;
                let prevSelection = selection;
                let header,value,slider,plus,minus;
                header = box()
                    .size(80*scaleDown,20)
                    .position(a.x, a.y*100 - initialOffset)
                    .text(b.title, 15*scaleDown)
                    .textStyle(true)
                    .textAlign('left', 0, 'top')
                    .run(()=>{
                        if (selection !== prevSelection){
                            value.clearInput(selection[b.variable]);
                            slider.sliderValue(selection[b.variable]);
                            prevSelection = selection;
                        };
                    })
                    .within(attributesMenu)
                value = box()
                    .size(b.titleWidth*scaleDown)
                    .rightOf(header,0)
                    .input((val)=>{
                        if (typeof val === "number"){
                            selection[b.variable] = int(val) 
                        } else {
                            selection[b.variable] = val;
                        };
                        if (!b.sliderMult) slider.sliderValue(selection[b.variable]);
                        if (b.callback) b.callback();
                    }, b.initialValue, "text", b.enter)
                    .within(attributesMenu)
                minus = box()
                    .submit(()=>{
                        selection[b.variable]--;
                        slider.sliderValue(selection[b.variable]);
                        value.clearInput(selection[b.variable]);
                        if (b.callback) b.callback();
                    }, 25)
                    .color(0,255,125,175)
                    .size(18*scaleDown)
                    .text("", 18)
                    .image("src/images/minus.png")
                    .rightOf(value, 15*scaleDown)
                    .within(attributesMenu)
                plus = box()
                    .submit(()=>{
                        selection[b.variable]++;
                        slider.sliderValue(selection[b.variable]);
                        value.clearInput(selection[b.variable]);
                        if (b.callback) b.callback();
                    }, 25)
                    .color(0,255,125,175)
                    .size(18*scaleDown)
                    .text("", 18)
                    .image("src/images/plus.png")
                    .rightOf(minus, 5*scaleDown)
                    .within(attributesMenu)
                slider = box()
                    .size(builderPane.w/2*scaleDown)
                    .rightOf(plus, 15*scaleDown)
                    .slider((val)=>{
                        selection[b.variable] = int(val);
                        value.clearInput(selection[b.variable]);
                        if (b.callback) b.callback();
                    }, b.sliderStart,b.sliderMult)
                    .sliderColor(0,255,125,175)
                    .within(attributesMenu)
                if (!b.sliderMult){
                    slider.hide();
                    plus.hide();
                    minus.hide();
                };
            })
            .builderTools({prev: createNewBox, title: "X Position", sliderMult: windowWidth, variable: 'x',
            callback: ()=>{
                injectEditor("position", `${selection.x}, ${selection.y}`);
            }})
            let positionY = box()
                .builderTools({prev: positionX, title: "Y Position", sliderMult: windowHeight, variable: 'y',
                callback: ()=>{
                    injectEditor("position", `${selection.x}, ${selection.y}`);
                }})
            let sizeW = box()
                .builderTools({prev: positionY, title: "Width", sliderMult: windowWidth, variable: 'w',
                callback: ()=>{
                    injectEditor("size", `${selection.w}, ${selection.h}`);
                }})
            let sizeH = box()
                .builderTools({prev: sizeW, title: "Height", sliderMult: windowHeight, variable: 'h',
                callback: ()=>{
                    injectEditor("size", `${selection.w}, ${selection.h}`);
                }})
            let curveShape = box()
                .builderTools({prev: sizeH, title: "Curves", sliderMult: 100, variable: 'curves',
                callback: ()=>{
                    selection.curves ? injectEditor("curves", `${selection.curves}, ${selection.curves}, ${selection.curves}, ${selection.curves}, true`) : injectEditor("curves");
                }})
            let polygonShape = box()
                .builderTools({prev: curveShape, title: "Polygon", sliderMult: selection.polygon.length-1, variable: 'polygonIndex',
                callback: ()=>{
                    selection.polygonIndex ? injectEditor("polygon", `"${selection.polygon[selection.polygonIndex]}"`) : injectEditor("polygon");
                }})
            let boxRotation = box()
                .builderTools({prev: polygonShape, title: "Rotation", sliderMult: 360, variable: 'rotation',
                callback: ()=>{
                    selection.rotation ? injectEditor("rotate", `${selection.rotation}`) : injectEditor("rotate");
                }})
            let colorR = box()
                .builderTools({prev: boxRotation, title: "Red", sliderMult: 256, variable: 'r',
                callback: ()=>{
                    selection.a ? injectEditor("color", `${selection.r}, ${selection.g}, ${selection.b}, ${selection.a}`) : injectEditor("color");
                }})
            let colorG = box()
                .builderTools({prev: colorR, title: "Green", sliderMult: 256, variable: 'g',
                callback: ()=>{
                    selection.a ? injectEditor("color", `${selection.r}, ${selection.g}, ${selection.b}, ${selection.a}`) : injectEditor("color");
                }})
            let colorB = box()
                .builderTools({prev: colorG, title: "Blue", sliderMult: 256, variable: 'b',
                callback: ()=>{
                    selection.a ? injectEditor("color", `${selection.r}, ${selection.g}, ${selection.b}, ${selection.a}`) : injectEditor("color");
                }})
            let colorA = box()
                .builderTools({prev: colorB, title: "Alpha", sliderMult: 256, variable: 'a',
                callback: ()=>{
                    selection.a ? injectEditor("color", `${selection.r}, ${selection.g}, ${selection.b}, ${selection.a}`) : injectEditor("color");
                }})
            const gradientCallback = () => {
                let line = '';
                switch(selection.gStyle){
                    case 0: injectEditor("gradient", false); return;
                    case 1: line = `false, ${selection.gDir}`; break;
                    case 2: line = `true, ${selection.gDir}`; break;
                };
                injectEditor("gradient", `${selection.gr}, ${selection.gg}, ${selection.gb}, ${selection.ga}, ${line}`);
            };
            let colorGradient = box()
                .builderTools({prev: colorA, title: "Gradient", sliderMult: 2, variable: 'gStyle', callback: gradientCallback})
            let gradDir = box()
                .builderTools({prev: colorGradient, title: "Grad-Dir", sliderMult: 360, variable: 'gDir', callback: gradientCallback})
            let colorGR = box()
                .builderTools({prev: gradDir, title: "Grad-R", sliderMult: 256, variable: 'gr', callback: gradientCallback})
            let colorGG = box()
                .builderTools({prev: colorGR, title: "Grad-G", sliderMult: 256, variable: 'gg', callback: gradientCallback})
            let colorGB = box()
                .builderTools({prev: colorGG, title: "Grad-B", sliderMult: 256, variable: 'gb', callback: gradientCallback})
            let colorGA = box()
                .builderTools({prev: colorGB, title: "Grad-A", sliderMult: 256, variable: 'ga', callback: gradientCallback})
            let outlineCallback = () => {
                selection.oa && selection.ow ? injectEditor("outline", `${selection.or}, ${selection.og}, ${selection.ob}, ${selection.oa}, ${selection.ow}`) : injectEditor("outline");
            };
            let colorOR = box()
                .builderTools({prev: colorGA, title: "Outline-R", sliderMult: 256, variable: 'or', callback: outlineCallback})
            let colorOG = box()
                .builderTools({prev: colorOR, title: "Outline-G", sliderMult: 256, variable: 'og', callback: outlineCallback})
            let colorOB = box()
                .builderTools({prev: colorOG, title: "Outline-B", sliderMult: 256, variable: 'ob', callback: outlineCallback})
            let colorOA = box()
                .builderTools({prev: colorOB, title: "Outline-A", sliderMult: 256, variable: 'oa', callback: outlineCallback})
            let colorOW = box()
                .builderTools({prev: colorOA, title: "Outline-W", sliderMult: 10, variable: 'ow', callback: outlineCallback})
            let textField = box()
                .builderTools({prev: colorOW, title: "Text", titleWidth: 412*scaleDown, sliderMult: 0, variable: 'text', initialValue: "Tip: \\n = enter", enter: false,
                callback: ()=>{
                    selection.text ? injectEditor("text", `"${selection.text}", ${selection.textSize}`) : injectEditor("text");
                }})
            let textSize = box()
                .builderTools({prev: textField, title: "Text Size", sliderMult: 100, variable: 'textSize',
                callback: ()=>{
                    selection.text ? injectEditor("text", `"${selection.text}", ${selection.textSize}`) : injectEditor("text");
                }})
            let textFont = box()
                .builderTools({prev: textSize, title: "Text Font", sliderMult: selection.textFont.length-1, variable: 'textFontIndex',
                callback: ()=>{
                    selection.textFontIndex ? injectEditor("textFont", `"${selection.textFont[selection.textFontIndex]}"`) : injectEditor("textFont");
                }})
            let textStyle = box()
                .builderTools({prev: textFont, title: "Text Style", sliderMult: selection.textStyle.length-1, variable: 'textStyleIndex',
                callback: ()=>{
                    let line =`false`;
                    switch(selection.textStyle[selection.textStyleIndex]){
                        default: injectEditor("textStyle"); return;
                        case "italic":
                            line = `false, true`;
                        break;
                        case "bold":
                            line = `true, false`;
                        break;
                        case "italicbold":
                            line = `true, true`;
                        break;
                        case "underline":
                            line = `false, false, 'underline'`;
                        break;
                        case "underlineitalic":
                            line = `false, true, 'underline'`;
                        break;
                        case "underlinebold":
                            line = `true, false, 'underline'`;
                        break;
                        case "underlineitalicbold":
                            line = `true, true, 'underline'`;
                        break;
                        case "overline":
                            line = `false, false, 'overline'`;
                        break;
                        case "overlineitalic":
                            line = `false, true, 'overline'`;
                        break;
                        case "overlinebold":
                            line = `true, false, 'overline'`;
                        break;
                        case "overlineitalicbold":
                            line = `true, true, 'overline'`;
                        break;
                        case "throughline":
                            line = `false, false, 'line-through'`;
                        break;
                        case "throughlineitalic":
                            line = `false, true, 'line-through'`;
                        break;
                        case "throughlinebold":
                            line = `true, false, 'line-through'`;
                        break;
                        case "throughlineitalicbold":
                            line = `true, true, 'line-through'`;
                        break;
                    };
                    injectEditor("textStyle", line);
                }})
            const textColorCallback = () => {
                selection.text ? injectEditor("textColor", `${selection.tr}, ${selection.tg}, ${selection.tb}, ${selection.ta}`) : injectEditor("textColor");
            };
            let colorTR = box()
                .builderTools({prev: textStyle, title: "Text-R", sliderMult: 256, variable: 'tr', callback: textColorCallback})
            let colorTG = box()
                .builderTools({prev: colorTR, title: "Text-G", sliderMult: 256, variable: 'tg', callback: textColorCallback})
            let colorTB = box()
                .builderTools({prev: colorTG, title: "Text-B", sliderMult: 256, variable: 'tb', callback: textColorCallback})
            let colorTA = box()
                .builderTools({prev: colorTB, title: "Text-A", sliderMult: 256, variable: 'ta', callback: textColorCallback})
            let defineMotionCallback = () => {
                selection.speed || selection.velocity ? injectEditor("move", `${selection.velocity}, ${selection.speed}`) : injectEditor("move");
            };
            let velocity = box()
                .builderTools({prev: colorTA, title: "Velocity", sliderMult: 360, variable: 'velocity',
                callback: defineMotionCallback})
            let speed = box()
                .builderTools({prev: velocity, title: "Speed", sliderMult: 100, variable: 'speed',
                callback: defineMotionCallback})
            let accVelocity = box()
                .builderTools({prev: speed, title: "Acc-V", sliderMult: 360, variable: 'accVelocity',
                callback: ()=>{
                    selection.accVelocity ? injectEditor("accelerateVelocity", `${selection.accVelocity}`) : injectEditor("accelerateVelocity");
                }})
            let boundaries = box()
                .builderTools({prev: accVelocity, title: "Boundaries", sliderMult: 4, variable: 'boundaryIndex',
                callback: ()=>{
                    selection.boundaryIndex ? injectEditor("boundaries", `"${selection.boundary[selection.boundaryIndex]}"`) : injectEditor("boundaries");
                }})
            let spin = box()
                .builderTools({prev: boundaries, title: "Spin", sliderStart: -100, sliderMult: 100, variable: 'spin',
                callback: ()=>{
                    selection.spin ? injectEditor("spin", `${selection.spin}`) : injectEditor("spin");
                }})
            let conclusion = box()
                .size(attributesMenu.w-40, attributesMenu.h*0.9)
                .position(20)
                .under(spin, -150+initialOffset/2)
                .text(
`If you would like to be able to manipulate 
images, video, audio, inputs, user-interaction,
containerization, scripting, and more, then you
should download the library and begin programming
in SpeedrunJS. There is only so much you can do
from a website builder GUI. You can also return
to the editor screen, because it will show you the
code you generated.

`, 14*scaleDown)
                .textStyle(false, true)
                .textAlign('center', 0, 'top', 0)
                .textInjectCSS('text-align', 'center')
                .within(attributesMenu)


        //? Editor Functions:
        let buttonsDiv = box()
            .div()
            .position(0, editorBuilder.h-50)
            .size(editorBuilder.w, 50)
            .overflow('scroll')
            .within(editorPane)
        if (!isMobile) buttonsDiv.overflow('visible');
        let runEditor = box()
            .prototype('editorButton', 
                (a, params)=>{
                    if (!params.width) params.width = 50;
                    if (!params.offset) params.offset = -1;
                    if (!params.dir) params.dir = "right";
                    a
                        .submit(params.callback)
                        .size(params.width,25)
                        .text(params.text,14)
                        .textStyle(true)
                        .textColor(0,75,125)
                        .color(0,255,125,175)
                        [params.dir](params.sister, params.offset)
                        .under(editorPane, -23+60)
                        .within(buttonsDiv)
                })
            .editorButton({text: "run", sister: editorPane, offset: -50, dir: 'left', callback: () => {
                stopCode();
                runCode();
            }})
        let stopEditor = box()
            .editorButton({text: "stop", sister: runEditor, callback: () => {
                stopCode();
        }})
        let saveEditor = box()
            .editorButton({text: "save", sister: stopEditor, callback: () => {
                setStorage("codeEditorContents", code);
        }})
        setInterval(() => {
            if (code) setStorage("codeEditorAutosavedContents", code);
        }, 500);      
        let moreOptions = box()
            .editorButton({text: "more options", sister: saveEditor, width: 116, callback: () => {
                injectCode.show();
                zoomIn.show();
                zoomOut.show();
                exitOptions.show();
            }})
        let hideNav = box()
        .editorButton({text: "hide nav", sister: moreOptions, width: 80, callback: () => {
            navigationBar.hide()
            hideNav.hide()
            showNav.show()
        }})
        let showNav = box()
            .editorButton({text: "show nav", sister: moreOptions, width: 80, callback: () => {
                navigationBar.show()
                hideNav.show()
                showNav.hide()
            }})
            .hide()
        let restoreAutosaveEditor = box()
        .editorButton({text: "restore autosave", sister: hideNav, width: 130, callback: () => {
            code = getStorage("codeEditorAutosavedContents");
            editorPane.clearInput(code);
            }})
        let restoreSaveEditor = box()
            .editorButton({text: "restore save", sister: restoreAutosaveEditor, width: 100, callback: () => {
                code = getStorage("codeEditorContents");
                editorPane.clearInput(code);
            }})
        let clearEditor = box()
            .editorButton({text: "clear", sister: restoreSaveEditor, callback: () => {
                editorPane.clearInput();
                clearConsole();
            }});
        let injectCode = box()
            .editorButton({text: "inject code", sister: showNav, width: 116, dir: "left", callback: () => {
                runCode();
            }})
            .under(moreOptions, -1)
            .hide()
        let zoomIn = box()
            .editorButton({text: "zoom in", sister: showNav, width: 116, dir: "left", callback: () => {
                editorPane.inputFont(++fontSize);
            }})
            .under(injectCode, -1)
            .hide()
        let zoomOut = box()
            .editorButton({text: "zoom out", sister: showNav, width: 116, dir: "left", callback: () => {
                editorPane.inputFont(--fontSize);
            }})
            .under(zoomIn, -1)
            .hide()
        let exitOptions = box()
            .editorButton({text: "exit", sister: showNav, width: 116, dir: "left", callback: () => {
                injectCode.hide();
                zoomIn.hide();
                zoomOut.hide();
                exitOptions.hide();
            }})
            .under(zoomOut, -1)
            .hide()
    
        setTimeout(()=>{
            editorPane.markAsNew();
        }, 500)
}

//? Page Last: Donate
let pageL;
{
    pageL = box()
        .div()
        .position(0,0)
        .size(windowWidth, windowHeight)
        .overflow('hidden', 'scroll')
        .hide()

    let scaleDown = 1;
    let scaleDownButton = 1;
    let buttonSpacing = 1;
    if (isMobile){
        scaleDown = 0.75;
        scaleDownButton = 0.75;
        buttonSpacing = 0;
    };
    //? Help Support SpeedrunJS
    let helpSupport = box()
        .size(windowWidth, 100)
        .position(mid, windowHeight/2-350)
        .text("Help Support SpeedrunJS", 39*scaleDown)
        .textColor(0,0,100)
        .within(pageL)

    let reasoningWidth = 0.35;
    if (isMobile){
        reasoningWidth = 0.8;
    };
    let reasoning = box()
        .size(windowWidth*reasoningWidth, 400)
        .position(mid)
        .under(helpSupport)
        .text(`
SpeedrunJS was developed with advancing the Free-and-Open-Source ecosystem in mind and without any strings attached. I've logged thousands of hours developing this library to help others without profit motive. However I cannot do this kind of thing forever, so if you'd like to see me expand this library and build other cool open source tools, I will need a good reason to do so.

Also for those new to this kind of thing, hosting websites isn't free, can be hundreds of dollars every year just to keep the site running. It is my intention to provide value to others, and if so things will stay up and running for many years to come.

Using SpeedrunJS will never cost you a dime, and I will do everything in my power not to cost you your time either. Which is why this library is oriented towards absolute beginners first but has robust tooling every developer can benefit from.

Thanks for visiting SpeedrunJS, and I hope you find value in what I am trying to create here.
        `, 18*scaleDown)
        .textAlign('center', 0, 'top', 0)
        .textInjectCSS('text-align', 'center')
        .textStyle(false, true)
        .within(pageL)

    let donateBTC = box()
        .size(300*scaleDownButton)
        .position(mid, windowHeight-250*scaleDownButton)
        .under(reasoning, 75*scaleDownButton)
        .bitcoinAccept('bc1qrfxcav7sq2r925k4ewj4xnrfe022epcg9cmhpk')
        .within(pageL)

    let donateXMR = box()
        .size(300*scaleDownButton)
        .position(mid, windowHeight-250*scaleDownButton)
        .leftOf(donateBTC, 150*buttonSpacing)
        .moneroAccept('424XKddxR4vRrR9c21ejjiAGkxivSnWpvbuPtv4NnEC63MAC1Q22258ArARoxHeLQEAanX8nDoVH1UUMxy9XZdmAACd7KDr')
        .within(pageL)

    let donateBCH = box()
        .size(300*scaleDownButton)
        .position(mid, windowHeight-250*scaleDownButton)
        .rightOf(donateBTC, 150*buttonSpacing)
        .bitcoinCashAccept('bitcoincash:qr87fvhly6arvzd8w6z9de2n3uq8j9f06q8d6kkqgs')
        .within(pageL)

    if (isMobile){
        let movementSpeed = 0.75;
        donateXMR
            .move(90, movementSpeed)
            .boundaries("wrap")
            .run((a)=>{
                if (a.x > -5) donateBCH.leftOf(a)
            })

        donateBTC
            .move(90, movementSpeed)
            .boundaries("wrap")
            .run((a)=>{
                if (a.x > -5) donateXMR.leftOf(a)
            })

        donateBCH
            .move(90, movementSpeed)
            .boundaries("wrap")
            .run((a)=>{
                if (a.x > -5) donateBTC.leftOf(a)
            })

        let stopMovement = box()
            .size(15)
            .position(windowWidth-30)
            .over(donateBTC, 15)
            .check((bool)=>{
                if (bool){
                    donateXMR.move(90,movementSpeed);
                    donateBTC.move(90,movementSpeed);
                    donateBCH.move(90,movementSpeed);
                } else {
                    donateXMR.move(90,0);
                    donateBTC.move(90,0);
                    donateBCH.move(90,0);
                };
            }, true)
            .within(pageL)
        let slideItems = box()
            .size(200, 40)
            .leftOf(stopMovement, 0)
            .text("Slide Items", 12)
            .textAlign('right', 2, 'top', 2)
            .within(pageL)
    }
}


//? Navigation
const hidePages = () =>{
    page1.hide();
    page2.hide();
    pageL.hide();
};
let navigationBar = box()
    .size(windowWidth-18+20, navLength)
    .position(-20, windowHeight-navLength)
    .tab([
    ["Home", ()=>{
        hidePages();
        page1.show();
    }],
    ["Try It Out", ()=>{
        hidePages();
        page2.show();
    }],
    ["Reference",()=>{
        page1.redirect(`https://gitlab.com/Spederan/SpeedrunJS/-/blob/main/documentation.md`, false);
    }],
    [],[],[],[],[],
    ["Download", ()=>{
        page1.redirect('https://gitlab.com/Spederan/SpeedrunJS', false);
    }],
    ["Donate", ()=>{
        hidePages();
        pageL.show();
    }]

    ], false)
    .tabSelect(0)
    .tabColor(0,255,125,125)
    .tabOutline(0,75,125)
    .tabText(18, 'serif')

    if (isMobile){
        navigationBar
        .size(windowWidth-2, navLength/2)
        .position(-1, windowHeight-navLength/2)
        .tab([
            ["Home", ()=>{
                hidePages();
                page1.show();
            }],
            ["Try It Out", ()=>{
                hidePages();
                page2.show();
            }],
            ["Reference",()=>{
                page1.redirect(`https://gitlab.com/Spederan/SpeedrunJS/-/blob/main/documentation.md`, false);
            }],
            ["Download", ()=>{
                page1.redirect('https://gitlab.com/Spederan/SpeedrunJS', false);
            }],
            ["Donate", ()=>{
                hidePages();
                pageL.show();
            }]
        
            ], false)
            .tabSelect(0)
            .tabColor(0,255,125,125)
            .tabOutline(0,75,125)
            .tabText(15, 'serif')
    };
