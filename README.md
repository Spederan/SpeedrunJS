### Speedrun.js

---


Speedrun.JS is a fast and beginner-friendly frontend tool for creating websites and user interfaces. Speedrun.JS is completely object-oriented, and all you do to create different elements is instantiate new instances of the *Box* class and apply whatever methods you need to it. With this tool you can create text boxes, buttons, images, input fields, animated elements with rotation and momentum, and user-interactable objects. In fact, an object can be all of these things simultaneously if you so choose.

To see what Speedrun.js can do for you, head over to the documentation in ``documentation.md``, and then begin writing in ``public/main.js``, which is a blank javascript file ready for you to use.

Speedrun.js requires no prior knowledge of CSS or HTML and only a beginner-level of Javascript, just download this library, open ``public/main.js``, and begin coding. It's that easy!

