# Here, you will be taught the basic principles of programming in javascript.

This is both a basic tutorial for how to become an intermediate+ Javascript programmer, and a reference to guide you. Not all of this content is necessary to learn, nor memorize. Just read the paragraphs and code along with the example code. It may be overwhelming to learn everything at once, which is why this tutorial is only 25 headers long, and is broken into two parts (where you are recommended to take a break and to begin coding).

<br/>

## Part 1: The Core Basics Recommended For Getting Started

<br/>

## 1) What is a programming language?

A programming language is a model that can simulate ordered logical behavior, to the extent that it is *turing complete*, which is just a fancy way of saying that a programming language can be used to program another programming language, at least in theory. 

<br/>

## 2) What is Javascript?

Javascript is a programming language defined by a list of standards, typically executed in the browser. Javascript can be used to create websites. Javascript is also executed at runtime, as opposed to requiring you to compile it manually. Furthermore, if you ever deal with Node.JS, that is simply a recreation of Javascript designed to run in the terminal, such as for creating servers and desktop applications. 

<br/>

## 3) Where can I run Javascript?

In speedrunjs you can follow the instructions in ``documentation.md`` and that will get you started. But in general, you run javascript by creating an HTML file, including a javascript file in a script tag, and writing your javascript in that file. If you need a quick environment for testing small amounts of javascript code, you can use tools like onecompiler, see ``https://onecompiler.com/javascript``. In fact, for the duration of this tutorial, I ***strongly recommend*** you open onecompiler or a similar tool and execute code along with this tutorial (and write the code, do not copy/paste).

<br/>

## 4) Do I need math? Do I need to memorize everything?

No and no, at least not initially. You can run into math problems eventually, but you won't anytime soon while you are getting started, and some developers say they never need math beyond basic arithmetic. As for memorization, just treat this file like extended documentation, refer back to it when you need to remember how to do something. You don't need to memorize, and you won't, until you've practiced writing code for a good long while.

**Alright, let's get started with syntax.**

<br/>

## 5) Variables

Variables have a few main types: ``string``, ``number``, ``boolean``, ``object``, ``function``, ``class``, and ``null``/``undefined``.

**string**: A string is enclosed in quotation marks, double quotation marks, or backticks, and can include arbitrary data. 
    Example: ``"Hello World"``. **Pass-By-Value**.

**number**: A number is a number that can be mathematically interacted with, without quotation marks. It can be negative, zero,    positive, or a decimal. However, keep in mind, performing arithmetic on decimals will result in floating point errors, this is a normal aspect of most programming languages. **Pass-By-Value**.
    Example: ``5``.
    Example of floating point error: ``0.1 + 0.2``. This will not result in exactly ``0.3``. 
    Example of solution to floating point error: ``Math.round((0.1 + 0.2)*10)/10``. This code will result in ``0.3``.

**boolean**: ``true`` or ``false``, no quotation marks. Non-boolean values can also be "truthy" or "falsy" (which is not the same as true/false). True is truthy, false is falsy, non-zero numbers are truthy, zero is falsy, strings are truthy, an empty string is falsy, null/undefined is falsy. Truthy/falsy values will be useful later. **Pass-By-Value**.

**object**, **function** and **class**: We will learn about these later. But know they can be variables too. **Pass-By-Reference**.

**null/undefined**: Null is a arbitrary classification (generally only happens if you assign a variable as null or theres an error), its falsy but its not ``false``. Its similar to ``undefined``, except undefined is a natural state of a variable before it is defined. ``undefined`` and ``null``  work the same way, but they are not directly equivalent just like they aren't the same as ``false``.  **Pass-By-Value**.

Note: Also, theres ``nan``, which stands for "not a number", and this is **not** a value that can be directly assigned, but is an error that occurs when you try to combine a number and a non-number. You'd get ``nan`` for example, if you did something like ``5 - "Hello"``.


**What is Pass-By-Value VS Pass-By-Reference?**

Pass-by-value means a variable, if assigned, becomes a new copy of that variable. 

Pass-by-reference means a variable, if assigned, becomes a reference to the other variable. So if you change one, it changes both. It's like giving the same person two names. To get a new copy of an object/array or other, you will have to perform some workaround.

<br/>


## 6) Declaring Variables

You can declare variables in three primary ways, using ``let``, ``const``, and ``var``. 

**You want to use ``let``. Forget the rest for now.**

``let`` is an improvement on ``var`` (which was the original way of declaring variables), and the improvement is that it makes more sense in terms of scope, and doesn't pollute the global namespace. 

As for ``const``, you can use that in place of ``let`` if you don't plan on changing the variable, because you *cannot* change a const variable. But it's not necessary as a beginner.

Both ``let`` and ``var`` will allow you to change the variable later, but again, don't use ``var``, use ``let``, unless you come across a good reason for doing so.

An example of declaring a variable:

``

let variableName = "This is the name of my variable."

console.log(variableName); //Logs "This is the name of my variable."

``

Since this is the first code you will be running, its good to know that ``console.log()`` is just a tool for seeing the value of an output show up in your console. Also you should know what comments are, they are ``//`` or ``/*  */`` and they can be used to write a message in your code, which will not run as code.

Final Note: Be explicit! its better to write long and ugly variable names, than make it so short that you forget what it does, or such that others can't read your code! Combine explicit variable-naming (or a fair balance of being explicit and concise for maximum readability), with good commenting habits.

<br/>


## 7) Scope

Scope is the idea that variables can only be accessed in the "scope" they are declared in, or in a lower "scope". This is a useful property of a programming language, because it means you can use variable names multiple times for different things, and as long as they are in different scopes, they don't affect each other.

Scope is generally creating using curly braces `{ }`. Curly braces are present for example in if-statements, functions, and you can even use them by themself. In this example I will use them by themselves (but generally you wouldn't do this).

Example 1 (Using scope correctly):

``

let variable = 1;

{
    variable = 5;
};

console.log(variable); //5

``


Example 2 (Using scope incorrectly):

``

{
    let variable = 5;

    console.log(variable); //5
};

console.log(variable); //undefined

``

Example 3 (Overriding a variable):

``

let variable = 1;

{
    let variable = 5;

    console.log(variable); //5
};

console.log(variable); //1

``

<br/>

## 8) Logical Operators and Math

Logical operators allow you to perform more complex logic, and perform arithmetic. Here is a list of the different things you can use.

**``&&``**:  The logical "and" operator. Example: ``console.log(true && false) // logs false``

**``||``**:  The logical "or" operator. Example: ``console.log(true || false) // logs true``

**``!``**:  The logical "not" operator. This will make "truthy" values ``false``, and "falsy" values ``true``. Example: ``console.log(!true) // logs false``

**``==``**: The loose equivalance operator. Allows for type coercion. Example: ``console.log(1 == "1") // logs true``

**``===``**: The strict equivalance operator. Example: ``console.log(1 === "1") // logs false``

**``+``**:  The addition operator. Example: ``console.log(1 + 1) // logs 2``

*Note: Also, the addition operator can also be used to concatenate strings. Example: ``console.log("Hello" + "World") // "Hello World"``*

**``-``**:  The subtraction operator. Example: ``console.log(1 - 1) // logs 0``

**``*``**:  The multiplication operator. Example: ``console.log(2 * 3) // logs 6``

**``/``**:  The division operator. Example: ``console.log(6 / 3) // logs 2``

**``%``**:  The modulus/modulo operator. Gives the remainder of a division. Example: ``console.log(5 % 6) // logs 1``

**``**``**:  The exponentiation operator. Example: ``console.log(3**2) // logs 9``

**``Math.sqrt()``**  Square root (opposite of exponentiation). Example: ``console.log(Math.sqrt(9)) // logs 3``

**``Math.floor()``**  Rounds down. Example: ``console.log(Math.floor(0.5)) // logs 0``

**``Math.round()``**  Rounds normally. Example: ``console.log(Math.floor(0.5)) // logs 1``

**``Math.ceil()``**  Rounds up. Example: ``console.log(Math.floor(0.5)) // logs 1``

**``Math.random()``**  Gives a random decimal between 0 and 1. Example: ``console.log(Math.random())``

**( )**:  Parentheses (forces statement to be run first). Example: ``console.log((1 + 2) * 3) // logs 9``

More: There is shorthand for some arithemtic operations, ``++`` will increment a variable by 1, ``--`` will decrement a variable by 1, and ``+=``, ``-=``, ``*=``, ``/=``, and ``%=`` are all shorthand for a variable equals itself operated by whatever number follows the operator. For example: ``let a = 1; a+= 5; console.log(a); //6``


## 9) Conditional Logic

Conditional logic is the idea of only executing code if a statement is met. It has three primary formats: if statement, switch statement, and ternary. You only need to learn if statements and maybe switch statements starting out.

In all three cases, the block of code is executed only if the value of the condition is truthy. 

<br/>

**If Statements**:  If statements are generally used for binary conditions, things that are either true, or false, with only one or two possible outcomes. You *can* chain a bunch of if-else statements or write a long list of if-statements, but the former is unreadable and the latter is inefficient, so opt for switch statment in that case instead.



    Example 1: With curly braces (for executing multiple lines of code):

    ``
    let condition = true;

    if (condition){
        console.log("Reading 1") //"Reading 1"
    };

    let condition2 = false;

    if (condition2){
        console.log("Reading 2") //Nothing will happen
    };

    ``

    Example 2: Without curly braces (for executing a single line of code):

    ``
    
    let condition = true;

    if (condition) console.log("Reading 1") //"Reading 1"

    let condition2 = false;

    if (condition2) console.log("Reading 1") //Nothing will happen

    ``

    Example 3: if-else (for efficiently executing only one condition):

    ``
    
    let condition = true;

    if (condition){
        console.log("Reading 1") //"Reading 1"
    } else {
        console.log("Reading 2") //Nothing will happen
    };

    ``
<br/>

**Switch Statements**:  Switch statements are for if you have a bunch of different possible values for a condition, such as a string with many possible values.

    Example 1: Standard (for checking the value of a condition):

    ``
    let letter = 'a';

    switch(letter){
        case 'a': 
        console.log("Reading A"); //"Reading A"
        break;

        case 'b': 
        console.log("Reading B"); //Nothing
        break;

        case 'c': 
        console.log("Reading C"); //Nothing
        break;
    };

    ``

    Example 2: Standard with default (for checking against values not defined explicitly):

    ``
    let letter = '5';

    switch(letter){
        default: 
        console.log("Not a letter."); //"Not a letter."
        break;

        case 'a': 
        console.log("Reading A"); //Nothing
        break;

        case 'b': 
        console.log("Reading B"); //Nothing
        break;

        case 'c': 
        console.log("Reading C"); //Nothing
        break;
    };

    ``

    Example 3: Alternative switch statement (for more advanced condition checking):

    ``
    let letter = 'a';

    switch(true){
        case letter === 'a': 
        console.log("Reading A"); //"Reading A"
        break;

        case letter === 'b': 
        console.log("Reading B"); //Nothing
        break;

        case letter === 'c': 
        console.log("Reading C"); //Nothing
        break;
    };

    ``
<br/>

**Ternary**: Like an if-else statement, but fits on one line. Not recommended for beginners, and not really necessary either.

    Example: 

    ``
    let condition = true;

    condition ? console.log(true) : console.log(false); //true

    ``

<br/>

## 10) Functions

Functions are reusable blocks of code, and sometimes return a value. Theres two main kinds, standard, and arrow.

Example:

``

//Standard

function twoPlusTwo(){
    return 2 + 2;
};
console.log(twoPlusTwo()); // 4

//Arrow

let twoPlusTwo = () => {
    return 2 + 2;
};
console.log(twoPlusTwo()); // 4

``

The difference between standard and arrow functions, besides appearance, is that standard functions have an internal value that can be accessed by calling ``this``, while arrow functions don't and that makes them useful for using inside of other functions and classes.

Example:

``
function func(){
    console.log(this) //"function func () {console.log(this)}"
};

let func = () => {
    console.log(this) //Something else
};

``

You can also pass parameters into functions, and you can call functions as many times as you want. One more note, stuff written after the ``return`` statement will not be executed.

Example: 

``
let addTwo = (a, b) => {
    return a + b; 
    console.log("Reading?); //Nothing
};

console.log(addTwo(1, 2)); //logs 3
console.log(addTwo(2, 3)); //logs 5

``

<br/>

## 11) Callback Functions

Callback functions are ordinary functions, but passed into another function, to execute asynchronously. Also, sometimes the function will pass a parameter into the callback function.

Understanding asynchronous code is *important*. Not everything will be run one line after the other, sometimes operations are computationally expensive or are uncertain as to when they will execute. This is why callback functions are useful, so you can execute your code *at the right time*.

Theres two ways of using callback functions.

Example 1: Define it first:

``
let func1 = (func) => {
    func("Hello World");
};

let func2 = (str) => {
    console.log(str);
};

func1(func2); //logs "Hello World"

``


Example 2: Define it inline:

``
let func1 = (func) => {
    func("Hello World");
};

func1((str)=>{
    console.log(str);
}); //logs "Hello World"

``

Which should you use? Generally, just use which one you think looks more readable. My recommendation though, is get used to doing it inline, because you will write your code faster, and it's easier to understand once you get used to it.


<br/>

## 12) Arrays

Arrays are an ordered list of variables, starting at index 0.

Example: 

``
let arr = [1, 2, 3];

console.log(arr); //[1, 2, 3]

console.log(arr[0]); //1

console.log(arr[1]); //2

console.log(arr[2]); //3

``

<br/>

## 13) Loops

Loops execute code multiple times. Theres two main kinds: for, and for in. Theres other kinds too, but these are the two I will touch on. Loops are good for interacting with arrays.

Example using a *for loop* to define and array, and a *for-in loop* to access it:

``

let arr = [];
let numTimes = 5;

// For loop
for (let i = 0; i < numTimes; i++){
    arr[i] = i * 5;
};

// For In Loop
for (let i in arr){
    console.log(arr[i]); // 0, 5, 10, 15, 20
};

``

A small note: The ``i`` in a for loop is a ``number``, but the ``i`` in a for-in loop is a ``string`` because it references an object key (techinically, arrays are objects). This is okay, you can reference an array like ``arr[1]`` or ``arr["1"]``, both are valid.

Also, for-in loops work on objects.

<br/>


**Note: If you are in a hurry to get coding or you are feeling overwhelmed, go ahead and take a break here, and go get started in ``documentation.md``. Up to this point you've learned the absolute basics, and you can go over the rest another day.**


<br/>

## Part 2: The Rest Of What A Javascript Programmer Ought To Know

<br/>


## 14) Objects

An object is an organized list of named variables, like an array, but trading enumeration for being named.

Example: 

``
let obj = {
    a: "hello",
    b: "world",
};

``

console.log(obj.a + obj.b); //"Hello World"
console.log(obj["a"] + obj["b"]); //"Hello World"


Optional Chaining allows you to check if a property of an object exists, while checking if the object itself exists.

For example:

``
let obj = {property: "Hello World"}
if (obj?.property){
    console.log(obj.property); //"Hello World"
};

Keep in mind, optional chaining is only valid inside of places like if statements or function calls, not variable assignments.



<br/>


## 15) Classes

A class is syntactic sugar built on top of a function, and classes are used for object-oriented programming. Functionally, its like if a function and an object had a baby.

Example:

``

class Example {
    //This code is only run once, when you call new Example()
    constructor(exampleOfParameter){
        this.exampleOfProperty = exampleOfParameter;
    };
    //This code is basically just a function, but it can access the "this" context of the class
    exampleOfMethod(exampleOfParameter2){
        console.log(this.exampleOfProperty + exampleOfParameter2);
    };
};
let example = new Example("Hello");
example.exampleOfMethod("World") //"Hello World"

``

Classes don't do anything that functions can't do if manipulated properly, but it's a lot easier to keep track of what's going on. Variables personal to the class can be instantiated under the "constructor", then each method can be run whenever you call it (just like a function, and in fact, methods ***are*** functions.)

<br/>

## 16) Maps

Maps are an alternative to objects and arrays, and they are good for keeping a list of values if you plan to create and delete from the list often. This actually works great in conjunction with object-oriented-programming.

Imagine if you were coding a webgame where a player shot bullets, you would want to create those bullets on the fly, but delete them when they are done, that way memory is freed up.

A map works by taking two values (any kind of value), the first one is the reference, and the second one is the result.

Example:

``
let map = new Map();
map.set(1, 2);
console.log(map.get(1)) //2
map.delete(1);
console.log(map.get(1)) //undefined

``

The reason maps are more efficient for deleting objects is that they can be deleted individually, while in an object or an array you'd have to loop through every member of that object/array to recreate a new one each time. The tradeoff is that looping through maps is less efficient, and more difficult.

<br/>

## 17) Sets

Like arrays, but you can only have one of each item.

Example: 

``
let set = new Set();
set.add(1);
set.add(2);
set.add(2);
set.add(3);
console.log(set) //{1,2,3}

``

Also, you can run an array through a set to delete duplicates.

Example: 

``
let arr = [1,2,2,3];
let set = new Set(arr);
arr = [...set];
console.log(arr); //[1,2,3]

``

The triple-dot you saw (``...``) aka the "spread operator" is just used to take an array or array-like-object, and create a new array from it. Otherwise, you'd be referencing the original object/array, not creating a new one. 

<br/>


## 18) Destructuring

Destructuring allows you to declare multiple variables at once.

For example:

``
let [a,b,c,d] = [1,2,3,4];
console.log(a) //1

``

<br/>

## 19) Template Literals

A fancy way to concatenate or inject variables inside of strings. Must use backticks, not quotation marks.

Example:

``
let a = "World";

console.log(`Hello ${a}!`); //"Hello World!"

``

Old way of doing it:

``
let a = "World";

console.log("Hello" + a + "!"); //"Hello World!"

``

Final note: backticks allow you to break in between lines, unlike quotation marks. For this reason they are good to get used to, as they are generally more useful.

<br/>


## 20) While Loops

While loops execute indefinitely until you use ``break`` or until the given condition is falsy. These aren't good for beginners, because they are notorious for causing an infinite loop and freezing your browser. All it takes is one bug, and instead of troubleshooting it, you will be sitting there patiently wishing you could.

While Loop Example:

``
let a = true;

//Runs indefinitely and will freeze your browser
while(a){

};

//Does not run
while(!a){

};

//Runs once
while(a){
 a = false;
};

//Runs once
while(a){
 break;
};

``

<br/>

## 21) Recursion

Recursion is when you call a function from within the function, creating a kind of infinite loop (like a while loop) until you break out of it somehow. Not recommended for beginners, because it usually has very poor readability. Recursion is necessary in very few situations.

Example: 

``

function incrementRecursively(a){
    a++;
    if (a < 5){
        a = incrementRecursively(a);
    };
    return a;
};
console.log(incrementRecursively(1)); //5

``

<br/>

## 22) Self-Executing functions

A self-executing function is a bit of a niche usecase for a function, and its not recommended for beginners, because it looks complicated. But it looks more complicated than it really is.

A self-executing function is defining a function and then executing it immediately, by placing the entire function in parentheses and then calling it.

Example (Named Arrow Function):

let func = (() => {
    console.log("Hello World");
})();

Example (Named Standard Function):

(function func(){
    console.log("Hello World");
})();

Example (Anonymous Arrow Function):

(() => {
    console.log("Hello World");
})();

Example (Anonymous Standard Function):

(function(){
    console.log("Hello World");
})();

<br/>

## 23) setTimeout(), setInterval(), and requestAnimationFrame()

All three of these functions are used in asynchronous programming, because they allow you to enter a callback function, which is not executed until after the other code in your file is executed.

setTimeout is a one-time callback execution, with a configurable ms delay.

Example: 

``
setTimeout(()=>{
    console.log("Executes immediately after rest of file");
}, 0);

``

setInterval is like setTimeout, except it will repeat forever, instead of only run once. You can clear it out using ``clearInterval()`` though.

Example:

``
let interval = setInterval(()=>{
    console.log("Executes once per second");
    clearInterval(interval); //Will no longer execute after this line of code is run
}, 1000);

``

requestAnimationFrame is a more recently invented alternative to setInterval, which is better for animations, as it smooths movement. By default it will run at about 60 fps when used continously. Not all browsers support it though, so I will give an example that defaults back to setInterval.

Example:

``
let callback;
callback = () => {
    console.log("Hello World!"); //logs "Hello World" 60 times per second
    if (window?.requestAnimationFrame) requestAnimationFrame(callback);
};
if (window?.requestAnimationFrame){
    requestAnimationFrame(callback);
} else {
    setInterval(callback, 17)
};

``


<br/>


## 24) Class Inheritance

An interesting usecase of classes is inheritance, which makes classes more useful for object oriented programming. This isn't a bad practice unless it is misused.

To use inheritance, youd use the keyword ``extends`` on the child class, and ``super()`` in the constructor.

Example:

``
class Hello{
    constructor(){
        this.a = "Hello";
        this.b = "World";
    };
    logHelloWorld(){
        console.log(this.a + this.b);
    };
};

class World extends Hello {
    constructor(){
        super();
        console.log(this.a) //"Hello"
        console.log(this.b) //"World"
    };
};

let world = new World();
world.logHelloWorld(); //"Hello World"

``

<br/>

## 25) Class Polymorphism

Another usecase of classes is polymorphism, which is just fancy jargon for saying "You could create two different classes that inherit from a parent, and do similar things with them."


Example:

``
class Vehicle {
    constructor(){
        this.x = 0;
        this.y = 0;
    };
    move(n){
        this[n]++;
    };
};

class Car extends Vehicle {
    constructor(){
        super();
    };
    move(){
        super.move('x');
    };
};

class Plane extends Vehicle {
    constructor(){
        super();
    };
    move(){
        super.move('y');
    };
};

let arr = [];
arr.push(new Car());
arr.push(new Plane());
for (let i in arr){
    arr[i].move();
    console.log(arr[i].x, arr[i].y) //(1,0), (0,1)
};

//Explanation: I created a parent class *Vehicle* with an x and y position, with a method that increments one of these values using the method move(). I then created two child classes *Car* and *Plane* which pass it 'x' or 'y'. I then create one of each class, loop through it, and call move() on both. They both have the same function, and they did similar but different things. This is polymorphism.

``

<br/>

---

If you've made it this far, congratulations, you passed the tutorial on how to code. Remember, use this as a reference, you do not have to memorize anything, nor be good at math. Just begin practicing writing clean and readable code. For more practice you can always go back through and code-along with the examples again, maybe even combine them in new ways. And if you need more information on something, look it up on the internet, it's what all programmers do.

And do not worry, SpeedrunJS does not require that you understand all of these concepts. Ideally you'd want a basic understanding of the first 13, but even that is not necessary if you can just replicate the code you see in the documentation.