### Speedrun.js

---

SpeedrunJS is an easy-to-use, fast, object-oriented front-end library for creating interactive websites and web UIs. SpeedrunJS is built on Javascript with dynamic HTML and CSS.

Usage: All you have to do is include an src of "speedrun.js" in your html file, and write your javascript code in main.js.

SpeedrunJS works by having programmable "box" objects, which visually look like rectangles (or ellipses), and can contain text, input fields, images, be interactable, animatable, and more. A detailed list of every usable method for Box is listed below.

<br/><br/>



<br/>

---

#### Getting Started (for Javascript Beginners):

*If you have never coded before in your life, then first visit ``how-to-code.md`` where the basics of Javascript programming will be covered. It is not hard to learn the bare basics, and it shouldn't take longer than a couple hours at worst to overview it. You can decide how much you want to learn, not all of it will be necessary or relevant, and each portion is marked as such.*

You can begin coding in SpeedrunJS in a few different ways:

*Note: All of these options involve downloading the repository, and writing your code in ``public/main.js``*.

**Option 1 (easiest):** Navigate to public/index.html in the repository. Double-click to launch in the browser. You will now be running any code written in ``public/main.js`` locally.

**Option 2:** Using Visual Studio Code, download live server, click "go live", and navigate to ``/public`` in your URL or by clicking the icon. You will now be hosting ``main.js`` using their live server.

**Option 3:** After downloading Node.js and performing ``npm i`` from the terminal (look up a Node.JS tutorial to figure this out), navigate to ``http://localhost:3000/``. You will now be hosting ``main.js`` from your own personal and customizable server. There are features of SpeedrunJS that are exclusive to this option, so after getting comfortable with SpeedrunJS you will want to learn how to do this.

<br/><br/>


#### Drawing a Box:

Boxes are the fundamental building block of all assets in SpeedRun.js. They can be rectangles, ellipses, have any color or none at all, interact with the user, contain text, and even be easily animated.

There are four main categories for box methods: Display, User-Interaction, Physics, and Organization.

<br/>

---



##### Instantiation

```box()```

*<sub> Creates a new box and returns it. </sub>*

*<sub> Note: By default the box will be an invisible 100x100 square. You must style it for it to appear. </sub>*

<br/>


For those new to Javascript:


- To use a method of Box, you do it like this (replace the word "method" with the name of the method, and the params with the values you plan to pass to it):

```box().method(param1, param2)```


*In the case of a parameter that takes a "callback" function, you can either pass it the name of a function, or you can create a function inline. Don't forget that some callback functions will be passed a value. You can create an inline function like this:*

*```box().method((val) => { console.log(val) })```* 


Also, you can chain methods since each method returns the box object.

``

    box()

        .method1()

        .method2()

        .method3()

 ``


 You can also assign the box to a variable and access it explicitly.

``

    let b = box(); 

    b.method1();

    b.method2();

    b.method2();

 ``


---
---




##### Methods

---




**Display:**

*<sub>Note: By default the functions in this category are only run once (unless they are stuck inside a callback function that is run continuously).</sub>*

<br/>


Core Properties (position, size, color, and shape):

```position(x, y, [centerX], [centerY])```

*<sub> Sets the location of a box using exact coordinates. </sub>*

*<sub> Note: The box is drawn starting at the top-left corner, and by default is always set to (0,0). </sub>*

*<sub> Tip: To place your box in the middle of the screen, perform position(mid, mid), as each mid will center the box with respect to the screen and its own dimensions. </sub>*

*<sub> x: The left side of your box. By default, defaults to the current x position. </sub>*

*<sub> y: The top side of your box. By default, defaults to the current y position. </sub>*

*<sub> centerX (optional): if true, it will draw the x position from the center. False by default. </sub>*

*<sub> centerY (optional): if true, it will draw the y position from the center. False by default. </sub>*

<br/>

```right(box, [padding], [ratio], [centered], [fixed])``` or ```left(...)``` or ```over(...)``` or ```under(...)```

*<sub> Sets a box to the right/left/top/bottom of another box, allowing for explicit dynamic positioning. </sub>*

*<sub> box: The other box that this one is being positioned relative to, as a reference or as its name (see ``name()``). </sub>*

*<sub> padding (optional): Some extra space to include between the boxes. 0 by default. </sub>*

*<sub> ratio (optional): What percentage of the width of the box the displacement is based on. 1 by default. Note: This is the opposite box's width for ``right`` and ``under``, and the same box's width for ``left`` and ``over``. </sub>*

*<sub> centered (optional): If centered, it will draw the x or y in the center instead of at the top-left corner. False by default. </sub>*

*<sub> fixed (optional): If fixed, it will *always* be at this location relative to the other box, overriding its own internal physics. False by default. </sub>*

<br/>

```rightOf(box, [padding], [ratio], [centered], [fixed])``` or ```leftOf(...)``` or ```topOf(...)``` or ```bottomOf(...)```

*<sub> A variation of right/left/over/under, which also sets the other dimension equal to the other object. </sub>*

<br/>

```above(box)``` or ```below(box)```

*<sub> Places a box over or under another box using z-index. </sub>*

*<sub> Note: Due to how z-index works, this will not force an object over another if prevented by it's stacking context. </sub>*

*<sub> box: The other box that this one is being positioned relative to, as a reference or as its name (see ``name()``). </sub>*


```size(w, h)```

*<sub> Sets the size of a box. </sub>*

*<sub> w: The width of your box. </sub>*

*<sub> h: The height of your box. </sub>*

*<sub> Note: When working with containers you want to use scale() and not size(), because this will prevent your content from getting distorted. See "scale" under "physics" for more information. </sub>*

<br/>

```curves(curve, [curve2], [curve3], [curve4], [absolute])```

*<sub> Set the curves of a box, using percentage. </sub>*

*<sub> absolute (optional): If enabled, uses pixel values instead of percentage. This is useful in some circumstances where the shape is interpreted differently, such as with range sliders. This could however make the box difficult to work with collision. False by default. </sub>*

<br/>

```polygon(str, [regular])```

*<sub> Create an n-sided polygon using css clip-path. This has collision detection that works with buttons. </sub>*

*<sub> Note: Polygons may not rotate perfectly around its axis, since it uses its rectangle's centerpoint. </sub>*

*<sub> str: A string either representing the name of a supported shape, or representing an arbitrary list of coordinates. Must be properly formatted CSS. By default it is a triangle. </sub>*

*<sub> regular (optional): If applicable, it will resize your box to make the shape closer to being a regular polygon. There are no guarantees it will be perfectly regular, and it only applies to triangle, and pentagon through decagon. False by default.  </sub>*

*<sub> A list of all default supported shapes: triangle, trapezoid, parallelogram, pentagon, hexagon, heptagon, octogon, nonagon, decagon, bevel, rabbet, chevron, arrow, pointer, star, cross, close, frame, and message. </sub>*

*<sub> Using CSS coordinates: For example: </sub>*

*``'50% 0%, 0% 100%, 100% 100%'``*

 *<sub> This code snippet will create a triangle. You must include percent signs, commas every two elements, and no semicolon at the end. </sub>*

<br/><br/>


Style Properties: 

```color(r, g, b, [a])```

*<sub> Changes the fill color of the box. </sub>*

*<sub> - r: The rgb-red value of the fill color. </sub>*

*<sub> - g: The rgb-green value of the fill color. </sub>*

*<sub> - b: The rgb-blue value of the fill color. </sub>*

*<sub> - a (optional): The rgb-alpha (transparency) value of the fill color. Default is 255. </sub>*

*<sub>Some Beginner Tips: </sub>*

*<sub> - White = (255,255,255,255) </sub>*

*<sub> - Black = (0,0,0,255) </sub>*

*<sub> - Grey = (130,130,130,255) </sub>*

*<sub> - Red = (255,0,0,255) </sub>*

*<sub> - Blue = (0,0,255,255) </sub>*

*<sub> - Green = (0,255,0,255) </sub>*

*<sub> - Yellow = (255,255,0,255) </sub>*

*<sub> - Orange = (255,130,0,255) </sub>*

*<sub> - Purple = (255,0,255,255) </sub>*

*<sub> - Pink = (255,130,130,255) </sub>*

*<sub>Note: From now on this will be the definition of r, g, b, and a.</sub>*

<br/>

```outline(r, g, b, [a], [weight])```

*<sub> Changes the outline color of the box. </sub>*
*<sub> - weight (optional): How thick the outline is. Default is 1. </sub>*

<br/>

```gradient(r, g, b, [a], [radial], [deg])```

*<sub> Creates a color gradient, using the current color and a new color. </sub>*
*<sub> - radial (optional): If true, makes a radial-gradient, else a linear-gradient. False by default. </sub>*
*<sub> - deg (optional): If radial is false, deg represents which direction the gradient comes from. 0 by default. </sub>*

<br/>

```nGradient(colorsArr, [radial], [deg])```

*<sub> Creates a color gradient, using an array of colors. Supports any number of colors and doesn't use the original color. </sub>*
*<sub> - colorsArr: An array of objects and/or arrays, each item representing a color. Each item can look like ``[num, num, num, num]`` or ``{r: num, g: num, b: num, a: num}``. </sub>*
*<sub> - radial (optional): If true, makes a radial-gradient, else a linear-gradient. False by default. </sub>*
*<sub> - deg (optional): If radial is false, deg represents which direction the gradient comes from. 0 by default. </sub>*

<br/>


<br/>


Meta Properties:

```remove()```

*<sub> Permanently deletes a box and all of its elements, freeing up usable CPU/memory on your site. </sub>*

<br/>

```isolate()```

*<sub> This will disassociate the box from the page's div. This allows you to have positioning fixed to the window rather than a potential modified window size. </sub>*

<br/>

```hide([disable])```

*<sub> Hides a box and all of its elements. </sub>*

*<sub> disable (optional): Will not only hide an element, but outright disable it so that the user cannot interact with it. True by default. If false, it only changes the visual opacity. </sub>*

<br/>

```show()```

*<sub> Shows a box and all of its elements after being hidden. </sub>*

<br/>

```opacity([num])```

*<sub> Sets the opacity of the box and all it's elements. Takes a value between 0 and 1. </sub>*

<br/>

```darken([num])```

*<sub> Darkens the box by some amount (or lightens if you use a negative value) </sub>*

<br/>

```undarken([num])```

*<sub> Reverses the change made by darken, if a change was made. It's called undarken and not lighten because it only works after using darken. </sub>*

<br/>

```disable()```

*<sub> Disables button aka detect() functionality and makes the box 75% transparent. </sub>*

<br/>

```enable()```

*<sub> Restores button aka detect() functionality and opacity. </sub>*

*<sub> Note: This will restore to 100% opacity even if it was something else beforehand. </sub>*

<br/>

```prototype(name, callback)``` => ```example([params])```

*<sub> Creates a new global method of Box. Useful for reducing code if you use many methods multiple times, and offers a primitive and low-overhead approach to basic object-oriented programming. </sub>*

*<sub> name: The name of the method you would like to create. Note: Make sure it is unique and will not conflict with other names. </sub>*

*<sub> callback: The code you will execute. The first parameter passed in will be the box being customized, the second parameter will be an object or array representing any paramaters if you choose to pass some in. </sub>*

*<sub> Example: </sub>*

``
// Creates two red 50-by-50 rectangles, one at (0,0) and one at (100,100)

let box1 = box()
    .prototype("example", (a, b)=>{
        a
            .size(50)
            .position(b.x, b.y)
            .color(255,0,0)
    })
    .example({x: 0, y: 0})

let box 2 = box()
    .example({x: 100, y: 100})

``

<br/>



Text Properties:

- *Note: all text here is highlightable.*

```text(text, size, [link])```

*<sub> Creates or replaces text with an arbitrary size, centered on the box. </sub>*

*<sub> - text: Whatever text you wish to display. </sub>*

*<sub> - size: How large you want your text to be. </sub>*

*<sub> - link (optional): A hyperlink that extends to the whole box. Note: detect() will override a hyperlink. </sub>*

<br/>

```textAlign(horizontalAlignment, [horizontalPadding], [verticalAlignment], [verticalPadding])```

*<sub> Re-aligns text to left/right/center and to top/bottom/center with custom padding. </sub>*

*<sub> - horizontalAlignment: 'left', 'right', or 'center'. If the method is called, then it is 'left' by default. </sub>*

*<sub> - horizontalPadding (optional): Padding to the left or right of the text. 10 by default. </sub>*

*<sub> - verticalAlignment (optional): 'top', 'bottom', or 'center'. If the method is called, then it is 'center' by default. </sub>*

*<sub> - verticalPadding (optional): Padding to the top or bottom of the text. 10 by default. </sub>*

<br/>

```textOffset(xOffset, yOffset)```

*<sub> Offsets the x and y values of the text. </sub>*

<br/>

```textColor(r, g, b, [a])```

*<sub> Colors any text you have created. </sub>*

<br/>

```textOutline(r, g, b, [a], [weight])```

*<sub> Creates and colors an outline/stroke for any text that you have created. </sub>*

*<sub> - weight (optional): How thick the outline is. Default is 0.2. </sub>*

<br/>

```textFont(font)```

*<sub> Sets the font to be used for the text. Note: Most browsers only supports a few fonts. </sub>*

*<sub> - font: Options: 'serif', 'sans-serif', and 'monospace'. Default is 'serif'. </sub>*

<br/>

```textStyle(bold, italic, [decorationType], [decorationStyle], [decorationThickness])```

*<sub> Allows for bolding, italicizing, and decorating text. </sub>*

*<sub> Note: You can only use one decoration at a time using this method. To use multiple at a time, then use textInjectCSS and use 'decoration' for the key, and the value of your multiple decorations, concatenated with just a space. </sub>*

*<sub> - bold: Whether or not the text is bolded. </sub>*

*<sub> - italic: Whether or not the text is italicized. </sub>*

*<sub> - decorationType (optional): Options: false, 'underline', 'overline', and 'line-through'. </sub>*

*<sub> - decorationStyle (optional): Options: 'solid', 'double', 'dotted', 'dashed', 'wavy'. </sub>*

*<sub> - decorationThickness (optional): Decoration thickness in pixels. Default is 2. </sub>*

<br/>

```textDecorationColor(r, g, b, [a])```

*<sub> Allows for coloring the decoration. If called but no parameters are passed, it sets the color equal to the text color. If it's not called, the color is black. </sub>*

<br/><br/>


Submit properties:

- *Definition: A "submit" element is an accessible user-friendly button that says "submit", for taking user input*.

```submit(callback)```

*<sub> Creates a submit button using custom graphics, by executing a series of functions in-line. This will resize and override properties of your box element. To customize it, just use the normal box methods. </sub>*

*<sub> callback: A function callback that's run once every time the button is clicked. </sub>*

<br/><br/>


Check properties:

- *Definition: A "check" element (or check-box) is a box that can be checked or unchecked, for taking user input*.

```check(callback, [label], [defaultValue])```

*<sub> Creates a n HTML checkbox element. </sub>*

*<sub> callback: A function callback that's run once every time a box state is changed. The first parameter passed in a boolean representing whether or not the box is checked. </sub>*

*<sub> label (optional): A string of text that sits to the right of the check box. This text is non-highlightable. </sub>*

*<sub> defaultValue (optional): Whether or not the box is checked by default. False by default. </sub>*

<br/>

```checkAccentColor(r, g, b, [a])```

*<sub> Changes the accent color of the check mark background afer the user checks the check box. Blue by default. </sub>*

<br/>

```checkLabelColor(r, g, b, [a])```

*<sub> Changes the label color of the check mark text. Black by default. </sub>*

<br/><br/>



Radio properties:

- *Definition: A "radio" element is a list of mutually-exclusive, circular check boxes, for taking user input*.


```radio(options, callback)```

*<sub> Creates an HTML radio element. This will not shrink the box, but rather the box width can be used to determine the alignment of the options. </sub>*

*<sub> options: An array, each item representing the different options that can be selected. </sub>*

*<sub> callback: A function callback that's run continuously after an option is selected. The first parameter passed in is a number representing the index of the option selected, starting from 0. </sub>*

<br/>

```radioDisable(arr/bool)```

*<sub> Disables or re-enables a radio. </sub>*

*<sub> arr/bool: A boolean value (if true all are disabled, if false all are enabled), or an array of boolean values (each true will disable a corresponding element, and each false will enable it). </sub>*

<br/>

```radioAccentColor(r, g, b, [a])```

*<sub> The accent color of the radio element. Blue by default. </sub>*

<br/>

```radioLabelColor(r, g, b, [a])```

*<sub> The accent color of the radio element. Blue by default. </sub>*

<br/><br/>


Select Properties:

- *Definition: A "select" element is a list of mutually-exclusive, contiguous rectangular fields with centered labels, for taking user input*.

```select(options, callback)```

*<sub> Creates a select element using custom graphics. </sub>*

*<sub> options: An array, each item representing the different options that can be selected, except for the first item, as the first item is the default name of the select box. </sub>*

*<sub> callback: A function callback, that's called once for every time a user selects a new option from the select list (except in the case of selecting the default name, as that will run the callback function too). The first parameter passed to your function is the value of the user's selection, as a string. </sub>*

<br/>

```selectDisable(option, active)```

*<sub> Disables a select element, or re-enables it. </sub>*

*<sub> option: A string representing which option you wish to disable. </sub>*

*<sub> active: Whether you are disabling (true by default) or not. If set to false it will re-enable the select. </sub>*

<br/>

```selectText(font, [size])```

*<sub> Modifies the font and size of the text used in a select. </sub>*

*<sub> font: 'sans-serif' by default. you can also use 'serif' and 'monospace'. </sub>*

*<sub> size: 15 by default. </sub>*

<br/>

```selectMargins(padding, [rightHandPadding])```

*<sub> Modifies the padding around the text used in the select. </sub>*

*<sub> padding: Extra space on either side of the text, divided by two. 10 by default. </sub>*

*<sub> rightHandPadding (optional): Extra space only on the righthand side of the text. 30 by default. </sub>*

<br/>

```selectTextColor(r, g, b, [a])```

*<sub> Modifies the color of the text used in the select. Black by default. </sub>*

<br/>

```selectBackgroundColor(r, g, b, [a])```

*<sub> Modifies the color of the background used in the select. White by default. </sub>*

<br/>

```selectHoverTextColor(r, g, b, [a])```

*<sub> Modifies the color of the text used in the select when hovered over. White by default. </sub>*

<br/>

```selectHoverBackgroundColor(r, g, b, [a])```

*<sub> Modifies the color of the background used in the select when hovered over. Blue by default. </sub>*

<br/>

```selectOutline(r, g, b, [a], [weight])```

*<sub> Modifies the color of the outline surrounding the initial select box and the backdrop behind the borderless options. Grey by default. </sub>*

```selectChangedOutline(r, g, b, [a], [weight])```

*<sub> Modifies the color of the outline surrounding the initial select box after a selection is made while the mouse has not yet clicked away. Black by default. </sub>*

<br/>

```selectImage(img)```

*<sub> Changes the image used as the select icon. A black down arrow by default. </sub>*

*<sub> Note: You will have to offset an image by modifying it in order to get it to look centered and properly sized as has been done for the down arrow. </sub>*

<br/><br/>


Navbar Properties:

- *Definition: A "navbar" is a stylistic list of boxes that redirect to some location or perform some action when clicked.*.

```navbar(arr)``` => ```name``` or ```[name, link, [callback]]```

*<sub> Creates a navbar using custom graphics. </sub>*

*<sub> Note: A navbar will automatically align itself horizontally if the width of the box is greater than the height, and vertically otherwise. Also, the size of the boxes are determined by the number of boxes used. </sub>*

*<sub> arr: An array of strings and/or arrays. Each item can be an array of 1-3 in length. Empty array items can be used to create unclickable space. </sub>*

*<sub>*<sub> name: Either a string, or the first index of an array item as a string, representing the name of a box. </sub>* </sub>*

*<sub>*<sub> link: The second index of an array item as a string, representing the link to redirect to. </sub>* </sub>*

*<sub>*<sub> callback (optional): Extra code to run once at the start of the program (not necessary if all you want to do is change the color style of the box or give it interactivity, see below). </sub>* </sub>*

For example: ```navbar(['Home', '', ''], 'News', 'Contact', '', '', '', '', '', '', '', '', '', 'About'])```

<br/>

```navbarFont(size, [style])```

*<sub> Changes the font size and style of all text in the navbar. </sub>*

*<sub> size: 15 by default. </sub>*

*<sub> style (optional): 'sans-serif' by default. </sub>*

<br/>

```navbarColor(r, g, b, [a])```

*<sub> Colors the background of the navbar. </sub>*

*<sub> Note: This also colors the boxes when unselected as they are transparent by default. </sub>*

<br/>

```navbarOutline(r, g, b, [a], [width])```

*<sub> Colors the outline of the navbar. </sub>*

<br/>

```navbarBoxOver(callback)```

*<sub> Executes some arbitrary code when the mouse crosses over a box.  </sub>*

<br/>

```navbarBoxOverColor(r, g, b, [a])``` or ```navbarTextOverColor(...)```

*<sub> Colors the boxes or text of the navbar a different way when the mouse crosses over them.  </sub>*

<br/>

```navbarBoxOverOutline(r, g, b, [a], [width])``` or ```navbarTextOverOutline(...)```

*<sub> Colors the outline of the boxes or text of the navbar a different way when the mouse crosses over them. </sub>*

<br/>

```navbarBoxOut(callback)```

*<sub> Executes some arbitrary code when the mouse leaves a box.  </sub>*

<br/>

```navbarBoxOutColor(r, g, b, [a])``` or ```navbarTextOutColor(...)```

*<sub> Colors the boxes or text of the navbar when not being interacted with. </sub>*

<br/>

```navbarBoxOutOutline(r, g, b, [a], [width])``` or ```navbarTextOutOutline(...)```

*<sub> Colors the outline of the boxes in the navbar when not being interacted with. </sub>*

<br/>

```navbarBoxPress(callback)```

*<sub> Executes some arbitrary code when the mouse presses a box.  </sub>*

<br/>

```navbarBoxPressColor(r, g, b, [a])``` or ```navbarTextPressColor(...)```

*<sub> Colors the boxes of the navbar a different way when the mouse presses them. </sub>*

<br/>

```navbarBoxPressOutline(r, g, b, [a], [width])``` or ```navbarTextPressOutline(...)```

*<sub> Colors the outline of the boxes of the navbar a different way when the mouse presses them. </sub>*

<br/><br/>



Tab Properties

- *Definition: A "tab" element is a horizontal list of buttons for taking user input.*

```tab(arr, smooth)``` arr => ```[name, callback]```

*<sub> Creates a tab using custom graphics. </sub>*

*<sub> arr: An array of arrays and/or strings. </sub>*

*<sub>*<sub> name: The name of the button, as either a string or as the first index of arr as a string. </sub>*</sub>*

*<sub>*<sub> callback: Callback function to execute, passed the box as the first parameter. The second index of arr. </sub>*</sub>*

*<sub> smooth: If true it smooths out the edges on the first and last button only. True by default. </sub>*

<br/>

```tabColor(r, g, b, [a], [intensity], [hoverIntensity])```

*<sub>Colors the buttons of a tab. </sub>*

*<sub> intensity: How much the lightness increases when selected. 25 by default. </sub>*

*<sub> hoverIntensity: How much the lightness decreases when hovered. 10 by default. </sub>*

<br/>

```tabOutline(r, g, b, [a], [weight], [offset])```

*<sub>Colors the outline of a tab. </sub>*

*<sub> offset: How separated each button is. 1 by default. </sub>*

<br/>

```tabText(size, [font])```

*<sub>Modifies the text of each tab. </sub>*

<br/>

```tabTextColor(r, g, b, [a])```

*<sub> Colors the text of each tab. </sub>*

<br/>

```tabSelect(index)```

*<sub> Remotely selects and executes a tab. </sub>*

<br/><br/>




Graph Properties

- *Definition: A "graph" is a data visualization tool.*

```graph(arr, type, [options])``` arr => ```[xLabel, yLabel, [callback]]```

*<sub> Creates a graph using custom graphics. </sub>*

*<sub> Note: Because a graph has so many intricate features, customization is done in a more advanced manner. You will get errors if you pass in values that don't make contextual sense, and styling is all up to you. </sub>*

*<sub> arr: An array of arrays. The first value is the x value/label, the second value is the y value/label, and the third value is an optional callback. </sub>*

*<sub>*<sub> xLabel: For bar and pie graphs this is a string or number, otherwise this is a number. </sub>*</sub>*

*<sub>*<sub> yLabel: For any graph (outside of hbar) this is strictly a number. </sub>*</sub>*

*<sub>*<sub> callback (optional): Callback function for customizing the object associated with an x and y. The first parameter passed in is this object (for example, the bars in a bar/hbar graph, or the points in a scatter/line graph), and the second parameter passed in is a secondary object if applicable (this is the line segments in a line graph, the x axis labels in a bar graph, the y axis labels in a hbar graph, and the labels in a pie graph). </sub>*</sub>*

*<sub> type: A string representing the type of graph being used. Your options are 'bar' 'hbar', 'scatter', 'line', and 'pie'. Default is 'bar', or a standard bar graph. </sub>*

*<sub>*<sub> 'bar': A graph with vertical bars, a set number of y axis labels, and a dynamic number of x axis labels. </sub>*</sub>*

*<sub>*<sub> 'hbar': A graph with horizontal bars, a set number of x axis labels, and a dynamic number of y axis labels. </sub>*</sub>*

*<sub>*<sub> 'scatter': A graph plotted with points, with set axises. </sub>*</sub>*

*<sub>*<sub> 'line': A scatter graph, sorted by x value, and with line segments connecting each point. An initial line segment will connect the first point to 0, by default. </sub>*</sub>*

*<sub>*<sub> 'pie': A circular graph split radially. </sub>*</sub>*

*<sub> options (optional): An object containing the names of any variables in the graph class. A few changeable parameters include: </sub>*

*<sub>*<sub> 'callback': a callback function executed at the end of the file, which can be used to modify objects in the graph. </sub>*</sub>*

*<sub>*<sub> 'title': a string, representing the title of the graph. </sub>*</sub>*

*<sub>*<sub> 'xAxis': a string, representing the xAxis label of the graph. </sub>*</sub>*

*<sub>*<sub> 'yAxis': a string, representing the yAxis label of the graph. </sub>*</sub>*

*<sub>*<sub> 'thickness': a number, representing the thickness of lines (2 by default). </sub>*</sub>*

*<sub>*<sub> 'padding': a number, representing the extra space separating the data from the lines (10 by default). </sub>*</sub>*

*<sub>*<sub> 'margin': a number, representing the space set aside for the title and axis labels (150 by default). </sub>*</sub>*

*<sub>*<sub> 'size': a number between 0 and 1, representing the space between data points (0.9 for bar graphs). Note: If you want uneven spacing, you can leave some labels falsy and it will remove the ticker and label for them, and this creates a larger "space". </sub>*</sub>*

*<sub>*<sub> 'precision': How numbers in labels are rounded. Default is 100, which allows for two decimal places. </sub>*</sub>*

*<sub>*<sub> 'textSize': Base size of most headers and labels. Default is 20. </sub>*</sub>*

*<sub>*<sub> 'pointSize': Size of points on scatter/line graphs. 6 by default. </sub>*</sub>*

*<sub>*<sub> 'pieKeyLimit': Maximum number of keys on each line for a pie chart. Default and max is 6. </sub>*</sub>*

*<sub>*<sub> 'displayLabelAsString': If enabled, will display the label over a pie chart instead of a percent. False by default. </sub>*</sub>*

*<sub>*<sub> 'initialSegment': If true, line graphs have a line touching the origin. True by default. </sub>*</sub>*

*<sub>*<sub> 'segmentThickness': How thick segments on a line graph are. 2 by default. </sub>*</sub>*

*<sub>*<sub> 'numXTicks', 'numYTicks': Number of numeric tick marks on the respective axises, if applicable. 10 by default. </sub>*</sub>*

*<sub>*<sub> 'xTickSize', 'yTickSize': The length of tick marks. 4 by default. </sub>*</sub>*

*<sub>*<sub> 'numXLabels', 'numYLabels': Number of numeric labels on the respective axises, if applicable. 10 by default. </sub>*</sub>*

*<sub>*<sub> 'xLabelSize', 'yLabelSize': The size of labels. 18 and 15 by default. </sub>*</sub>*

*<sub>*<sub> More settings which can be disabled or enabled (if applicable): enableSlices, enablePieKeys, enablePieLabels, enableBars, enableHBars, enablePoints, enableSegments, enableLines, enableHeaders, enableYTicks, enableXTicks, enableYLabels, enableXLabels. </sub>*</sub>*

*<sub>Example Usage (In this example I will create a colorful bar graph about eating delicious pies): </sub>*

```
.graph(
        [
            ['Apple Pie', 20, (box)=>{box.color(255,0,0)}], 
            ['Pumpkin Pie', 30, (box)=>{box.color(255,125,0)}], 
            ['BlueBerry Pie', 10, (box)=>{box.color(0,0,255)}]
        ], 
            'bar', {
            title: 'My Favorite Pies',
            xAxis: 'Type of Pie',
            yAxis: "Deliciousness Level",
            callback: (obj) => {
                obj.master.color(255,0,0,20);
                for (let i in obj.yLabels){
                    obj.yLabels[i].textColor(0,125,255);
                };
                for (let i in obj.xLabels){
                    obj.xLabels[i].textColor(0,125,125);
                };
            }
        })
```


<br/><br/>



Color Picker Properties:

- *Definition: A "color picker" element is a graphical tool for taking user input to create or select a color*.

```colorPicker(callback)```

*<sub> Creates an HTML color picker element. </sub>*

*<sub> Note: The size of the color picker will be set to be exactly the size of your box. </sub>*

*<sub> callback: A callback function that reports back the current color value. The first parameter will be an array of four values representing the rgba values of the selected color. Runs once for every unique color. </sub>*

<br/>

```colorPickerColor(r, g, b, [a])```

*<sub> Sets a default color for the color picker. It will also appear as this color. </sub>*

<br/><br/>



Range Slider Properties:

```slider(callback)```

*<sub> Creates an HTML range slider, with additional custom graphics if chosen. </sub>*

*<sub> The first parameter passed to your callback will necessarily be the value of the received input. Also, the height of the box will shrink to match the slider, but this can be adjusted in sliderSize(). </sub>*

*<sub> Note: If no graphical properties are modified outside of sliderColor(), then a default DOM element will be used, and it wil be more efficient. Otherwise a slider graphically simulated from scratch (but close to identical) will take its place, which takes slightly more memory. </sub>*

*<sub> - callback: A function to run after input is received and enter is pressed. Runs once for every unique value of the slider. </sub>*

<br/>

```sliderColor(r, g, b, [a], [custom])```

*<sub> Colors the thumb and the trail of a slider element using CSS's accent-color, or custom graphics. Default is the blue autostyle. </sub>*

*<sub> Note: This is the only styling method that by default won't utilize custom graphics. And in some cases, a bright color will cause a browser to use a dark track background.</sub>*

*<sub> - custom: If enabled it will force enable custom graphics. False by default. </sub>*

<br/>

```sliderSize(height, [thumbWidth], [thumbHeight], [trailHeight])```

*<sub> Sets the size of the various elements of the slider. </sub>*

*<sub> height: Sets the height of the track and trail (or just the track if the trail is specified below). </sub>*

*<sub> thumbWidth (optional): Sets the width of the thumb. </sub>*

*<sub> thumbHeight (optional): Sets the height of the thumb. If your thumb is still a circle, then this is not useful. </sub>*

*<sub> trailHeight (optional): Sets the height of the trail, distinct from the track. </sub>*


<br/>

*Slider: Custom Graphics Fine-Tuning:*

```sliderStatic()```

*<sub> Makes slider unusable outside of sliderValue(). </sub>*

<br/>

```sliderValue(val)```

*<sub> Remotely changes value of slider. </sub>*

<br/>

```sliderThumbColor(r, g, b, [a])``` and ```sliderTrailColor(...)``` and ```sliderTrackColor(...)```

*<sub> Colors the specific item. Default upon being called is black. </sub>*

<br/>

```sliderThumbOutline(r, g, b, [a])``` and ```sliderTrailOutline(...)``` and ```sliderTrackOutline(...)```

*<sub> Colors the specific item's outline. Default upon being called is black. </sub>*

<br/>

```sliderOutlineWeight(r, g, b, [a], [weight])```

*<sub> Sets the stroke weight for all three elements. They have to all be the same otherwise there is positioning problems. Make stroke transparent to hide it. </sub>*

<br/>

```sliderThumbCurves(curve, [curve2], [curve3], [curve4])``` and ```sliderTrailCurves(...)``` and ```sliderTrackCurves(...)```

*<sub> Modifies the curves of a specific item. Default is 15 for track and trail, and 100 for thumb. Uses absolute curves. </sub>*

<br/>

```sliderThumbImage(image)``` and ```sliderTrailImage(...)``` and ```sliderTrackImage(...)```

*<sub> Replaces solid color with an image, set to match the curves of the item. </sub>*

*<sub> Image: The path of the image. It will look something like './yourImage.png' </sub>*

<br/>

```sliderIntensity(num)```

 *<sub> How intensely the colors will shift black on hover and white on press. </sub>*

 *<sub> num: Default value is 25. </sub>*

 <br/>

 ```sliderHover(r, g, b, a, weight)```

 *<sub> How intensely the colors will shift to an arbitrary value on hover. Overrides sliderIntensity(). </sub>*

*<sub> Note: Higher number means less of a color. </sub>*

<br/>

  ```sliderPress(r, g, b, a, weight)```

 *<sub> How intensely the colors will shift to an arbitrary value on press. Overrides sliderIntensity(). </sub>*

 *<sub> Note: Higher number means less of a color. </sub>*

 <br/>

 ```sliderOverEffect(callback)``` and ```sliderOutEffect(...)``` and ```sliderPressEffect(...)``` and ```sliderReleaseEffect(...)```

*<sub> Adds an additional effect to the various mouse interactions. </sub>*

<br/><br/>


Input Properties:

```input(callback, [defaultText], [type], [enter], [alwaysReading])```

*<sub> Creates an HTML input element. </sub>*

*<sub> The first parameter passed to your callback will necessarily be the value of the received input. </sub>*

*<sub> - callback: A function to run after input is received and enter is pressed. </sub>*

*<sub> - defaultText (optional): Text inside of your input box. If left empty it will be empty. </sub>*

*<sub> - type (optional): 'text' by default. Can also be other input types, like 'number', 'password', 'search'. See more: https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input </sub>*

*<sub> - enter (optional): If true, fires callback when enter is pressed, otherwise, continuously. True by default. </sub>*

*<sub> - alwaysReading (optional): If true, will always read input. False by default. </sub>*

<br/>

```clearInput([text])```

*<sub> Removes text from the input/message or replaces it. </sub>*

<br/>

```modifyInput([type])```

*<sub> Changes the type of input, for example from 'password' to 'text' and vice versa. </sub>*

<br/>

```inputFont(size, [font])```

*<sub> Changes the font of an input ot message. </sub>*

*<sub> Size: Size in pixels. 12 by default. </sub>*

*<sub> font: Font family. 'sans-serif' by default, can also be 'serif' or 'monospace'. </sub>*

<br/>

```inputFillColor(r, g, b, [a])```

*<sub> Colors the background of an input or message. </sub>*

<br/>

```inputTextColor(r, g, b, [a])```

*<sub> Colors the text of an input or message. </sub>*

<br/>

```inputBorderColor(r, g, b, [a], [weight])```

*<sub> Colors the (unselected) border of an input or message. </sub>*

*<sub> weight: How thick the border is. Default is 0.1. </sub>*

<br/>

```inputOutlineColor(r, g, b, [a])```

*<sub> Colors the (selected) outline of an input or message. </sub>*

<br/>


Message Properties:

```message(callback, [defaultText], [alwaysReading])```

*<sub> Creates an HTML ``textArea`` element. </sub>*

*<sub> Note: You can use the input methods to style the message. </sub>*

*<sub> - callback: A function to run after input is received. </sub>*

*<sub> - defaultText (optional): Text inside of your message box. If left empty it will be empty. </sub>*

*<sub> - alwaysReading (optional): If true, will always read input. False by default. </sub>*

<br/>


Contact Properties:

```contact(callback, [alignment])```

*<sub> Creates a simple "Contact Us" form. </sub>*

*<sub> - callback: A function to run after the "submit" button is pressed, passes the name, email, and message as parameters. </sub>*

*<sub> - alignment (optional): Determines whether the inner labels are 'left' aligned, 'center' aligned, or 'right' aligned. Left by default. </sub>*

<br/>


Login Properties:

```login(callback, [errCallback], [minLen], [rounds], [size])```

*<sub> Creates a username/password login field. </sub>*

*<sub> Note: By default, the password output will be processed through ``deriveSecureKey`` salted with the value of the username, which performs 1000 rounds of PBKDF2 hashing in order to create a 128 character hexadecimal password. To disable this, set rounds to 0, and it will return the raw password instead (not recommended). </sub>*

*<sub> - callback: A function to run after the "sign in" button is pressed, passes the username and password as parameters. </sub>*

*<sub> - errCallback (optional): A function run continously, which returns a string value that will be used to create an error message under the username field. Its passed the username, the password, and the value of the login object, as parameters. This can also be used to add more logic and customization to the login field. </sub>*

*<sub> - minLen (optional): The minimum length a password can be. 12 by default, and it can't be lower than 8. </sub>*

*<sub> - rounds (optional): How many pbkdf2 rounds to perform on the password. 1000 by default. It is recommended for security to not change this. </sub>*

*<sub> - size (optional): How long the derived key will be. 128 by default. It is recommended for security to not change this. </sub>*

<br/><br/>



Image Properties:

```image(imgPath, w, h)```

*<sub> Loads an HTML image element over a box. </sub>*

*<sub> - imgPath: The path of an image. It will look something like './yourImage.png'. </sub>*

*<sub> - w (optional): The width of an image. If left undefined it will be equal to the width of the box. </sub>*

*<sub> - h (optional): The height of an image. If left undefined it will be equal to the height of the box. </sub>*

<br/>

```imageBorder(r, g, b, [a], [weight], [style])```

*<sub> Creates a border around an image. </sub>*

*<sub> - weight: How wide the border is. Default is 1. </sub>*

*<sub> - style: The style of border. For example: 'solid', 'dotted', and 'dashed'.  </sub>*

<br/>

```imageFilter(type, value)```

*<sub> Creates a filter over an image. </sub>*

*<sub> Note: You can only use one at a time using this method. To use multiple at a time, then use imageInjectCSS and use 'filter' for the key, and the value of your multiple filters, concatenated with just a space. </sub>*

*<sub> - type: Options: 'hue-rotate', 'invert', 'grayscale', 'saturate', 'sepia', 'opacity', 'brightness', 'contrast', 'blur'</sub>*

*<sub> - value: Typically represented in percentage, except in the case of 'hue-rotate' (degrees) and 'blur' (pixels).  </sub>*

<br/>


```imageColor(deg, [brightness])```

*<sub> Recolors images (including white images) using some fancy filtering. </sub>*

*<sub> Note: May override filter(). Also, color is an emergent property of two values here, so you can't use rgb colors, and may be limited to certain hues. </sub>*

*<sub> - deg: A number between 0 and 360. </sub>*

*<sub> - brightness: A number between 0 and 100. 50 by default.  </sub>*

<br/><br/>


Audio/Video Properties:

<br/>

```video(src, [autoplay], [active])``` or ```audio(src, [autoplay], [active])```

*<sub> Creates a video/audio element. </sub>*

*<sub> src: The path of your video/audio. </sub>*

*<sub> autoplay: Whether or not the video/audio plays automatically (which will also set volume to zero). Default: False on video, true on audio. </sub>*

*<sub> active: True by default. Can also be false or 'toggle'. </sub>*

<br/>

```mediaVolume(volume)```

*<sub> Adjust the volume of a video/audio element. </sub>*

*<sub> volume: A value between 0 and 1. 1 by default unless autoplay is enabled. </sub>*

<br/>

```mediaLoop()```

*<sub> Causes video/audio to play in loop. </sub>*

<br/>

```mediaFit()```

*<sub> For video: Resizes the box to the corrected dimensions of the video (since videos cannot be stretched and you'd otherwise likely have a gap).</sub>*

*<sub> For audio: Sets the height to be ~56% of the width of a box, as this is the dimension ratio of common video formats, and audio does not intrinsically have any dimensions.</sub>*

<br/>

```mediaPlayer([toolbarOffset])```

*<sub> Adds custom media player controls to the video/audio, allowing for timeline scrolling, a play/pause button, a next video button, and more. </sub>*

*<sub> toolbarOffset (optional): Offsets the horizontal position of the toolbar. Necessary for correcting weirldy formatted videos. </sub>*

<br/>

```mediaList(arr)```

*<sub> Adds a list of videos/audios to a video/audio player. This will automatically create a video/audio and video/audio player if its not been done so already. </sub>*

*<sub> arr: An array of video/audio sources. If you want to use a single video/audio it still needs to be enclosed in brackets. </sub>*

<br/>


Media Player Styling:

```mediaPlayerPrimaryColor(r, g, b)```

*<sub> Changes the "primary" color pallete, which includes the color of the range sliders and some of the colors in the skip buttons. </sub>*

<br/>

```mediaPlayerSecondaryColor(r, g, b)```

*<sub> Changes the "secondary" color pallete, which includes the color of the partially-transparent toolbar and some of the colors used in the skip buttons. </sub>*

<br/>

```mediaPlayerTertiaryColor(colorDegrees)```

*<sub> Changes the "tertiary" color pallete, which includes the color of the every image-based button. These images by default are white. </sub>*

*<sub> Note: Due to the nature of forcing images to be different colors, rgb values cannot be used here, and instead you must use some value between 1 and 360 degrees. If you use null/undefined, image colors will be disabled. </sub>*

<br/>

```mediaPlayerTextColor(r, g, b, [a])```

*<sub> Changes the color of the video/audio duration text. </sub>*

<br/>

```mediaPlayerTextOutline(r, g, b, [a], [weight])```

*<sub> Changes the outline color of the video/audio duration text. </sub>*

<br/><br/>


Div and Span properties:

- *Definition: A "div" and "span" element are HTML objects for containing content.*.

```div([text])```

*<sub> Creates div element. </sub>*

*<sub> Note: If no other element is declared, a div will automatically be created for an object, otherwise the parent element of the object will be the what you declare (such as rectangle, image, button, etc...). </sub>*

*<sub> text (optional): Some text to display inside the div. This helps visualize the div. </sub>*

<br/>

```span([text])```

*<sub> Creates span element. This will override a div element. </sub>*

*<sub> text (optional): Some text to display inside the span. This helps visualize the span. </sub>*

<br/>

```overflow(x, [y])```

*<sub> Defines overflow properties of div. </sub>*

*<sub> x/y: Can be 'visible', 'hidden', or 'scroll'. if y is undefined then x is applied to both x and y. </sub>*

<br/><br/>


QR Code properties:

- *Definition: A QR Code is a camera-readable code corresponding to some string*.

```qrcode(string)```

*<sub> Generates a qr code from a string. </sub>*

<br/>


Cryptocurrency properties:

```cryptoAccept(address, name, logo, [compact])```

*<sub> Creates a coin-themed "Crypto Accepted Here" button with receiving information. </sub>*

*<sub> Note: In order to accept cryptocurrency in exchange for goods/services, you must provide an address generated from a server, specifically for the customer you are serving. If it's just for good-faithed donations, then this is not necessary. </sub>*

*<sub> Note2: The dimensions are fixed but its resizable with an auto-scaling size. </sub>*

*<sub> address: The recipient address as a string. </sub>*

*<sub> name: The name of the cryptocurrency. By default, "MONERO". </sub>*

*<sub> logo: The file path to the image of the cryptocurrency. By default, ``./src/images/monero.png``. </sub>*

*<sub> compact (optional): If false, the receiving page expands to 3x the size for increased visibility, if true it all fits within the horizontal button. True by default. </sub>*


```moneroAccept(address, [compact])```

*<sub> Monero shorthand for cryptoAccept(). Creates a "Monero Accepted Here" button with receiving information. </sub>*

*<sub> address: The recipient address as a string. </sub>*

*<sub> compact (optional): If false, the receiving page expands to 3x the size for increased visibility, if true it all fits within the horizontal button. True by default. </sub>*



```moneroWallet()```

*<sub> Creates a monero web wallet with a simple user interface. </sub>*

*<sub> Note: In order to use this feature, you must copy the files from ``wallets/monero`` to ``public``, or it will not work. The reason being the files are enormous, almost 25 megabytes in size. And just because you want to build on monero does not mean you need to create a web wallet, as its not a good security practice for users to load their keys into every website. Also check out ``cryptoAccept()``.  </sub>*

*<sub> Note2: You can create your own web wallet from scratch using the ``Monero`` class. A functioning and complex user interface can be difficult to create though, as you must remember real funds are on the line, and you have to wait for the wallet to synchronize every time you test it. </sub>*

<br/>


Advanced Customization:

```objInject(callback)```

*<sub> Injects a callback function into a custom object (such as mediaPlayer, select, navbar, tab, graph, contact, and login). </sub>*

*<sub> - callback: Passed the value of the custom object. If you are unsure of what things to customize, console.log() it. Always use built-in functionality first though, as it's been optimized for the task. </sub>*

<br/>

```divInjectCSS(key, value)```

*<sub> Injects CSS settings into your div, to open up more creative possibilities. </sub>*

*<sub> - key: The CSS key or label. Must be a string, and properly formatted. </sub>*

*<sub> - value: The CSS value. Must be a string, and properly formatted. </sub>*

<br/>

```spanInjectCSS(key, value)```

*<sub> Injects CSS settings into your span, to open up more creative possibilities. </sub>*

*<sub> - key: The CSS key or label. Must be a string, and properly formatted. </sub>*

*<sub> - value: The CSS value. Must be a string, and properly formatted. </sub>*

<br/>

```boxInjectCSS(key, value)```

*<sub> Injects CSS settings into your (visible) box, to open up more creative possibilities. </sub>*

*<sub> - key: The CSS key or label. Must be a string, and properly formatted. </sub>*

*<sub> - value: The CSS value. Must be a string, and properly formatted. </sub>*

<br/>

```buttonInjectCSS(key, value)```

*<sub> Injects CSS settings into your (non-visible) button, to open up more creative possibilities. </sub>*

*<sub> - key: The CSS key or label. Must be a string, and properly formatted. </sub>*

*<sub> - value: The CSS value. Must be a string, and properly formatted. </sub>*

<br/>

```textInjectCSS(key, value)```

*<sub> Injects CSS settings into your text, to open up more creative possibilities. </sub>*

*<sub> - key: The CSS key or label. Must be a string, and properly formatted. </sub>*

*<sub> - value: The CSS value. Must be a string, and properly formatted. </sub>*

<br/>

```inputInjectCSS(key, value)```

*<sub> Injects CSS settings into your input, to open up more creative possibilities. </sub>*

*<sub> - key: The CSS key or label. Must be a string, and properly formatted. </sub>*

*<sub> - value: The CSS value. Must be a string, and properly formatted. </sub>*

<br/>

```imageInjectCSS(key, value)```

*<sub> Injects CSS settings into your image, to open up more creative possibilities. </sub>*

*<sub> - key: The CSS key or label. Must be a string, and properly formatted. </sub>*

*<sub> - value: The CSS value. Must be a string, and properly formatted. </sub>*


<br/>

```checkInjectCSS(key, value)```

*<sub> Injects CSS settings into your check box, to open up more creative possibilities. </sub>*

*<sub> - key: The CSS key or label. Must be a string, and properly formatted. </sub>*

*<sub> - value: The CSS value. Must be a string, and properly formatted. </sub>*


<br/>


<br/>

---







**User-Interaction:**

*<sub>Note: By default using some of the functions in this category will cause the draw loop to draw continuously, which takes considerably more graphical processing power. Any time there is motion, the page is being re-drawn.</sub>*

<br/>

```detect(callback, [mode], [active])```

*<sub> Detects cursor collision in some form. </sub>*

*<sub> Note: You can create a separate function for every different "mode" of collision, however if you call the same mode twice it will override it and disable it. Also, browsers may enforce a minimum button width. </sub>*

*<sub> Note2: If you are just trying to create a simple button, then consider using ``submit()`` instead. </sub>*

*<sub> - callback: A function to run after collision is detected. </sub>*

*<sub> - mode (optional but recommended): How the mouse interacts with the element. Options: </sub>*

*<sub>*<sub>  - "over": Mouse moves over the box, and a function is run once (default). </sub>*</sub>*

*<sub>*<sub>  - "out": Mouse leaves the box, and a function is run once. </sub>*</sub>*

*<sub>*<sub>  - "click": Mouse is pressed and released, and a function is run once. </sub>*</sub>*

*<sub>*<sub>  - "double": Mouse is clicked twice, and a function is run once. </sub>*</sub>*

*<sub>*<sub>  - "press": Mouse is pressed, and a function is run once. </sub>*</sub>*

*<sub>*<sub>  - "release": Mouse is released, and a function is run once. </sub>*</sub>*

*<sub>*<sub>  - "move": Mouse is moved, and a function is run continuously as you move. </sub>*</sub>*

*<sub> - active: true by default. Can also be false, or 'toggle'. </sub>*

<br/>

```follow([callback], [center], [active])```

*<sub> Follows cursor relative to starting point. </sub>*

*<sub> Meant to be used primarily in conjunction with detect(). Also, this will work in conjunction with containers and rotation. </sub>*

*<sub> - callback: A function to run after collision is detected. </sub>*

*<sub> - center: If set to true it will completely center the element. False by default. </sub>*

*<sub> - active: true by default. Can also be false, or 'toggle'. </sub>*

<br/>

```drag([callback1], [callback2], [center], [active])```

*<sub> A shorthand method that allows you to drag an object, using detect() and follow(). </sub>*

*<sub> Note: This will override detect() on 'press' and 'release', as well as override follow(). </sub>*

*<sub> - callback1 (optional): A function to run after the object is pressed. </sub>*

*<sub> - callback2 (optional): A function to run after the object is released. </sub>*

*<sub> - center (optional): If set to true it will completely center the element. False by default. </sub>*

*<sub> - active (optional): If set to false will no longer drag. True by default. </sub>*

```stick([callback], [center])```

*<sub> A shorthand method that allows you to click an object and have it follow you, using detect() and follow(). </sub>*

*<sub> Note: This will override detect() on 'press', 'over', 'out', as well as override follow(). </sub>*

*<sub> - callback (optional): A function to run after the object is pressed. </sub>*

*<sub> - center (optional): If set to true it will completely center the element. False by default. </sub>*

<br/>

```draggableIcon([callback], [size])```

*<sub> A shorthand method of drag() that will create a visual icon that will allow you to drag some parent object, but only inside of the icon. </sub>*

*<sub> - callback (optional): A function, which returns the newly created object. You can for example modify its color and outline. </sub>*

*<sub> - size (optional): A number, representing how big the icon is. 20 by default. </sub>*

<br/><br/>

---






**Physics:**

*<sub>Note: By default using some of the functions in this category will cause the draw loop to draw continuously, which takes considerably more graphical processing power. Any time there is motion, the page is being re-drawn.</sub>*

<br/>

```run(callback, [active/skip], [skip])```

*<sub> Runs a callback function constantly inside the draw loop. This can be used to incrementally modify values of the box. </sub>*

*<sub> Note: You can use multiple run statements and they won't override each other. </sub>*

*<sub> - callback: The function you want to run. Passes the box object as a parameter, so you can modify it directly. </sub>*

*<sub> - active: true by default. Can also be false, or 'toggle'. Update: You can also put skip here as a number and ignore active. </sub>*

*<sub> - skip: 1 by default. If increased will skip iterations. For example if you use 60 then the code will be executed around once per second. </sub>*

<br/>

```clearRun()```

*<sub> Deletes all run statements and prevents them from executing further. </sub>*

<br/>

```scale(amount, [minLimit], [maxLimit])```

*<sub> Scales the width and height of a box once. </sub>*

*<sub> Note: When working with containers you want to use scale() or grow() and not size(), because this will prevent your content from getting distorted. </sub>*

*<sub> - amount: The amount you will multiply the width and height values by. </sub>*

<br/> - minLimit (optional): The minimum size your box can be. Default is 20.

<br/> - maxLimit (optional): The maximum size your box can be. Default is 5000.

<br/>

```grow(amount, [minLimit], [maxLimit])```

*<sub> Scales the width and height of a box continuously. </sub>*

*<sub> Note: When working with containers you want to use scale() or grow() and not size(), because this will prevent your content from getting distorted. </sub>*

*<sub> - amount: The amount you will multiply the width and height values by. </sub>*

<br/> - minLimit (optional): The minimum size your box can be. Default is 20.

<br/> - maxLimit (optional): The maximum size your box can be. Default is 5000.

<br/>

```move(velocity, speed)```

*<sub> Gives a box direction and speed. </sub>*

*<sub> - velocity: The direction of movement, in degrees. Tip: 0 = up, 90 = right, 180 = down, 270 = left. </sub>*

*<sub> - speed: How fast the movement occurs. Tip: Using a speed faster than 30 may look choppy due to pixel skipping. </sub>*

<br/>

```accelerateSpeed(increment, [minLimit], [maxLimit])```

*<sub> Changes the speed of the motion of a box. </sub>*

*<sub> - increment: How much the speed will be incremented. </sub>*

*<sub> - minLimit (optional): Minimum acceptable speed. </sub>*

*<sub> - maxLimit (optional): Maximum acceptable speed. </sub>*

<br/>

```accelerateVelocity(increment, [minLimit], [maxLimit])```

*<sub> Changes the direction of the motion of a box. </sub>*

*<sub> - increment: How much the angle will be incremented. </sub>*

*<sub> - minLimit (optional): Minimum acceptable angle. </sub>*

*<sub> - maxLimit (optional): Maximum acceptable angle. </sub>*

<br/>

```rotate(degrees)```

*<sub> Rotates a box once. </sub>*

*<sub> Note: Boundary collision will be broken, but UI collision is fine. </sub>*

*<sub> Note2: Rotating containers will also rotate child elements, and rotations can be nested. </sub>*

*<sub> - degrees: The direction of rotation, in degrees. Tip: 0 = up, 90 = right, 180 = down, 270 = left. </sub>*


<br/>

```spin(speed)```

*<sub> Rotates a box continuously. </sub>*

*<sub> Note2: Rotating containers will also rotate child elements, and rotations can be nested. </sub>*

*<sub> - speed (optional): If alwaysRotating is enabled the box will rotate at this specified speed. 1 by default. </sub>*

<br/>


```boundaries(type, [rightType], [topType], [bottomType])```

*<sub> Creates four boundaries that can interact with moving objects. </sub>*

*<sub> - type: How boundaries are handled. Four types: "delete", "wrap", "stop", "bounce". "delete" by default. If this is the only parameter, then it applies to all four sides, otherwise it handles the left side and you can specify the other sides. </sub>*

<br/>

```boundarySize(x, y, w, h)```

*<sub> Relocates where the boundaries are located on boundaries(). It is the page dimensions by default. </sub>*

<br/>

```boundaryCollision(simple, [variance], [width])```

*<sub> Allows for the adjustment of three settings on boundaries(). </sub>*

*<sub> - simple: If enabled, collision is simple rectangle collision, otherwise it uses more accurate collision maps. False by default. </sub>*

*<sub> - variance (optional): A value that adjusts what angular degree an object can bounce of the boundaries when set to "bounce". 45 by default. </sub>*

*<sub> - width (optional): How wide the boundaries are. 10 by default. </sub>*


<br/>

```collision(collider, callback, [simple])```

*<sub> Checks for a collision between two objects once upon being called, for any kind of shape. </sub>*

*<sub> How it works: A variable number of small hitboxes (>500 by default) is laid out in a grid, fixed to it's parent shape and stretching as needed to prevent gaps, inside of the smallest but accurate possible bounding box. This is termed a collision map, and it can be edited easily. To make it more efficient by default, the inside of each collision map is hollowed out, and only the perimeter has any hitboxes. This design will only be faulty in the rare circumstance that you teleport a small shape inside of another shape, in which case, you can edit the collision map. </sub>*

*<sub> Note: All polygons listed under polygon() with every form of curves() has a preloaded collision map. Custom polygons outside of the defaults, and any shape with curves on only some of the corners, do not have default collision maps, and will default to its parent shape without curves, or to a rectangle. Collision maps will orient themselves properly in rotated containers. </sub>*

*<sub> - collider: The box you are checking for collision with. </sub>*

*<sub> - callback: A callback function that determines what to do after a collision is detected. </sub>*

*<sub> - simple (optional): If enabled it will disable all complex collison analysis and look for a simple rectangular collision. This will break collision with rotated rectangles, rounded shapes, and other polygons. The advantage of this is that in theory this should be up to 50-100x faster (not that this matters in most circumstances, because collison detection is a O(n^2) algorithm and the population of an object matters much more). False by default. </sub>*

<br/>

```watchCollisions(arr, callback, [simple])```

*<sub> Continually watched for collisions with the box from an array of boxes. </sub>*

*<sub> Note: if you call watchCollision() again it will allow you to change the array, callback, and state of simple, without causing a memory leak or any conflicts. </sub>*

*<sub> Also note: This will not check for collision between the colliders. To do that, run watchCollision in a loop. </sub>*

*<sub> - arr: An array of colliders. </sub>*

*<sub> - callback: A callback function that determines what to do after a collision is detected. The first parameter passed to it is the name of the collider. </sub>*

*<sub> - simple (optional): Same as in collision(). </sub>*

<br/>

```setHitBoxes(arr)```

*<sub> Creates or redefines a collision map for an object. </sub>*

*<sub> Note: The easiest way to create your own collision map is with showHitBoxes(), then copy the array automatically logged to the console. </sub>*

*<sub> - arr: An array, the first item being the number of hitboxes, the remaining items being the number of each hitbox as a number or string. </sub>*

<br/>

```showHitBoxes([limit], [greenOnly])```

*<sub> Allows you to view and edit the hitboxes of a collision map, to make it easy to create custom ones. </sub>*

*<sub> Note: This is just a tool to visually see and transcribe the location of hitboxes into a usable array. This can cause lagging due to the number of elements being drawn to the screen, and of course should be removed after you are done using it. </sub>*

*<sub> - limit (optional): How many hitboxes to draw (506 by default). If you plan to change this, you should take some things into consideration. 1) You will most likely have an uneven number of hitboxes so you will have to fine-tune the number. 2) Increasing it drastically can cause extreme lag while you view it. 3) Increasing it drastically will take forever to edit, and could also shrink your boxes so small it distorts its geometry when shrunk. 4) Decreasing it will make edges rougher and will cost marginally less computational power, and vice versa. </sub>*

*<sub> - greenOnly (optional): Disables edit mode, and only renders the hitbox of the shape, for use in visual simulations. </sub>*

<br/>


<br/>

---


**Organization:**

<br/>

```name(string)```

*<sub> Gives the box an intrinsic name, and adds it to a global collection of different named boxes. Can be accessed with ``within()`` or ``contain()``. </sub>*

*<sub> Note: You can't use the same name twice otherwise the new one will override the previous one. </sub>*

<br/>

```within(container, [autoFit], [orderedPositioning])```

*<sub> Becomes contained by another box. A counterpart to ``contain()``, achieving the same end-result. </sub>*

*<sub> - container: The new container of this box. It can also be a name of the box, as a string, set with ``name()``. </sub>*

*<sub> - autoFit (optional): Sets the size of the container to exactly the dimensions of all its inner elements. True by default. </sub>*

*<sub> - orderedPositioning (optional): If enabled, containers who define their position prior to their children will offset their children to their uncontainerized position. This is a legacy behavior that has been deprecated since it causes more confusion than it helps. It is false by default, and by being false, it does not matter what order container and child position is declared. 
</sub>*

<br/>

```contain(boxesArray, [autoFit], [orderedPositioning])```

*<sub> Contains an array of boxes, setting their positions, sizes, and rotations relative to the container (which is itself relative to the page). </sub>*

*<sub> - boxesArray: An array full of variables referencing instances of the Box class, or their names if set with ``name()``. </sub>*

*<sub> - autoFit (optional): Sets the size of the container to exactly the dimensions of all its inner elements. True by default. 
</sub>*

*<sub> - orderedPositioning (optional): If enabled, containers who define their position prior to their children will offset their children to their uncontainerized position. This is a legacy behavior that has been deprecated since it causes more confusion than it helps. It is false by default, and by being false, it does not matter what order container and child position is declared. 
</sub>*

<br/>

```redirect(path, [onClickOnly])```

*<sub> Redirects the user to a subdirectory. </sub>*

*<sub> Note: To see an example of how to create a custom subdirectory, go to ``README.md`` and under 'subdirectories' it will give you easy, step-by-step instructions on how to create a custom path with a separate ``main.js`` file. </sub>*

*<sub> - path: The path to a new subdirectory. If left blank it will take you to an example subdirectory. </sub>*

*<sub> - onClickOnly (optional): The redirect will only occur if the user clicks on the box. True by default. Note, this will override other detect() statements that rely on ``click``. If false then the user is redirected instantly. </sub>*

<br/>




#### Some Global Variables and Functions

<br/><br/>

There's a few useful global functions and variables you can access within main.js (note, these are not methods, but standalone functions and variables):

<br/>

```isMobile```

*<sub> A variable, whose value is true if the user's device is a mobile device, and false otherwise. </sub>*

<br/>

```frameCount```

*<sub> How many times the drawing loop has been executed. </sub>*

<br/>

```int()``` and ```flr()``` and ```ceil()```

*<sub> Shorthand functions for: Math.round(), Math.floor(), and Math.ceil(). </sub>*

<br/>

```mouseX``` and ```mouseY```

*<sub> The raw x and y coordinates of your mouse/cursor. </sub>*

<br/>

```mouseIsPressed```

*<sub> Tells you whether or not the mouse is currently pressed. </sub>*

<br/>

```keyIsDown(key)```

*<sub> Tells you whether or not a key is currently down. Can use a numeric keycode, or a string representing the name of the key. </sub>*

<br/>

```windowWidth```

*<sub> The width of your window. </sub>*

<br/>

```windowHeight```

*<sub> The height of your window. </sub>*

<br/>

```resizeWindow(width, height)```

*<sub> A function you can call that will increase or decrease the size of the window, and it will automatically place vertical and horizontal scrollbars as necessary so that you can navigate the page's increased size. </sub>*

*<sub> Note: These values are relative to windowWidth and windowHeight, and either increment or decrement windowWidth and windowHeight.  If you want to make your window an exact size, you can say ``resizeWindow(n - windowWidth, n - windowHeight)``. </sub>*

*<sub> width / height: The amount the window will increase or decrease by (not the total size). Negative values are allowed. </sub>*

<br/>

```backgroundColor```

*<sub> An object variable containing the properties r, g, b, and a. Moifying these values will change the color of the background. </sub>*

<br/>

```components```

*<sub> A variable that will allow you to load any additional files you would like to run. Note, any files you load will be run after main.js. </sub>*

*<sub> Note: As of now you must also list your component files in ``server.js`` if they are being sent to production.</sub>*

*<sub>Example:</sub>*

```components.push('./nameOfFile')```

<br/>

```clearHTML()```

*<sub> A function you can call to clear all HTML from a page (SpeedrunJS functions will still work, but you will have to create new objects after this function is run). If you wrap all your SpeedrunJS code in a function, you can use that combined with clearHTML() as a way of dynamically refreshing your page and all of it's logic.</sub>*

<br/>

```setStorage(name, item, [persistent])```

*<sub> A function you can call to store data into localStorage or sessionStorage. </sub>*

*<sub> name: A string, representing the key that you can use later to retrieve data. </sub>*

*<sub> item: The data you wish to store. It can be a string, a number, an object, an array, a set, or a map. JSON is not needed, this function will automatically encode your data in the correct format, and getStorage() will automatically decode it in the correct format too. </sub>*

*<sub> persistent (optional): If true uses localStorage, if false uses sessionStorage. True by default. </sub>*

<br/>

```getStorage(name, [persistent])```

*<sub> A function you can call to retrieve data stored in localStorage or sessionStorage. </sub>*

*<sub> name: A string, representing the key that you set using setStorage(). </sub>*

*<sub> persistent (optional): If true it will look through localStorage, if false it will look through sessionStorage, if undefined or not set it will look through both and prioritize sessionStorage. undefined by default. </sub>*

<br/>

```clearStorage([persistent])```

*<sub> A function you can call to completely clear localStorage and/or sessionStorage. </sub>*

*<sub> persistent (optional): If true it will delete everything in localStorage, if false it will delete everything in sessionStorage, if undefined or not set it will delete both. undefined by default. </sub>*

<br/>

```fetchData(callback, URL, [options])```

*<sub> An alternative to javascript's fetch() API-data-retrieval function which allows you to pass in a callback instead of having to manipulate .then() statements. It will try to convert the response to JSON, and if unsuccessful, to text.  </sub>*

*<sub> Note: You will have to refer to whatever site you are retrieving data from to know what options to use. A form of authentication may also be required in the options.  </sub>*

*<sub> callback: A callback function to run after the API data is retrieved. </sub>*

*<sub> URL: The URL of the site you are making a request to. </sub>*

*<sub> options (optional): A javascript object, containing any options required by the site. </sub>*

<br/>


```schedule(callback, date, [utc])```

*<sub> Schedule a callback function to be executed at a set time, either using local time or universal time. </sub>*

*<sub> callback: The callback function to execute. </sub>*

**<sub> date: The date, as an object. It looks like this: ``{year: null, month: null, day: null, hour: null, min: null, sec: null, ms: null}``. If a value is null, it will be substituted for the current time, so fill out everything you need. Alternatively, if you pass date a number, it will be interpreted as an exact numeric timestamp. </sub>*

*<sub> unix (optional): If true, will use universal time instead of local time. False by default. </sub>*

<br/>


```consistentLoop(callback, frequency)```

*<sub> Call a function to execute repeatedly, on a consistent time frame. </sub>*

*<sub> Note: This uses schedule() and recursion in order to achieve accurate time progression. It may be less efficient than using setInterval() by itself but it's less likely to drift out of sync with real time. </sub>*

*<sub> callback: The callback function to execute repeatedly. </sub>*

*<sub> frequency: How many times per second to execute the function. The maximum is 1000, the default is 60. There is no minimum, and you can use decimals. </sub>*

<br/>

```truncateNumber(num, zeros)```

*<sub>  Trims the end of the number, converting them to zeros. Useful for rounding. </sub>*

*<sub> num: The number to modify </sub>*

*<sub> zeros: How many zeros will be on the end of the number. </sub>*

<br/>

```deleteLine(text, identifier, [numLines], [allInstances])```

*<sub>  Deletes a line in a paragraph of text. </sub>*

*<sub> text: A paragraph of text, separated by line breaks. </sub>*

*<sub> identifier: A string that identifies which line should be deleted. Or a number fepresenting the index of the line to be deleted. </sub>*

*<sub> numLines (optional): How many lines to delete. By default, 1. </sub>*

*<sub> allInstances (optional): If false, only deletes the first instance of a found string. False by default. </sub>*

<br/>

```ran```

*<sub>  A string, which when passed into certain functions serves as a shorthand for getting a contextually relevant random value. Can be used on position(), tempPosition(), size(), curves(), color(), and every other function manipulating color or curves. </sub>*

*<sub> Example: ``color(ran)`` will return a completely random and opaque color. </sub>*

<br/>

```mid```

*<sub>  A string, which when passed into certain functions serves as a shorthand for getting a contextually relevant middle value. Can be used on position(), tempPosition(), size(), curves(), color(), and every other function manipulating color or curves. </sub>*

*<sub> Example: ``position(mid, mid)`` will center an element on the screen. </sub>*

<br/>

```u```

*<sub>  Shorthand for ``undefined``. This will speed up your coding if you need to pass ``undefined`` into the parameter of different methods multiple times. Example: ``size(u, 100)`` will change the height of your box, but leave the width unchanged as the value is undefined. </sub>*

<br/>

```numExists(variable)```

*<sub>  Return ``true`` if a variable is  a number (including zero) else returns false. If the variable is a string representing a number or a boolean, it will also return true. </sub>*

<br/><br/>



Server Tools:

```server(URL, onMessage, callback)```

*<sub> Creates a socket.io server and creates a callback function to execute after some message is received.  </sub>*

*<sub> Note: You can run this function multiple times to create multiple listening events. However, these will only run at startup. </sub>*

*<sub> Note2: This will only work if you launched your application from the terminal using server.js. How to use: Step 1) Download a Linux operating system or a linux compatible terminal, Step 2) Download Node.js and npm and upgrade to the latest stable version, Step 3) type ``npm i`` to download all needed dependencies, Step 4) type ``node server.js`` into terminal, Step 5) Visit the URL or port the site is running on, for example ``http://localhost:3000``, Step 6) use ``server('test', (data)=>{console.log(data)})`` and the server will tell you ``Hello World!``. If you are not on linux then look up a NodeJS tutorial that works for your operating system. </sub>*

*<sub> URL: The URL the site is running on, by default it is http://localhost:3000. </sub>*

*<sub> onMessage: What message the program is looking out for, as a string. By default is "data". </sub>*

*<sub> callback: What logic to execute after a message is received. </sub>*

<br/>


```server.send(onMessage, data, [hashpuzzle], [prefix])```

*<sub> Sends data to the server, at any point in time.  </sub>*

*<sub> onMessage: What message the server is looking out for, as a string. By default is "data". </sub>*

*<sub> data: What message or object the client is sending. </sub>*

*<sub> hashpuzzle (optional): A cryptographic spam-prevention mechanism included with the message, either as a string or as a boolean. "instantPow" by default, but if you want to change or disable it, it must be done on the server as well. Can be "instantpow", "fastpow", "medpow", "slowpow", or false. </sub>*

*<sub> prefix (optional): A prefix for the hashpuzzle, as a string. This should be something unique, like a domain/URL, and it must match on the server. ``'http://localhost:3000/'`` by default. </sub>*

<br/>



Cryptographic Tools:

*<sub> Note: These functions also works on the serverside (Node.JS). </sub>*

<br/>


```random(start, stop, [seed])```

*<sub>  A function that generates a cryptographically-secure random or pseudorandom decimal value between two numbers. If the seed is left blank, it will return a random number, but if a seed is given it will return a repeatable pseudorandom number. </sub>*

*<sub> Note: if you want to create an independent and separate source of pseudorandomness, you can call ``let newRan = new Randomness()`` and use the methods ``newRan.random()``, ``newRan.randomString()``, and ``newRan.reset()``. </sub>*

*<sub> start: The lowest value it can be, as a number. </sub>*

*<sub> stop: The highest value it can be, as a number. </sub>*

*<sub> seed (optional): A string, which will enable pseudorandom mode and feed the PRNG it's initial entropy. </sub>*

<br/>


```randomString(length, [mode], [seed], [fastMode])```

*<sub>  A function that creates a cryptographically-secure random or pseudorandom string, using random(). </sub>*

*<sub> length: Exactly how long you want your string to be. </sub>*

*<sub> mode: An array of strings/characters, representing the character set you want to construct your new string from. You can also input a few defaults (as strings), which include 'BASE64', 'BASE62', 'BASE56', 'BASE16', and 'BASE10'. 'BASE56' is default because it balances being high-entropy with being unambiguous. </sub>*

*<sub> seed (optional): A string, which will enable pseudorandom mode and feed the PRNG it's initial entropy. </sub>*

*<sub> fastMode (optional): If enabled, it will allow for speeds around three times as fast, but it sacrifices the guarantee of perfect statistical distribution, with variance around 5-10% for certain characters. False by default, for security reasons. </sub>*

<br/>


```encrypt(message, password)```

*<sub> Encrypts a message using AES256 and an arbitrary password. </sub>*

<br/>


```decrypt(message, password)```

*<sub> Decrypts an encrypted message using AES256 and an arbitrary password. </sub>*

*<sub> Note: Returns an empty string if the password is wrong. </sub>*

<br/>


```sha256(data)```

*<sub>  Returns a deterministic SHA256 hash after being fed a string. </sub>*

*<sub>  Note: Unless you absolutely must use SHA256, using SHA512 is exponentially more secure, which is always preferable given that security is an arms race.  </sub>*

<br/>

```sha512(data)```

*<sub>  Returns a deterministic SHA512 hash after being fed a string. </sub>*

<br/>

```pbkdf2(data, [rounds], [size])```

*<sub>  Returns a deterministic PBKDF2 hash after being fed a string. </sub>*

*<sub>  rounds (optional): How many rounds to perform. by default 1000. </sub>*

*<sub>  size (optional): How large of a hash to create by SHA standards. 512 by default. </sub>*

<br/>


```uniqueHash(data, type, callback, [rounds], [size])```

*<sub> Executes a callback with a unique random hash after being fed a string, using one of four different algorithms. </sub>*

*<sub> data: The string you wish to hash. </sub>*

*<sub> type: The algorithm: 'sha256', 'sha512', 'pbkdf2', and 'bcrypt' are your options. Bcrypt is default, and it is arguably the strongest hash for hashing passwords. </sub>*

*<sub> callback: The callback function to execute once the hash is complete. Passed the hash. </sub>*

*<sub> rounds (optional): How many rounds to perform of bcrypt or pbkdf2. The defaults are 10 and 1000, respectively (each weighted differently). it is generally recommended not to lower these too much. </sub>*

*<sub> size (optional): Affects how long the resulting hash will be (512 like sha512 by default). </sub>*

<br/>


```checkHash(hash, data, type, callback, [rounds], [size])```

*<sub> Checks a hash created by uniqueHash() </sub>*

*<sub> hash: The  hash you wish to check. </sub>*

*<sub> data: The string that was used to hash. </sub>*

*<sub> type: The algorithm: 'sha256', 'sha512', 'pbkdf2', and 'bcrypt' are your options. Bcrypt is default, and it is arguably the strongest hash for hashing passwords. </sub>*

*<sub> callback: The callback function to execute once the hash is complete. Passed a boolean. </sub>*

*<sub> rounds (optional): How many rounds to perform of bcrypt or pbkdf2. The defaults are 10, and 1000, respectively (each weighted differently). it is generally recommended not to lower these too much. </sub>*

*<sub> size (optional): Affects how long the resulting hash will be (512 like sha512 by default). </sub>*


<br/>





Web Worker and Asynchronous Cryptography Tools:

*<sub> Note: These functions only work on the clientside, and they will invoke the use of eval() once per call. </sub>*

<br/>


```injectWorker(callback1, callback2, [vars], [alwaysExecute])```

*<sub>  Injects a function (callback1) into a web worker file, executes it, and passes the value to another callback function (callback2). </sub>*

*<sub> callback1: The logic you are injecting into a web worker. NOTE: This does not automatically have access to outside variables, however, all of the cryptography utilities are loaded in. </sub>*

*<sub> callback2: The logic you want to execute once you get the data back from the web worker. Passed the parameter of the result of callback1. </sub>*

*<sub> vars (optional): An array of variables, that you have access to inside of callback1. </sub>*

*<sub> alwaysExecute (optional): If true, it will execute the functions on the main thread if web workers are unavailable. True by default. </sub>*

**<sub> Example Usage: ``injectWorker(()=>{return vars[0]}, (val)=>{console.log(val)}, ["Hello World!"])`` </sub>*

<br/>


```deriveSecureKey(password, callback, [rounds], [size])```

*<sub> Injects a pbkdf2() call into web workers, allowing you to asynchronously derive a key from a password without causing the webpage to freeze. </sub>*

*<sub> Recommended Usage: It is preferable to send this to a server rather than a raw password, that way 1) They do not have access to the user's original password, and 2) a hacker would have a harder time brute-forcing your password. You can also mix a hardcoded salt into the password to make the derivations unique to your domain (see randomString()). </sub>*

<br/>


```hashpuzzle(callback, [mode], [prefix], [timeout], [numPuzzles], [rounds], [targetLength], [worker])```

*<sub>  A function that generates a cryptographic hashpuzzle, for the purpose of preventing spammers from spamming a server. </sub>*

*<sub>  callback: A function to execute after the hashpuzzle is solved. </sub>*

*<sub>  mode (optional): The form of hashpuzzle, as a string. There are 4 combination modes, and 3 pure modes. </sub>*

*<sub>*<sub>  'fastpow' (default): Performs a Bcrypt and PBKDF2 hashpuzzle. Takes around 10 seconds to complete on desktop.  </sub>*</sub>*

*<sub>*<sub>  'instantpow': Performs a Bcrypt and PBKDF2 hashpuzzle, but with smaller targets. Takes around 2 seconds to complete on desktop.  </sub>*</sub>*

*<sub>*<sub>  'medpow': Equivalent to doing fastpow twice, takes around 20 seconds on desktop.  </sub>*</sub>*

*<sub>*<sub>  'slowpow': Equivalent to doing fastpow three times, takes around 30 seconds on desktop.  </sub>*</sub>*

*<sub>*<sub>  'pbkdf2': Performs a PBKDF2 hashpuzzle.  </sub>*</sub>*

*<sub>*<sub>  'bcrypt': Performs a Bcrypt hashpuzzle.  </sub>*</sub>*

*<sub>  prefix (optional): A necessary parameter, as a string, defaulting as the name of your domain. It can be anything, but it ought to be unique and particular to your website. Without this, someone could solve hashpuzzles on one website and use them on another website, defeating the purpose of them. **Note (Mentioning this to relieve potential worry)**: On your own website, when verifying a hashpuzzle using the built-in method, your server would automatically store the proof-of-works in a small database and make sure each one is unique. **However, you do need to make sure to give the server the correct prefix**. </sub>*

*<sub>  timeout (optional): When a proof-of-work attempt times-out (gives up). 90 seconds by default. </sub>*

*<sub>  numPuzzles (optional, 'bcrypt'/'pbkdf2' only): How many of a puzzle to run. If you want to make a hashpuzzle more difficult, this is the parameter you should change the most. </sub>*

*<sub>  rounds (optional, 'bcrypt'/'pbkdf2' only): How many rounds to perform. Don't set this too high or it can cause website slowness. </sub>*

*<sub>  targetLength (optional, 'bcrypt'/'pbkdf2' only): How many of a character in a row the algorithm is looking for. Don't set this too high or a hashpuzzle will run indefinitely. </sub>*

*<sub>  worker (optional): If true it will attempt to use web workers. True by default. </sub>*

<br/>


Cryptocurrency Tools (serverside and clientside)

***<sub> Notice 1: Do not load or send more money than you are willing to lose. You are solely liable for loss of funds as a result of any reason, including misuse and/or malfunction of the software. Pass this warning to the end-user. </sub>***

***<sub> Notice 2: The creator of this library is not responsible for loss of funds as a result from the Monero (or other) network hard-forking or making any non-backwards-compatible update, and the underlying infrastructure (such as monero-javascript, the default nodes in the list, and the software in this library) failing to be updated, or the end-developer allowing the end-user to use broken software. The end-developer is responsible for warning users about software that is actively or potentially outdated and/or bugged. </sub>***

***<sub> Notice 3: Cryptocurrencies are not legal in every part of the world, and if the usage of this software is not currently legal in your area, you must delete it from the ``./wallets`` directory and not use it. </sub>***

***<sub> By continuing and using the stated software, you agree to the terms and conditions listed in Notice 1-3. </sub>***

*<sub> Note: In order to use these features on the clientside, you must copy all of the files from ``./wallets/monero`` and paste them into ``./public`` or else they will not work. This includes replacing the empty ``monero.dist.js`` file. The reason they are not in the public folder to start with is they are massive files, and it's more than a developer would likely want to deal with if they were uninterested in it. Also, most monero-related applications does not need and should not use a web wallet (instead, you should use a qr code / address and let the user use their own wallet, see ``.cryptoAccept()``). </sub>*

<br/>


```Monero([node], [network], [language], [subaddressLimit])```

*<sub> Instantiates a new "Monero" class, which can be used to create a new monero wallet, based on monero-javascript. </sub>*

*<sub> node (optional): A specific node to connect to. If no node is selected, it will default to a random community node. </sub>*

*<sub> network (optional): Mainnet by default. </sub>*

*<sub> language (optional): English by default. </sub>*

*<sub> subaddressLimit (optional): How many subaddresses a wallet will generate. 10 by default. </sub>*

<br/>

```Monero.openWallet(seed, restoreHeight, seedOffset, callback, privateData, [name], [password])```

*<sub> Opens, restores, or creates a wallet. </sub>*

*<sub> seed: The mnemonic seed representing what wallet you want to open. If one is not provided, it will generate a new one and return the results in ``privateData``. If you lose your seed then your funds are gone forever. </sub>*

*<sub> restoreHeight: The restore height of the wallet. Necessary to have the correct restore height, too early and you will waste time syncing, too late and your wallet might be missing transactions and have a smaller balance. If no restoreHeight is provided then it will grab the latest block height. If you lost your restoreHeight then either guess the smallest possible value or use zero, and prepare to wait a long time for it to sync. </sub>*

*<sub> seedOffset: A password that will generate a new seed, to give your wallet extra security. If you use this, and then lose it, you will lose access to your funds. </sub>*

*<sub> callback: A callback function, which passes the wallet as the first parameter. ```Monero.sync`` and ``Monero.send`` must be used inside of this callback function. </sub>*

*<sub> privateData: Passes the private data of the wallet if a new wallet is created. This includes mnemonic seed, private key, view key, and the primary address. A second callback will be executed if you used a seedOffset, including all the new metadata (this would be the wallet information that you want to actually use). </sub>*

*<sub> name: Name of wallet. If provided (and not on clientside) it will create a save file. Leave blank on clientside. </sub>*

*<sub> password: Password of wallet. Leave blank on clientside. </sub>*

<br/>

```Monero.sync(wallet, [progressCallback], [metadataCallback], [receivedCallback], [refreshRate])```

*<sub> Synchronizes a wallet. Must be passed inside ``Monero.openWallet()``. </sub>*

*<sub> wallet: The value of the wallet, passed from ``Monero.openWallet()``. </sub>*

*<sub> progressCallback (optional): A callback function, which passes information about the progress of the synchronization. </sub>*

*<sub> metadataCallback (optional): A callback function, which passes information about the balance and the addresses of the wallet after syncing. </sub>*

*<sub> receivedCallback (optional): A callback function, which passes information about the transactions received, if and when a transaction is sent to the wallet. </sub>*

*<sub> refreshRate (optional): A number, representing how often a wallet looks for updates on transactions received. 5000, or 5 seconds, by default. </sub>*


<br/>

```Monero.send(wallet, recipient, amount, [txCallback])```

*<sub> Sends coins. Must be passed inside ``Monero.openWallet()``. </sub>*

*<sub> wallet: The value of the wallet, passed from ``Monero.openWallet()``. </sub>*

*<sub> recipient: The monero address or subaddress receiving the funds. Make sure it is correct or you will lose your funds! </sub>*

*<sub> amount: The monero to send, denominated in normal units, NOT atomic units. 1 = 1 Monero. If you send your entire balance, it will sweep the wallet (meaning, send everything). If you try to send 0, or too much, it will result in it logging an error to the console. Make sure you type in the right amount, or you will lose your funds! </sub>*

*<sub> txCallback (optional): A callback function, passes the value of the tx created. </sub>*


<br/>

```Monero.subaddress(wallet, callback)```

*<sub> Creates a new subaddress. Must be passed inside ``Monero.openWallet()``. </sub>*

*<sub> wallet: The value of the wallet, passed from ``Monero.openWallet()``. </sub>*

*<sub> callback: A callback function that passes the value of the subaddress. </sub>*

<br/>

```Monero.close(wallet, [save])```

*<sub> Closes a wallet or clears it from memory. Must be passed inside ``Monero.openWallet()``. </sub>*

*<sub> wallet: The value of the wallet, passed from ``Monero.openWallet()``. </sub>*

*<sub> save: Saves wallet, if applicable. False by default. </sub>*

<br/>


### Tips for larger projects:

<br/><br/>

#### How To Use Multiple Javascript Files

Because files are loaded dynamically, you *do not* want to include separate javascript files in index.html, at least if they are reliant on SpeedrunJS in any way. Instead, you want to use the *components* variable. Just do ```components.push('./nameOfFile')``` or ```components.concat(newArrayContainingTheNamesOfAllYourFiles)```. Note, any files you load will be run *after* ``main.js``. If you are using the server to create a production build, then you will also need to include your component files in server.js, where marked.

<br/>

#### How to Use Subdirectories, or Multiple Paths and HTML files

In ``./speedrunjs`` there is a folder called ``/exampleOfSubdirectory`` which gives you an example of how to create a new subdirectory. To create a new subdirectory from scratch:

1) Create a new folder in ``./speedrunjs``. Whatever you name it will be the name that goes into the URL later.

2) Copy and paste ``index.html`` and ``main.js`` into this new folder, and clear out ``main.js`` for the new code you plan to write.

3) In ``index.html`` add a period to two script declarations. For example, they will look like:

``<script>var path='../';</script>``
``<script src="../src/speedrun.js"></script>``

The double period tells the program to back out one directory. As opposed to your initial ``index.html`` file, which looks like this instead:

``<script>var path='./';</script>``
``<script src="./src/speedrun.js"></script>``

If you want to go deeper another directory, you'd just add an additional period to the two script declarations for each level of depth.

4) In order to redirect a user to a different subdirectory, use ``.redirect()``. 

Note: Components can safely be used in conjunction with subdirectories. Also, ``main.js`` cannot be renamed without going into ``speedrun.js``. Don't bother changing it, just add new files using ``components`` detailed above, and name the new ones whatever you want.

<br/>

#### How to Create a Static (No-Script) Version of Your Site

After writing the code for your site, you can run it and then inspect the DOM in your browser. Copy and paste the HTML into your HTML file, and it will create a static site for you. Note that you will lose animations, anything scripted, and potentially critical functionality, like taking user input. To clear static HTML content, just run the function ``clearHTML()`` at the beginning of your file.

#### How to Manage State In Your Application
You can use the functions setStorage() and getStorage() to store strings, objects, arrays, sets, and maps into localStorage or sessionStorage. You don't have to directly interface with JSON in any way, these functions do it for you.



