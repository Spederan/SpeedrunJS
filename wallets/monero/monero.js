/*
Usage Instructions:

Serverside: Until further notice, works on NodeJS v<18, or if using the flag "-no-experimental-fetch", for example:
  "node --no-experimental-fetch main.js". You can also run "npm test", which will do the same thing.

Clientside: Monero can be referenced as "Monero" and monerojs can be referenced as "monerojs" as long as this 
  file is included before you define it. This file itself will not be run on the clientside, it is a copy of the source file.

See Monero-Javascript documentation: https://moneroecosystem.org/monero-javascript/global.html

Example: See bottom of file.
*/
const monerojs = require("monero-javascript");
let serverside = true;
try {
  window.exists = true;
  serverside = false;
} catch (e) {};
class Monero {
  constructor(node, subaddressLimit = 10, network = "mainnet", language = "English"){
    if (serverside){
      this.nodes = ["http://xmr-node.cakewallet.com:18081", "http://node.sethforprivacy.com:18089", 
        "http://nodes.hashvault.pro:18081", "http://node.c3pool.com:18081", "http://node.community.rino.io:18081"]
    } else {
      this.nodes = ["https://node-xmr.encryp.ch:18089", "https://fallacy.fiatfaucet.com:18089",
      "https://community.organic-meatballs.duckdns.org:443"];
    };    
    if (node){
      this.node = node;
    } else {
      this.node = this.nodes[this.ran(this.nodes.length)];
    };
    this.network = network;
    this.language = language;
    this.subaddressLimit = subaddressLimit;
    this.rate = 1000000000000;
    this.named = false;
    this.readyToSend = false;
  };
  ran(max){
    return Math.floor(Math.random() * max);
  };
  removeArr(arr, value) {
    var index = arr.indexOf(value);
    if (index > -1) {
      arr.splice(index, 1);
    }
    return arr;
  };
  connect(callback = null, node = this.node){
    (async () => {
      try {
        let daemon = await monerojs.connectToDaemonRpc(node, "superuser", "abctesting123");
        let height = await daemon.getHeight();
        let feeEstimate = await daemon.getFeeEstimate();
        let txsInPool = await daemon.getTxPool(); 
        callback({height: height, feeEstimate: feeEstimate, txsInPool: txsInPool});
      } catch (error) {
        this.removeArr(this.nodes, this.node);
        this.node = this.nodes[this.ran(this.nodes.length)];
        this.connect();
      };
    })();
  };
  openWallet(seed = null, restoreHeight, seedOffset, callback = null, privateCallback = null, name, password = "supersecretpassword123", serverUsername = "superuser", serverPassword = "abctesting123"){
    if (!serverside){
      name = undefined;
    };
    if (name) this.named = true;
    //? Define logic in a function we will call shortly later
    const openWallet = (restoreHeight) => {

      //?Over-arching asynchronous function necessary to perform await calls (as these are implicit promises)
      (async () => {

          //? Try, do something else if fails
          try {
            let wallet = await monerojs.openWalletFull({
              path: name, 
              password: password,
              networkType: this.network,
              serverUri: this.node,
            });
            if (wallet && callback){
              callback(wallet);
            };
          } catch (error1) {

            //? Function that will create wallet that we will call shortly later (only executes if opening wallet failed)
            const createWallet = async (_seed = seed, _restoreHeight = restoreHeight) => {
              try {
                let wallet = await monerojs.createWalletFull({
                  path: name,
                  password: password,
                  networkType: this.network,
                  serverUri: this.node,
                  serverUsername: serverUsername,
                  serverPassword: serverPassword,
                  mnemonic: _seed,
                  seedOffset: seedOffset,
                  restoreHeight: _restoreHeight,
                });
                if (seedOffset){
                  let newSeed = await wallet.getMnemonic();
                  let address = await wallet.getAddress(0,0);
                  let privateKey = await wallet.getPrivateSpendKey();
                  let viewKey = await wallet.getPrivateViewKey();
                  if (privateCallback) privateCallback({seedOffset: seedOffset, seed: newSeed, privateKey: privateKey, viewKey: viewKey, address: address, restoreHeight: restoreHeight})
                };
                if (wallet && callback){
                  callback(wallet);
                };
              } catch (error2) {
                //? Log both errors if this fails
                if (error1) console.log(error1);
                if (error2) console.log(error2);
              };
            };

            //? Execute create wallet function, and create new seed if one was not provided (again, only if opening wallet failed)
            if (!seed){
              try {
                let walletKeys = await monerojs.createWalletKeys({networkType: this.network, language: this.language});
                let newSeed = await walletKeys.getMnemonic();
                let address = await walletKeys.getAddress(0,0);
                let privateKey = await walletKeys.getPrivateSpendKey();
                let viewKey = await walletKeys.getPrivateViewKey();
                createWallet(newSeed); //* Final step in process, path 2: create from randomly generated seed
                if (privateCallback) privateCallback({seedOffset: undefined, seed: newSeed, privateKey: privateKey, viewKey: viewKey, address: address, restoreHeight: restoreHeight})
              } catch (error) {
                //? Log any errors
                console.log(error);
              };
            } else {
              createWallet(); //* Final step in process, path 3: create from existing seed
            };
          };
      })();
    };

    //?Connect to daemon and get restore height if necessary, otherwise, use listed restoreHeight
    
    this.connect((values)=>{
      if (!restoreHeight && restoreHeight !== 0){
        restoreHeight = values.height;
      };
      openWallet(restoreHeight);
    });
  };
  close(wallet, save = false){
    (async () => {
      try {
        await wallet.close(save);
      } catch (error) {
        console.log(error);
      };
    })();
  };
  subaddress(wallet, callback){
    (async () => {
      try {
        let subaddress = (await wallet.createSubaddress()).state.address;
        if (callback) callback(subaddress);
      } catch (error) {
        console.log(error);
      };
    })();
  };
  sync(wallet, syncCallback = null, balanceCallback = null, receivedCallback = null, syncInterval = 5000){
    (async () => {
      const initSync = async () => {
        try {
          //? synchronize with progress notifications
          await wallet.sync(new class extends monerojs.MoneroWalletListener {
            onSyncProgress(height, startHeight, endHeight, percentDone, message) {
              if (syncCallback) syncCallback(
                {
                  height: height, 
                  startHeight: startHeight, 
                  endHeight: endHeight, 
                  percentDone, percentDone, 
                  message: message
                });
            }
          });
        } catch (error) {
          this.removeArr(this.nodes, this.node);
          this.node = this.nodes[this.ran(this.nodes.length)];
          initSync();
        };
      };
      initSync();
      try {
        //? synchronize in the background every 5 seconds
        await wallet.startSyncing(syncInterval);
      } catch (error) {
        console.log("Continual Syncing Error:", error);
      };
      try {
        const getBalance = async () => {
          let bal = await wallet.getBalance();
          let balance = parseInt(bal)/this.rate;
          let subaddresses = [];
          for (let i = 0; i < this.subaddressLimit; i++){
            subaddresses.push((await wallet.createSubaddress()).state.address);
          };
          let address = await wallet.getAddress(0,0);
          let metadata = {address: address, subaddress: subaddresses, balance: balance}
          if (balanceCallback) balanceCallback(metadata);
        };
        if (this.named){
          try {
            await wallet.save();
          } catch (error) {
            console.log("Failed to save wallet: ", error)
          };
        };
        try {
          //? receive notifications when funds are received, confirmed, and unlocked
        let fundsReceived = false;
        await wallet.addListener(new class extends monerojs.MoneroWalletListener {
          onOutputReceived(output) {
            let amount = output.getAmount();
            let txHash = output.getTx().getHash();
            let isConfirmed = output.getTx().isConfirmed();
            let isLocked = output.getTx().isLocked();
            if (amount) fundsReceived = true;
            if (receivedCallback) receivedCallback(
              {
                amount: amount, 
                txHash: txHash, 
                isConfirmed: isConfirmed, 
                isLocked, isLocked, 
                fundsReceived: fundsReceived
              });
          };
        });
        } catch (error) {
          console.log("Funds Receive Error:", error);
        };
        getBalance();
      } catch (error) {
        console.log(error);
      };
    })();
    this.readyToSend = true;
    
  };
  send(wallet, recipient, amount, callback = null){
    if (wallet && recipient && amount){
      let interval = setInterval(()=>{
        if (this.readyToSend){
          (async () => {
            try {
              let tx;
              let bal = await wallet.getBalance();
              let balance = parseInt(bal)/this.rate;
              if (amount.toLowerCase() === "max") amount = balance;
              if (amount <= 0){
                if (this.readyToSend){
                  console.log("Warning: No funds available to send.");
                  console.log("Failed to send coins.");
                };
                return;
              };
              if (amount > balance + 0.0001){
                console.log("Warning: Not enough funds.");
                console.log("Failed to send coins.");
                return;
              };
              if (amount >= balance - 0.0001){
                tx = await wallet.sweepUnlocked({
                  address: recipient,
                  relay: true
                });
              } else {
                tx = await wallet.createTx({
                  accountIndex: 0,
                  address: recipient,
                  amount: `${amount*this.rate}`, 
                  relay: true
                });
              };
              if (callback) callback(tx);
            } catch (error) {
              console.log(error);
            };
          })();
          clearInterval(interval);
        };
      }, 5000);
    } else {
      console.log("You are missing a wallet, a recipient, or an amount.")
    };
  };
};
try {
  window.Monero = Monero;
  window.monerojs = monerojs;
} catch (error) {};
module.exports = Monero;

//? Example of Usage

/*
const monero = new Monero();
let seed = null;
let restoreHeight = null;
let seedOffset = null;
let callback = (wallet) => {
const refresh = 5000;
monero.sync(wallet, 
    ((progress) => console.log(progress)), 
    ((metadata) => console.log(metadata)),
    ((received) => console.log(received)), 
    refresh);
};
monero.openWallet(seed, restoreHeight, seedOffset, callback, ((privateData) => console.log(privateData)));*/




