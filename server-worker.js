
const {parentPort, workerData} = require("worker_threads");
const Cryptography = require('./public/src/SybilShielder/cryptography');
const SybilShield = require('./public/src/SybilShielder/SybilShielder');
const Database = require('./Easy-DB/easy-db');

//? Logic to execute if the client is validated
const logic = (data) => {
    console.log("Hashpuzzle validated inside of worker thread: ", data);

    //? Send a message back to the server main thread
    let test = "Hello main thread!"
    parentPort.postMessage({test: test});
};

//? Validate the data the client sent, from the main thread
try {
    let shield = new SybilShield('instantpow', 'http://localhost:3000/'); //! Settings must match the clientside!
    shield.checkWork((valid)=>{
        if (valid){
            logic(workerData.data.message);
        } else {
            console.log("Hashpuzzle failed to validate inside of worker thread.");
        };
    }, workerData.data.hashpuzzle, true, true);
} catch (e) {};
