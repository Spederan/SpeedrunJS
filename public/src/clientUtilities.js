const createHTMLElement = (type = 'div', ref, inputType, defaultID)  => {
    let obj = document.createElement(type);
    if (inputType){
        for (let key in inputType){
            obj[key] = inputType[key];
        };
    };
    let id;
    if (defaultID){
        id = defaultID;
    } else {
        id = randomString(32, 'Base56');
    };
    obj.id = id;
    if (ref) ref.id = id;
    document.body.appendChild(obj);
    return document.getElementById(id);
};
let frameCount = 0;
const int = (val) => Math.round(val); 
const flr = (val) => Math.floor(val); 
const ceil = (val) => Math.ceil(val); 
let mouseIsPressed = 0;
let mouseX;
let mouseY;
document.body.onmousedown = function() { 
  ++mouseIsPressed;
};
document.body.onmouseup = function() {
  --mouseIsPressed;
};
const defineMouse = (xOffset = 0, yOffset = 0) => {
    onmousemove = function(e){mouseX = e.clientX + xOffset; mouseY = e.clientY + yOffset};
};
defineMouse();

let pressedKeyCodes = {};
let pressedKeys = {};
window.onkeyup = function(e) { pressedKeyCodes[e.keyCode] = false; pressedKeys[e.key] = false; }
window.onkeydown = function(e) { pressedKeyCodes[e.keyCode] = true; pressedKeys[e.key] = true;}
const keyIsDown = (type) => {
    switch(typeof type){
    case "number": 
        return pressedKeyCodes[type];
    case "string": 
        return pressedKeys[type];
    };
};
function textDimensions(pText, pStyle, pFontSize = 12) {
    let lDiv = document.createElement('div');
    document.body.appendChild(lDiv);
    if (pStyle != null) {
        lDiv.style = pStyle;
    };
    lDiv.style.fontSize = "" + pFontSize + "px";
    lDiv.style.position = "absolute";
    lDiv.style.left = -1000;
    lDiv.style.top = -1000;
    lDiv.textContent = pText;
    let lResult = {
        width: lDiv.clientWidth,
        height: lDiv.clientHeight
    };
    document.body.removeChild(lDiv);
    lDiv = null;
    return lResult;
};
function textWidth(pText, pStyle, pFontSize = 12){
    return textDimensions(pText, pStyle, pFontSize).width;
};
function hexToRGB(hex) {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
      r: parseInt(result[1], 16),
      g: parseInt(result[2], 16),
      b: parseInt(result[3], 16)
    } : null;
  };
function RGBToHex(r, g, b) {
    function componentToHex(c) {
        var hex = c.toString(16);
        return hex.length == 1 ? "0" + hex : hex;
    };
    return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
};
const u = undefined;
const numExists = (variable) => {
    if (typeof variable === 'string') variable+=0;
    if (typeof variable === 'number' && (variable || variable === 0)) return true;
    return false;
};
const mid = 'mid';
const ran = 'ran';
let speedrun_global = [];
let speedrun_global_boxes = {};
let speedrun_global_video_count = 0;
let speedrun_global_audio_count = 0;
const clearHTML = () => {
    document.body.replaceChildren();
};
const injectWorker = (callback, callback2, vars = [], alwaysExecute = true) => {
    if (typeof(Worker) !== "undefined") {
        w = new Worker("src/client-worker.js");
        w.onmessage = function(event){
            callback2(event.data);
          };
        w.postMessage([`(${callback})();`, vars]);
    } else {
        if (alwaysExecute){
            let data = callback();
            callback2(data);
        };
    };
};
const schedule = (callback, date, utc = false) => { 
    if (typeof date === 'object'){
        let futureDate;
        let currentDate = new Date();
        if (utc){
            if (date.year == null) date.year = currentDate.getFullYear();
            if (date.month == null) date.month = currentDate.getUTCMonth() + 1;
            if (date.day == null) date.day = currentDate.getUTCDate();
            if (date.hour == null) date.hour = currentDate.getUTCHours();
            if (date.min == null) date.min = currentDate.getUTCMinutes();
            if (date.sec == null) date.sec = currentDate.getUTCSeconds();
            if (date.ms == null) date.ms = currentDate.getUTCMilliseconds();
        } else {
            if (date.year == null) date.year = currentDate.getFullYear();
            if (date.month == null) date.month = currentDate.getMonth() + 1;
            if (date.day == null) date.day = currentDate.getDate();
            if (date.hour == null) date.hour = currentDate.getHours();
            if (date.min == null) date.min = currentDate.getMinutes();
            if (date.sec == null) date.sec = currentDate.getSeconds();
            if (date.ms == null) date.ms = currentDate.getMilliseconds();
        };
        futureDate = new Date(date.year, date.month-1, date.day, date.hour, date.min, date.sec, date.ms);
        date = futureDate.getTime();
    };
    if (callback){
        let interval = setInterval(()=>{
            if (Date.now() >= date){
                callback();
                clearInterval(interval);
            };
        }, 1);
    };   
};
const truncateNumber = (num, zeros = 0) => {
    let str = `${num}`;
    str = str.substring(0, str.length - zeros);
    while (zeros > 0){
        zeros--;
        str+= 0;
    };
    return +str;
};
const consistentLoop = (callback, speed = 60) => {
    let frameCount = 0;
    let rate = Math.floor(1000/speed);
    let repeat = true;
    let stop = () => {
        repeat = false;
    };
    const replay = (callback) => {
        const time = Date.now() + rate;
        schedule(()=>{
            callback(frameCount++, stop);
            if (repeat) replay(callback);
        }, time);
    };
    replay(callback);
};
function isMap(o) {
    try {
        Map.prototype.has.call(o);
        return true;
    } catch(e) {
        return false;
    };
};
function isSet(o) {
    try {
        Set.prototype.has.call(o);
        return true;
    } catch(e) {
        return false;
    };
};
const setStorage = (name, obj, persistent = true) => {
    let item = obj;
    switch(true){
        case (isMap(obj)):
            item = 'map: '+JSON.stringify([...obj]);
        break;
        case (isSet(obj)):
            item = 'set: '+JSON.stringify([...obj]);
        break;
        case (typeof obj === "object"):
            item = JSON.stringify(obj)
        break;
    };
    switch(persistent){
        case true:
            localStorage.setItem(name, item);
        break;
        case false:
            sessionStorage.setItem(name, item);
        break;
    };
};

const getStorage = (name, persistent = undefined) => {
    let attempt1 = undefined;
    let attempt2 = undefined;
    let item = undefined;
    const tryLocal = () => {
        try {
            attempt1 = localStorage.getItem(name);
        if (attempt1){
            item = attempt1;
        };
        } catch (e) {}
    };
    const trySession = () => {
        try {
            attempt2 = sessionStorage.getItem(name);
        if (attempt2){
            item = attempt2;
        };
        } catch (e) {}
    };
    switch(persistent){
        default: tryLocal(); trySession(); break;
        case true: tryLocal(); break;
        case false: trySession(); break;
    };
    let type;
    if (!item) return undefined;
    let prefix = item.substring(0,5);
    if (prefix === 'map: '){
        type = 'map';
        item = item.substring(5);
    };
    if (prefix === 'set: '){
        type = 'set';
        item = item.substring(5);
    };
    let entry;
    switch(type){
        default:
            try {
                entry = JSON.parse(item);
                if (typeof entry === "object"){
                    item = entry;
                };
            } catch (e) {};
        break;
        case 'map':
            try {
                entry = new Map(JSON.parse(item));
                if (typeof entry === "object"){
                    item = entry;
                };
            } catch (e) {};
        break;
        case 'set':
            try {
                entry = new Set(JSON.parse(item));
                if (typeof entry === "object"){
                    item = entry;
                };
            } catch (e) {};
        break;
    };
    return item;
};
const clearStorage = (persistent) => {
    switch(persistent){
        default: localStorage.clear(); sessionStorage.clear(); break;
        case true: localStorage.clear(); break;
        case false: sessionStorage.clear(); break;
    };
};
const fetchData = (callback = null, url = '', options = {}) => {
    try {
        fetch(url, options)
        .then((res) => { 
            try {
                return res.json();
            } catch (e) {
                return res.text()
            };
        })
        .then((data) => {if (callback) callback(data)})
    } catch (error) {
        console.log(error)
    };
};
let filter_speedrun_global = false;
let win = window,
    doc = document,
    docElem = doc.documentElement,
    body = doc.getElementsByTagName('body')[0],
    windowWidth = win.innerWidth || docElem.clientWidth || body.clientWidth,
    windowHeight = win.innerHeight|| docElem.clientHeight|| body.clientHeight;
const pageSize = {
    x: 0,
    y: 0,
    w: windowWidth, 
    h: windowHeight,
    wOffset: 0,
    hOffset: 0,
    relativeLocation: true,
};
const canvasPrevious = {
    x: 0,
    y: 0,
    w: windowWidth, 
    h: windowHeight,
    wOffset: 0,
    hOffset: 0,
    relativeLocation: true,
};
const resizeWindow = (width = 0, height = 0) => {
    pageSize.wOffset = -(width);
    pageSize.hOffset = -(height);
    resizeWindow.state = true;
    resizeWindow.w = width;
    resizeWindow.h = height;
};
const measureDistance = (ax, ay, bx, by) => {
    let y = bx - ax;
    let x = by - ay;
    return Math.sqrt(x * x + y * y);
}; 
const measureAngle = (ax, ay, bx, by, modX = 0, modY = 0) => {
    return Math.atan2(by + modY - ay, bx + modX - ax) * 180 / Math.PI + 90;
};
function angle(p1x, p1y, p2x, p2y) {
    let angle = Math.atan2(p2y - p1y, p2x - p1x) * 180 / Math.PI;
    while (angle < 0){
       angle+= 360;
    };
    angle = angle % 360;
    return angle;
};
const degToRad = (degrees) => {
    return degrees * (Math.PI / 180);
};
const calculateVelocityFromDegrees = (a) => {
    while(a < 0){
      a+= 360;
    };
    a = a % 360;
    let A = degToRad(a-90);
    return {x: Math.cos(A), y: Math.sin(A), a: a};
};
const rotatePoint = (x, y, centerx, centery, degrees) => {
    var newX = (x - centerx) * Math.cos(degrees * Math.PI / 180) - (y - centery) * Math.sin(degrees * Math.PI / 180) + centerx;
    var newY = (x - centerx) * Math.sin(degrees * Math.PI / 180) + (y - centery) * Math.cos(degrees * Math.PI / 180) + centery;
    return [newX, newY];
};
const rectCollide = (x1, y1, w1, h1, x2, y2, w2, h2) => {
    return (x1 + w1 >= x2 && x1 <= x2 + w2 && y1 + h1 >= y2 && y1 <= y2 + h2);
};
const rectCollider = (box1, box2) => {
    return rectCollide(box1.x, box1.y, box1.w, box1.h, box2.x, box2.y, box2.w, box2.h);
};

const scaleValue = (num, constant, size) => {
    let factor = num / constant;
    return factor * size;
};
const deleteLine = (text, identifier = 1, numLines = 1, allInstances = false, replace) => {
    let lines = text.split('\n');
    if (typeof identifier === "string"){
        for (let i in lines){
            if (lines[i].includes(identifier)){
                if (replace){
                    lines[i] = replace;
                } else {
                    lines.splice(lines.indexOf(lines[i]), numLines);
                };
                if (!allInstances) break;
            };
        };
    } else {
        if (typeof identifier === "number"){
            if (replace){
                lines[identifier] = replace;
            } else {
                lines.splice(identifier, numLines);
            };
        };
    };
    return lines.join('\n');
};


const detectMobile = () => {
    if (window.orientation > -1 ||
        navigator.userAgent.match(/Android/i) ||
        navigator.userAgent.match(/webOS/i) ||
        navigator.userAgent.match(/iPhone/i) ||
        navigator.userAgent.match(/iPad/i) ||
        navigator.userAgent.match(/iPod/i) ||
        navigator.userAgent.match(/BlackBerry/i) ||
        navigator.userAgent.match(/Windows Phone/i)) {
        return true;
    } else {
        return false;
    };
};
let isMobile = detectMobile();

const clientFunc = function(){
    let funcs = clientFunc.primaryFuncs.concat(clientFunc.secondaryFuncs, clientFunc.tertiaryFuncs);
    for (let i in funcs){
        this[funcs[i]] = () => {};
    };
};
let _markAsNew = false;
let defaultBoundaries = {windowWidth: windowWidth, windowHeight: windowHeight, windowStartX: 0, windowStartY: 0};
clientFunc.primaryFuncs = ['div', 'rect', 'image', 'text', 'button'];
clientFunc.secondaryFuncs = ['slider', 'input', 'message', 'login', 'contact', 'graph', 'select', 'mediaPlayer', 
    'navbar','tab', 'radio', 'check', 'colorPicker', 'cryptoAccept', 'moneroWallet'];
clientFunc.tertiaryFuncs = ['polygon', 'hitBoxes', 'boundaries'];
clientFunc();