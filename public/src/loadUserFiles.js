
components = components.concat(necessaryComponents);
function load(i) {
    if (i < components.length) {
        loadScript(components[i], function () {
            load(++i);
        });
    };
};
load(0);
