/*Note: This slider is not operable by itself, it must be used in conjunction with the box class. 

See documentation for more information.*/

class Slider  {
    constructor(min = 0, max = 1, defaultValue = 0, step = 0, parent){
        this.min = min;
        this.max = max;
        this.defaultValue = defaultValue;
        this.step = step; 
        this.valueState = defaultValue;
        this.master = box()
            .size(u, 15)
        this.track = box()
            .size(u, 15)
        this.trail = box()
            .size(u, 15)
        this.thumb = box()
            .size(u, 15)
        this.invis = box()
            .div()
            .size(u, 15)
        this.master.run(null, false);
        this.size(100, 8, 15, true);
        this.track.position(0, 5);
        this.trail.position(0, 5);
        this.thumb.position(0, 5);
        this.intensity = {
            r1: 25,
            g1: 25,
            b1: 25,
            a1: 0,
            weight1: 0,
            r2: -25,
            g2: -25,
            b2: -25,
            a2: 0,
            weight2: 0,
        };
        this.trackc = {};
        this.trailc = {};
        this.thumbc = {};
        this.tracko = {};
        this.trailo = {};
        this.thumbo = {};
        this.position(0,0,true);
        this.trackColor(240,240,240);
        this.trailColor(0,128,255);
        this.thumbColor(0,128,255);
        this.trackColor(240,240,240);
        this.trailOutline(175,175,175,0);
        this.thumbOutline(175,175,175,0);
        this.trackOutline(175,175,175,255);
        this.combinedStroke = 2;
        let prevX = 0;
        let prevY = 0;
        let prevW = 0;
        let prevH = 0;
        this.trail.run(()=>{
            if (this.trail.w !== prevW || this.trail.h !== prevH || 
                this.thumb.x !== prevX || this.thumb.y !== prevY){
                this.drawAgain();
                this.trail.size(this.thumb.x + this.thumb.w/2 - this.trail.x, this.trail.h);
                prevX = this.thumb.x;
                prevY = this.thumb.y;
                prevW = this.trail.w;
                prevH = this.trail.h;
            };
        });
        this.master.size(parent.trueW, parent.trueH);
        this.master.contain([this.track, this.trail, this.thumb, this.invis], false);
    };
    drawAgain(){
        this.master.drawAgain = true;
        this.track.drawAgain = true;
        this.thumb.drawAgain = true;
        this.trail.drawAgain = true;
    };
    position(x, y, init = false){
        this.x = x;
        this.y = y;
        if (init) this.thumb.x = this.defaultValue*(this.w - this.t); 
        this.master.position(x,y);
    };
    size(w = 100, h = 8, t = 15, init = false, trailH = h, thumbW = t){
        this.w = w;
        this.h = h;
        this.t = t;
        this.track.size(w, h);
        if (init) this.trail.size(this.defaultValue*w, trailH);
        this.trail.size(u, trailH);
        this.thumb
            .size(thumbW, t)
            .position(u, 5-this.t/2+this.h/2)
    };
    trackColor(r, g = r, b = r, a = 255, update = true){
        this.master.drawAgain = true;
        this.track.drawAgain = true;
        if (update){
            this.trackc.r = r;
            this.trackc.g = g;
            this.trackc.b = b;
            this.trackc.a = a;
        };
        this.track.color(r, g, b, a);
    };
    trailColor(r, g = r, b = r, a = 255, update = true){
        this.master.drawAgain = true;
        this.trail.drawAgain = true;
        if (update){
            this.trailc.r = r;
            this.trailc.g = g;
            this.trailc.b = b;
            this.trailc.a = a;
        };
        this.trail.color(r, g, b, a);
    };
    thumbColor(r, g = r, b = r, a = 255, update = true){
        this.master.drawAgain = true;
        this.thumb.drawAgain = true;
        if (update){
            this.thumbc.r = r;
            this.thumbc.g = g;
            this.thumbc.b = b;
            this.thumbc.a = a;
        };
        this.thumb.color(r, g, b, a);
    };
    trackOutline(r, g = r, b = r, a = 255, weight = 1, update = true){
        this.combinedStroke = weight*2;
        if (update){
            this.tracko.r = r;
            this.tracko.g = g;
            this.tracko.b = b;
            this.tracko.a = a;
        };
        this.track.outline(r, g, b, a, weight);
    };
    trailOutline(r, g = r, b = r, a = 255, weight = 1, update = true){
        this.combinedStroke = weight*2;
        if (update){
            this.trailo.r = r;
            this.trailo.g = g;
            this.trailo.b = b;
            this.trailo.a = a;
        };
        this.trail.outline(r, g, b, a, weight);
    };
    thumbOutline(r, g = r, b = r, a = 255, weight = 1, update = true){
        this.combinedStroke = weight*2;
        if (update){
            this.thumbo.r = r;
            this.thumbo.g = g;
            this.thumbo.b = b;
            this.thumbo.a = a;
        };
        this.thumb.outline(r, g, b, a, weight);
    };
    thumbShape(curve = 100, curve2 = curve, curve3 = curve, curve4 = curve){
        this.thumb.curves(curve, curve2, curve3, curve4, true);
    };
    trailShape(curve = 30, curve2 = curve, curve3 = curve, curve4 = curve){
        this.trail.curves(curve, curve2, curve3, curve4, true);
    };
    trackShape(curve = 30, curve2 = curve, curve3 = curve, curve4 = curve){
        this.track.curves(curve, curve2, curve3, curve4, true);
    };
    thumbImage(image = undefined){
        this.thumb.image(image);
    };
    trailImage(image = undefined){
        this.trail.image(image);
    };
    trackImage(image = undefined){
        this.track.image(image);
    };
    rotate(deg){
        this.master.rotate(deg)
    };
    z_Index(z){
        this.master.parentElement.style['z-index'] = `${z}`;
        this.track.parentElement.style['z-index'] = `${z+5}`;
        this.trail.parentElement.style['z-index'] = `${z+10}`;
        this.thumb.parentElement.style['z-index'] = `${z+15}`;
    };
    value(){
        return this.valueState;
    };
    updateValue(value){
        this.drawAgain();
        let offset = (0 - (value/(this.max-this.min)))*this.combinedStroke;
        this.thumb.x = this.track.x + ((value/(this.max-this.min)) - this.min)*(this.track.w-this.thumb.w)-offset;
        this.valueState = value;
    };
};

clientFunc.slider = (parent) => {
    //?Slider Field
    if (parent.sliderProperties?.state){
        let a = parent.sliderProperties;
        parent.changeObj(a, () => {
            a.slide?.remove();
            a.parentInit2 = true;
        });
        //?Protect against unloaded slider
        if (a.modified && !a.slide){
            a.slide = null;
            a.obj = null;
            a.parentInit = true;
        };
        parent.createObj(a, () => {
            parent.h = a.track.height;
            parent.trueValues();
            if (a.step <= 0) a.step = 0.000001;
            if (a.modified){
                a.slide = new Slider(a.minValue, a.maxValue, 
                    a.defaultValue, a.step, this);
                a.slide.master.create();
                a.slide.track.create();
                a.slide.trail.create();
                a.slide.thumb.create();
            };
            a.obj = createHTMLElement('input', a, {"type": "range", 
            "min": `${a.minValue}`, "max": `${a.maxValue}`, 
            "step": `${a.step}`, "value": `${a.defaultValue}`});
            //?Update and Use Value
            parent._clearRun();
            parent._run(()=>{
                let value = +a.obj.value;
                if (a.prevValue !== value){
                    if (a.modified) a.slide.updateValue(value);
                    if (a.callback) a.callback(value);
                    a.prevValue = value;
                };
            });
            if (a.modified){
                a.slide.z_Index(-9999999);
                a.obj.style['opacity'] = `${0}`;
                setTimeout(() => {
                    a.slide.invis.parentElement.appendChild(a.obj);
                    if (a.sliderStatic){
                        a.obj.remove();
                    };
                }, 100);
            };
        });
        //?Size
        if (a.modified) a.slide.size(parent.trueW, a.track.height, a.thumb.height, false, a.trail.height, a.thumb.width);
        a.obj.style['width'] = `${parent.trueW}px`;
        a.obj.style['height'] = `${a.track.height+2}px`;
        parent.size(parent.w, a.track.height+2);
        
        //?Positioning
        parent.positionLogic(a);
        if (a.modified && a.parentInit2 && 
            a.slide?.master?.parentElement){
            parent.parentElement.appendChild(a.slide.master.parentElement);
            a.parentInit2 = false;
        };
        a.obj.style['z-index'] = `${parent.zIndex+40}`;
        a.obj.style['accent-color'] = `rgba(${a.trail.r}, ${a.trail.g}, ${a.trail.b}, ${a.trail.a/255})`;
        if (a.modified){
            //?Set hover values
            let [b, c, d] = [a.slide.intensity, a.intensitySet, parent.intensity];
            b.r1 = c.r1;
            b.g1 = c.g1;
            b.b1 = c.b1;
            b.a1 = c.a1;
            b.weight1 = c.weight1;
            b.r2 = c.r2;
            b.g2 = c.g2;
            b.b2 = c.b2;
            b.a2 = c.a2;
            b.weight2 = c.weight2;
            //?Hovering
            a.obj.addEventListener("mouseover", ()=>{
                parent._();
                a.slide.drawAgain();
                d.r = b.r1;
                d.g = b.g1;
                d.b = b.b1;
                d.a = b.a1;
                d.weight = b.weight1;
                d.multiplier = 2;
                if (a.overProperties) a.overProperties();
            });
            a.obj.addEventListener("mouseout", ()=>{
                parent._();
                a.slide.drawAgain();
                d.r = 0;
                d.g = 0;
                d.b = 0;
                d.a = 0;
                d.weight = 0;
                d.multiplier = 1;
                if (a.outProperties) a.outProperties();
            });
            a.obj.addEventListener("mousedown", ()=>{
                parent._();
                a.slide.drawAgain();
                d.r = b.r2;
                d.g = b.g2;
                d.b = b.b2;
                d.a = b.a2;
                d.weight = b.weight2;
                d.multiplier = 1;
                if (a.pressProperties) a.pressProperties();
            });
            a.obj.addEventListener("mouseup", ()=>{
                parent._();
                a.slide.drawAgain();
                d.r = 0;
                d.g = 0;
                d.b = 0;
                d.a = 0;
                d.weight = 0;
                d.multiplier = 1;
                if (a.releaseProperties) a.releaseProperties();
            });
            //?Styling
            a.slide.trackOutline(
                a.track.outline.r - d.r, 
                a.track.outline.g - d.g, 
                a.track.outline.b - d.b, 
                a.track.outline.a - d.a);
            a.slide.trailOutline(
                a.trail.outline.r - d.r, 
                a.trail.outline.g - d.g, 
                a.trail.outline.b - d.b, 
                a.trail.outline.a - d.a);
            a.slide.thumbOutline(
                a.thumb.outline.r - d.r, 
                a.thumb.outline.g - d.g, 
                a.thumb.outline.b - d.b, 
                a.thumb.outline.a - d.a);
            a.slide.trackShape(a.track.curve, a.track.curve2, 
                a.track.curve3, a.track.curve4);
            a.slide.trailShape(a.trail.curve, a.trail.curve2, 
                a.trail.curve3, a.trail.curve4);
            a.slide.thumbShape(a.thumb.curve, a.thumb.curve2, 
                a.thumb.curve3, a.thumb.curve4);
            if (a.track.objState){
                a.slide.trackImage(a.slide.obj);
                a.slide.trackColor(0,0,0,0);
            } else {
                a.slide.trackColor(
                    a.track.r - d.r, 
                    a.track.g - d.g, 
                    a.track.b - d.b, 
                    a.track.a - d.a);
            };
            if (a.trail.objState){
                a.slide.trailImage(a.slide.obj);
                a.slide.trailColor(0,0,0,0);
            } else {
                a.slide.trailColor(
                    a.trail.r - d.r*d.multiplier, 
                    a.trail.g - d.g*d.multiplier, 
                    a.trail.b - d.b*d.multiplier, 
                    a.trail.a - d.a);
            };
            if (a.thumb.objState){
                a.slide.thumbImage(a.slide.obj);
                a.slide.thumbColor(0,0,0,0);
            } else {
                a.slide.thumbColor(
                    a.thumb.r - d.r*d.multiplier, 
                    a.thumb.g - d.g*d.multiplier, 
                    a.thumb.b - d.b*d.multiplier, 
                    a.thumb.a - d.a);
            };
        };
        if (parent.hideGraphics){
            if (a.modified){
                a.slide.trackColor(0,0,0,0);
                a.slide.trailColor(0,0,0,0);
                a.slide.thumbColor(0,0,0,0);
                a.slide.trackOutline(0,0,0,0);
                a.slide.trailOutline(0,0,0,0);
                a.slide.thumbOutline(0,0,0,0);
                if (a.thumb.objState){
                    a.thumb.imageProperties.obj.style['display'] = 'none';
                };
                if (a.track.objState){
                    a.track.imageProperties.obj.style['display'] = 'none';
                };
                if (a.trail.objState){
                    a.trail.imageProperties.obj.style['display'] = 'none';
                };
            } else {
                a.obj.style['display'] = 'none';
            };
        } else {
            if (a.modified){
                if (a.thumb.objState){
                    a.thumb.imageProperties.obj.style['display'] = 'initial';
                };
                if (a.track.objState){
                    a.track.imageProperties.obj.style['display'] = 'initial';
                };
                if (a.trail.objState){
                    a.trail.imageProperties.obj.style['display'] = 'initial';
                };
            } else {
                a.obj.style['display'] = 'initial';
            };
        };
        return;
    };
};


Box.prototype.slider = function (callback = null, minValue = 0, maxValue = 1, defaultValue = 0, step = 0) {
    let a = this.defineObject('slider');
    a.state = true;
    a.callback = callback;
    a.minValue = minValue;
    a.maxValue = maxValue;
    a.defaultValue = defaultValue;
    a.step = step;
    return this;
};
Box.prototype.sliderColor =  function (r = 0, g = r, b = r, a = 255, custom = false) {
    let c = this.defineObject('slider');
    [r, g, b, a] = this.randomColor(r, g, b, a);
    let [c1, c2] = [c.thumb, c.trail];
    c1.r = r;
    c1.g = g;
    c1.b = b;
    c1.a = a;
    c2.r = r;
    c2.g = g;
    c2.b = b;
    c2.a = a;
    if (custom) c.modified = true;
    return this;
};
Box.prototype.sliderSize = function(trackHeight = 8, thumbWidth = 15, thumbHeight = thumbWidth, trailHeight = trackHeight){
    let a = this.defineObject('slider');
    a.modified = true;
    a.thumb.width = thumbWidth;
    a.thumb.height = thumbHeight;
    a.track.height = trackHeight;
    a.trail.height = trailHeight;
    return this;
};
Box.prototype.sliderStatic = function(){
    let a = this.defineObject('slider');
    a.modified = true;
    a.sliderStatic = true;
    return this;
};
Box.prototype.sliderValue = function(value){
    let a = this.defineObject('slider');
    setTimeout(() => {
        if (a.slide) a.slide.updateValue(value);
        if (a.obj) a.obj.value = value;
    }, 1);
    return this;
};
Box.prototype.sliderOverEffect = function(callback){
    let a = this.defineObject('slider');
    a.modified = true;
    a.overProperties = callback;
    return this;
};
Box.prototype.sliderOutEffect = function(callback){
    let a = this.defineObject('slider');
    a.modified = true;
    a.outProperties = callback;
    return this;
};
Box.prototype.sliderPressEffect = function(callback){
    let a = this.defineObject('slider');
    a.modified = true;
    a.pressProperties = callback;
    return this;
};
Box.prototype.sliderReleaseEffect = function(callback){
    let a = this.defineObject('slider');
    a.modified = true;
    a.releaseProperties = callback;
    return this;
};
Box.prototype.sliderThumbColor = function(r = 0, g = r, b = r, a = 255){
    let c = this.defineObject('slider');
    [r, g, b, a] = this.randomColor(r, g, b, a);
    c.modified = true;
    let d = c.thumb;
    d.r = r;
    d.g = g;
    d.b = b;
    d.a = a;
    return this;
};
Box.prototype.sliderTrailColor = function(r = 0, g = r, b = r, a = 255){
    let c = this.defineObject('slider');
    [r, g, b, a] = this.randomColor(r, g, b, a);
    c.modified = true;
    let d = c.trail;
    d.r = r;
    d.g = g;
    d.b = b;
    d.a = a;
    return this;
};
Box.prototype.sliderTrackColor = function (r = 0, g = r, b = r, a = 255){
    let c = this.defineObject('slider');
    [r, g, b, a] = this.randomColor(r, g, b, a);
    c.modified = true;
    let d = c.track;
    d.r = r;
    d.g = g;
    d.b = b;
    d.a = a;
    return this;
};
Box.prototype.sliderOutlineWeight = function(weight = 1){
    let c = this.defineObject('slider');
    [r, g, b, a] = this.randomColor(r, g, b, a);
    c.modified = true;
    c.thumb.outline.weight = weight;
    c.track.outline.weight = weight;
    c.trail.outline.weight = weight;
    return this;
};
Box.prototype.sliderThumbOutline = function(r = 0, g = r, b = r, a = 255){
    let c = this.defineObject('slider');
    [r, g, b, a] = this.randomColor(r, g, b, a);
    c.modified = true;
    let d = c.thumb.outline;
    d.r = r;
    d.g = g;
    d.b = b;
    d.a = a;
    return this;
};
Box.prototype.sliderTrailOutline = function(r = 0, g = r, b = r, a = 255){
    let c = this.defineObject('slider');
    [r, g, b, a] = this.randomColor(r, g, b, a);
    c.modified = true;
    let d = c.trail.outline;
    d.r = r;
    d.g = g;
    d.b = b;
    d.a = a;
    return this;
};
Box.prototype.sliderTrackOutline = function(r = 0, g = r, b = r, a = 255){
    let c = this.defineObject('slider');
    [r, g, b, a] = this.randomColor(r, g, b, a);
    c.modified = true;
    let d = c.track.outline;
    d.r = r;
    d.g = g;
    d.b = b;
    d.a = a;
    return this;
};
Box.prototype.sliderThumbCurves = function(curve, curve2 = curve, curve3 = curve, curve4 = curve){
    let a = this.defineObject('slider');
    [curve, curve2, curve3, curve4] = this.randomCurves(curve, curve2, curve3, curve4, absolute);
    a.modified = true;
    let b = a.thumb;
    b.curve = curve;
    b.curve2 = curve2;
    b.curve3 = curve3;
    b.curve4 = curve4;
    return this;
};
Box.prototype.sliderTrailCurves = function(curve, curve2 = curve, curve3 = curve, curve4 = curve){
    let a = this.defineObject('slider');
    [curve, curve2, curve3, curve4] = this.randomCurves(curve, curve2, curve3, curve4, absolute);
    a.modified = true;
    let b = a.trail;
    b.curve = curve;
    b.curve2 = curve2;
    b.curve3 = curve3;
    b.curve4 = curve4;
    return this;
};
Box.prototype.sliderTrackCurves = function(curve, curve2 = curve, curve3 = curve, curve4 = curve){
    let a = this.defineObject('slider');
    [curve, curve2, curve3, curve4] = this.randomCurves(curve, curve2, curve3, curve4, absolute);
    a.modified = true;
    let b = a.track;
    b.curve = curve;
    b.curve2 = curve2;
    b.curve3 = curve3;
    b.curve4 = curve4;
    return this;
};
Box.prototype.sliderThumbImage = function(image, explicitActive = undefined){
    let a = this.defineObject('slider');
    a.modified = true;
    let b = a.thumb;
    let active = !b.objState;
    if (explicitActive !== undefined && (explicitActive === true || explicitActive === false)){
        active = explicitActive;
    };
    b.objState = active;
    b.obj = image;
    return this;
};
Box.prototype.sliderTrailImage = function(image, explicitActive = undefined){
    let a = this.defineObject('slider');
    a.modified = true;
    let b = a.trail;
    let active = !b.objState;
    if (explicitActive !== undefined && (explicitActive === true || explicitActive === false)){
        active = explicitActive;
    };
    b.objState = active;
    b.obj = image;
    return this;
};
Box.prototype.sliderTrackImage = function(image, explicitActive = undefined){
    let a = this.defineObject('slider');
    a.modified = true;
    let b = a.track;
    let active = !b.objState;
    if (explicitActive !== undefined && (explicitActive === true || explicitActive === false)){
        active = explicitActive;
    };
    b.objState = active;
    b.obj = image;
    return this;
};
Box.prototype.sliderIntensity = function(intensity = 25){
    let a = this.defineObject('slider');
    let b = a.intensitySet;
    a.modified = true;
    b.r1 = intensity;
    b.g1 = intensity;
    b.b1 = intensity;
    b.r2 = -intensity;
    b.g2 = -intensity;
    b.b2 = -intensity;
    return this;
};
Box.prototype.sliderHover = function(r = 0, g = r, b = g, a = 0, weight = 1){
    let c = this.defineObject('slider');
    [r, g, b, a] = this.randomColor(r, g, b, a);
    c.modified = true;
    let d = c.intensitySet;
    d.r1 = r;
    d.g1 = g;
    d.b1 = b;
    d.a1 = a;
    d.weight1 = weight;
    return this;
};
Box.prototype.sliderPress = function(r = 0, g = r, b = g, a = 0, weight = 1){
    let c = this.defineObject('slider');
    [r, g, b, a] = this.randomColor(r, g, b, a);
    c.modified = true;
    let d = c.intensitySet;
    d.r2 = r;
    d.g2 = g;
    d.b2 = b;
    d.a2 = a;
    d.weight2 = weight;
    return this;
};