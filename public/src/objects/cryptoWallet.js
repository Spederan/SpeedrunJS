class CryptoWallet{
    constructor(trueW, trueH, name, ticker, logo, walletWarning = true, walletExpired = false, dismissable = true, data, color){
        this.trueW = trueW;
        this.trueH = trueH;
        this.name = name;
        this.ticker = ticker;
        this.logo = logo;
        this.walletWarning = walletWarning;
        this.walletExpired = walletExpired;
        this.dismissable = dismissable;
        this.mnemonicSeed = "Loading...";
        this.seedOffset = '';
        this.seedOffsetPrev = '';
        this.restoreHeight = "Loading...";
        this.address = "Loading...";
        this.balance = 0;
        this.balanceStr = `${this.round(this.balance)}`;
        this.txDetails = {
            txid: "n/a",
            pid: "n/a",
            date: "n/a",
            height: "n/a",
            amount: "n/a",
            fee: "n/a",
            recipient: "n/a",
            txkey: "n/a",
        };
        this.transactionIndex = 0; //? Identifies which tx detail tab is open, if any.
        this.walletOpened = false; //? Identifies when wallet is opened
        this.walletCreated = false; //? Identifies when wallet is created
        this.submitAllMetadata = false; //? Sends relevant metadata downstream
        this.recipient_Address = null;
        this.recipient_Amount = null;
        this.loadedProgress = 0;
        this.atHome = true;
        //? Update Values
        
        let valuesCallback = () => {
            let arr = ['address', 'mnemonicSeed', 'restoreHeight', 'balance', 'txDetails'];
            for (let i in arr){
                if (data && (data[arr[i]] || numExists(data[arr[i]]))) this[arr[i]] = data[arr[i]];
            };  
            setTimeout(() => {
                this.loadedProgress = data.loadedProgress;
            }, 5000);
            if (this.loadedProgress < 1){
                this.page4?._progressBar?.show();
                this.page4?._blockNav?.show();
                this.page4?._blockCover?.show();
                this.page4?._nav?.tabSelect(0);
            };
        };
        setInterval(() => {
            valuesCallback();
        }, 500);
        
        //? Parent Element
        this.master = box()
            .size(this.trueW, this.trueH)
            .color(255,125,0)
            .outline(150, 150, 150, 150, 2)
        if (color){
            this.master.color(color.r, color.g, color.b, color.a);
        };
        this.#frontPage();
        setTimeout(() => {
            this.#createNewWallet();
            this.#openExistingWallet();
        }, 500);
        setTimeout(() => {
            this.#walletInternal();
            this.#walletReceive();
            this.#walletSend();
            this.#walletHome();
        }, 1000);
    };

    round(num){
        return Math.floor(num*100000)/100000;
    };

    #frontPage(){
        //? Front Page
        this.page1 = box()
            .div()
            .size(this.trueW, this.trueH)
            .within(this.master, false)
        //* Title
        this.page1._title = box()
            .size(this.trueW, this.trueH/16)
            .position(0, this.trueH/12)
            .text(`${this.name} Web Wallet`, 27)
            .textColor(255)
            .textFont('sans-serif')
            .within(this.page1, false)
        //* Subheader
        this.page1._subheader = box()
            .size(this.trueW, this.trueH/32)
            .under(this.page1._title)
            .text("100% free, open-source, and clientside", 12)
            .textColor(255)
            .textFont('sans-serif')
            .within(this.page1, false)
        //* Logo 
        this.page1._logo = box()
            .size(60, 60)
            .position(this.trueW/2, this.trueH/3.2, true, true)
            .image(this.logo)
            .within(this.page1, false)
        //* Create New Wallet
        this.page1._createNewWallet = box()
            .submit(()=>{
                this.walletCreated = true;
                this.page1.hide();
                this.page2.show();
            })
            .color(0,200,25)
            .text("Create New Wallet", 15)
            .textColor(255)
            .textFont('sans-serif')
            .textStyle(true)
            .outline(100)
            .size(this.trueW*0.85, this.trueH/17)
            .curves(15,15,15,15,true)
            .position(this.trueW/2, this.trueH/2, true, true)
            .within(this.page1, false)
        if (this.walletExpired || this.walletWarning) this.page1._createNewWallet.disable();

        //* Open Existing Wallet
        this.page1._openExistingWallet = box()
            .submit(()=>{
                this.walletOpened = true;
                this.page1.hide();
                this.page3.show();
            })
            .color(0,0,0,0)
            .outline(0,0,0,0)
            .curves(15,15,15,15,true)
            .size(this.trueW*0.85, this.trueH/17)
            .text("or Open Existing Wallet", 12)
            .textColor(255)
            .textFont('sans-serif')
            .position(this.trueW/2, this.trueH/2, true, true)
            .under(this.page1._createNewWallet)
            .within(this.page1, false)
        if (this.walletExpired || this.walletWarning) this.page1._openExistingWallet.disable();
        //* Warning Message
        if (this.walletExpired || this.walletWarning){
            let [m1, m2, m3, m4, m5, size, size2] = [,,,,,,]
            if (this.walletWarning){
                size = 11;
                size2 = 12;
                [m1, m2, m3, m4, m5] = [
                    'NOTICE:', 
                    `${this.name} could have a breaking upgrade at any time.`,
                    'To avoid loss of funds, always send a test amount first.',
                    'Never load more money than you can afford to lose.',
                    'I understand'
                ];
            };
            if (this.walletExpired){
                size = 15;
                size2 = 15;
                [m1, m2, m3, m4, m5] = [
                    'Warning:', 
                    'This wallet is using outdated software.',
                    'If you use it your funds will be lost.',
                    'Come back in a few days.',
                    'Dismiss'
                ];
            };
            let warning = randomString(32);
            this.page1._warning = box()
                .prototype(warning, (a)=>{
                    a
                    .size(this.trueW*0.85, 25)
                    .position(this.trueW/2, 0, true, true)
                    .textColor(255,0,0)
                    .textStyle(true)
                    .within(this.page1, false)
                })
                .text(m1, size2)
                [warning]()
                .under(this.page1._openExistingWallet, 20)
            this.page1._warning2 = box()
                [warning]()
                .under(this.page1._warning)
                .text(m2, size)
            this.page1._warning3 = box()
                [warning]()
                .under(this.page1._warning2)
                .text(m3, size)
            this.page1._warning4 = box()
                [warning]()
                .under(this.page1._warning3)
                .text(m4, size)
            if (this.dismissable){
                this.page1._dismiss = box()
                .submit(()=>{
                    this.walletExpired = false;
                    this.page1._createNewWallet.enable();
                    this.page1._openExistingWallet.enable();
                    this.page1._dismiss.hide();
                    this.page1._warning.hide();
                    this.page1._warning2.hide();
                    this.page1._warning3.hide();
                    this.page1._warning4.hide();
                })
                .size(80,21)
                .position(this.trueW/2, u, true, true)
                .under(this.page1._warning4, 20)
 
                .text(m5, size)
                .within(this.page1, false)
            };            
        };
    };


    #openExistingWallet(){
        //? Open Existing Wallet
        this.page3 = box()
        .div()
        .size(this.trueW, this.trueH)
        .within(this.master, false)
        .hide()
        //* Title
        this.page3._title = box()
            .size(this.trueW, 40)
            .position(0, 10)
            .text("Import Mnemonic Phrase", 20)
            .textColor(255)
            .textFont('sans-serif')
            .textStyle(true)
            .within(this.page3, false)
        //* Subheader
        this.page3._subheader = box()
            .size(this.trueW, this.trueH/16)
            .under(this.page2._title, 0)
            .text("Mnemonic phrase", 15)
            .textColor(255)
            .textFont('sans-serif')
            .textAlign('left', 20)
            .within(this.page3, false)
        //* Mnemonic
        this.page3._mnemonic = box()
            .message((seed)=>{
                if (seed){
                    this.mnemonicSeed = seed;
                };
            }, u, true)
            .clearInput(this.mnemonicSeed)
            .messageInjectCSS('resize', 'none')
            .messageInjectCSS('scroll', 'none')
            .messageInjectCSS('border',`${2}px solid rgb(${0}, ${0}, ${0}, ${1})`)
            .curves(15,15,15,15,true)
            .size(this.trueW*0.85, 70)
            .position(this.trueW/2, u, true, true)
            .under(this.page3._subheader)
            .within(this.page3, false)
        //* Restore Height Title
        this.page3._restoreTitle = box()
            .size(this.trueW, this.trueH/16)
            .text("Restore Height", 15)
            .textColor(255)
            .textFont('sans-serif')
            .textAlign('left', 20)
            .position(this.trueW/2, u, true, true)
            .under(this.page3._mnemonic, 10)
            .within(this.page3, false)
        //* Restore Height 
        this.page3._restoreHeight = box()
            .input((restoreHeight)=>{
                if (restoreHeight){
                    this.restoreHeight = restoreHeight;
                };
            }, u, u, false, true)
            .inputInjectCSS('border',`${2}px solid rgb(${0}, ${0}, ${0}, ${1})`)
            .curves(5,5,5,5,true)
            .size(this.trueW*0.85, 20)
            .position(this.trueW/2, u, true, true)
            .under(this.page3._restoreTitle)
            .within(this.page3, false)
        //* Seed Offset Title
        this.page3._seedOffsetTitle = box()
            .text('Seed Offset (Optional)', 15)
            .size(this.trueW, 40)
            .textColor(255)
            .position(this.trueW/2, u, true, true)
            .textFont('sans-serif')
            .textAlign('left', 20)
            .under(this.page3._restoreHeight)
            .within(this.page3, false)
        //* Seed Offset Input
        this.page3._seedOffsetInput = box()
            .input((offset)=>{
                if (offset){
                    this.seedOffset = offset;
                };
            }, "Encrypts mnemonic with a password", u, false, true)
            .inputInjectCSS('border',`${2}px solid rgb(${0}, ${0}, ${0}, ${1})`)
            .curves(5,5,5,5,true)
            .size(this.trueW*0.85, 20)
            .position(this.trueW/2, u, true, true)
            .under(this.page3._seedOffsetTitle, -5)
            .within(this.page3, false)
        //* Warning #1
        this.page3._warning1 = box()
            .size(this.trueW*0.8, 40)
            .text("⚠️ Do not load more money into a web wallet than you are willing to lose.", 10)
            .color(255,0,0,0)
            .curves(15,15,15,15,true)
            .detect(()=>{
                this.page3._warning1.color(255,0,0,255)
            }, 'over')
            .detect(()=>{
                this.page3._warning1.color(255,0,0,0)
            }, 'out')
            .textColor(255)
            .textStyle(true)
            .textAlign('left')
            .position(this.trueW/2, u, true, true)
            .under(this.page3._seedOffsetInput, 15)
            .within(this.page3, false)
        //* Warning #2
        this.page3._warning2 = box()
        .size(this.trueW*0.8, 40)
        .text(`⚠️ Make sure that ${this.name} hasen't upgraded too recently. Always send a test amount first.`, 10)
        .color(255,0,0,0)
        .curves(15,15,15,15,true)
        .detect(()=>{
            this.page3._warning2.color(255,0,0,255)
        }, 'over')
        .detect(()=>{
            this.page3._warning2.color(255,0,0,0)
        }, 'out')
        .textColor(255)
        .textStyle(true)
        .textAlign('left')
        .position(this.trueW/2, u, true, true)
        .under(this.page3._warning1, 0)
        .within(this.page3, false)
        //* Warning #3
        this.page3._warning3 = box()
            .size(this.trueW*0.8, 40)
            .text("⚠️ Double check the URL of this website, and beware of phishing, malware, and scams.", 10)
            .color(255,0,0,0)
            .detect(()=>{
                this.page3._warning3.color(255,0,0,255)
            }, 'over')
            .detect(()=>{
                this.page3._warning3.color(255,0,0,0)
            }, 'out')
            .curves(15,15,15,15,true)
            .textColor(255)
            .textStyle(true)
            .textAlign('left')
            .position(this.trueW/2, u, true, true)
            .under(this.page3._warning2, 0)
            .within(this.page3, false)
        //* Create
        this.page3._create = box()
            .submit(()=>{
                this.submitAllMetadata = true;
                this.page4.show();
                this.page3.hide();
            })
            .size(this.trueW*0.8, 40)
            .position(this.trueW/2, u, true, true)
            .color(0,200,25)
            .outline(0,0,0,255,2)
            .text("Import", 20)
            .textColor(255)
            .textFont('sans-serif')
            .textStyle(true)
            .curves(15,15,15,15,true)
            .under(this.page3._warning3, 10)
            .within(this.page3, false)
        //* Go back
        this.page3._create = box()
            .submit(()=>{
                this.atHome = true;
                this.walletOpened = false;
                this.page3.hide();
                this.page1.show();
            })
            .size(this.trueW*0.8, 40)
            .position(this.trueW/2, u, true, true)
            .color(0,200,25,0)
            .outline(0,0,0,0,2)
            .text("Go back", 15)
            .textColor(250)
            .textFont('sans-serif')
            .curves(15,15,15,15,true)
            .under(this.page3._create, 0)
            .within(this.page3, false)
    };

    #createNewWallet(){
        //? Create New Wallet
        this.page2 = box()
            .div()
            .size(this.trueW, this.trueH)
            .within(this.master, false)
            .hide()
        //* Title
        this.page2._title = box()
            .size(this.trueW, 40)
            .position(0, 10)
            .text("Save Your Mnemonic Phrase", 20)
            .textColor(255)
            .textFont('sans-serif')
            .textStyle(true)
            .within(this.page2, false)
        //* Subheader
        this.page2._subheader = box()
            .size(this.trueW, this.trueH/16)
            .under(this.page2._title, 0)
            .text("Mnemonic phrase", 15)
            .textColor(255)
            .textFont('sans-serif')
            .textAlign('left', 20)
            .within(this.page2, false)
        //* Mnemonic
        this.page2._mnemonic = box()
            .message()
            .clearInput(this.mnemonicSeed)
            .messageInjectCSS('font-weight', 'bold')
            .messageInjectCSS('resize', 'none')
            .messageInjectCSS('scroll', 'none')
            .messageInjectCSS('border',`${2}px solid rgb(${0}, ${0}, ${0}, ${1})`)
            .run(()=>{
                if (this.mnemonicSeed) this.page2._mnemonic.clearInput(this.mnemonicSeed);
            })
            .curves(15,15,15,15,true)
            .size(this.trueW*0.85, 70)
            .position(this.trueW/2, u, true, true)
            .under(this.page2._subheader)
            .within(this.page2, false)
        //* Restore Height Title
        this.page2._restoreTitle = box()
            .size(this.trueW, this.trueH/16)
            .text("Restore Height", 15)
            .textColor(255)
            .textFont('sans-serif')
            .textAlign('left', 20)
            .position(this.trueW/2, u, true, true)
            .under(this.page2._mnemonic, 10)
            .within(this.page2, false)
        //* Restore Height 
        this.page2._restoreHeight = box()
            .input()
            .run((a)=>{
                if (this.restoreHeight) a.clearInput(this.restoreHeight)
            })
            .inputInjectCSS('border',`${2}px solid rgb(${0}, ${0}, ${0}, ${1})`)
            .curves(5,5,5,5,true)
            .size(this.trueW*0.85, 20)
            .position(this.trueW/2, u, true, true)
            .under(this.page2._restoreTitle)
            .within(this.page2, false)
        //* Warning #1
        this.page2._warning1 = box()
            .size(this.trueW*0.8, 40)
            .text("⚠️ Do not lose your mnemonic phrase or seed offset, or else all funds will be lost.", 10)
            .color(255,0,0,0)
            .curves(15,15,15,15,true)
            .detect(()=>{
                this.page2._warning1.color(255,0,0,255)
            }, 'over')
            .detect(()=>{
                this.page2._warning1.color(255,0,0,0)
            }, 'out')
            .textColor(255)
            .textStyle(true)
            .textAlign('left')
            .position(this.trueW/2, u, true, true)
            .under(this.page2._restoreHeight, 15)
            .within(this.page2, false)
        //* Warning #2
        this.page2._warning2 = box()
        .size(this.trueW*0.8, 40)
        .text("⚠️ Do not lose your restore height, or else you will have to resync the entire blockchain.", 10)
        .color(255,0,0,0)
        .curves(15,15,15,15,true)
        .detect(()=>{
            this.page2._warning2.color(255,0,0,255)
        }, 'over')
        .detect(()=>{
            this.page2._warning2.color(255,0,0,0)
        }, 'out')
        .textColor(255)
        .textStyle(true)
        .textAlign('left')
        .position(this.trueW/2, u, true, true)
        .under(this.page2._warning1, 0)
        .within(this.page2, false)
        //* Warning #3
        this.page2._warning3 = box()
            .size(this.trueW*0.8, 40)
            .text("⚠️ Keep the mnemonic phrase secret at all times or else all funds can be stolen.", 10)
            .color(255,0,0,0)
            .detect(()=>{
                this.page2._warning3.color(255,0,0,255)
            }, 'over')
            .detect(()=>{
                this.page2._warning3.color(255,0,0,0)
            }, 'out')
            .curves(15,15,15,15,true)
            .textColor(255)
            .textStyle(true)
            .textAlign('left')
            .position(this.trueW/2, u, true, true)
            .under(this.page2._warning2, 0)
            .within(this.page2, false)
        //* Create
        this.page2._create = box()
            .submit(()=>{
                this.submitAllMetadata = true;
                this.page4.show();
                this.page2.hide();
            })
            .size(this.trueW*0.8, 40)
            .position(this.trueW/2, u, true, true)
            .color(0,200,25)
            .outline(0,0,0,255,2)
            .text("Create", 20)
            .textColor(255)
            .textFont('sans-serif')
            .textStyle(true)
            .curves(15,15,15,15,true)
            .under(this.page2._warning3, 10)
            .within(this.page2, false)
        //* Go back
        this.page2._create = box()
            .submit(()=>{
                this.atHome = true;
                this.walletCreated = false;
                this.page2.hide();
                this.page1.show();
            })
            .size(this.trueW*0.8, 40)
            .position(this.trueW/2, u, true, true)
            .color(0,200,25,0)
            .outline(0,0,0,0,2)
            .text("Go back", 15)
            .textColor(250)
            .textFont('sans-serif')
            .curves(15,15,15,15,true)
            .under(this.page2._create, 0)
            .within(this.page2, false)
    };


    #walletInternal(){
        //? Wallet Internal
        setInterval(()=>{
            this.balanceStr = `${this.round(this.balance)}`;
            this.tickerStr = this.ticker;
            let extra = 0;
            let offset = 0;
            if (this.loadedProgress < 1){
                extra = 10;
                offset = -2;
                this.balanceStr = 'Synchronizing...';
                this.tickerStr = `(${Math.floor(this.loadedProgress*100)}%)`
            };
            if (this.page4?._amount && this.page4._ticker){
                let size = 25;
                if (this.loadedProgress < 1) size = 15;
                this.page4._amount.text(this.balanceStr, size)
                this.page4._ticker.text(this.tickerStr, 12);
                this.page4._ticker.position(this.trueW/2 + textWidth(this.balanceStr, 'sans-serif', size)/2 + textWidth(this.tickerStr, 'sans-serif', 12)/2 + 5 + extra, 54 + offset, true, true)
            };
            if (this.page4?.availableBalanceAmount && this.page4.availableBalanceTicker){
                this.page4.availableBalanceAmount.text(this.balanceStr, 18);
                this.page4.availableBalanceTicker.position(60 + textWidth(this.tickerStr, 'sans-serif', 10) + 5, 120 - 14 + 2);
            };
        }, 500);
        
        this.page4 = box()
            .div()
            .size(this.trueW, this.trueH)
            .within(this.master, false)
            .hide()

        //* Navigation Tab
        this.page4._nav = box()
        .size(this.trueW+5, 70)
        .position(0, this.trueH - 70)
        .tab([["Home", ()=>{
            this.page4._slide1?.show();
            this.page4._slide2?.hide();
            this.page4._slide3?.hide();
            this.page4._slide4?.hide();
        }, true], ["Send", ()=>{
            this.page4._slide2?.show();
            this.page4._slide3?.hide();
            this.page4._slide1?.hide();
            this.page4._slide4?.hide();
        }], ["Receive", ()=>{
            this.page4._slide1?.hide();
            this.page4._slide2?.hide();
            this.page4._slide3?.show();
            this.page4._slide4?.hide();
        }], ["Close", ()=>{
            this.atHome = true;
            this.page1?.show(); 
            this.page4?.hide();
            this.mnemonicSeed = "";
            this.restoreHeight = "";
            this.balance = 0;
            this.balanceStr = '0';
        }]], false)
        .tabText(15)
        .tabOutline(0,0,0,255,2,-2)
        .within(this.page4, false)
        //* White Cover (Block Nav)
        this.page4._blockNav = box()
            .size(this.trueW+5, 70)
            .position(0, this.trueH - 70)
            .color(255,255,255,100)
            .run((a)=>{
                if (this.loadedProgress >= 1) a.hide();
            })
            .within(this.page4, false)
        //* White Background
        this.page4._whiteBlock = box()
            .size(this.trueW-3, this.trueH - 72 - 100)
            .position(0, 100)
            .color(255)
            .outline(0,0,0,255,2)
            .within(this.page4, false)
    };




    #walletReceive(){
        //? Third Slide: Wallet Receive
        this.page4._slide3 = box()
            .div()
            .size(this.trueW, this.trueH - 70)
            .within(this.page4, false)
        //* Send 
        this.page4._receiveText = box()
            .size(this.trueW*0.6, 100)
            .position(this.trueW/2, 50, true, true)
            .text("Receive", 20)
            .textFont('sans-serif')
            .textStyle(true)
            .within(this.page4._slide3, false)
        //* Qr Code
        this.page4._qrcode = box()
            .size(this.trueW*0.4)
            .position(this.trueW/2, this.trueH/2.25, true, true)
            //.qrcode(this.address)
            .run((a)=>{
                if (this.address && this.address !== 'Loading...') a.qrcode(this.address);
            })
            .within(this.page4._slide3, false)
        //* Address
        this.page4._userAddress = box()
            .size(this.trueW*0.8, 130)
            .position(this.trueW/2, this.trueH/2.25, true, true)
            .under(this.page4._qrcode, 5)
            .text(this.address, 13)
            .run((a)=>{
                a.text(this.address, 13);
            })
            .textColor(100)
            .textStyle(true)
            .textInjectCSS("overflow-wrap", "anywhere")
            .within(this.page4._slide3, false)
    };


    #walletSend(){
        //? Second Slide: Wallet Send
        this.page4._slide2 = box()
            .div()
            .size(this.trueW, this.trueH - 70)
            .within(this.page4, false)
        //* Send 
        this.page4._sendText = box()
            .size(this.trueW*0.6, 100)
            .position(this.trueW/2, 50, true, true)
            .text("Send", 20)
            .textFont('sans-serif')
            .textStyle(true)
            .within(this.page4._slide2, false)
        //* Available Balance
        this.page4._availableBalance = box()
            .size(200, 20)
            .position(15, 120)
            .text("Available Balance: ", 13)
            .textColor(100)
            .textFont('sans-serif')
            .textAlign('left')
            .within(this.page4._slide2, false)
        //* Available Balance Amount
        this.page4.availableBalanceAmount = box()
            .size(200, 50)
            .position(60, 120 - 14)
            .textFont('sans-serif')
            .textAlign('right')
            .textStyle(true)
            .within(this.page4._slide2, false)
        //* Available Balance Ticker
        this.page4.availableBalanceTicker = box()
            .size(200, 50)
            .text(`${this.ticker}`, 10)
            .textFont('sans-serif')
            .textAlign('right')
            .textStyle(true)
            .within(this.page4._slide2, false)
        //* Recipient Address Text
        this.page4.sendAddress = box()
            .size(200, 20)
            .position(15)
            .under(this.page4._availableBalance, 20)
            .text("Address: ", 13)
            .textColor(100)
            .textFont('sans-serif')
            .textAlign('left')
            .within(this.page4._slide2, false)
        //* Input field For Sending
        this.recipient_AddressChanged = false;
        this.page4.sendAddressInput = box()
            .size(250, 85)
            .curves(5,5,5,5,true)
            .position(23)
            .under(this.page4.sendAddress, 5)
            .message((val)=>{
                if (this.recipient_Address !== val){
                    this.recipient_AddressChanged = true;
                };
                this.recipient_Address = val;
            }, "Always double check the first and last few digits of an address. And always send a small test amount first. There's no refunds on the blockchain.")
            .messageInjectCSS('resize', 'none')
            .messageInjectCSS('font-weight', 'bold')
            .messageInjectCSS('border',`${2}px solid rgb(${0}, ${0}, ${0}, ${1})`)
            .run((a)=>{
                if (this.recipient_AddressChanged){
                    this.recipient_AddressChanged = false;
                    this.recipient_Address = this.recipient_Address.replace(/(\r\n|\n|\r)/gm, "");
                    this.recipient_Address = this.recipient_Address.replaceAll(' ', '');
                    a.clearInput(this.recipient_Address);
                };
            })
            .within(this.page4._slide2, false)
        //* Amount Text
        this.page4.sendAmount = box()
            .size(200, 20)
            .position(15)
            .under(this.page4.sendAddressInput, 20)
            .text("Amount: ", 13)
            .textColor(100)
            .textFont('sans-serif')
            .textAlign('left')
            .within(this.page4._slide2, false)
        //* Input field For Sending
        this.recipient_AmountChanged = false;
        this.page4.sendAmountInput = box()
            .size(230, 12)
            .curves(5,5,5,5,true)
            .position(23)
            .under(this.page4.sendAmount, 5)
            .textStyle(true)
            .input((val)=>{
                val = +val;
                if (val !== this.recipient_Amount){
                    if (numExists(val) && typeof val === 'number'){
                        this.recipient_Amount = val;
                    } else {
                        this.page4.sendAmountInput.clearInput(this.recipient_Amount);
                    };
                };
            }, "", 'text', false)
            .inputInjectCSS('border',`${2}px solid rgb(${0}, ${0}, ${0}, ${1})`)
            .inputInjectCSS('font-weight', 'bold')
            .inputInjectCSS('text-align', 'right')
            .within(this.page4._slide2, false)
        //* Ticker Symbol
        this.page4._sendAmountTicker = box()
            .size(50, 20)
            .under(this.page4.sendAmount, 5)
            .right(this.page4.sendAmountInput)
            .text(`${this.ticker}`, 10)
            .textFont('sans-serif')
            .textAlign('left')
            .textStyle(true)
            .within(this.page4._slide2, false)
        //* Submit / Send
        this.page4._sendAmountSubmit = box()
            .submit(()=>{

            }, 50)
            .color(0,255,125)
            .outline(0,0,0,255,2)
            .size(260, 25)
            .curves(15,15,15,15,true)
            .text("Send")
            .textStyle(true)
            //.position(this.trueW/2 + 2, u, true, true)
            .position(23)
            .under(this.page4.sendAmountInput, 35)
            .within(this.page4._slide2, false)
    };


    #walletHome(){
        //? First Slide: Wallet Home
        this.page4._slide1 = box()
            .div()
            .size(this.trueW, this.trueH - 70)
            .within(this.page4, false)
        //* White Cover (Block Background)
        this.page4._blockCover = box()
            .size(this.trueW-3, this.trueH - 72 - 100)
            .position(0, 100)
            .color(255)
            .outline(0,0,0,255,2)
            .within(this.page4._slide1, false)
        //* Temporary Logo
        this.page4._tempLogo = box()
            .size(this.trueH - this.page4._nav.trueH - 70 - 100)
            .position(this.trueW/2, this.trueH/2, true, true)
            .outline(0,0,0,0,2)
            .run((a)=>{
                a.opacity(this.loadedProgress)
            })
            .image(this.logo)
            .within(this.page4._blockCover, false)
        //* Amount
        this.page4._amount = box()
            .size(this.trueW*0.6, 100)
            .position(this.trueW/2, 50, true, true)
            .textFont('sans-serif')
            .textStyle(true)
            .within(this.page4._slide1, false)
        //* Ticker
        this.page4._ticker = box()
            .size(this.trueW*0.6, 100)
            .text(`${this.ticker}`, 12)
            .textFont('sans-serif')
            .textStyle(true)
            .below(this.page4._amount)
            .within(this.page4._slide1, false)
        //* Top Line
        this.page4._topLine = box()
            .color(0,0,0,255)
            .size(this.trueW + 2, 2)
            .position(-1, 100)
            .within(this.page4._slide1, false)
        //* Progress Bar
        this.page4._progressBar = box()
            .slider()
            .sliderColor(0,255,125)
            .sliderSize(8, 8)
            .sliderStatic()
            .run((a)=>{
                a.sliderValue(this.loadedProgress);
                if (this.loadedProgress >= 1){
                    a.hide();
                };
            })
            .size(this.trueW*0.8, 20)
            .position(this.trueW/2, u, true, true)
            .over(this.page4._topLine, 10)
            .within(this.page4._slide1, false)

    };
};