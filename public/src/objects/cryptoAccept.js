//server.send("monero", "hello monero")
//server('monero', (data)=>{console.log("subaddress: ", data)})

class CryptoAccept {
    constructor(trueW, address = undefined, name = 'MONERO', logo = './src/images/monero.png', compact = true, titleSize = 50){
        this.address = address;
        let trueH = trueW/2.6573;

        if (!this.address){
            try {
                server(name.toLowerCase(), (addr) => {
                    this.address = addr;
                })
            } catch (error) {console.log("error: ", error)}
        };

        //? Scaling factor
        const s = (num) => {
            return scaleValue(num, 380, trueW);
        };

        this.master = box()
            .size(trueW, trueH)
            .color(240)
            .outline(150)
            .curves(s(80),s(80),s(80),s(80),true)

        this.banner = box()
            .submit((a)=>{
                setTimeout(()=>{
                    a.hide()
                    this.details?.show()
                    if (!this.address){
                        try {
                            server.send(name.toLowerCase(), "subaddress");
                        } catch (error) {console.log("error: ", error)}
                    };
                }, 15);
                setTimeout(()=>{
                    a.show()
                    this.details?.hide()
                }, 30000);
            }, 50)
            .text('')
            .size(trueW, trueH)
            .curves(s(80),s(80),s(80),s(80),true)
            .color(220,220,220,100)
            .within(this.master, false)

        this.banner._name = box()
            .size(trueW, s(106))
            .text(name, s(titleSize))
            .textColor(100)
            .textAlign('right', s(30))
            .textFont('sans-serif')
            .textStyle(true)
            .within(this.banner, false)
        
        this.banner._accepted = box()
            .size(trueW, s(30))
            .under(this.banner._name, -s(15))
            .text("ACCEPTED HERE", s(24))
            .textColor(100)
            .textAlign('right', s(35))
            .textFont('sans-serif')
            .textStyle(false, true)
            .within(this.banner, false)

        this.banner._logo = box()
            .size(s(80), s(80))
            .position(s(30), s(10))
            .image(logo)
            .within(this.banner, false)

        let loadingArr = ["   Loading", "  Loading.", " Loading..", "Loading..."];
        let count = 0;
        let counter = 0;
        if (compact){
            this.details = box()
                .hide()
                .run((a)=>{
                    if (!this.address){
                        if (counter++ > 20){
                            counter = 0;
                            count++;
                            count%= loadingArr.length;
                        }
                        a.text(loadingArr[count], s(30))
                        a.textColor(100)
                        a.textFont("sans-serif")
                        a.textAlign('left', a.w/2 - textWidth("Loading", 'sans-serif', s(30))/2)
                        a.textInjectCSS("overflow", "visible")
                    } else {
                        a.text("")
                    };
                })
                .size(trueW, trueH)
                .color(240)
                .curves(s(80),s(80),s(80),s(80),true)
                .within(this.master, false)

            this.details._qr = box()
                .size(trueH*0.8, trueH*0.8)
                .position(trueH*0.6, trueH/2, true, true)
                .run((a)=>{
                    if (this.address) a.qrcode(this.address)
                })
                .within(this.details, false)

            this.details._addr = box()
                .size(trueW - (this.details._qr.x + this.details._qr.w) - s(20), trueH*0.8)
                .run((a)=>{
                    if (this.address) a.text(name + " ONLY: " + this.address, s(12.5))
                })
                .textInjectCSS("overflow-wrap", "anywhere")
                .textInjectCSS('overflow-y', 'hidden')
                .textInjectCSS('white-space', 'normal')
                .textColor(50)
                .textFont('sans-serif')
                .textStyle(true)
                .position(u, this.details._qr.y)
                .right(this.details._qr, s(10))
                .within(this.details, false)
        } else {
            this.details = box()
                .hide()
                .size(trueW, trueH*3)
                .curves(s(80),s(80),s(80),s(80),true)
                .color(240)
                .outline(150)
                .within(this.master, false)

            this.details._qr = box()
                .size(trueH*2, trueH*2)
                .position(trueW/2, s(20), true)
                .run((a)=>{
                    if (this.address) a.qrcode(this.address)
                })
                .within(this.details, false)

            this.details._addr = box()
                .size(trueW*0.9, trueH*0.8)
                .run((a)=>{
                    if (this.address) a.text(name + " ONLY: " + this.address, s(15))
                })
                .textInjectCSS("overflow-wrap", "anywhere")
                .textInjectCSS('overflow-y', 'hidden')
                .textColor(50)
                .textFont('sans-serif')
                .textStyle(true)
                .position(trueW/2, u, true)
                .under(this.details._qr, s(5))
                .within(this.details, false)
        };  
    };
};

clientFunc.cryptoAccept = (parent) => {
    let a = parent.cryptoAcceptProperties;
    if (a?.state){
        parent.changeObj(a);
        if (!a.obj){
            a.obj = new CryptoAccept(parent.trueW, a.address, a.name, a.logo, a.compact, a.titleSize);
            parent._();
        };
        if (a.obj?.master?.parentElement){
            if (a.parentInit){
                parent.parentElement.appendChild(a.obj.master.parentElement);
                a.parentInit = false;
            };
        } else {
            parent._();
        };
        return;
    };
};


Box.prototype.cryptoAccept = function(address, name, logo, compact, titleSize){
    let a = this.defineObject('cryptoAccept');
    a.state = true;
    a.address = address;
    a.name = name;
    a.logo = logo;
    a.compact = compact;
    a.titleSize = titleSize;
    return this;
};

Box.prototype.moneroAccept = function(address, compact){
    this.cryptoAccept(address, "MONERO", './src/images/monero.png', compact)
    return this;
};

Box.prototype.bitcoinAccept = function(address, compact){
    this.cryptoAccept(address, "BITCOIN", './src/images/bitcoin.png', compact, 52)
    return this;
};

Box.prototype.bitcoinCashAccept = function(address, compact){
    this.cryptoAccept(address, "BITCOIN CASH", './src/images/bitcoincash.png', compact, 30)
    return this;
};