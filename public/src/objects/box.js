class Box {
    constructor(){
        this.isABox = true;
        this.removeStartingXY = true;
        this.id = randomString(10);
        this.x = 0;
        this.y = 0;
        this.w = 100;
        this.h = 100;
        this.xCon = this.x;
        this.yCon = this.y;
        this.moving = true;
        this.children = [];
        this.relativeContainerSize = true;
        this.associateDiv = true;
        this.relativeMultiplierW = 1;
        this.relativeMultiplierH = 1;
        this.canvasTimer = 0;
        this.defaultOpacity = 1;
        this.zIndex= 1000+(speedrun_global.length*5);
        this.markedAsNew = _markAsNew;
        this.windowStartX = defaultBoundaries.windowStartX;
        this.windowStartY = defaultBoundaries.windowStartY;
        this.windowWidth = defaultBoundaries.windowWidth;
        this.windowHeight = defaultBoundaries.windowHeight;
        this.cachedW = this.windowWidth;
        this.cachedH = this.windowHeight;
        this.cachedW2 = this.windowWidth;
        this.cachedH2 = this.windowHeight;
        speedrun_global.push(this);
        return this._();
    };
    _(){
        this.drawAgain = true; 
        return this;
    };
    sustain(){
        this.#containment();
        this.#following();
        this.#movement();
        //?Arbitrary Code
        if (this.hiddenRunCode?.state){
            for (let i in this.hiddenRunCode.callback){
                if (this.hiddenRunCode.callback[i] && frameCount % this.hiddenRunCode.skip[i] === 0){
                    this.hiddenRunCode.callback[i](this);
                };
            };
        }; 
        if (this.runCode?.state){
            for (let i in this.runCode.callback){
                if (this.runCode.callback[i] && frameCount % this.runCode.skip[i] === 0){
                    this.runCode.callback[i](this);
                };
            };
        }; 
        if (this.deleted){
            for (let i in speedrun_global){
                if (speedrun_global[i] === this){
                    delete speedrun_global[i];
                };
            };
            filter_speedrun_global = true;
        };
    };
    #defineContainerMemory(){
        if (this.containerMemory?.init === true && this.container){
            this.containerMemory.init = false;
            this.containerMemory.startingX = this.container.x;
            this.containerMemory.startingY = this.container.y;
            this.containerMemory.startingW = this.container.w;
            this.containerMemory.startingH = this.container.h;
        };
        if (this.container){
            this.containerMemory.wRatio = this.containerMemory.startingW/this.w;
            this.containerMemory.hRatio = this.containerMemory.startingH/this.h;
        };
    };
    #containment(){
        if (!this.container) return;
        //?Initialize Relative Definitions
        this.#defineContainerMemory();
        if (this.containerMemory.prevX && (
            this.containerMemory.prevX !== this.container.x || 
            this.containerMemory.prevY !== this.container.y || 
            this.containerMemory.prevW !== this.container.w || 
            this.containerMemory.prevH !== this.container.h
        )){
            this._();
        };
        this.containerMemory.prevX = this.container.x;
        this.containerMemory.prevY = this.container.y;
        this.containerMemory.prevW = this.container.w;
        this.containerMemory.prevH = this.container.h;
    };
    setBoundingBox(){
        this.defineCollision();
        let diagonal = Math.sqrt(this.trueW**2 + this.trueH**2);
        let offset = (diagonal-((this.trueW+this.trueH)/2));
        this.objectCollision.boundingBox = {x: this.trueX-offset/2, y: this.trueY-offset/2, w: this.trueW+offset, h: this.trueH+offset};
    };
    setSimpleBox(){
        this.objectCollision.boundingBox = {x: this.trueX, y: this.trueY, w: this.trueW, h: this.trueH};
    };
    presetHitBoxes(){
        this.#definePolygon();
        clientFunc.hitBoxes(this);
    };
    #following(){
        let a = this.followCursor;
        let d = 0;
        if (this.rotation) d = this.rotation.deg;
        if (!a?.state) return;
        let mx = mouseX;
        let my = mouseY;
        //? Recursively rotate mouse over every parent container
        const recursiveFollowRotate = (container) => {
            let rotatedMouse = rotatePoint(mx, my, container.x+container.w/2, container.y+container.h/2, 360-container.rotation.deg-container.rotation.speed*2);
            mx = rotatedMouse[0];
            my = rotatedMouse[1]; 
            if (container.container){
                recursiveFollowRotate(container.container);
            };
        };
        if (this.container?.rotation){
            recursiveFollowRotate(this.container);
        };
        //?Find the offset if the shape isnt being force-centered.
        if (!a.center && a.init && (mouseX > 0 || mouseY > 0)){
            a.xOffset = (this.trueX + this.trueW/2) - mx;
            a.yOffset = (this.trueY + this.trueH/2) - my;
            a.init = false;
        };
        //? Calculate the new location and apply it as needed.
        this.trueValues();
        if (!this.cursorXOffset) this.cursorXOffset = 0;
        if (!this.cursorYOffset) this.cursorYOffset = 0;
        let tempX = mx - this.w/2 + a.xOffset - (this.trueX-this.x) + this.cursorXOffset;
        let tempY = my - this.h/2 + a.yOffset - (this.trueY-this.y) + this.cursorYOffset; 
        if (numExists(tempX) && numExists(tempY) && (tempX !== this.x || tempY !== this.y)){
            this._();
            this.x = tempX;
            this.y = tempY;
        };
        if (a.callback) a.callback(a);
    };
    #movement(){
        if (this.velocity?.speed){
            this._();
            this.#acceleratingSpeed();
            this.#acceleratingVelocity();
            this.x+= this.velocity.x*this.velocity.speed;
            this.y+= this.velocity.y*this.velocity.speed;
            if (this.followCursor?.cachedY && !mouseIsPressed){
                this.followCursor.cachedY = null;
            };
        };
        if (this.boundary?.state) clientFunc.boundaries(this);
        (this.xCon === this.x && this.yCon === this.y) ? this.moving = false: this.moving = true;
        this.xCon = this.x;
        this.yCon = this.y;        
    };
    #acceleratingSpeed(){
        if (!this.accSpeed.increment) return;
        this.velocity.speed+= this.accSpeed.increment;
        if (this.velocity.speed <= this.accSpeed.minLimit) this.velocity.speed = this.accSpeed.minLimit;
        if (this.velocity.speed >= this.accSpeed.maxLimit) this.velocity.speed = this.accSpeed.maxLimit;
    };
    #acceleratingVelocity(){
        if (!this.accVel.increment) return;
        this.velocity.angle+= this.accVel.increment;
        if (this.velocity.angle <= this.accVel.minLimit) this.velocity.angle = this.accVel.minLimit;
        if (this.velocity.angle >= this.accVel.maxLimit) this.velocity.angle = this.accVel.maxLimit;
        this.move(this.velocity.angle, this.velocity.speed);
    };
    trueValues(){
        if (this.delayTruth){
            this.delayTruth = false;
            return;
        };
        let [wRatio, hRatio, startingX, startingY, startingW, startingH] = [,,,,,,];
        let containerize = false;
        if (this.containerMemory){
            this.#defineContainerMemory();
            wRatio = this.containerMemory.wRatio;
            hRatio = this.containerMemory.hRatio;
            startingX = this.containerMemory.startingX;
            startingY = this.containerMemory.startingY;
            startingW = this.containerMemory.startingW;
            startingH = this.containerMemory.startingH;
            (!this.container || this.containerMemory.init) ? containerize = false : containerize = true;
        };

        //?Relative Sizing     
        if (containerize && this.relativeContainerSize){
            this.trueW = this.container.trueW/wRatio;
            this.trueH = this.container.trueH/hRatio;
        } else {
            this.trueW = this.w;
            this.trueH = this.h;
        };
        //?Relative Positioning    
        if (pageSize.relativeLocation){
        let leftMargin = this.x;
        let rightMargin = this.cachedW - this.trueW;
        let marginRatioX = rightMargin/leftMargin;
        let topMargin = this.y;
        let bottomMargin = this.cachedH - this.trueH;
        let marginRatioY = bottomMargin/topMargin;
        this.trueX = ((this.x + pageSize.x) + (this.windowWidth - this.cachedW)/marginRatioX);
        this.trueY = ((this.y + pageSize.y) + (this.windowHeight - this.cachedH)/marginRatioY);
        if (this.removeStartingXY){
            startingX = 0;
            startingY = 0;
        }
        if (containerize){
            this.trueX = (this.x - startingX) * (this.container.trueW/startingW) + this.container.trueX;
            this.trueY = (this.y - startingY) * (this.container.trueH/startingH) + this.container.trueY;
        };
        } else {
            this.trueX = this.x;
            this.trueY = this.y;
        };
    };
    changeObj(propertyContainer, callback = null){
        if (propertyContainer.changeObj && propertyContainer.obj){
            if (propertyContainer?.obj?.master?.parentElement){
                propertyContainer.obj.master?.parentElement.remove();
            } else {
                propertyContainer.obj.remove();
            };
            propertyContainer.obj = null;
            propertyContainer.changeObj = false;
            propertyContainer.parentInit = true;
            if (callback) callback();
        };
    };
    createObj(propertyContainer, callback = null){
        if (!propertyContainer.obj){
            this._();
            if (callback) callback();
            if (this.associateDiv) pageDiv.appendChild(propertyContainer.obj);
            if (!this.parentElement) this.parentElement = propertyContainer.obj;
        };
    };
    positionLogic(propertyContainer, xOffset = 0, yOffset = 0){
        if (this.parentElement !== propertyContainer.obj){
            if (propertyContainer.parentInit && propertyContainer.obj && this.parentElement){
                this.parentElement.appendChild(propertyContainer.obj);
                propertyContainer.parentInit = false;
            };
            propertyContainer.obj.style['left'] = `${xOffset}px`;
            propertyContainer.obj.style['top'] = `${yOffset}px`;
        } else {
            if (this.container){
                if (propertyContainer.parentInit && this.container.parentElement &&
                    propertyContainer.obj !== this.container.parentElement){
                        this.container.parentElement.appendChild(propertyContainer.obj);
                        propertyContainer.parentInit = false;
                };
                propertyContainer.obj.style['left'] = `${this.trueX - this.container.trueX + xOffset}px`;
                propertyContainer.obj.style['top'] = `${this.trueY - this.container.trueY + yOffset}px`;
            } else {
                propertyContainer.obj.style['left'] = `${this.trueX + xOffset}px`;
                propertyContainer.obj.style['top'] = `${this.trueY + yOffset}px`;
            };
            if (this.rotation?.state){
                propertyContainer.obj.style['transform-origin'] = `${this.trueW/2}px ${this.trueH/2}px`;
                propertyContainer.obj.style['transform'] = `rotate(${this.rotation.deg}deg)`;
            };
        };
    };
    textLogic(propertyContainer){
        if (!this.textProperties?.content) return;
        propertyContainer.obj.style['font-size'] = `${this.textSize}px`;
        propertyContainer.obj.style['-webkit-text-stroke-width'] = `${this.textStroke.weight}px`;
        propertyContainer.obj.style['-webkit-text-stroke-color'] = `rgb(${this.textStroke.r}, ${this.textStroke.g}, ${this.textStroke.b}, ${this.textStroke.a/255})`;
        propertyContainer.obj.style['color'] = `rgb(${this.textColors.r}, ${this.textColors.g}, ${this.textColors.b}, ${this.textColors.a/255})`;
        propertyContainer.obj.style['font-family'] = `${this.textProperties.font}`;
        //?Bolding
        if (this.textProperties.bold){
            propertyContainer.obj.style['font-weight'] = 'bold';
        } else {
            propertyContainer.obj.style['font-weight'] = 'normal';
        };
        //?Italicizing
        if (this.textProperties.italic){
            propertyContainer.obj.style['font-style'] = 'italic';
        } else {
            propertyContainer.obj.style['font-style'] = 'normal';
        };
        //?Text Decoration
        if (this.textProperties.decoration.state){
            propertyContainer.obj.style['text-decoration'] = `${this.textProperties.decoration.type}`;
            propertyContainer.obj.style['text-decoration-style'] = `${this.textProperties.decoration.style}`;
            propertyContainer.obj.style['text-decoration-thickness'] = `${this.textProperties.decoration.thickness}px`;
            propertyContainer.obj.style['text-decoration-color'] = `rgb(${this.textProperties.decoration.r}, ${this.textProperties.decoration.g}, ${this.textProperties.decoration.b}, ${this.textProperties.decoration.a/255})`;
        } else {
            propertyContainer.obj.style['text-decoration'] = 'normal';
        };
    }
    injectionLogic(propertyContainer, displayType = 'initial'){
        if (this.hideGraphics){
            propertyContainer.obj.style['opacity'] = '0';
            if (this.disableGraphics){
                propertyContainer.obj.style['display'] = 'none';
            };
        } else {
            propertyContainer.obj.style['display'] = `${displayType}`;
            propertyContainer.obj.style['opacity'] = `${this.defaultOpacity}`;
        };
        for (let i in propertyContainer.injection){
            if (propertyContainer.injection[i].state){
                propertyContainer.obj.style[`${propertyContainer.injection[i].key}`] = `${propertyContainer.injection[i].value}`;
            };
        };
    };
    create(){
        if (this.deleteThis){
            this.parentElement?.remove();
            this.deleteThis = false;
            this.deleted = true;
        };
        if (this.deleted) return;
        for (let i in this.children){
            this.children[i].drawAgain = true;
        };
        this.trueValues();
        //?Rotation
        { let a = this.rotation;
            if (a?.state && a.always){
                this._();
                a.deg+= a.speed;
                while(a.deg < 0){
                    a.deg+= 360;
                };
                a.deg = a.deg % 360;
            };
        };
        //? Curves Logic
        this.borderRadiusMode;
        this.curveAbsolute ? this.borderRadiusMode = 'px' : this.borderRadiusMode = '%';
        let core = clientFunc.primaryFuncs;
        for (let i in core){
            clientFunc[core[i]]?.(this);
        };
        //?Create Parent Element if None Exist and Recursively Define It
        if (!this.parentElement){
            this.div();
            this.create();
            return;
        };
        let extra = clientFunc.secondaryFuncs;
        for (let i in extra){
            clientFunc[extra[i]]?.(this);
        };        
    };

    randomColor(r, g, b, a){
        switch (r) {
            case 'mid': r = 255/2; break;
            case 'ran': r = random(0, 255); break;
        };
        switch (g) {
            case 'mid': g = 255/2; break;
            case 'ran': g = random(0, 255); break;
        };
        switch (b) {
            case 'mid': b = 255/2; break;
            case 'ran': b = random(0, 255); break;
        };
        if (numExists(a)){
            switch (a) {
                case 'mid': a = 255/2; break;
                case 'ran': a = random(0, 255); break;
            };
            return [r, g, b, a];
        };
        return [r, g, b];
    };
    randomCurves(curve, curve2, curve3, curve4, absolute){
        switch (curve) {
            case 'mid': absolute ? curve = (this.trueW+this.trueH)/2 : curve = 50; break;
            case 'ran': absolute ? curve = random(0, (this.trueW+this.trueH)) : curve = random(0, 100); break;
        };
        switch (curve2) {
            case 'mid': absolute ? curve2 = (this.trueW+this.trueH)/2 : curve2 = 50; break;
            case 'ran': absolute ? curve2 = random(0, (this.trueW+this.trueH)) : curve2 = random(0, 100); break;
        };
        switch (curve3) {
            case 'mid': absolute ? curve3 = (this.trueW+this.trueH)/2 : curve3 = 50; break;
            case 'ran': absolute ? curve3 = random(0, (this.trueW+this.trueH)) : curve3 = random(0, 100); break;
        };
        switch (curve4) {
            case 'mid': absolute ? curve4 = (this.trueW+this.trueH)/2 : curve4 = 50; break;
            case 'ran': absolute ? curve4 = random(0, (this.trueW+this.trueH)) : curve4 = random(0, 100); break;
        };
        return [curve, curve2, curve3, curve4];
    };
    randomPosition(x, y){
        switch (x) {
            case 'mid': x = this.windowWidth/2 - this.trueW/2; break;
            case 'ran': x = random(this.windowStartX, this.windowWidth - this.trueW); break;
        };
        switch (y) {
            case 'mid': y = this.windowHeight/2 - this.trueH/2; break;
            case 'ran': y = random(this.windowStartY, this.windowHeight - this.trueH); break;
        };
        return [x, y];
    };
    randomSize(w, h){
        switch (w) {
            case 'mid': w = this.windowWidth/2; break;
            case 'ran': w = random(1, this.windowWidth - this.trueX); break;
        };
        switch (h) {
            case 'mid': h = this.windowHeight/2; break;
            case 'ran': h = random(1, this.windowHeight - this.trueY); break;
        };
        return [w, h];
    };
    randomSpeed(s){
        if (s === 'ran') s = random(0, 32);
        return [s];
    };
    randomRotation(r){
        if (r === 'ran') r = random(0, 359);
        return [r];
    };

    //!METHODS FOR DEVELOPERS
    //?Display
    prototype(name, callback){
        Box.prototype[name] = function (params) {
            callback(this, params);
            return this._();
        };
        return this._();
    };
    hide(disable = true){
        this.hideGraphics = true;
        disable ? this.disableGraphics = true: this.disableGraphics = false;
        return this._();
    };
    show(){
        this.hideGraphics = false;
        this.disableGraphics = false;
        return this._();
    };
    opacity(opacity){
        if (!numExists(opacity) && !this.defaultOpacity) opacity = 1;
        if (!numExists(opacity) && this.defaultOpacity) opacity = this.defaultOpacity;
        this.defaultOpacity = opacity;
        return this._();
    };
    darken(amount){
        this.darkened = true;
        this.color(this.r-amount, this.g-amount, this.b-amount, this.a);
        return this._();
    };
    undarken(amount){
        if (this.darkened){
            this.darkened = false;
            this.color(this.r+amount, this.g+amount, this.b+amount, this.a);
        };
        return this._();
    };
    darkenText(amount){
        this.defineObject('text');
        this.textDarkened = true;
        this.textColor(this.textColors.r-amount, this.textColors.g-amount, this.textColors.b-amount, this.textColors.a);
        return this._();
    };
    undarkenText(amount){
        this.defineObject('text');
        if (this.textDarkened){
            this.textDarkened = false;
            this.textColor(this.textColors.r+amount, this.textColors.g+amount, this.textColors.b+amount, this.textColors.a);
        };
        return this._();
    };
    defineObject(type){
        type+= 'Properties';
        this._();
        if (!this[type]){
            this[type] = {
                injection: [],
                parentInit: true,
                parentInit2: true,
                parentInit3: true,
                injection: [],
                xOffset: 0,
                yOffset: 0,
            };
            if (type === 'boxProperties'){
                this.#defineVisuals();
            };
            if (type === 'buttonProperties'){
                this.#defineVisuals();
                this.detectCollision = {
                    state: false,
                    x: 0,
                    y: 0,
                    w: 0,
                    h: 0,
                    mode: {},
                    heat: 0,
                };
                let modes = ['click', 'double', 'press', 'over', 'out', 'move', 'release'];
                for (let key in modes){
                    this.detectCollision.mode[modes[key]] = {
                        active: false,
                        callback: null,
                        mode: modes[key],
                    };
                };
            };
            if (type === 'imageProperties'){
                this.#defineVisuals();
                this[type].strokeStyle = 'solid';
                this[type].injection = [];
                this[type].filter = {
                    state: false,
                    type: 'hue-rotate',
                    value: 0,
                    suffix: 'deg',
                };
            };
            if (type === 'audioProperties' || type === 'videoProperties'){
                this.mediaType = type;
                type === 'audioProperties' ? this[type].volume = 1 : this[type].volume = 0;
                this[type].prevTime = 0;
                this[type].play = true;
                this[type].ratio = false;
                this[type].list = [];
                this[type].r = 0;
                this[type].g = 0;
                this[type].b = 255;
                this[type].r2 = 0;
                this[type].g2 = 0;
                this[type].b2 = 0;
                this[type].colorDeg = 0;
                this[type].r3 = 255;
                this[type].g3 = 255;
                this[type].b3 = 255;
                this[type].a3 = 255;
                this[type].r4 = 255;
                this[type].g4 = 255;
                this[type].b4 = 255;
                this[type].a4 = 255;
                this[type].weight4 = 0;
            };
            if (type === 'graphProperties'){
                this[type].data = [];
                this[type].type = 'bar';
                this[type].options = {};
                this[type].background = {
                    r: 0,
                    g: 0,
                    b: 0,
                    a: 255,
                };
            };
            if (type === 'textProperties' || type === 'boxProperties'){
                this[type].horizontalPadding = 0;
                this[type].verticalPadding = 0;
                if (!this.textSize) this.textSize = 10;
                this.padding = {
                    horizAlign: null,
                    vertAlign: null,
                    x: 0,
                    y: 10,
                    w: 0,
                    h: 10,
                };
                this.textColors = {
                    r: 0,
                    g: 0,
                    b: 0,
                    a: 255,
                };
                this.textStroke = {
                    r: 0,
                    g: 0,
                    b: 0,
                    a: 255,
                    weight: 0,
                };
                this[type].content = "";
                this[type].hyperlink = "";
                this[type].font = 'serif';
                this[type].align = 'center';
                this[type].decoration = {
                    type: 'underline',
                    style: 'solid',
                    thickness: 2,
                    r: 0,
                    g: 0,
                    b: 0,
                    a: 255,
                };
            };
            if (type === 'inputProperties' || type === 'messageProperties'){
                this[type].xMinus = 4;
                this.inputColors = {
                    font: 'sans-serif',
                    size: 12,
                    fill:{
                        r: 255,
                        g: 255,
                        b: 255,
                        a: 255,
                    },
                    text:{
                        r: 0,
                        g: 0,
                        b: 0,
                        a: 255,
                    },
                    border:{
                        r: 0,
                        g: 0,
                        b: 0,
                        a: 255,
                        weight: 0.1,
                    },
                    outline:{
                        r: 0,
                        g: 0,
                        b: 0,
                        a: 255,
                    },
                };
            };
            if (type === 'sliderProperties'){
                this[type].prevValue = -1;
                this[type].minValue = 0;
                this[type].maxValue = 1;
                this[type].defaultValue = 0;
                this[type].step = 0;
                this[type].intensitySet = {
                    r1: 25,
                    g1: 25,
                    b1: 25,
                    a1: 0,
                    weight1: 0,
                    r2: -25,
                    g2: -25,
                    b2: -25,
                    a2: 0,
                    weight2: 0,
                };
                this[type].track = {
                    r: 240,
                    g: 240,
                    b: 240,
                    a: 255,
                    curve: 30,
                    outline: {
                        r: 175,
                        g: 175,
                        b: 175,
                        a: 255,
                        weight: 1,
                    },
                    objState: false,
                    obj: null,
                    height: 6,
                };
                this[type].thumb = {
                    r: 0,
                    g: 128,
                    b: 255,
                    a: 255,
                    curve: 100,
                    outline: {
                        r: 175,
                        g: 175,
                        b: 175,
                        a: 0,
                        weight: 1,
                    }, 
                    objState: false,
                    obj: null,
                    width: 12,
                    height: 12,
                };
                this[type].trail = {
                    r: 0,
                    g: 128,
                    b: 255,
                    a: 255,
                    curve: 30,
                    outline: {
                        r: 175,
                        g: 175,
                        b: 175,
                        a: 0,
                        weight: 1,
                    },
                    objState: false,
                    obj: null,
                    height: 6,
                };
                this.intensity = {
                    r: 0,
                    g: 0,
                    b: 0,
                    a: 0,
                    weight: 0,
                    multiplier: 1,
                };
            };
            if (type === 'checkProperties' || type === 'radioProperties'){
                this[type].text = {
                    r: 0,
                    g: 0,
                    b: 0,
                    a: 255
                };
                this[type].accent = {
                    r: 0,
                    g: 128,
                    b: 255,
                    a: 255,
                };
                this[type].rgba = [];
                this[type].rgbaPrev = [];
                this[type].defaultValue = {r: 0, g: 0, b: 0, a: 255};
            };
            if (type === 'navbarProperties' || type === 'tabProperties' || type === 'selectProperties'){
                this[type].form = "fix";
                this[type].options = [];
                this[type].optionsInit = true;
                this[type].modified = true;
                this[type].meta = {
                    font: 'sans-serif',
                    size: 15,
                    textPadding: 10,
                    rightHandPadding: 30,
                    disable: {},
                };
                this[type].colors = {
                    font: 'sans-serif',
                    size: 15,
                    textSize: 15,
                    textFont: 'sans-serif',
                    textColor: {
                        r: 0,
                        g: 0,
                        b: 0,
                        a: 255
                    },
                    smooth: true,
                    offset: -1,
                    intensity: 25,
                    hoverIntensity: 10,
                    background: {
                        r: 225,
                        g: 225,
                        b: 225,
                        a: 255
                    },
                    outline: {
                        r: 100,
                        g: 100,
                        b: 100,
                        a: 255,
                        weight: 1
                    },
                    navbar: {
                        background: {
                            r: 30,
                            g: 30,
                            b: 30,
                            a: 255,
                        },
                        outline: {
                            r: 0,
                            g: 0,
                            b: 0,
                            a: 0,
                            weight: 0,
                        },
                    },
                    unselected: {
                        callback: null,
                        text: {
                            r: 255,
                            g: 255,
                            b: 255,
                            a: 255,
                        },
                        background: {
                            r: 255,
                            g: 255,
                            b: 255,
                            a: 0,
                        },
                        outline: {
                            r: 0,
                            g: 0,
                            b: 0,
                            a: 0, 
                            weight: 0,
                        },
                        textOutline: {
                            r: 0,
                            g: 0,
                            b: 0,
                            a: 0, 
                            weight: 0,
                        },
                    },
                    selected: {
                        callback: null,
                        text: {
                            r: 255,
                            g: 255,
                            b: 255,
                            a: 255,
                        },
                        background: {
                            r: 0,
                            g: 0,
                            b: 0,
                            a: 255,
                        },
                        outline: {
                            r: 0,
                            g: 0,
                            b: 0,
                            a: 0,
                            weight: 0,
                        },
                        textOutline: {
                            r: 0,
                            g: 0,
                            b: 0,
                            a: 0, 
                            weight: 0,
                        },
                    },
                    pressed: {
                        state: false,
                        callback: null,
                        text: {
                            r: 255,
                            g: 255,
                            b: 255,
                            a: 255,
                        },
                        background: {
                            r: 0,
                            g: 0,
                            b: 0,
                            a: 255,
                        },
                        outline: {
                            r: 0,
                            g: 0,
                            b: 0,
                            a: 0,
                            weight: 0,
                        },
                        textOutline: {
                            r: 0,
                            g: 0,
                            b: 0,
                            a: 0, 
                            weight: 0,
                        },
                    },
                };
            };
        };
        return this[type];
    };
    disable(level = 0.75){
        this.defineObject('button');
        for (let k in this.detectCollision.mode){
            if (this.detectCollision.mode[k]) this.detectCollision.mode[k].active = false;
        };
        this.opacity(level);
        return this;
    };
    enable(level = 1){
        this.defineObject('button');
        for (let k in this.detectCollision.mode){
            if (this.detectCollision.mode[k]) this.detectCollision.mode[k].active = true;
        };
        this.opacity(level);
        return this;
    };
    remove(){
        this.deleteThis = true;
        return this._();
    };
    isolate(state = true){
        let types = ['box', 'button', 'image', 'text', 'check', 'input',
            'radio', 'colorPicker', 'slider', 'select', 'navbar', 'graph', 'tab', 
            'audio', 'video'];
        for (let i in types){
            let a = this[types[i]+'Properties'];
            if (a?.obj) a.changeObj = true;
        };
        this.associateDiv = !state;
        return this._();
    };
    position(x = this.x, y = this.y, centerX = false, centerY = false){
        [x, y] = this.randomPosition(x, y);
        if (numExists(x)) centerX ? this.x = x - this.w/2: this.x = x;
        if (numExists(y)) centerY ? this.y = y - this.h/2: this.y = y;
        this.cachedW = this.windowWidth;
        this.cachedH = this.windowHeight;
        this.trueValues();
        return this._();
    };
    containerPosition(x, y, centerX, centerY){
        setTimeout(() => {
            this.position(x, y, centerX, centerY);
        }, 1);
        return this;
    };
    #nextTo(obj, padding = 0, ratio = 1, centered = false, fixed = false, orientation = 'right'){
        obj = this.#checkName(obj);
        const orient = () => {
            let offset = 0;
            if (centered){
                if (orientation === 'right' || orientation === 'left'){
                    offset = this.w/2;
                } else {
                    offset = this.h/2;
                };
            };
            switch(orientation){
                case 'right': this.x = obj.x + obj.w*ratio + padding - offset; break;
                case 'left': this.x = obj.x - this.w*ratio - padding - offset; break;
                case 'under': this.y = obj.y + obj.h*ratio + padding - offset; break;
                case 'over': this.y = obj.y - this.h*ratio - padding - offset; break;
            };
        };
        if (fixed){
            this._run(()=>{
                if (obj.drawAgain || this.drawAgain){
                    this._();
                    orient();
                };
            });
        } else {
            orient();
        };
        this.trueValues();
        return this._();
    };
    right(obj, padding = 0, ratio = 1, centered = false, fixed = false){
        return this.#nextTo(obj, padding, ratio, centered, fixed, 'right');
    };
    rightOf(obj, padding = 0, ratio = 1, centered = false, fixed = false){
        this.position(obj.x, obj.y);
        return this.right(obj, padding, ratio, centered, fixed);
    };
    left(obj, padding = 0, ratio = 1, centered = false, fixed = false){
        return this.#nextTo(obj, padding, ratio, centered, fixed, 'left');
    };
    leftOf(obj, padding = 0, ratio = 1, centered = false, fixed = false){
        this.position(obj.x, obj.y);
        return this.left(obj, padding, ratio, centered, fixed);
    };
    over(obj, padding = 0, ratio = 1, centered = false, fixed = false){
        return this.#nextTo(obj, padding, ratio, centered, fixed, 'over');
    };
    topOf(obj, padding = 0, ratio = 1, centered = false, fixed = false){
        this.position(obj.x, obj.y);
        return this.over(obj, padding, ratio, centered, fixed);
    };
    under(obj, padding = 0, ratio = 1, centered = false, fixed = false){
        return this.#nextTo(obj, padding, ratio, centered, fixed, 'under');
    };
    bottomOf(obj, padding = 0, ratio = 1, centered = false, fixed = false){
        this.position(obj.x, obj.y);
        return this.under(obj, padding, ratio, centered, fixed);
    };
    size(w, h = w){
        [w, h] = this.randomSize(w, h);
        if (w === this.windowWidth) w = this.windowWidth/1.0001;
        if (h === this.windowHeight) h = this.windowHeight/1.0001;
        if (numExists(w)) this.w = w;
        if (numExists(h)) this.h = h;
        this.trueValues();
        return this._();
    };
    tempPosition(x = this.trueX, y = this.trueY, w = this.trueW, h = this.trueH){
        [x, y] = this.randomPosition(x, y);
        [w, h] = this.randomSize(w, h);
        this.delayTruth = true;
        this.trueX = x;
        this.trueY = y;
        this.trueW = w;
        this.trueH = h;
        return this._();
    };
    curves(curve = 15, curve2 = curve, curve3 = curve, curve4 = curve, absolute = false){
        this.#defineVisuals();
        [curve, curve2, curve3, curve4] = this.randomCurves(curve, curve2, curve3, curve4, absolute);
        this.curve = curve;
        this.curve2 = curve2;
        this.curve3 = curve3;
        this.curve4 = curve4;
        this.curveAbsolute = absolute;
        return this._();
    };
    #definePolygon(){
        if (!this.npolygon){
            this.npolygon = {
                state: false,
                type: 'rectangle',
                str: ''
            };
        };
    };
    polygon(str = 'triangle', regular = false){
        this.#definePolygon();
        clientFunc.polygon(this, str, regular);
        return this._();
    };
    #defineVisuals(){
        if (!this.stroke){
            this.curve = 0;
            this.curve2 = 0;
            this.curve3 = 0;
            this.curve4 = 0;
            this.curveAbsolute = false;
            this.r = 255;
            this.b = 255;
            this.g = 255;
            this.a = 0;
            this.stroke = {
                state: false,
                r: 200,
                g: 200,
                b: 200,
                a: 0,
                weight: 0,
            };
        };
    };
    outline(r, g = r, b = r, a = 255, weight = 1){
        this.defineObject('box');
        [r, g, b, a] = this.randomColor(r, g, b, a);
        let s = this.stroke;
        s.state = true;
        s.r = r;
        s.g = g;
        s.b = b;
        s.a = a;
        s.weight = weight;
        this.boxProperties.state = true;
        return this._();
    };
    color(r, g = r, b = r, a = 255){
        this.defineObject('box');
        [r, g, b, a] = this.randomColor(r, g, b, a);
        this.boxProperties.state = true;
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a;
        return this._();
    };
    gradient(r = 0, g = r, b = r, a = 255, radial = false, deg = 0){
        this.nGradient([[this.r, this.g, this.b, this.a], [r, g, b, a]], radial, deg, false);
        return this._();
    };
    textGradient(r = 0, g = r, b = r, a = 255, deg = 0){
        this.nGradient([[this.textColors.r, this.textColors.g, this.textColors.b, this.textColors.a], [r, g, b, a]], false, deg, true);
        return this._();
    };
    nGradient(arr, radial, deg, textMode = false){
            this.defineObject('box');
            let colors = [];
            let gradient = "linear";
            if (radial){
                gradient = "radial";
            };
            for (let i in arr){
                //? Red
                if (numExists(arr[i][0])){
                    arr[i].r = arr[i][0];
                } else {
                    if (!numExists(arr[i].r)) arr[i].r = 0;
                };
                //? Green
                if (numExists(arr[i][1])){
                    arr[i].g = arr[i][1];
                } else {
                    if (!numExists(arr[i].g)) arr[i].g = arr[i].r;
                };
                //? Blue
                if (numExists(arr[i][2])){
                    arr[i].b = arr[i][2];
                } else {
                    if (!numExists(arr[i].b)) arr[i].b = arr[i].r;
                };
                //? Alpha
                if (numExists(arr[i][3])){
                    arr[i].a = arr[i][3];
                } else {
                    if (!numExists(arr[i].a)) arr[i].a = 255;
                };
                colors[i] = `rgb(${arr[i].r}, ${arr[i].g}, ${arr[i].b}, ${arr[i].a/255})`;
            };
            let text = `${gradient}-gradient(`;
            if (!radial){
                text+= `${deg}deg,`;
            };
            for (let i in colors){
                text+= colors[i]
            };
            text+= `)`;
            text = text.replaceAll(")rgb", "),rgb");
            if (textMode){
                this.textInjectCSS("background", "-webkit-"+text);
                this.textInjectCSS("-webkit-background-clip", "text");
                this.textInjectCSS("-webkit-text-fill-color", "transparent");
            } else {
                this.boxInjectCSS("background-image", text);
                if (this.buttonProperties?.state) this.buttonInjectCSS("background-image", text);
            };
        return this._();
    };
    boxInjectCSS = function(key, value){
        let a = this.defineObject('box');
            a.injection.push({
                state: true,
                key: key,
                value: value
            });
        return this;
    };
    objInject(callback){
        this.objectInject = callback;
        return this._();
    };
    #defineFollow(){
        if (!this.followCursor){
            this.followCursor = {
                state: false,
                callback: null,
                xOffset: 0,
                yOffset: 0,
                xActive: true,
                yActive: true,
                center: false,
                init: true,
            };
        };
    };
    follow(callback = null, center = false, active = true){
        this.#defineFollow();
        switch (active){
            case 'true' || true: active = true; break;
            case 'false' || false: active = false; break;
            case 'toggle': active = !this.followCursor.state; break;
        };
        this.followCursor.state = active;
        this.followCursor.center = center;
        this.followCursor.callback = callback;
        this.followCursor.init = true;
        return this._();
    };

    #checkName(container){
        if (typeof container === 'string'){
            if (speedrun_global_boxes[container] && typeof speedrun_global_boxes[container] === 'object'){
                container = speedrun_global_boxes[container];
            } else {
                console.log('Error, box does not exist.');
            };
        };
        return container;
    };
    above(box){
        box = this.#checkName(box);
        this.zIndex = box.zIndex + 5;
        box._();
        return this._();
    };
    below(box){
        box = this.#checkName(box);
        this.zIndex = box.zIndex - 5;
        box._();
        return this._();
    };

    //?Physics
    #defineRun(){
        if (!this.runCode){
            this.runCode = {
                state: false,
                callback: [],
                skip: [],
            };
        };
    };
    #defineHiddenRun(){
        if (!this.hiddenRunCode){
            this.hiddenRunCode = {
                state: false,
                callback: [],
                skip: []
            };
        };
    };
    run(callback, active = true, skip = 1){
        if (typeof active === "number"){
            skip = active;
            active = true;
        };
        if (skip < 1) skip = 1;
        this.#defineRun();
        switch (active){
            case 'true' || true: active = true; break;
            case 'false' || false: active = false; break;
            case 'toggle': active = !this.runCode.state; break;
        };
        this.runCode.state = active;
        this.runCode.skip.push(skip);
        this.runCode.callback.push(callback);
        return this._();
    };
    _run(callback, active = true, skip = 1){
        if (typeof active === "number"){
            skip = active;
            active = true;
        };
        if (skip < 1) skip = 1;
        this.#defineHiddenRun();
        switch (active){
            case 'true' || true: active = true; break;
            case 'false' || false: active = false; break;
            case 'toggle': active = !this.hiddenRunCode.state; break;
        };
        this.hiddenRunCode.state = active;
        this.hiddenRunCode.skip.push(skip);
        this.hiddenRunCode.callback.push(callback);
        return this._();
    };
    clearRun(){
        this.#defineRun();
        this.runCode.callback = [];
        this.runCode.skip = [];
        return this._();
    };
    _clearRun(){
        this.#defineHiddenRun();
        this.hiddenRunCode.callback = [];
        this.hiddenRunCode.skip = [];
        return this._();
    };
    #scaling(scaleAmount, minLimit, maxLimit){
        this.w*= scaleAmount;
        this.h*= scaleAmount;
        let whRatio = this.w/this.h;
        if (this.w > this.h){
            if (this.w > maxLimit){
                this.w = maxLimit;
                this.h = maxLimit/whRatio;
            };
            if (this.h < minLimit){
                this.h = minLimit;
                this.w = minLimit*whRatio;
            };
        } else {
            if (this.h > maxLimit){
                this.h = maxLimit;
                this.w = maxLimit/whRatio;
            };
            if (this.w < minLimit){
                this.w = minLimit;
                this.h = minLimit*whRatio;
            };
        };  
    };
    scale(scaleAmount = 2, minLimit = 20, maxLimit = 5000){
        this.#scaling(scaleAmount, minLimit, maxLimit);
        return this;
    };
    grow(scaleAmount = 1.005, minLimit = 20, maxLimit = 5000){
        this.run(()=>{
            this.#scaling(scaleAmount, minLimit, maxLimit);
        });
        return this;
    };
    #defineMove(){
        if (!this.velocity){
            this.velocity = {
                x: 0,
                y: 0,
                speed: 0,
                angle: 90,
                drag: 0,
                stop: false,
            };
            this.accSpeed = {
                increment: 0,
                minLimit: -Infinity,
                maxLimit: Infinity,
            };
            this.accVel = {
                increment: 0,
                minLimit: -Infinity,
                maxLimit: Infinity,
            };
        };
    };
    move(angle = 90, speed = 1){
        this.#defineMove();
        [angle] = this.randomRotation(angle);
        [speed] = this.randomSpeed(speed);
        let velocity = calculateVelocityFromDegrees(angle);
        this.velocity.speed = speed;
        this.velocity.angle = angle;
        this.velocity.x = velocity.x;
        this.velocity.y = velocity.y;
        return this._();
    };
    accelerateSpeed(increment, minLimit = -Infinity, maxLimit = Infinity){
        this.#defineMove();
        this.accSpeed.increment = increment;
        this.accSpeed.minLimit = minLimit;
        this.accSpeed.maxLimit = maxLimit;
        return this._();
    };
    accelerateVelocity(increment, minLimit = -Infinity, maxLimit = Infinity){
        this.#defineMove();
        this.accVel.increment = increment;
        this.accVel.minLimit = minLimit;
        this.accVel.maxLimit = maxLimit;
        return this._();
    };
    #defineBoundaries(){
        if (!this.boundary){
            this.boundary = {
                state: false,
                leftType: "delete",
                rightType: "delete",
                topType: "delete",
                bottomType: "delete",
                variance: 45,
                simple: false,
                width: 10,
                x: this.windowStartX,
                y: this.windowStartY,
                w: this.windowWidth,
                h: this.windowHeight,
            }
        };
    };
    boundaries(leftType = "delete", rightType = leftType, topType = leftType, bottomType = leftType){
        this.#defineBoundaries();
        this.boundary.state = true;
        this.boundary.leftType = leftType;
        this.boundary.rightType = rightType;
        this.boundary.topType = topType;
        this.boundary.bottomType = bottomType;
        return this._();
    };
    boundaryCollision(simple = false, variance = 45, width = 10){
        this.#defineBoundaries();
        this.boundary.simple = simple;
        this.boundary.variance = variance;
        this.boundary.width = width;
        return this._();
    };
    boundarySize(x = 0, y = 0, w = this.windowWidth, h = this.windowHeight){
        this.#defineBoundaries();
        this.boundary.x = x;
        this.boundary.y = y;
        this.boundary.w = w;
        this.boundary.h = h;
        return this._();
    };
    defineRotate(){
        if (!this.rotation){
            this.rotation = {
                state: false,
                deg: 0,
                speed: 0,
            };
        };
    };
    rotate(deg = 0){
        this.defineRotate();
        [deg] = this.randomRotation(deg);
        this.rotation.state = true;
        this.rotation.deg = deg;
        return this._();
    };
    spin(speed = 1, rate = 0){
        this.defineRotate();
        [speed] = this.randomSpeed(speed);
        this.rotation.state = true;
        this.rotation.deg = 0;
        this.rotation.always = true;
        this.rotation.speed = speed;
        if (rate){
            this.run((a)=>{
                a.rotation.speed+= rate;
            })
        };
        return this._();
    };
 

    //?Organization
    name(id){
        this.boxName = id;
        speedrun_global_boxes[this.boxName] = this;
        return this._();
    };
    redirect(path = './exampleOfSubdirectory', onClickOnly = true){
        if (!path) return this._();
        if (onClickOnly){
            this.detect(()=>{
                window.location.href = path;
            }, 'click', true);
        } else {
            window.location.href = path;
        };
        return this._();
    };
    contain(box, fit = false, orderedPositioning = false){
        if (Array.isArray(box)){
            for (let i in box){
                box[i] = this.#checkName(box[i]);
                this.children.push(box[i]);
            };
            box = [...new Set(this.children)];
            if (orderedPositioning){
                this.removeStartingXY = true;
                for (let i in box){
                    this.removeStartingXY = true;
                };
            };
            let smallest = Infinity;
            let smallestX = Infinity;
            let largestW = -Infinity;
            let smallestY = Infinity;
            let largestH = -Infinity;
            for (let i in box){
                if (box[i] && typeof box[i] === 'object'){
                    if (box[i].zIndex < smallest) smallest = box[i].zIndex;
                    if (box[i].x < smallestX) smallestX = box[i].x;
                    if (box[i].x + box[i].w > largestW) largestW = box[i].x + box[i].w;
                    if (box[i].y < smallestY) smallestY = box[i].y;
                    if (box[i].y + box[i].h > largestH) largestH = box[i].y + box[i].h;
                };
            };
            if (fit){
                this.x = smallestX;
                this.y = smallestY;
                this.w = largestW - this.x;
                this.h = largestH - this.y;
            };
            const recursiveCheck = (box, parent) => {
                if (box.length > 0){
                    for (let i in box){
                        if (box[i] && typeof box[i] === 'object'){
                            box[i].drawAgain = true;
                            box[i].container = parent;
                            box[i].containerMemory = {
                                init: true,
                                childInit: true,
                                wRatio: undefined,
                                hRatio: undefined,
                                prevX: null,
                                prevY: null,
                                prevW: null,
                                prevH: null,
                            };
                            box[i].trueValues();
                            parent.drawAgain = true;
                            if (box[i].children.length > 0){
                                recursiveCheck(box[i].children, box[i])
                            };
                        };
                    };
                };
            };
        this.trueValues();
        recursiveCheck(box, this);
        this.zIndex = smallest - 5;
        } else{
            console.log("Error: Box is not an array.")
        };
        return this._();
    };
    within(container, fit = false, orderedPositioning = false){
        container = this.#checkName(container);
        if (container !== this) container.contain([this], fit, orderedPositioning);
        return this._();
    };
    markAsNew(){
        _markAsNew = true;
        return this._();
    };
    listNew(callback){
        let newObjs = [];
        for (let i in speedrun_global){
            if (speedrun_global[i] && speedrun_global[i].markedAsNew){
                newObjs.push(speedrun_global[i]);
            };
        };
        if (callback) callback(newObjs);
        return this._();
    };
};

const box = () => {
    return new Box();
};