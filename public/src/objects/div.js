clientFunc.div = (parent) => {
    //?Divs and Spans
    if (parent.divProperties?.state){
        let a = parent.divProperties;
        parent.changeObj(a);
        parent.createObj(a, ()=>{
            if (a.spanMode){
                a.obj = createHTMLElement('span', a, `<p>${a.text}</p>`);
            } else {
               a.obj = createHTMLElement('div', a, `<p>${a.text}</p>`);
            };
            a.obj.style['outline'] = 'none';
            a.obj.style['overflow'] = 'initial';
            a.obj.style['box-shadow'] = 'none';
            a.obj.style['position'] = 'absolute';
        });
        parent.positionLogic(a);
        a.obj.style['z-index'] = `${parent.zIndex-1}`;
        a.obj.style['width'] = `${parent.trueW+2}px`;
        a.obj.style['height'] = `${parent.trueH+2}px`;
        parent.injectionLogic(a);
    };
};

Box.prototype.div = function(text = ''){
    let a = this.defineObject('div');
    a.state = true;
    a.text = text;
    if (a.obj) a.changeObj = true;
    a.spanMode = false;
    return this;
};
Box.prototype.span = function(text = ''){
    let a = this.defineObject('div');
    a.state = true;
    a.text = text;
    if (a.obj) a.changeObj = true;
    a.spanMode = true;
    return this;
};
Box.prototype.divInjectCSS = function(key, value){
    let a = this.defineObject('div');
    a.injection.push({
        state: true,
        key: key,
        value: value
    });
    return this;
};
Box.prototype.spanInjectCSS = function(key, value){
    this.divInjectCSS(key, value);
    return this;
};

Box.prototype.overflow = function(modeX = 'visible', modeY = modeX){
    //? Modes: visible, hidden, scroll, scrollX, scrollY
    this.divInjectCSS('overflow-x', modeX);
    this.divInjectCSS('overflow-y', modeY);
    return this;
};