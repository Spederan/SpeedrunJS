clientFunc.colorPicker = (parent) => {
    if (parent.colorPickerProperties?.state){
        let a = parent.colorPickerProperties;
        parent.changeObj(a);
        parent.createObj(a, () => {
            a.obj = createHTMLElement('input', a, {"type": "color", "value": 
                RGBToHex(a.defaultValue.r, a.defaultValue.g, a.defaultValue.b)});
            parent._clearRun();
            parent._run(()=>{
                a.rgba = hexToRGB(a.obj.value);
                if (a.callback && JSON.stringify(a.rgba) !== JSON.stringify(a.rgbaPrev)){
                    a.callback(a.rgba);
                    a.rgbaPrev = a.rgba;
                };
            });
        });
        parent.positionLogic(a);
        a.obj.style['z-index'] = `${parent.zIndex+4}`;
        a.obj.style['width'] = `${parent.trueW}px`;
        a.obj.style['height'] = `${parent.trueH}px`;
        parent.injectionLogic(a);
        return;
    };
};
Box.prototype.colorPicker = function(callback = null){
    let a = this.defineObject('colorPicker');
    a.state = true;
    a.callback = callback;
    return this;
};
Box.prototype.colorPickerColor = function(r = 0, g = r, b = r, a = 255){
    let c = this.defineObject('colorPicker');
    [r, g, b, a] = this.randomColor(r, g, b, a);
    c.defaultValue = {r: r, g: g, b: b};
    this.colorPickerInjectCSS('opacity', a/255);
    return this;
};
Box.prototype.colorPickerInjectCSS = function(key, value){
    let a = this.defineObject('colorPicker');
    a.injection.push({
        state: true,
        key: key,
        value: value
    });
    return this;
};