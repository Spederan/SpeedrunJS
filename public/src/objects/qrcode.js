Box.prototype.qrcode = function(text = 'Hello World'){
    let canvases = document.getElementsByTagName("canvas");
    for (let i in canvases){
        if (!canvases[i].id) canvases[i].id = randomString(10);
    };
    this.defineObject('div');
    let int = setInterval(() => {
        if (!this.qrcodeObj && this.divProperties.obj){
            this.qrcodeObj = new QRCode(this.divProperties.obj, {
                text: text,
                width: this.trueW,
                height: this.trueH,
            });
            //? Fix for bug in qrcodejs library, fixes resizing issues
            let canvases = document.getElementsByTagName("canvas");
            for (let i in canvases){
                if (!canvases[i].id) canvases[i].id = "canvas"+this.id;
            };
            let qrCanvas = document.getElementById("canvas"+this.id);
            qrCanvas.style["width"] = `${this.trueW}px`;
            qrCanvas.style["height"] = `${this.trueH}px`;
            qrCanvas.style["left"] = 0;
            qrCanvas.style["top"] = 0;
            clearInterval(int);
        };
    }, 10);
    return this;
};