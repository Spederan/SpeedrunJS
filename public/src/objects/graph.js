class Graph {
    constructor(w, h, type, options){
        if (type == 'pie') h = w;
        this.type = type;
        this.options = options;
        this.coordinates = [];
        this.displayLabelAsString = false;
        this.pieKeyLimit = 6;
        this.enableSlices = true; this.slices = [];
        this.enablePieKeys = true; this.pieKeys = [];
        this.enablePieLabels = true; this.pieLabels = [];
        this.enableBars = false; this.bars = [];
        this.enableHBars = false; this.hbars = [];
        this.enablePoints = false; this.points = [];
        this.enableSegments = false; this.segments = [];
        this.enableLines = true; this.lines = [];
        this.enableHeaders = true; this.headers = [];
        this.enableYTicks = true; this.yTicks = []; 
        this.enableXTicks = true; this.xTicks = []; 
        this.enableYLabels = true; this.yLabels = [];
        this.enableXLabels = true; this.xLabels = [];
        this.customXLabels = false;
        this.customYLabels = false;
        this.horizontal = false;
        this.segmentThickness = 2;
        this.initialSegment = true;
        this.pointSize = 6;
        this.numYTicks = 10;
        this.xTickSize = 4;
        this.numXTicks = 10;
        this.yTickSize = 4;
        this.numYLabels = 10;
        this.xLabelSize = 18;
        this.numXLabels = 10;
        this.yLabelSize = 15;
        this.title = '';
        this.xAxis = '';
        this.yAxis = '';
        this.size = 0.9;
        this.thickness = 2;
        this.padding = 10;
        this.margin = 150;
        this.precision = 100;
        this.textSize = 20;
        this.master = box()
            .size(w, h)
            .color(0,0,0,10)
            .divInjectCSS('overflow', 'hidden')
    };
    standardGraph(){
        let effectiveWidth = this.master.w - this.margin;
        let effectiveHeight = this.master.h - this.margin;
        let largest = 0;
        for (let i in this.coordinates){
            if (this.coordinates[i].y > largest) largest = this.coordinates[i].y;
        };
        let n = largest;
        largest = 0;
            for (let i in this.coordinates){
                if (this.coordinates[i].x > largest) largest = this.coordinates[i].x;
            };
        let n2 = largest;
        let x = Math.ceil((this.master.w - effectiveWidth)/2+this.padding);
        let y = 0;
        let w = (effectiveWidth/this.coordinates.length)*this.size;
        let h = effectiveHeight;
        //?Bars
        if (this.enableBars) 
        {
            for (let i in this.coordinates){
                if (i > 0){
                    x = this.bars[i-1].x + Math.ceil(this.bars[i-1].w/this.size);
                };
                h = Math.ceil(effectiveHeight*(this.coordinates[i].y/n)-this.padding);
                y = Math.ceil((this.master.h - effectiveHeight)/2 + (this.coordinates[i].y/n) + (effectiveHeight - h)-this.padding);
                this.bars[i] = box()
                    .position(x, y)
                    .size(w, h)
                    .color(0)
            this.coordinates[i].obj = this.bars[i];
            };
            this.master.contain(this.bars, false);
        };
        //?HBars
        if (this.enableHBars)
        {
            let x = 0;
            let y = Math.ceil((this.master.h - effectiveHeight)/2 - this.padding/2);
            let w = effectiveWidth;
            let h = (effectiveHeight/this.coordinates.length)*this.size;
            for (let i in this.coordinates){
                if (i > 0){
                    y = this.hbars[i-1].y + Math.ceil(this.hbars[i-1].h/this.size);
                };
                w = Math.ceil(effectiveWidth*(this.coordinates[i].x/n2)-this.padding);
                x = Math.ceil((this.master.w - effectiveWidth)/2 + (this.coordinates[i].x/n2) + this.padding);
                this.hbars[i] = box()
                    .position(x, y)
                    .size(w, h)
                    .color(0)
            this.coordinates[i].obj = this.hbars[i];
            };
            this.master.contain(this.hbars, false);
        }
        //?Segments
        if (this.enableSegments)
        {
            function sortObj(list, key) {
                function compare(a, b) {
                    a = a[key];
                    b = b[key];
                    var type = (typeof(a) === 'string' ||
                                typeof(b) === 'string') ? 'string' : 'number';
                    var result;
                    if (type === 'string') result = a.localeCompare(b);
                    else result = a - b;
                    return result;
                }
                return list.sort(compare);
            }
            this.coordinates = sortObj(this.coordinates, 'x');
            let x = 0;
            let prevX = 0;
            let y = 0;
            let prevY = 0;
            let w = 100;
            let h = this.thickness;
            let eWidth = effectiveWidth - this.padding;
            let eHeight = effectiveHeight - this.padding;
            let newCoordinates = ([...this.coordinates]);
            if (this.initialSegment){
                newCoordinates.unshift({x: 0, y: 0, obj: null, callback: null})
            };
            for (let i in newCoordinates){
                x = this.margin/2 + ((newCoordinates[i].x*eWidth)/(n2));
                y = this.margin/2 + eHeight-((newCoordinates[i].y*eHeight)/(n));
                this.segments[i] = box();
                this.segments[i]
                    .position(x, y)
                    .size(w, h)
                    .color(0)
                if (i > 0){
                    let deg = angle(x, y, prevX, prevY)
                    this.segments[i-1].rotate(deg);
                    let dist = measureDistance(this.segments[i-1].x, this.segments[i-1].y, this.segments[i].x, this.segments[i].y);
                    this.segments[i-1].size(dist, this.segmentThickness);
                    let newPoints = rotatePoint(this.segments[i-1].x, this.segments[i-1].y, this.segments[i-1].x + this.segments[i-1].w/2, this.segments[i-1].y + this.segments[i-1].h/2, deg);
                    this.segments[i-1].position((newPoints[0] - this.segments[i-1].w), (newPoints[1] - this.segments[i-1].h));
                    this.coordinates[i-1].obj2 = this.segments[i-1];
                };
                prevX = x;
                prevY = y;
            };
            this.segments.pop().remove();
        this.master.contain(this.segments, false);
        };
        //?Points
        if (this.enablePoints) 
        {
            let x = 0;
            let y = 0;
            let size = this.pointSize;
            let eWidth = effectiveWidth - this.padding;
            let eHeight = effectiveHeight - this.padding;
            for (let i in this.coordinates){
                x = this.margin/2 + ((this.coordinates[i].x*eWidth)/(n2));
                y = this.margin/2 + eHeight-((this.coordinates[i].y*eHeight)/(n));
                this.points[i] = box()
                    .position(x - size/2, y - size/2)
                    .size(size, size)
                    .color(0)
                    .curves(100)
                this.coordinates[i].obj = this.points[i];
            };
            this.master.contain(this.points, false);
        };
        //?Lines
        let heightOffset = (this.master.h - effectiveHeight)/2;
        if (this.enableLines) 
        {
            x = Math.ceil((this.master.w - effectiveWidth)/2);
            y = 0;
            for (let i = 0; i < 2; i++){
                this.lines[i] = box()
                    .color(0)
            };
            let height = (effectiveHeight + (this.master.h - effectiveHeight)/2);
            this.lines[0]
                .position(x, heightOffset)
                .size(this.thickness, height-heightOffset)
            this.lines[1]
                .position(x, height)
                .size(this.master.w - x*2 + this.thickness, this.thickness)
            this.master.contain(this.lines, false);
        };
        //?Y-Ticks
        if (this.enableYTicks)
        {
            let h = (effectiveHeight/this.coordinates.length)*this.size;
            let barOffset = 0;
            if (this.customYLabels){
                this.numYTicks = this.coordinates.length;
                barOffset = h/2;
            };
            for (let i = 0; i < this.numYTicks; i++){
                this.yTicks[i] = box()
                    .color(0)
                    .size(this.yTickSize, this.thickness)
                    .position(this.margin/2-this.yTickSize, this.margin/2 + ((effectiveHeight-this.padding)/this.numYTicks)*(i) + barOffset)
            if (this.customYLabels && !this.coordinates[i].y) this.yTicks[i].remove();
            };
            this.master.contain(this.yTicks, false);
        };
        //?X-Ticks
        if (this.enableXTicks)
        {
            let barOffset = 0;
            if (this.customXLabels){
                this.numXTicks = this.coordinates.length;
                barOffset = w/2;
            };
            for (let i = 0; i < this.numXTicks; i++){
                this.xTicks[i] = box()
                    .color(0)
                    .size(this.thickness, this.xTickSize)
                    .position(this.margin/2 + ((effectiveWidth)/this.numXTicks)*(i+1) - barOffset, effectiveHeight+this.margin/2+this.thickness)
            if (this.customXLabels && !this.coordinates[i].x) this.xTicks[i].remove();
            };
            this.master.contain(this.xTicks, false);
        };
        //?Y-Labels
        if (this.enableYLabels)
        {
            let h = (effectiveHeight/this.coordinates.length)*this.size;
            let barOffset = 0;
            let label;
            if (this.customYLabels){
                this.numYLabels = this.coordinates.length;
                barOffset = h/2;
            };
            for (let i = 0; i < this.numYLabels; i++){
                if (this.customYLabels){
                    label = this.coordinates[i].y;
                } else {
                    let j = this.numYLabels - i;
                    label = (Math.floor(((j*n/this.numYLabels))*this.precision)/this.precision)+'';
                };
                this.yLabels[i] = box()
                    .text(label, this.yLabelSize)
                    .divInjectCSS('overflow', 'hidden')
                    .textInjectCSS('overflow', 'hidden')
                    .size(this.margin/2)
                    .position(this.margin/8, this.margin/4 + this.textSize/2 - this.thickness + ((effectiveHeight)/this.numYLabels)*(i) - this.yLabelSize/2 + barOffset)
            if (this.customYLabels) this.coordinates[i].obj2 = this.yLabels[i];
            };
            this.master.contain(this.yLabels, false);
        };
        //?X-Labels
        if (this.enableXLabels)
        {
            let label = '';
            let barOffset = (effectiveWidth/this.numXLabels)*this.size;
            let width = barOffset*2;
            if (this.customXLabels){
                this.numXLabels = this.coordinates.length;
                barOffset = w;
                width = w;
            };          
            for (let i = 0; i < this.numXLabels; i++){
                if (this.customXLabels){
                    label = this.coordinates[i].x;
                } else {
                    label = (Math.floor(((i+1)*n2/this.numXLabels)*this.precision)/this.precision)+'';
                };
                this.xLabels[i] = box()
                    .text(label, this.xLabelSize)
                    .divInjectCSS('overflow', 'hidden')
                    .textInjectCSS('overflow', 'hidden')
                    .size(width, this.margin/2)
                    .position(this.margin/2 + ((effectiveWidth)/this.numXLabels)*(i+1) - barOffset, effectiveHeight+this.margin/2.5+this.thickness)
            if (this.customXLabels) this.coordinates[i].obj2 = this.xLabels[i];
            };
            this.master.contain(this.xLabels, false);
        };
    };
    pieGraph(){
        const loadArray = [];
        for (let i in this.coordinates){
            loadArray.push(this.coordinates[i].y);
        };
        const ratios = [...percentRound(loadArray)];

        if (this.enableSlices)
        {
            let rgb = {
                r: 0,
                g: 0,
                b: 0,
                a: 255,
            };
            let totalRotations = 0;
            for (let i in this.coordinates){
                rgb = {
                    r: random(0, 255),
                    g: random(0, 255),
                    b: random(0, 255),
                    a: 255,
                };
                this.slices[i] = box();
                this.slices[i]
                    .color(rgb.r, rgb.g, rgb.a, rgb.a)
                    .position(this.margin/2, this.margin/2)
                    .size(this.master.w - this.margin, this.master.h - this.margin)
                    .curves(100)
                    .boxInjectCSS('background', `conic-gradient(rgb(${rgb.r}, ${rgb.g}, ${rgb.b}, ${rgb.a/255}) ${ratios[i]}%, rgb(${255}, ${255}, ${255}, ${0}) 0%)`)
                    .run(()=>{
                        if ((this.slices[i].r != this.slices[i].g) || (this.slices[i].r != this.slices[i].b))
                            this.slices[i].boxInjectCSS('background', `conic-gradient(rgb(${this.slices[i].r}, ${this.slices[i].g}, ${this.slices[i].b}, ${this.slices[i].a/255}) ${ratios[i]}%, rgb(${255}, ${255}, ${255}, ${0}) 0%)`);
                    })
                if (i > 0){
                    totalRotations+= 360*(ratios[i-1]/100);
                    this.slices[i].rotate(totalRotations)
                };
                this.coordinates[i].obj = this.slices[i];
            };
            this.master.contain(this.slices, false);
        };
        if (this.enablePieLabels)
        {
            let rotate = 45;
            let prevRotate = 45;
            let textSize = this.master.w/30;
            let radii = 0;
            this.circle = [];
            for (let i in this.coordinates){
                this.circle[i] = box();
                radii = (this.master.w - this.margin)/2;
                if (radii < 0) radii = 0;
                this.circle[i]
                    .position(this.margin/2, this.margin/2)
                    .size(radii*2, radii*2)
                    .curves(100)

                this.pieLabels[i] = box();
                let displayedText = '';
                this.displayLabelAsString ? displayedText = this.coordinates[i].x : displayedText = ratios[i]+'%';
                this.pieLabels[i]
                    .position(this.master.w/2 - radii + this.padding, this.master.h/2 - radii + this.padding)
                    .size(radii)
                    .text(displayedText, textSize)
                    
                this.coordinates[i].obj2 = this.pieLabels[i];
                this.circle[i].contain([this.pieLabels[i]], false)

                rotate+= (360*(ratios[i]/100));
                let deg = (rotate + prevRotate)/2;
                prevRotate = rotate;
                this.circle[i].rotate(deg);
                this.pieLabels[i].rotate(-deg);
            };
            this.master.contain(this.circle, false);
            for (let i in this.coordinates){
                this.circle[i].divInjectCSS('width', '0px');
                this.circle[i].divInjectCSS('height', '0px');
            };
        };
        if (this.enablePieKeys)
        {
            this.pieDots = [];
            this.dotSize = this.master.w/60;
            let lowerBound = 40;
            if (this.coordinates.length > 12) lowerBound = (this.coordinates.length)*3;
            if (lowerBound > 66) lowerBound = 66;
            this.pieDotTextSize = this.master.w/lowerBound;
            let yOffset = 0;
            let size = this.pieDotTextSize*6;
            let limit;
            limit = Math.floor(this.master.w/(size));
            if (this.pieKeyLimit < limit) limit = this.pieKeyLimit;
            let effectiveLimit = limit;
            let len = limit;
            if (this.coordinates.length < len) len = this.coordinates.length;
            for (let i in this.coordinates){    
                if (i >= effectiveLimit){
                    yOffset+= size/2;
                    if (this.coordinates.length < 13) len = this.coordinates.length - (effectiveLimit); 
                    effectiveLimit+= limit;
                };
                this.pieKeys[i] = box()
                    .size(size, size/2)
                    .position(this.master.w/2 + size*(i%limit) - (size*(len)/2), this.master.h - size/2 - this.padding - yOffset)
                    .text(this.coordinates[i].x, this.pieDotTextSize)

                this.pieDots[i] = box();
                this.pieDots[i]
                    .size(this.dotSize, this.dotSize)
                    .position(this.pieKeys[i].x + size/2 - this.dotSize/2, this.pieKeys[i].y + this.pieKeys[i].h/2 + this.pieDotTextSize - this.dotSize/2)
                    .color(255,0,0)
                    .outline(255)
                    .curves(100)
                    .run(()=>{
                        this.pieDots[i]
                            .color(this.slices[i].r, this.slices[i].g, this.slices[i].b)
                            .outline(this.slices[i].textColors.r, this.slices[i].textColors.g, this.slices[i].textColors.b)
                    })
            };
            this.master.contain(this.pieKeys, false);
            this.master.contain(this.pieDots, false);
        };
    };
    structure(){
        if (this.type === 'bar'){
            this.enableBars = true;
            this.customXLabels = true;
        };
        if (this.type === 'hbar'){
            this.enableHBars = true;
            this.customYLabels = true;
        };
        if (this.type === 'scatter' || this.type === 'line'){
            this.enablePoints = true;
            this.padding = 0;
            if (this.type === 'line'){
                this.enableSegments = true;
            };
        };
        if (this.type === 'pie'){
            this.margin = 200;
        };
        let keys = Object.keys(this.options);
        let i = 0;
        for (let k in this.options){
            this[keys[i]] = this.options[k];
            i++;
        };
        switch(this.type){
            default: this.standardGraph(); break;
            case 'pie': this.pieGraph(); break;
        };
        //?Headers
        if (this.enableHeaders) 
        {
            let effectiveHeight = this.master.h - this.margin;
            let heightOffset = (this.master.h - effectiveHeight)/2;
            let hOffset = heightOffset/2;
            for (let i = 0; i < 3; i++){
                this.headers[i] = box();
                this.headers[i]
                    .size(this.master.w+this.thickness, hOffset+this.padding)
            };
            this.headers[0].text(this.title, this.textSize+this.padding);
            this.headers[1]
                .text(this.xAxis, this.textSize)
                .position(0, this.master.h-hOffset)
                .size(this.master.w+this.thickness, hOffset)
            this.headers[2]
                .text(this.yAxis, this.textSize)
                .rotate(-90)
                .size(this.master.h+2, hOffset)
                .position(-this.master.h/this.thickness + hOffset/2, this.master.h/2-hOffset/2)
            this.master.contain(this.headers, false);
        };
        for (let i in this.coordinates){
            if (this.coordinates[i].callback){
                this.coordinates[i].callback(this.coordinates[i].obj, this.coordinates[i].obj2);
            };
        };
    };
    loadOptions(data, objInject){
        for (let i in data){
            let xData = 0;
            let yData = 0;
            let callbackData = null;
            if (!data[i]) break;
            if (typeof data[i] === 'object'){
                xData = data[i][0];
                yData = data[i][1];
                if (data[i][2]) callbackData = data[i][2];
            };
            this.coordinates[i] = {
                x: xData,
                y: yData,
                callback: callbackData,
                obj: null,
                obj2: null,
            };
        };
        this.structure();
        if (this.callback){
            this.callback(this);
        };
        if (objInject) objInject(this);
    };
};

clientFunc.graph = (parent) => {
    if (parent.graphProperties?.state){
        let a = parent.graphProperties;
        parent.changeObj(a);
        if (!a.obj){
            a.obj = new Graph(parent.trueW, parent.trueH, a.type, a.options);
            parent._();
        };
        if (a.obj?.master?.parentElement){
            if (a.parentInit){
                parent.parentElement.appendChild(a.obj.master.parentElement);
                a.obj.loadOptions(a.data, parent.objectInject);
                a.parentInit = false;
            };
        } else {
            parent._();
        };
        return;
    };
};

Box.prototype.graph = function(data, type = 'bar', options = {}){
    let a = this.defineObject('graph');
    a.state = true;
    a.data = data;
    a.type = type;
    a.options = options;
    return this._();
};