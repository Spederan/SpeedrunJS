class MediaPlayer {
    constructor(trueW, trueH, type = 'audio', obj, parent, objInject, toolbarOffset = 0){
        this.objInject = objInject;
        this.trueW = trueW;
        this.trueH = trueH;
        this.funcType = type;
        switch(type){
            case 'audio':
                this.type = 'audioProperties';
            break;
            case 'video':
                this.type = 'videoProperties';
            break;
        };
        this._properties = obj;
        this._properties.listCount = 0;
        this.parent = parent;
        this.master = box()
            .size(this.trueW, this.trueH)
            .containerPosition(u, toolbarOffset)
        this.master.buttonWidth = 45;
        this.master.buttonHeight = 35;
        this.master.toolHeight = 50;
        let buttonWidth = this.master.buttonWidth;
        let buttonHeight = this.master.buttonHeight;
        let toolHeight = this.master.toolHeight;
        this.drawAgain = true;
        this.master.drawAgain = true;
        this.master.hideElements = () => {
            this.master.hideElements.state = true;
            if (!this.toolbar.hideGraphics) this.toolbar.hide();
            if (!this.timebar.hideGraphics) this.timebar.hide();
            if (!this.nextbutton.hideGraphics) this.nextbutton.hide();
            if (!this.playbutton.hideGraphics) this.playbutton.hide();
            if (!this.volumeicon.hideGraphics) this.volumeicon.hide();
            if (!this.volumebar.hideGraphics) this.volumebar.hide();
            if (!this.videoduration.hideGraphics) this.videoduration.hide();
            if (!this.fullscreen.hideGraphics) this.fullscreen.hide();
            if (!this.fastforward.hideGraphics) this.fastforward.hide();
            if (!this.spedup.hideGraphics) this.spedup.hide();
            if (!this.jumpforward.hideGraphics) this.jumpforward.hide(false);
            if (!this.jumpbackward.hideGraphics) this.jumpbackward.hide(false);
        };
        this.master.hideElements.state = false;
        this.master.showElements = () => {
            this.master.hideElements.state = false;
            if (this.toolbar.hideGraphics) this.toolbar.show();
            if (this.timebar.hideGraphics) this.timebar.show();
            if (this.nextbutton.hideGraphics) this.nextbutton.show();
            if (this.playbutton.hideGraphics) this.playbutton.show();
            if (this.volumeicon.hideGraphics) this.volumeicon.show();
            if (this.videoduration.hideGraphics) this.videoduration.show();
            if (this.fullscreen.hideGraphics) this.fullscreen.show();
            if (this.fastforward.hideGraphics) this.fastforward.show();
        };
        //?Added space
        this.buffer = box()
            .color(0,0,0,0)
            .outline(0,0,0,0)
            .size(this.trueW, this.trueH)
        //?Toolbar
        this.toolbar = box()
            .curves(0)
            .size(this.trueW, toolHeight);
        //?Nextbutton
        this.nextbutton = box()
            .image('./src/images/nextButton.png', true)
            .detect(() => {
                this.parent[this.funcType](this._properties.list[this._properties.listCount], this._properties.autoplay, true);
                this._properties.ref.currentTime = 0;
                this.timebar.sliderProperties.slide.updateValue(0);
                this.timebar.sliderProperties.obj.value = 0;
                console.log(this._properties.listCount, this._properties.list)
                this._properties.listCount = (this._properties.listCount + 1) % this._properties.list.length;
                this.master.hideTimer = 0;
            }, 'press')
            .imageInjectCSS('opacity', '0.7')
            .detect(() => {
                this.nextbutton.imageInjectCSS('opacity', '1');
                this.master.hideTimer = 0;
            }, 'over')
            .detect(() => {
                this.nextbutton.imageInjectCSS('opacity', '0.7');
                this.master.hideTimer = 0;
            }, 'out')
            .size(buttonWidth, buttonHeight)
        this.nextbutton.relativeContainerSize = false;
        //?Playbutton
        this.playbutton = box();
        this._properties.autoplay ? this.playbutton.image('./src/images/pauseButton.png', true) : this.playbutton.image('./src/images/playButton.png', true);
        let playFirst = true;
        this.playbutton.detect(()=>{
            if (playFirst){
                this.playbutton.image('./src/images/pauseButton.png', true);
                this._properties.obj.play();
                this._properties.ref.currentTime = 1;
                playFirst = false;
            } else {
                if (this._properties.ref.paused){
                    this.playbutton.image('./src/images/pauseButton.png', true);
                    this._properties.obj.play();
                } else {
                    this.playbutton.image('./src/images/playButton.png', true);
                    this._properties.obj.pause();
                };
            };
            this.master.hideTimer = 0;
        }, 'press', true);
        this.buffer.detect(()=>{
            if (this._properties.ref.paused){
                this.playbutton.image('./src/images/pauseButton.png', true);
                this._properties.obj.play();
            } else {
                this.master.hideTimer = 0;
                this.master.showElements();
                this.jumpforward.show();
                this.jumpbackward.show();
                this.playbutton.image('./src/images/playButton.png', true);
                this._properties.obj.pause();
            };
            this.master.hideTimer = 0;
        }, 'press', true);
        this.playbutton.imageInjectCSS('opacity', '0.7');
        this.playbutton.detect(() => {
            this.playbutton.imageInjectCSS('opacity', '1');
            this.master.hideTimer = 0;
        }, 'over');
        this.playbutton.detect(() => {
            this.playbutton.imageInjectCSS('opacity', '0.7');
            this.master.hideTimer = 0;
        }, 'out');  
        this.playbutton.relativeContainerSize = false;
        this.playbutton.drawAgain = true;
        this.playbutton.size(buttonWidth, buttonHeight);
        //?Video Duration
        this.videoduration = box();
        this.videoduration
            .color(0,0,0,0)
            .text('0:00', 17)   
            .size(150,25)   
            .run(()=>{
                if (!this.master.hideElements.state){
                    if (this.videoduration.offsetPosition){
                        this.videoduration.delayTruth = false;
                        this.videoduration.trueValues();
                        if (!this.fullscreen?.enableFullscreen) this.videoduration.tempPosition(this.videoduration.trueX+75);
                    };
                };
            })    
        this.videoduration.relativeContainerSize = false;
        this.videoduration.offsetPosition = false;
        
        //?Volume Icon 
        this.volumeicon = box();
        this.volumeicon
            .color(0,0,0,0)
            .size(buttonWidth, buttonHeight)
            .imageInjectCSS('opacity', '0.7')
            .detect(() => {
                this.volumeicon.imageInjectCSS('opacity', '1');
                this.volumebar.show();
                this.videoduration.offsetPosition = true;
                this.volumebar.hideTimer = 0;
                this.master.hideTimer = 0;
            }, 'move')
            .detect(() => {
                this.volumeicon.imageInjectCSS('opacity', '0.7');
                this.master.hideTimer = 0;
            }, 'out')
            .detect(() => {
                if (this._properties.volume <= 0){
                    this._properties.volume = 1;
                } else {
                    this._properties.volume = 0;
                };
                this.master.hideTimer = 0;
            }, 'press')
        this._properties.autoplay ? this.volumeicon.image('./src/images/muteButton.png', true) : this.volumeicon.image('./src/images/volumeButton.png', true);
        this.volumeicon.relativeContainerSize = false;
        //?Volume Bar
        this.volumebar = box();
        this.volumebar.color(0,0,0,0);
        this.volumebar.audioProperties_prevValue = 0;
        this.volumebar.slider((value)=>{
            if (this.volumebar.audioProperties_prevValue === value) return;
            this.videoduration.offsetPosition = true;
            if (value < 0) value = 0;
            if (value > 1) value = 1;
            this._properties.volume = value;
            if (value > 0){
                this.volumeicon.image('./src/images/volumeButton.png', true);
            } else {
                this.volumeicon.image('./src/images/muteButton.png', true);
            };
            this.master.hideTimer = 0;
            this.volumebar.hideTimer = 0;
            this.volumebar.audioProperties_prevValue = value;
        });
        this.volumebar.hideTimer = 0;
        this.runOnceForVolumeBar = true;
        this.volumebar.run(()=>{
            if (this.volumebar.hideTimer++ > 160){
                if (!this.volumebar.hideGraphics) this.volumebar.hide();
                this.volumebar.hideTimer = 0;
                this.videoduration.offsetPosition = false;
            };
            if (this.runOnceForVolumeBar && this.volumebar?.sliderProperties?.obj){
                this.volumebar.sliderProperties.obj.addEventListener("mouseover", ()=>{
                    this.videoduration.offsetPosition = true;
                    if (this.volumebar.hideGraphics) this.volumebar.show();
                });
                this.runOnceForVolumeBar = false;
            };
            if (this.prevVolume !== this._properties.volume){
                this._properties.volume > 0 ? this.volumeicon.image('./src/images/volumeButton.png', true) : this.volumeicon.image('./src/images/muteButton.png', true);
                this.volumebar?.sliderProperties?.slide?.updateValue(this._properties.volume);
                this.prevVolume = this._properties.volume;
            };
        });     
                    
        this.volumebar.sliderSize(5, 11);
        this.volumebar.drawAgain = true;
        this.volumebar.relativeContainerSize = false;
        this.volumebar.size(75,75);
        //?Fullscreen
        this.fullscreen = box();
        this.fullscreen
            .color(0,0,0,0)            
            .image('./src/images/fullscreenButton.png', true)
            .imageInjectCSS('opacity', '0.7')
            .detect(() => {
                this.fullscreen.imageInjectCSS('opacity', '1');
                this.master.hideTimer = 0;
            }, 'over')
            .detect(() => {
                this.fullscreen.imageInjectCSS('opacity', '0.7');
                this.master.hideTimer = 0;
            }, 'out')
        this.fullscreen.enableFullscreen = false;
        this.fullscreen.detect(() => {
            this.master.hideElements();
            this.master.drawAgain = true;
            if (!this.fullscreen.enableFullscreen){
                this.fullscreen.enableFullscreen = true;
                this.fullscreen.image('./src/images/minimizeButton.png', true);
            } else {
                this.fullscreen.enableFullscreen = false;
                this.fullscreen.image('./src/images/fullscreenButton.png', true);
            };    
            this.master.hideTimer = 0;                    
        }, 'press');
        this.fullscreen.relativeContainerSize = false;
        this.fullscreen.drawAgain = true;
        this.fullscreen.size(buttonWidth, buttonHeight);
        //?Spedup Text
        this.spedup = box()
            .color(0,0,0,0)
            .text('x1',10)
            .textColor(255)
            .size(buttonWidth/2, buttonHeight)
            .hide()
        //?Timebar
        this.timebar = box()
            .slider((value)=>{
                if (!this._properties.ref.duration) return;
                let time = +value*+this._properties.ref.duration;
                if (time === this._properties.prevTime) return;
                this._properties.ref.currentTime = time;
                this._properties.prevTime = time;
                this.master.hideTimer = 0;
            })     
            .sliderSize(4, 0)
            .sliderColor(255, 0, 0, 255, true)
            .size(this.trueW, this.trueH)
        this.timebarStarted = false;
        let prevTime = '';
        const updateTimebar = () => {
            if (prevTime !== this._properties.ref.currentTime){
                this.timebar.drawAgain = true;
                this.timebar.sliderProperties.slide.updateValue(this._properties.ref.currentTime/this._properties.ref.duration);
                prevTime = this._properties.ref.currentTime;
            };
        };
        this.timebar.run(()=>{
            if (!this.timebarStarted) {
                this._properties.ref.currentTime = 0;
                updateTimebar();
                this.timebarStarted = true;
            };
            if (!this.master.hideElements.state || this.master.hideTimer < 2){
                updateTimebar();
            };
        })
        this.timebar.thumbTransparency = 0;
        this.timebar.sliderOverEffect(()=>{
            this.timebar.sliderSize(8, 14);
            this.timebar.sliderThumbColor(this._properties.r,this._properties.g,this._properties.b,255);
            this.timebar.thumbTransparency = 255;
        });
        this.timebar.sliderOutEffect(()=>{
            this.timebar.sliderSize(4);
            this.timebar.sliderThumbColor(this._properties.r,this._properties.g,this._properties.b,0);
            this.timebar.thumbTransparency = 0;
        });
        //?Fast Forward
        this.fastforward = box();
        this.fastforward
            .color(0,0,0,0)
            .image('./src/images/fastforwardButton.png')           
            .imageInjectCSS('opacity', '0.7')
            .size(buttonWidth, buttonHeight)
            .detect(() => {
                this.fastforward.imageInjectCSS('opacity', '1');
                this.spedup.show();
                this.master.hideTimer = 0;
            }, 'over')
            .detect(() => {
                this.fastforward.imageInjectCSS('opacity', '0.7');
                this.spedup.hide();
                this.master.hideTimer = 0;
            }, 'out')
            .detect(() => {
                let uppidy;
                this._properties.ref.playbackRate >= 3 ? uppidy = 1 : uppidy = 0.25;
                this._properties.ref.playbackRate+= uppidy;
                if (this._properties.ref.playbackRate > 10) this._properties.ref.playbackRate = 0.25;
                if (this._properties.ref.playbackRate > 5) this._properties.ref.playbackRate = 10;
                this.spedup.text(`x${this._properties.ref.playbackRate}`);
                this._properties.ref.defaultPlaybackRate = this._properties.ref.playbackRate;
                this.master.hideTimer = 0;
            }, 'press')
        this.fastforward.relativeContainerSize = false;
        //?Jump Forwards
        this.jumpforward = box();
        this.jumpforward        
            .curves(100)
            .outline(0,0,0,0)
            .image('./src/images/jumpforwardButton.png')
            .imageInjectCSS('opacity', '0.7')
            .size(50, 50)
            .text('+5',11)
            .textFont('monospace')
            .textColor(this._properties.r,this._properties.g,this._properties.b,200)
            .detect(()=>{
                this.jumpforward.imageInjectCSS('opacity', '1');
                this.jumpforward.color(this._properties.r/2,this._properties.g/2,this._properties.b/2,75);
                this._properties.ref.currentTime+= 5;
                this.master.hideTimer = 0;
            }, 'press', true)
            .detect(()=>{
                this.jumpforward.imageInjectCSS('opacity', '0.7');
                this.jumpforward.color(this._properties.r2/2,this._properties.g2/2,this._properties.b2/2,75);
                this.master.hideTimer = 0;
            }, 'release', true)
            .detect(()=>{
                this.master.hideTimer = 0;
                this.jumpbackward.show();
                this.jumpforward.show();
                this.master.showElements();
            }, 'move', true)
            .detect(()=>{
                this.jumpforward.imageInjectCSS('opacity', '0.7');
                this.jumpforward.color(this._properties.r2/2,this._properties.g2/2,this._properties.b2/2,75);
                this.master.hideTimer = 0;
                this.jumpbackward.hide(false);
                this.jumpforward.hide(false);
            }, 'out', true)
        this.jumpforward.relativeContainerSize = false;
        //?Jump Backwards
        this.jumpbackward = box();
        this.jumpbackward          
            .curves(100)
            .outline(0,0,0,0)
            .image('./src/images/jumpbackwardButton.png')
            .imageInjectCSS('opacity', '0.7')
            .size(50, 50)
            .text('-5',11)
            .textFont('monospace')
            .textColor(this._properties.r,this._properties.g,this._properties.b,200)
            .detect(()=>{
            this.jumpbackward.imageInjectCSS('opacity', '1');
            this.jumpbackward.color(this._properties.r/2,this._properties.g/2,this._properties.b/2,75);
            this._properties.ref.currentTime-= 5;
            this.master.hideTimer = 0;
        }, 'press', true)
        .detect(()=>{
            this.jumpbackward.imageInjectCSS('opacity', '0.7');
            this.jumpbackward.color(this._properties.r2/2,this._properties.g2/2,this._properties.b2/2,75);
            this.master.hideTimer = 0;
        }, 'release', true)
        .detect(()=>{
            this.jumpbackward.show();
            this.jumpforward.show();
            this.master.hideTimer = 0;
            this.master.showElements();
        }, 'move', true)
        .detect(()=>{
            this.jumpbackward.imageInjectCSS('opacity', '0.7');
            this.jumpbackward.color(this._properties.r2/2,this._properties.g2/2,this._properties.b2/2,75);
            this.master.hideTimer = 0;
            this.jumpbackward.hide(false);
            this.jumpforward.hide(false);
        }, 'out', true)
        this.jumpbackward.relativeContainerSize = false;
        //?Define Positions
        this.master.setPositions = () => {
            this.videoduration.position(buttonWidth*0.8*2.75, (this.trueH-toolHeight)+12.5);
            this.toolbar.position(0, (this.trueH-toolHeight));
            this.timebar.position(0, (this.trueH-toolHeight-5));
            this.playbutton.position(0, (this.trueH-toolHeight)+5);
            this.nextbutton.position(this.playbutton.x + buttonWidth*0.8, (this.trueH-toolHeight)+5);
            this.volumeicon.position(0 + buttonWidth*0.8*2, (this.trueH-toolHeight)+5);
            this.volumebar.position(buttonWidth*0.8*3.25, (this.trueH-toolHeight)+20-5);
            this.fullscreen.position(this.trueW - 50, (this.trueH-toolHeight)+7);
            this.fastforward.position(this.trueW - 85, (this.trueH-toolHeight)+7);
            this.spedup.position(this.trueW - 60, (this.trueH-toolHeight)+7+8);
            this.jumpforward.position(this.trueW - 50, this.trueH/2-50/2);
            this.jumpbackward.position(0, this.trueH/2-50/2);
        };
        this.master.setPositions();
        //?Containment
        this.master.containerizeElements = () => {
            this.master.contain([this.buffer, this.toolbar, this.timebar, this.playbutton, this.nextbutton, this.volumeicon, this.volumebar, this.videoduration, this.fullscreen, this.fastforward, this.spedup, this.jumpforward, this.jumpbackward], false);
        };
        this.master.containerizeElements();
        this.master.hideElements();
        this.master.hideTimer = 0;
        this.master.run(()=>{
            if (this.master.hideTimer++ > 180){
                this.master.hideElements();
            };
            if (!this.master.hideElements.state){
                this.jumpbackward.color(this._properties.r2,this._properties.g2,this._properties.b2,75);
                if (this._properties.colorDeg){
                    this.jumpbackward.imageColor(this._properties.colorDeg);
                    this.fastforward.imageColor(this._properties.colorDeg);
                    this.fullscreen.imageColor(this._properties.colorDeg);
                    this.volumeicon.imageColor(this._properties.colorDeg);
                    this.playbutton.imageColor(this._properties.colorDeg);
                    this.nextbutton.imageColor(this._properties.colorDeg);
                };
                this.jumpforward.color(this._properties.r2/2,this._properties.g2/2,this._properties.b2/2,75);
                this.volumebar.sliderColor(this._properties.r,this._properties.g,this._properties.b,255);
                this.videoduration.textColor(this._properties.r3,this._properties.g3,this._properties.b3, this._properties.a3);
                this.videoduration.textOutline(this._properties.r4,this._properties.g4,this._properties.b4, this._properties.a4, this._properties.weight4);
                this.timebar.sliderColor(this._properties.r,this._properties.g,this._properties.b,255, true);
                this.timebar.sliderThumbColor(this._properties.r,this._properties.g,this._properties.b,this.timebar.thumbTransparency);
                this.toolbar.color(this._properties.r2,this._properties.g2,this._properties.b2,100);
                let currentTime = Math.floor(this._properties.ref.currentTime);
                let duration = Math.floor(this._properties.ref.duration);
                this.videoduration.text(this.assembleText(currentTime, duration), 17);
                this._properties.ref.volume = this._properties.volume;
                if (!this._properties.ref.paused){
                    this.playbutton.image('./src/images/pauseButton.png', true);
                } else {
                    this.playbutton.image('./src/images/playButton.png', true);
                };
            };
                if (this.type === 'videoProperties') this._properties.obj.style['object-fit'] = 'contain';
                if (this._properties.parentInit2 && this.master?.parentElement && this.parent.parentElement && !this.fullscreen.enableFullscreen){
                    this.parent.parentElement.appendChild(this.master.parentElement);
                    this.master.parentElement.style['top'] = '0';
                    this.master.parentElement.style['left'] = '0';
                    this._properties.parentInit2 = false;
                };
                if (this.parent.container && this._properties.enableFullscreen && !this.fullscreen.enableFullscreen){
                    this.parent.container.parentElement.appendChild(this.parent.parentElement);
                };       
                if (this._properties.player && this.fullscreen.enableFullscreen){
                    //?Perform this once, if the aformentioned value hasen't been updated yet
                    if (!this._properties.enableFullscreen){
                        pageDiv.appendChild(this.master.parentElement);
                        this._properties.parentInit2 = true;
                        this.parent.parentElement.style['top'] = '0';
                        this.parent.parentElement.style['left'] = '0';
                        this.parent.parentElement.style['width'] = `${windowWidth}px`;
                        this.parent.parentElement.style['height'] = `${windowHeight}px`;
                        if (this.parent.rotation?.deg != 0) this.parent.rotate(0);
                        if (this.parent.container){
                            pageDiv.appendChild(this.parent.parentElement);
                        };
                    };
                    if (this.type === 'videoProperties'){
                        this._properties.parentInit = true;
                        pageDiv.appendChild(this._properties.obj);
                        this._properties.obj.style['top'] = '0';
                        this._properties.obj.style['left'] = '0';
                        this._properties.obj.style['width'] = `${windowWidth}px`;
                        this._properties.obj.style['height'] = `${windowHeight}px`;
                        this._properties.obj.style['object-fit'] = 'cover';
                    };
                    let yPos = windowHeight-50;
                    let yPos2 = windowHeight/2-50/2;
                    let yPos3 = 0;
                    let xPos = 0;
                    this.buffer.tempPosition(xPos, yPos3, windowWidth, windowHeight);
                    this.toolbar.tempPosition(xPos, yPos, windowWidth);
                    this.timebar.tempPosition(xPos, yPos-5, windowWidth);
                    this.playbutton.tempPosition(xPos, yPos+5);
                    this.nextbutton.tempPosition(this.playbutton.trueX + this.master.buttonWidth*0.8, yPos+5);
                    this.volumeicon.tempPosition(xPos + this.master.buttonWidth*0.8*2, yPos+5);
                    this.volumebar.tempPosition(xPos + this.master.buttonWidth*0.8*3.25, yPos+20-5);
                    if (this.videoduration.offsetPosition){
                        this.videoduration.tempPosition(xPos + this.master.buttonWidth*0.8*2.75 + 75, yPos+12.5);
                    } else {
                        this.videoduration.tempPosition(xPos + this.master.buttonWidth*0.8*2.75, yPos+12.5);
                    };
                    this.fullscreen.tempPosition(xPos+windowWidth - 50, yPos+7);
                    this.fastforward.tempPosition(xPos+windowWidth - 85, yPos+7);
                    this.spedup.tempPosition(xPos+windowWidth - 60, yPos+7+8);
                    this.jumpforward.tempPosition(xPos + windowWidth - 50, yPos2);
                    this.jumpbackward.tempPosition(xPos, yPos2);
                    this.master.drawAgain = true;
                };
                this._properties.enableFullscreen = this.fullscreen.enableFullscreen;
                if (this._properties.player && this._properties.ratio && this._properties.callback){
                    this._properties.callback(this);
                };
                if (this.objInject) this.objInject(this);
        });
        this.master.detect(()=>{
            this.parent.drawAgain = true;
        }, 'press');
        this.master.detect(()=>{
            this.parent.drawAgain = true;
        }, 'release');
        this.master.detect(()=>{
            this.parent.drawAgain = true;
        }, 'move');
        this.master.detect(()=>{
            this.parent.drawAgain = true;
        }, 'out');
        this.toolbar.detect(()=>{
            this.master.hideTimer = 0
            this.master.showElements();
        }, 'move', true);
        this.buffer.detect(()=>{
            this.master.hideTimer = 0
            this.master.showElements();
        }, 'move', true);
        this.master.counter = 0;
        this.assembleText = (currentTime, duration) => {
            if (currentTime <= 0) return '';
            //?Current Time Calculation
            {
                var seconds = Math.floor(currentTime)%60;
                var minutes = Math.floor(currentTime/60)%60;
                var hours = Math.floor(currentTime/60/60)%100;
                var hrsStr = hours+'';
                var minsStr = minutes+'';
                var secsStr = seconds+'';
                if (secsStr.length === 0) secsStr = '00';
                if (secsStr.length === 1) secsStr = '0'+secsStr;
                if (minsStr.length === 0) minsStr = '00';
                if (minsStr.length === 1) minsStr = '0'+minsStr;
                if (hrsStr.length === 0 || hours == '0') hrsStr = '';
                if (hrsStr.length === 1) hrsStr = '0'+hrsStr;
                if (hrsStr.length > 0) hrsStr+=':'
            };
            //?Duration Calculation
            {
                var seconds2 = Math.floor(duration)%60;
                var minutes2 = Math.floor(duration/60)%60;
                var hours2 = Math.floor(duration/60/60)%100;
                var hrsStr2 = hours2+'';
                var minsStr2 = minutes2+'';
                var secsStr2 = seconds2+'';
                if (secsStr2.length === 0) secsStr2 = '00';
                if (secsStr2.length === 1) secsStr2 = '0'+secsStr2;
                if (minsStr2.length === 0) minsStr2 = '00';
                if (minsStr2.length === 1) minsStr2 = '0'+minsStr2;
                if (hrsStr2.length === 0 || hours2 == '0') hrsStr2 = '';
                if (hrsStr2.length === 1) hrsStr2 = '0'+hrsStr2;
                if (hrsStr2.length > 0) hrsStr2+=':'
            };
            return `${hrsStr}${minsStr}:${secsStr} / ${hrsStr2}${minsStr2}:${secsStr2}`;
        };
        
    };
    run(trueW, trueH){
        this.trueW = trueW;
        this.trueH = trueH;
        this.master.size(this.trueW, this.trueH);  
        //?Duration
        this.master.drawAgain = true;
    };
};

clientFunc.mediaPlayer = (parent) => {
    //?Audio
    if (parent.audioProperties?.state){
        let a = parent.audioProperties;
        parent.changeObj(a, () => {
            speedrun_global_audio_count--;
        });
        parent.createObj(a, () => {
            a.obj = createHTMLElement('audio', a, {"preload": "metadata", "src": `${a.src}`}, 'audio'+speedrun_global_audio_count++);
            a.obj.autoplay = a.autoplay;
            if (a.loop) a.obj.loop = true;
            a.list.push(a.src);
            a.ref = document.querySelectorAll('audio')[a.id.replace('audio', '')];
            a.obj.load();
            a.loaded = false;
            a.obj.onloadstart = (e) => {
                a.obj.addEventListener('loadedmetadata', ()=>{
                    a.loaded = true;
                    a.ref.currentTime = 0;
                }, false);
            };
        });
        if (a.player && a.loaded){
            if (!a.playerObj) a.playerObj = new MediaPlayer(parent.trueW, parent.trueH, 'audio', a, parent, parent.objectInject, toolbarOffset);
            a.playerObj.run(parent.trueW, parent.trueH);
        };
        if (!a.playerObj?.master?.parentElement) parent._();
    };
    //?Video
    if (parent.videoProperties?.state){
        let a = parent.videoProperties;
        parent.changeObj(a, () => {
            speedrun_global_video_count--;
        });
        parent.createObj(a, () => {
            parent._();
            a.obj = createHTMLElement('video', a, {"preload": "auto", "src": `${a.src}`}, 'video'+speedrun_global_video_count++);
            a.list.push(a.src);
            a.ref = document.querySelectorAll('video')[a.id.replace('video', '')];
            if (a.autoplay){
                a.obj.volume = 0;
                a.obj.play();
                a.play = false;
            };
            a.obj.volume = a.volume;
            if (a.loop){
                a.obj.loop = true;
                a.loop = false;
            };
            a.obj.style['z-index'] = `${parent.zIndex+1}`;
            a.obj.style['position'] = `absolute`;
        });
        if (!a.ratio && a.obj.videoWidth > 0 && a.obj.videoHeight > 0){
            a.ratio = a.obj.videoWidth/a.obj.videoHeight;
            if (a.fit) parent.h = parent.w/(a.ratio);
            parent.trueValues();
        };
        //?Positioning and Sizing
        parent.positionLogic(a);
        if (a.ratio && !a.enableFullscreen){
            a.obj.style['width'] = `${parent.trueW}px`;
            a.obj.style['height'] = `${parent.trueH}px`;
        };
        parent.injectionLogic(a);
        //?Video Player Features
        if (a.player && a.ratio){
            if (!a.playerObj){
                a.playerObj = new MediaPlayer(parent.trueW, parent.trueH, 'video', a, parent, parent.objectInject, a.toolbarOffset);
                parent._();
            };
            a.playerObj.run(parent.trueW, parent.trueH);
        };
        if (!a.playerObj?.master?.parentElement) parent._();
        return;
    };
};

Box.prototype.audio = function(src, autoplay = true, active = true){
    let a = this.defineObject('audio');
    if (!src){if (!a.state) console.log("No audio file has been provided."); return this;};
    switch (active){
        case 'true' || true: active = true; break;
        case 'false' || false: active = false; break;
        case 'toggle': active = !a.state; break;
    };
    if (a.obj && src !== a.src){
        a.changeObj = true;
    };
    a.state = active;
    a.src = src;
    a.autoplay = autoplay;
    return this;
};
Box.prototype.video = function(src, autoplay = false, active = true){
    let a = this.defineObject('video');
    if (!src){ if (!a.state) console.log("No video file has been provided."); return this;};
    switch (active){
        case 'true' || true: active = true; break;
        case 'false' || false: active = false; break;
        case 'toggle': active = !a.state; break;
    };
    if (a.obj && src !== a.src){
        a.changeObj = true;
    };
    a.state = active;
    a.src = src;
    a.autoplay = autoplay;
    return this;
};
Box.prototype.mediaVolume = function(volume = 1){
    if (!this.mediaType){console.log('Audio/Video must be defined first.'); return this;};
    this[this.mediaType].volume = volume;
    return this._();
};
Box.prototype.mediaLoop = function(){
    if (!this.mediaType){console.log('Audio/Video must be defined first.'); return this;};
    this[this.mediaType].loop = true;
    return this._();
};
Box.prototype.mediaFit = function(){
    if (!this.mediaType){console.log('Audio/Video must be defined first.'); return this;};
    this.h = this.w/1.7778;
    return this._();
};
Box.prototype.mediaPlayer = function(toolbarOffset = 0){
    if (!this.mediaType){console.log('Audio/Video must be defined first.'); return this;};
    if (this.mediaType === 'videoProperties') this.mediaFit();
    this[this.mediaType].player = true;
    this[this.mediaType].toolbarOffset = toolbarOffset;
    return this._();
};
Box.prototype.mediaList = function(srcList){
    if (!this.mediaType){console.log('Audio/Video must be defined first.'); return this;};
    for (let i in srcList){
        this[this.mediaType].list.push(srcList[i]);
    };
    return this._();
};
Box.prototype.mediaPlayerPrimaryColor = function(r = 255, g = r, b = r){
    if (!this.mediaType){console.log('Audio/Video must be defined first.'); return this;};
    [r, g, b] = this.randomColor(r, g, b);
    let c = this[this.mediaType];
    c.r = r;
    c.g = g;
    c.b = b;
    return this._();
};
Box.prototype.mediaPlayerSecondaryColor = function(r = 0, g = r, b = r){
    if (!this.mediaType){console.log('Audio/Video must be defined first.'); return this;};
    [r, g, b] = this.randomColor(r, g, b);
    let c = this[this.mediaType];
    c.r2 = r;
    c.g2 = g;
    c.b2 = b;
    return this._();
};
Box.prototype.mediaPlayerTertiaryColor = function(colorDeg){
    if (!this.mediaType){console.log('Audio/Video must be defined first.'); return this;};
    this[this.mediaType].colorDeg = colorDeg;
    return this._();
};
Box.prototype.mediaPlayerTextColor = function(r = 0, g = r, b = r, a = 255){
    if (!this.mediaType){console.log('Audio/Video must be defined first.'); return this;};
    [r, g, b, a] = this.randomColor(r, g, b, a);
    let c = this[this.mediaType];
    c.r3 = r;
    c.g3 = g;
    c.b3 = b;
    c.a3 = a;
    return this._();
};
Box.prototype.mediaPlayerTextOutline = function(r = 255, g = r, b = r, a = 255, weight = 1){
    if (!this.mediaType){console.log('Audio/Video must be defined first.'); return this;};
    [r, g, b, a] = this.randomColor(r, g, b, a);
    let c = this[this.mediaType];
    c.r4 = r;
    c.g4 = g;
    c.b4 = b;
    c.a4 = a;
    c.weight4 = weight;
    return this._();
};
Box.prototype.mediaPropertiesPlayerInject = function(callback = null){
    if (!this.mediaType){console.log('Audio/Video must be defined first.'); return this;};
    this[this.mediaType].callback = callback;
    return this._();
};