class Tab {
    constructor(w, h, options, colors, objInject){
        this.options = options;
        this.colors = colors;
        this.w = w;
        this.h = h;
        this.smooth = this.colors.smooth;
        this.squish = this.colors.offset;
        this.master = box()
            //.opacity(0)
            .color(this.colors.background.r, this.colors.background.g, this.colors.background.b, this.colors.background.a)
            .outline(this.colors.outline.r, this.colors.outline.g, this.colors.outline.b, this.colors.outline.a, this.colors.outline.weight)
            .size(this.w+this.squish, this.h)
        this.buttons = [];
        this.properties = [];
        for (let i in this.options){
            this.properties[i] = {name: '', callback: null, toggled: false};
            if (typeof this.options[i] === 'object'){
                this.properties[i].name = this.options[i][0];
                if (this.options[i][1]) this.properties[i].callback = this.options[i][1];
                if (this.options[i][2] && this.options[i][2] === true){
                    this.defaultToggle = {state: +i};
                };
            } else {
                this.properties[i].name = this.options[i];
            };
            {
                let x = 0;
                let y  = 0;
                if (i > 0) x = (this.buttons[i-1].x + this.buttons[i-1].w + this.squish);
                let w = (this.w - this.squish)/this.options.length;
                let h = this.h;
                this.buttons[i] = box();
                let intensity = this.colors.intensity;
                let hoverIntensity = this.colors.hoverIntensity;
                /*Shorthand Variables:*/ let [c, p, int, hint] = [this.buttons[i], this.properties[i], intensity, hoverIntensity]; 
                const darken = (c, i) => {c.color(c.r - i, c.g - i, c.b - i, c.a + i)}
                const lighten = (c, i) => {c.color(c.r + i, c.g + i, c.b + i, c.a - i)}
                setTimeout(()=>{
                    this.master.opacity();
                }, 1)
                this.toggle = (i) => {
                    let [c, p, int, hint] = [this.buttons[i], this.properties[i], intensity, hoverIntensity];
                    if (!p.toggled){
                        if (p.callback) p.callback(c);
                        lighten(c, hint);
                        lighten(c, int);
                        for (let j in this.buttons){
                            /*Shorthand Variables:*/ let [c2, p2] = [this.buttons[j], this.properties[j]];
                            if (c2 !== c){
                                if (p2.toggled){
                                    darken(c2, int);
                                    p2.toggled = false;
                                };
                            };
                        };
                        p.toggled = true;
                    };
                }
                if (this.defaultToggle?.state === +i){
                    setTimeout(() => {
                        this.toggle(i);
                    }, 5);
                };
                this.buttons[i]
                    .color(0,0,0,0)
                    .outline(0,0,0,0)
                    .position(x, y-1)
                    .size(w+1, h)
                if (p.name){
                    this.buttons[i]
                        .color(this.colors.background.r, this.colors.background.g, this.colors.background.b, this.colors.background.a)
                        .outline(this.colors.outline.r, this.colors.outline.g, this.colors.outline.b, this.colors.outline.a, this.colors.outline.weight)
                        .text(p.name, this.colors.textSize)
                        .textFont(this.colors.textFont)
                        .textColor(this.colors.textColor.r, this.colors.textColor.g, this.colors.textColor.b, this.colors.textColor.a)
                        .textInjectCSS('overflow', 'hidden')
                        .detect(()=>{
                            if (!p.toggled) darken(c, hint);
                        }, 'over')
                        .detect(()=>{
                            if (!p.toggled) lighten(c, hint);
                        }, 'out')
                        .detect(()=>{
                            this.toggle(i)
                        }, 'press')
                };
                switch(+i){
                    case 0:
                        if (this.colors.smooth) this.buttons[i].curves(500,0,0,500,true);
                    break;
                    case this.options.length - 1:
                        if (this.colors.smooth) this.buttons[i].curves(0,500,500,0,true);
                    break;
                };
            };
        };
        this.master.contain(this.buttons, false);
        if (objInject) objInject(this);
    };
};

clientFunc.tab = (parent) => {
    if (parent.tabProperties?.state){
        let a = parent.tabProperties;
        parent.changeObj(a);
        if (!a.obj){
            a.obj = new Tab(parent.trueW, parent.trueH, a.options, a.colors, parent.objectInject);
            parent._();
        };
        if (a.obj?.master?.parentElement){
            if (a.parentInit){
                parent.parentElement.appendChild(a.obj.master.parentElement);
                a.parentInit = false;
            };
        } else {
            parent._();
        };
        return;
    };
};

Box.prototype.tab = function(options, smooth = true){
    let a = this.defineObject('tab');
    a.state = true;
    a.options = options;
    a.colors.smooth = smooth;
    return this;
};
Box.prototype.tabSelect = function(i){
    let a = this.defineObject('tab');
    setTimeout(() => {
        a.obj.toggle(i);
    }, 100);
    return this;
};
Box.prototype.tabText = function(textSize = 5, textFont = 'sans-serif'){
    let a = (this.defineObject('tab')).colors;
    a.textSize = textSize;
    a.textFont = textFont;
    return this;
};
Box.prototype.tabTextColor = function(r = 0, g = r, b = r, a = 255){
    this.defineObject('tab');
    [r, g, b, a] = this.randomColor(r, g, b, a);
    this.tabProperties.colors.textColor.r = r;
    this.tabProperties.colors.textColor.g = g;
    this.tabProperties.colors.textColor.b = b;
    this.tabProperties.colors.textColor.a = a;
    return this;
};
Box.prototype.tabColor = function(r = 255, g = r, b = g, a = 255, intensity = 25, hoverIntensity = 10){
    let c = (this.defineObject('tab')).colors;
    [r, g, b, a] = this.randomColor(r, g, b, a);
    let d = c.background;
    d.r = r;
    d.g = g;
    d.b = b;
    d.a = a;
    c.intensity = intensity;
    c.hoverIntensity = hoverIntensity;
    return this;
};
Box.prototype.tabOutline = function(r = 100, g = r, b = g, a = 255, weight = 1, offset = -1){
    let c = (this.defineObject('tab')).colors;
    [r, g, b, a] = this.randomColor(r, g, b, a);
    let d = c.outline;
    d.r = r;
    d.g = g;
    d.b = b;
    d.a = a;
    d.weight = weight;
    c.offset = offset;
    return this;
};