clientFunc.image = (parent) => {
    if (parent.imageProperties?.state){
        let a = parent.imageProperties;
        parent.changeObj(a);
        parent.createObj(a, () => {
            a.obj = createHTMLElement('img', a, {"src": `${a.objPath}`});
            a.obj.style['position'] = 'absolute';
         });
        parent.positionLogic(a);
        a.obj.style['z-index'] = `${parent.zIndex+1}`;
        a.obj.style['width'] = `${parent.trueW}px`;
        a.obj.style['height'] = `${parent.trueH}px`;
        a.obj.style['border'] = `${a.stroke}px ${a.strokeStyle} rgb(${a.r}, ${a.g}, ${a.b}, ${a.a/255})`;
        if (parent.npolygon?.state){
            a.obj.style['clip-path'] = `polygon(${parent.npolygon.str})`;
        };
        a.obj.style['border-radius'] = `${parent.curve/2}${parent.borderRadiusMode} ${parent.curve2/2}${parent.borderRadiusMode} ${parent.curve3/2}${parent.borderRadiusMode} ${parent.curve4/2}${parent.borderRadiusMode}`;
        parent.injectionLogic(a);
        if (a.filter.state){
            let b = a.filter;
            a.obj.style['filter'] = `${b.type + '('+b.value + b.suffix+')'}`;
        };
    };
};

Box.prototype.image = function(objPath = './src/images/spiral.png', explicitActive = undefined){
    let a = this.defineObject('image');
    let active =  !a.state;
    if (a.obj && objPath !== a.objPath){
        a.changeObj = true;
    };
    if (explicitActive !== undefined && (explicitActive === true || explicitActive === false)){
        active = explicitActive;
    };
    a.state = active;
    a.objPath = objPath;
    a.w = this.w;
    a.h = this.h;
    return this;
};
Box.prototype.imageBorder = function(r = 0, g = 0, b = 0, a = 255, width = 1, style = 'solid'){
    let c = this.defineObject('image');
    [r, g, b, a] = this.randomColor(r, g, b, a);
    c.r = r;
    c.g = g;
    c.b = b;
    c.a = a;
    c.stroke = width;
    c.strokeStyle = style;
    return this;
};
Box.prototype.imageFilter = function(type, value){
    let c = (this.defineObject('image')).filter;
    c.state = true;
    c.type = type;
    c.value = value;
    switch(c.type){
        default: c.suffix = '%'; break;
        case 'hue-rotate': c.suffix = 'deg'; break;
        case 'blur': c.suffix = 'px'; break;
    };
    return this;
};
Box.prototype.imageInjectCSS = function(key, value){
    let a = this.defineObject('image');
    a.injection.push({
        state: true,
        key: key,
        value: value
    });
    return this;
};
Box.prototype.imageColor = function(deg = 0, brightness = 50){
    this.imageInjectCSS(`filter`, `brightness(${brightness}%) sepia(100) saturate(100) hue-rotate(${deg%360}deg)`);
    return this;
};