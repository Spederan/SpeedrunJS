class Contact {
    constructor(trueW, trueH, callback = null, alignment = 'left', objInject){
        let standardSize = 20;
        let standardSpacing = 10;
        this.name = '';
        this.email = '';
        this.message = '';
        this.master = box()
            .size(trueW, trueH)

        this.contactLabel = box()
            .size(trueW, standardSize)
            .text("Contact Us", 17)
            .textAlign('center', 0)
            .textStyle(false, false, 'underline')
            .within(this.master, false)

        this.nameLabel = box()
            .size(trueW, standardSize)
            .under(this.contactLabel, standardSpacing)
            .text("Name: ", 17)
            .textAlign(alignment, 0)
            .textStyle(false, true)
            .within(this.master, false)

        this.nameInput = box()
            .size(trueW, standardSize)
            .under(this.nameLabel, standardSpacing)
            .input((name)=>{
                this.name = name;
            }, u, u, false)
            .within(this.master)

        this.emailLabel = box()
            .size(trueW, standardSize)
            .under(this.nameInput, standardSpacing)
            .text('Email: ', 17)
            .textAlign(alignment, 0)
            .textStyle(false, true)
            .within(this.master)

        this.emailInput = box()
            .size(trueW, standardSize)
            .under(this.emailLabel, standardSpacing)
            .input((email)=>{
                this.email = email;
            }, u, u, false)
            .within(this.master)

        this.messageLabel = box()
            .size(trueW, standardSize)
            .under(this.emailInput, standardSpacing)
            .text('Your message: ', 17)
            .textAlign(alignment, 0)
            .textStyle(false, true)
            .within(this.master)

        this.messageBox = box()
            .size(trueW, trueW/2)
            .under(this.messageLabel, standardSpacing)
            .message((message)=>{
                this.message = message;
            })
            .within(this.master)

        this.submit = box()
            .submit(()=>{
                if (this.name && this.email && this.message){
                    if (callback){
                        callback(this.name, this.email, this.message)
                    };
                    this.nameInput.clearInput();
                    this.emailInput.clearInput();
                    this.messageBox.clearInput();
                };
            })
            .position(trueW/2, u, true)
            .under(this.messageBox, standardSpacing*2)
            .within(this.master)
        if (objInject) objInject(this);
    };
};

clientFunc.contact = (parent) => {
    if (parent.contactProperties?.state){
        let a = parent.contactProperties;
        parent.changeObj(a);
        if (!a.obj){
            a.obj = new Contact(parent.trueW, parent.trueH, a.callback, a.alignment, parent.objectInject);
            parent._();
        };
        if (a.obj?.master?.parentElement){
            if (a.parentInit){
                parent.parentElement.appendChild(a.obj.master.parentElement);
                a.parentInit = false;
            };
        } else {
            parent._();
        };
        return;
    };
};

Box.prototype.contact = function(callback = null, alignment = 'left'){
    let a = this.defineObject('contact');
    a.state = true;
    a.callback = callback;
    a.alignment = alignment;
    return this;
};