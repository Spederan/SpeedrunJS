clientFunc.button =  (parent) => {
    //?Interactive Button
    if (parent.detectCollision?.state){
        let [a, b, c, d] = [parent.buttonProperties, parent.detectCollision, parent.textProperties, parent.stroke];
        if (a?.obj){
            for (let k in b.mode){
                if (b.mode[k].active){
                    let callback = b.mode[k].callback;
                    switch(b.mode[k].mode){
                        default: a.obj.addEventListener("mouseover", callback); break;
                        case 'over': a.obj.addEventListener("mouseover", callback); break;
                        case 'out': a.obj.addEventListener("mouseout", callback); break;
                        case 'click': a.obj.addEventListener("click", callback); break;
                        case 'double': a.obj.addEventListener("dblclick", callback); break;
                        case 'press': a.obj.addEventListener("mousedown", callback); break;
                        case 'move': a.obj.addEventListener("mousemove", callback); break;
                        case 'release': a.obj.addEventListener("mouseup", callback); break;
                    };
                } else {
                    let callback = b.mode[k].callback;
                    switch(b.mode[k].mode){
                        default: a.obj.removeEventListener("mouseover", callback); break;
                        case 'over': a.obj.removeEventListener("mouseover", callback); break;
                        case 'out': a.obj.removeEventListener("mouseout", callback); break;
                        case 'click': a.obj.removeEventListener("click", callback); break;
                        case 'double': a.obj.removeEventListener("dblclick", callback); break;
                        case 'press': a.obj.removeEventListener("mousedown", callback); break;
                        case 'move': a.obj.removeEventListener("mousemove", callback); break;
                        case 'release': a.obj.removeEventListener("mouseup", callback); break;
                    };
                };
            };
        };
        parent.changeObj(a);
        let text  = '';
        if (c?.content && !c.state){
            text = c.content;
        };
        parent.createObj(parent.buttonProperties, () => {
            a.obj = createHTMLElement('button', a);
            if (text) a.obj['textContent'] = `${text}`;
            a.obj.style['border-radius'] = `${parent.curve/2}${parent.borderRadiusMode} ${parent.curve2/2}${parent.borderRadiusMode} ${parent.curve3/2}${parent.borderRadiusMode} ${parent.curve4/2}${parent.borderRadiusMode}`;
            a.obj.style['background-color'] = `rgb(${255}, ${255}, ${255}, ${0})`;
            a.obj.style['border-width'] = `${0}px`;
            a.obj.style['border-color'] = `rgb(${255}, ${255}, ${255}, ${0})`;
            a.obj.style['outline'] = 'none';
            a.obj.style['position'] = 'absolute';
            a.obj.style['box-shadow'] = 'none';
        });
        a.obj.style['z-index'] = `${parent.zIndex+3}`;
        if (a.singular){
            if (text) a.obj['textContent'] = `${text}`;
            a.obj.style['border-radius'] = `${parent.curve/2}${parent.borderRadiusMode} ${parent.curve2/2}${parent.borderRadiusMode} ${parent.curve3/2}${parent.borderRadiusMode} ${parent.curve4/2}${parent.borderRadiusMode}`;
            a.obj.style['background-color'] = `rgb(${parent.r}, ${parent.g}, ${parent.b}, ${parent.a/255})`;
            a.obj.style['border'] = `${d.weight}px solid rgb(${d.r}, ${d.g}, ${d.b}, ${d.a/255})`;
            parent.textLogic(a);
            if (c){
                a.obj.style['text-align'] = `${c.align}`;
                if (c.align === 'left') a.obj.style['padding-left'] = `${c.horizontalPadding}px`;
                if (c.align === 'right') a.obj.style['padding-right'] = `${c.horizontalPadding}px`;
            };
        };
        parent.positionLogic(a);
        a.obj.style['width'] = `${parent.trueW}px`;
        a.obj.style['height'] = `${parent.trueH}px`;
        if (parent.npolygon?.state){
            a.obj.style['clip-path'] = `polygon(${parent.npolygon.str})`;
        };
        parent.injectionLogic(a);
    };
};

Box.prototype.detect = function(callback = null, mode = 'over', active = true){
    this.defineObject('button');
    if (!this.boxProperties) this.color(0,0,0,0);
    this.detectCollision.state = true;
    this.buttonProperties.state = true;
    switch (active){
        case 'true' || true: active = true; break;
        case 'false' || false: active = false; break;
        case 'toggle': active = !this.detectCollision.mode[mode].active; break;
    };
    this.detectCollision.mode[mode] = {callback: callback, active: active, mode: mode};
    return this;
};
Box.prototype.buttonInjectCSS = function(key, value){
    let a = this.defineObject('button');
    a.injection.push({
        state: true,
        key: key,
        value: value
    });
    return this;
};
Box.prototype.drag = function(callback1 = null, callback2 = null, center = false, active = true){
    this.detect(()=>{
        this.follow(callback1, center, active);
    }, 'press');
    this.detect(()=>{
        this.follow(callback2, center, false);
    }, 'release');
    this.run(()=>{
        if (!mouseIsPressed){
            this.follow(callback2, center, false);
        };
    })
    return this._();
};
Box.prototype.stick = function(callback = null, center = false){
    this.stick_outCounter = 0;
    this.detect(()=>{
        this.follow(callback, center, 'toggle');
    }, 'press');
    this.detect(()=>{
        this.stick_outCounter--;
    }, 'over');
    this.detect(()=>{
        this.stick_outCounter++;
    }, 'out');
    this.run(()=>{
        if (mouseIsPressed && this.stick_outCounter >= 0){
            this.follow(callback, center, false);
        };
    })
    return this._();
};
Box.prototype.draggableIcon = function(callback, size = 20){
    this
        .drag()
        .drag(u,u,u,false)
    this.draggingIcon = box()
        .size(size)
        .containerPosition(this.w - size, -size)
        .detect(()=>{
            this.drag()
        }, 'over')
        .detect(()=>{
            this.drag(u,u,u,false)
        }, 'out')
        .image('/src/images/move.png')
        .within(this)
    if (callback){
        callback(this.draggingIcon);
    };
    return this._();
};
Box.prototype.submit = function(callback = null, darken = 12){
    let normalOpacity = 1;
    setTimeout(() => {
        //this.opacity(normalOpacity);
    }, 15);
    //this.opacity(normalOpacity);
    this.detect(()=>{
        if (callback){
            callback(this);
        };
    }, 'click', true);
    this.detect(()=>{
        this.undarkenText(darken*1.5);
        this.undarken(darken);
        //this.opacity(0.6);
    }, 'press', true);
    this.detect(()=>{
        this.darkenText(darken*1.5);
        this.darken(darken);
        //this.opacity(normalOpacity);
    }, 'release', true);
    this.detect(()=>{
        this.darkenText(darken*1.5);
        this.darken(darken);
    }, 'over', true);
    this.detect(()=>{
        this.undarkenText(darken*1.5);
        this.undarken(darken);
    }, 'out', true);
    this.size(55, 20);
    this.curves(2.5, 2.5, 2.5, 2.5, true);
    this.color(210);
    this.outline(75)
    this.text('Submit', 12.5);
    this.textFont('sans-serif');
    return this;
};