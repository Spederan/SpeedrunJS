class Login {
    constructor(trueW, trueH, callback, errCallback = null, minimumPasswordLength = 12, pbkdf2rounds = 1000, size = 512, objInject){
        this.spacing = 25;
        if (minimumPasswordLength < 8) minimumPasswordLength = 8;
        this.username = '';
        this.password = '';
        this.master = box()
            .div()
            .size(trueW, trueH)

        this.usernameLabel = box()
            .text("Username:", 15)
            .textFont('sans-serif')
            .textAlign('left', 0)
            .size(trueW, this.spacing)
            .within(this.master, false)

        this.usernameField = box()
            .input((val)=>{if (val) this.username = val}, u, 'text', false)
            .under(this.usernameLabel)
            .size(trueW, this.spacing)
            .within(this.master, false)

        this.error = '';
        this.prevError = '';
        this.userPrompt = box()
            .size(300, this.spacing)
            .under(this.usernameField, -10)
            .text(``, 13)
            .textColor(255,0,0,255)
            .textAlign('left')
            .within(this.master, false)
            .run(()=>{
                if (errCallback){
                    this.error = errCallback(this.username, this.password, this);
                    if (this.error !== this.prevError){
                        this.prevError = this.error;
                        if (!this.error){
                            this.userPrompt.text(' ', 13);
                        } else {
                            if (this.error){
                                this.userPrompt.text(this.error, 13);
                            };
                        };
                    }; 
                };                
            })

        this.passwordLabel = box()
            .text("Password:", 15)
            .textFont('sans-serif')
            .textAlign('left', 0)
            .size(trueW, this.spacing)
            .under(this.usernameField, this.spacing/2)
            .within(this.master, false)

        this.revealed = false;
        this.prevRevealed = false;
        this.passwordField = box()
            .input((val) => {
                if (val){
                    this.password = val;
                };
            }, u, 'password', false)
            .under(this.passwordLabel)
            .size(trueW, this.spacing)
            .within(this.master, false)
            .run(()=>{
                if (this.revealed !== this.prevRevealed){
                    this.prevRevealed = this.revealed;
                    if (this.revealed){
                        this.passwordField.modifyInput('text');
                    } else {
                        this.passwordField.modifyInput('password');
                    };
                };
            })

        this.signin = box()
            .submit(()=>{
                if (this.username && !this.error && this.password.length > minimumPasswordLength){
                    this.usernameField.clearInput();
                    this.passwordField.clearInput();
                    if (pbkdf2rounds){
                        deriveSecureKey(this.password+this.username, (password)=>{
                            if (callback){
                                callback(this.username, password);
                            };
                        }, pbkdf2rounds, size);
                    } else {
                        if (callback){
                            callback(this.username, this.password);
                        };
                    };
                };
            })
            .text('Sign in', 12)
            .position(trueW/2, u, true)
            .under(this.passwordField, this.spacing + 5)
            .within(this.master, false)

        this.prompt = box()
            .size(300, this.spacing)
            .under(this.passwordField, -5)
            .text(``, 15)
            .textAlign('left')
            .within(this.master, false)
            .run(()=>{
                if (this.password){
                    setTimeout(() => {
                        if (this.password.length < minimumPasswordLength){
                            this.prompt
                                .text(`Your password must be at least ${minimumPasswordLength} characters.`, 13)
                                .textColor(255,0,0,255)
                        };  
                    }, 5000);
                    if (this.password.length >= minimumPasswordLength || !this.password){
                        this.prompt
                            .textColor(255,0,0,0)
                    };
                };          
            })
        
        this.reveal = box()
            .submit(()=>{
                this.revealed = !this.revealed;
                if (!this.revealed){
                    this.reveal.text('Show', 13);
                } else {
                    this.reveal.text('Hide', 13);
                };
            })
            .size(60, this.passwordField.h-7)
            .right(this.passwordField, -60 + 2)
            .under(this.passwordField, 0, 0)
            .color(220)
            .outline(100)
            .text('Show', 13)
            .textFont('serif')
            .within(this.master, false)
    if (objInject) objInject(this);
    };
};

clientFunc.login = (parent) => {
    if (parent.loginProperties?.state){
        let a = parent.loginProperties;
        parent.changeObj(a);
        if (!a.obj){
            a.obj = new Login(parent.trueW, parent.trueH, a.callback, a.errCallback, a.minLen, a.rounds, a.size, parent.objectInject);
            parent._();
        };
        if (a.obj?.master?.parentElement){
            if (a.parentInit){
                parent.parentElement.appendChild(a.obj.master.parentElement);
                a.parentInit = false;
            };
        } else {
            parent._();
        };
        return;
    };
};

Box.prototype.login = function(callback = null, errCallback = null, minLen = 12, rounds = 1000, size = 512){
    let a = this.defineObject('login');
    a.state = true;
    a.callback = callback;
    a.errCallback = errCallback;
    a.minLen = minLen;
    a.rounds = rounds;
    a.size = size;
    return this;
};