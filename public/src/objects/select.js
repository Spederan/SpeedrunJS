class Select {
    constructor(){
        this.init = true;
        this.master = box()
            .size(100,25)
        this.width = this.master.trueW;
        this.height = this.master.trueH;
        this.option = [];
        this.optionText = [];
        this.displaying = false;
        this.textSize = 15;
        this.font = 'sans-serif';
        this.obj = './src/images/downarrow.png';
        this.textLength = 0;
        this.textPadding = 10;
        this.rightHandPadding = 30;
        this.selection = -1;
        this.prevSelection = -1;
        this.overs = 0;
        this.disable = {};
        this.lostOpacity = '0.95';
        this.unselected = {
            text: {
                r: 0,
                g: 0,
                b: 0,
                a: 255
            },
            background: {
                r: 255,
                g: 255,
                b: 255,
                a: 255
            },
            outline: {
                r: 100,
                g: 100,
                b: 100,
                a: 255, 
                weight: 1
            }
        }
        this.selected = {
            text: {
                r: 255,
                g: 255,
                b: 255,
                a: 255
            },
            background: {
                r: 0,
                g: 128,
                b: 255,
                a: 255
            },
            outline: {
                r: 0,
                g: 0,
                b: 0,
                a: 255,
                weight: 1
            }
        }
    };
    run(){
        this.display(this.displaying);
    };
    colors(colors){
        this.unselected = colors.unselected;
        this.selected = colors.selected;
    };
    preferences(meta){
        this.textPadding = meta.textPadding || 10;
        this.rightHandPadding = meta.rightHandPadding || 30;
        this.textSize = meta.size || 15;
        this.font = meta.font || 'sans-serif';
        this.obj = meta.obj || './src/images/downarrow.png';
        this.disable = meta.disable;
    };
    
    options(arr, callback = null, objInject){
        for (let i in arr){
            if (textWidth(arr[i], this.font) > this.textLength) this.textLength = (textWidth(arr[i], this.font))*1.2;
        };
        if (!this.option[0]) this.option[0] = box();
            this.option[0]
            .size(this.textLength*1.25+this.textPadding+this.rightHandPadding, this.master.trueH)
            .color(this.unselected.background.r, this.unselected.background.g, this.unselected.background.b, this.unselected.background.a)
            .textColor(this.unselected.text.r, this.unselected.text.g, this.unselected.text.b, this.unselected.text.a)
            .outline(this.unselected.outline.r, this.unselected.outline.g, this.unselected.outline.b, this.unselected.outline.a, this.unselected.outline.weight)
            .detect(()=>{
                this.displaying = !this.displaying;    
            }, 'click', true)
            .boxInjectCSS('opacity', this.lostOpacity)
            .buttonInjectCSS('opacity', this.lostOpacity)
            .textInjectCSS('opacity', this.lostOpacity)
            .detect(()=>{
                this.option[0].boxInjectCSS('opacity', '1');
                this.option[0].buttonInjectCSS('opacity', 1);
                this.option[0].textInjectCSS('opacity', '1');
                this.overs++;
            }, 'over', true)
            .detect(()=>{
                this.option[0].boxInjectCSS('opacity', this.lostOpacity);
                this.option[0].buttonInjectCSS('opacity', this.lostOpacity);
                this.option[0].textInjectCSS('opacity', this.lostOpacity);
                this.overs--;
            }, 'out', true);
        let nar = [...arr];
        if (arr[0]){
            this.option[0].text(nar.shift(), this.textSize);
        } else {
            this.option[0].text(" ");
        };
        this.option[0].textFont(this.font);
        this.option[0].textAlign();
        this.option[0].run(()=>{
            if (this.selection >= 0 && this.prevSelection !== this.selection){
                if (callback) callback(arr[this.selection]);
                this.option[0].text(arr[this.selection], this.textSize);
                this.option[0].textFont(this.font);
                this.option[0].drawAgain = true;
                this.option[0].boxInjectCSS('opacity', '1');
                this.option[0].buttonInjectCSS('opacity', '1');
                this.option[0].textInjectCSS('opacity', '1');
                this.option[0].outline(this.selected.outline.r, this.selected.outline.g, this.selected.outline.b, this.selected.outline.a, this.selected.outline.weight);
            };
            this.prevSelection = this.selection;
            if (mouseIsPressed && this.overs <= 0){
                this.displaying = false;
                this.option[0].outline(this.unselected.outline.r, this.unselected.outline.g, this.unselected.outline.b, this.unselected.outline.a, this.unselected.outline.weight);
                this.option[0].boxInjectCSS('opacity', this.lostOpacity);
                this.option[0].buttonInjectCSS('opacity', this.lostOpacity);
                this.option[0].textInjectCSS('opacity', this.lostOpacity);
            };
        }, true);
        this.width = this.textLength*1.25+this.textPadding+this.rightHandPadding;
        this.height = this.master.trueH*nar.length;
        if (!this.backdrop) this.backdrop = box();
        this.backdrop
            .size(this.width-2, this.height-3)
            .position(0, this.master.trueH)
            .color(this.unselected.background.r, this.unselected.background.g, this.unselected.background.b, this.unselected.background.a)
            .outline(this.unselected.outline.r, this.unselected.outline.g, this.unselected.outline.b, this.unselected.outline.a, this.unselected.outline.weight)
        if (!this.icon) this.icon = box();
        this.icon
            .size(this.master.trueH, this.master.trueH)
            .position(this.textLength*1.25+this.textPadding+this.rightHandPadding-this.unselected.outline.weight*2-this.master.trueH, 0)
            .image(this.obj, true)
            .detect(()=>{
                this.displaying = !this.displaying;    
            }, 'click', true)
            .detect(()=>{
                this.overs++;
            }, 'over', true)
            .detect(()=>{
                this.overs--;
            }, 'out', true)
        for (let j in nar){
            let i = (j*1) + 1;
            if (!this.option[i]){
                this.option[i] = box();
                this.option[i].disable = false;
            };
            for (let key in this.disable){
                if (key === nar[i-1]){
                    this.option[i].disable = this.disable[key];
                    if (!this.option[i].disable){
                        this.option[i].buttonInjectCSS('opacity', '1');
                    } else {
                        this.option[i].buttonInjectCSS('opacity', '0.5');
                    };
                };
            };
            this.option[i].size(this.textLength*1.25+this.textPadding+this.rightHandPadding-this.unselected.outline.weight*2, this.master.trueH-this.unselected.outline.weight);
            this.option[i].position(this.unselected.outline.weight, (this.master.trueH*i)-i+this.unselected.outline.weight);
            this.option[i].color(this.unselected.background.r, this.unselected.background.g, this.unselected.background.b, this.unselected.background.a);
            this.option[i].textColor(this.unselected.text.r, this.unselected.text.g, this.unselected.text.b, this.unselected.text.a);
            this.option[i].drawAgain = true;
            this.option[i].text(nar[i-1], this.textSize);
            this.option[i].textAlign();
            this.option[i].textFont(this.font);
            this.option[i].detect(()=>{
                if (this.displaying && !this.option[i].disable){
                    this.selection = i;
                    this.displaying = false;
                };        
            }, 'click', true);
            this.option[i].detect(()=>{
                if (!this.option[i].disable){
                    this.option[i].color(this.selected.background.r, this.selected.background.g, this.selected.background.b, this.selected.background.a);
                    this.option[i].textColor(this.selected.text.r, this.selected.text.g, this.selected.text.b, this.selected.text.a);
                };
                this.overs++;
            }, 'over', true);
            this.option[i].detect(()=>{
                this.option[i].color(this.unselected.background.r, this.unselected.background.g, this.unselected.background.b, this.unselected.background.a);
                this.option[i].textColor(this.unselected.text.r, this.unselected.text.g, this.unselected.text.b, this.unselected.text.a);
                this.overs--;
            }, 'out', true);
        };
        this.master.size(this.width, this.height);
        this.master.contain((this.option.concat(this.backdrop, this.icon)), false);
        this.master.run(()=>{
            this.run()
        })
        this.master.drawAgain = true;
        if (objInject) objInject(this);
    };
    display(bool){
        if (bool){
            if (this.backdrop?.hideGraphics) this.backdrop.show();
            for (let i in this.option){
                if (i > 0 && this.option[i].hideGraphics) this.option[i].show();
            };
        } else {
            if (!this.backdrop?.hideGraphics) this.backdrop.hide();
            for (let i in this.option){
                if (i > 0 && !this.option[i].hideGraphics) this.option[i].hide();
            };
        };
    };
};

clientFunc.select = (parent) => {
    if (parent.selectProperties?.state){
        let a = parent.selectProperties;
        parent.changeObj(a, ()=>{a.modified = true;});
        if (!a.obj){
            a.obj = new Select();
            parent.w = a.obj.width*0.89;
            parent.h = a.obj.height;
        };
        if (a.modified){
            a.obj.preferences(a.meta);
            a.obj.colors(a.colors);
            a.obj.options(a.options, a.callback, parent.objectInject);
            a.modified = false;
        };
        if (a?.obj?.master?.parentElement){
            if (a.parentInit){
                parent.parentElement.appendChild(a.obj.master.parentElement);
                a.parentInit = false;
            };
        } else {
            parent._();
        };
        return;
    };
};

Box.prototype.select = function(options = [], callback = null){
    let a = this.defineObject('select');
    this.selectTextColor();
    this.selectBackgroundColor(255);
    this.selectHoverTextColor(255);
    this.selectHoverBackgroundColor(0,0,255);
    this.selectOutline(100,100,100,255,1);
    this.selectChangedOutline(100,100,100,255,1);
    a.modified = true;
    a.state = true;
    a.options = options;
    a.callback = callback;
    return this;
};
Box.prototype.selectDisable = function(option, disable = true){
    let a = this.defineObject('select');
    if (a.obj) a.changeObj = true;
    a.modified = true;
    a.meta.disable[option] = disable;
    return this;
};
Box.prototype.selectText = function(font, size){
    let a = this.defineObject('select');
    a.modified = true;
    let b = a.meta;
    b.font = font;
    b.size = size;
    return this;
};
Box.prototype.selectMargins = function(textPadding = 10, rightHandPadding = 30){
    let a = this.defineObject('select');
    a.modified = true;
    let b = a.meta;
    b.textPadding = textPadding;
    b.rightHandPadding = rightHandPadding;
    return this;
};
Box.prototype.selectTextColor = function(r = 0, g = r, b = r, a = 255){
    let c = this.defineObject('select');
    [r, g, b, a] = this.randomColor(r, g, b, a);
    c.modified = true;
    let d = c.colors.unselected.text;
    d.r = r;
    d.g = g;
    d.b = b;
    d.a = a;
    return this;
};
Box.prototype.selectBackgroundColor = function(r = 0, g = r, b = r, a = 255){
    let c = this.defineObject('select');
    [r, g, b, a] = this.randomColor(r, g, b, a);
    c.modified = true;
    let d = c.colors.unselected.background;
    d.r = r;
    d.g = g;
    d.b = b;
    d.a = a;
    return this;
};
Box.prototype.selectHoverTextColor = function(r = 0, g = r, b = r, a = 255){
    let c = this.defineObject('select');
    [r, g, b, a] = this.randomColor(r, g, b, a);
    c.modified = true;
    let d = c.colors.selected.text;
    d.r = r;
    d.g = g;
    d.b = b;
    d.a = a;
    return this;
};
Box.prototype.selectHoverBackgroundColor = function(r = 0, g = r, b = r, a = 255){
    let c = this.defineObject('select');
    [r, g, b, a] = this.randomColor(r, g, b, a);
    c.modified = true;
    let d = c.colors.selected.background;
    d.r = r;
    d.g = g;
    d.b = b;
    d.a = a;
    return this;
};
Box.prototype.selectOutline = function(r = 0, g = r, b = r, a = 255, weight = 1){
    let c = this.defineObject('select');
    [r, g, b, a] = this.randomColor(r, g, b, a);
    c.modified = true;
    let d = c.colors.unselected.outline;
    d.r = r;
    d.g = g;
    d.b = b;
    d.a = a;
    d.weight = weight;
    return this;
};
Box.prototype.selectChangedOutline = function(r = 0, g = r, b = r, a = 255, weight = 1){
    let c = this.defineObject('select');
    [r, g, b, a] = this.randomColor(r, g, b, a);
    c.modified = true;
    let d = c.colors.selected.outline;
    d.r = r;
    d.g = g;
    d.b = b;
    d.a = a;
    d.weight = weight;
    return this;
};
Box.prototype.selectImage = function(obj = './src/images/downarrow.png'){
    let a = this.defineObject('select');
    a.modified = true;
    a.meta.obj = obj;
    return this;
};