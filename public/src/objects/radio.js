clientFunc.radio = (parent) => {
    if (parent.radioProperties?.state){
        let a = parent.radioProperties;
        parent.changeObj(a);
        parent.createObj(a, () => {
            a.obj = createHTMLElement('label', a);
            a.radios = [];
            a.spans = [];
            for (let i in a.options){
                a.radios[i] = createHTMLElement('input', a.radios[i], {"type": "radio", "name": `${a.id}`});
                a.spans[i] = createHTMLElement('span', a.spans[i]);
                a.spans[i]['textContent'] = a.options[i];
                a.obj.appendChild(a.radios[i]);
                a.obj.appendChild(a.spans[i]);
            };
            parent._clearRun();
            parent._run(()=>{
                for (let i in a.options){
                    if (a.disableItems?.[i]){
                        a.radios[i].disabled = !!a.disableItems[i];
                    };
                    let value = a.radios[i].checked;
                    if (a.callback && value && +a.prevValue !== +i){
                        a.callback(+i);
                        a.prevValue = i;
                    };
                };
            });
        });
        parent.positionLogic(a);
        a.obj.style['z-index'] = `${parent.zIndex+4}`;
        a.obj.style['width'] = `${parent.trueW}px`;
        a.obj.style['text-align'] = 'left';
        a.obj.style['display'] = 'inline';
        let [b, c] = [a.text, a.accent]
        a.obj.style['color'] = `rgb(${b.r},${b.g},${b.b},${b.a/255})`;
        a.obj.style['accent-color'] =  `rgb(${c.r},${c.g},${c.b},${c.a/255})`;
        if (a.disableItems === true){
            a.disableItems = [];
            for (let i in a.options){
                a.disableItems[i] = true;
            };
        };
        if (a.disableItems === false){
            a.disableItems = [];
            for (let i in a.options){
                a.disableItems[i] = false;
            };
        };            
        parent.injectionLogic(a);
        return;
    };
};

Box.prototype.radio = function(options = ['a','b','c','d'], callback = null){
    let a = this.defineObject('radio');
    if (a.obj){
        a.changeObj = true;
    };
    a.state = true;
    a.callback = callback;
    a.options = options;
    return this;
};
Box.prototype.radioDisable = function(items = []){
    let a = this.defineObject('radio');
    a.disableItems = items;
    a.changeObj = true;
    return this;
};
Box.prototype.radioAccentColor = function(r = 0, g = r, b = r, a = 255){
    let c = (this.defineObject('radio')).accent;
    [r, g, b, a] = this.randomColor(r, g, b, a);
    c.r = r;
    c.g = g;
    c.b = b;
    c.a = a;
    return this;
};
Box.prototype.radioLabelColor = function(r = 0, g = r, b = r, a = 255){
    let c = (this.defineObject('radio')).text;
    [r, g, b, a] = this.randomColor(r, g, b, a);
    c.r = r;
    c.g = g;
    c.b = b;
    c.a = a;
    return this;
};