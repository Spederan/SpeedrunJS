clientFunc.input = (parent) => {
    if (parent.inputProperties?.state){
        let a = parent.inputProperties;
        parent.changeObj(a);
        parent.createObj(a, () => {
            a.obj = createHTMLElement('input', a, 
            {"type": `${a.type}`, 
            "placeholder": `${a.defaultText}`,
            });
            a.obj.style['position'] = `absolute`;
            if (a.callback){
                if (a.enter){
                    a.obj.onkeydown = (e) => {
                        if (e.key === "Enter"){
                            a.callback(a.obj.value)
                            a.obj.value = '';
                        };
                    };
                } else {
                    parent._clearRun();
                    let prevValue = '';
                    parent._run(()=>{
                        let value = a.obj.value;
                        if (a.alwaysRecord || value && value != prevValue){
                            prevValue = value;
                            a.callback(value);
                        };
                    });
                };
            };   
        });
        parent.positionLogic(a);
        a.obj.style['z-index'] = `${parent.zIndex+4}`;
        let [b, c] = [a.obj.style, parent.inputColors];
        if (parent.curve) a.obj.style['border-radius'] = `${parent.curve/2}${parent.borderRadiusMode} ${parent.curve2/2}${parent.borderRadiusMode} ${parent.curve3/2}${parent.borderRadiusMode} ${parent.curve4/2}${parent.borderRadiusMode}`;
        if (a.obj){
            b['font-family'] = `${c.font}`;
            b['font-size'] = `${c.size}px`;
            b['width'] = `${parent.trueW-a.xMinus}px`;
            b['backgroundColor'] = `rgb(${c.fill.r}, ${c.fill.g}, ${c.fill.b}, ${c.fill.a/255})`;
            b['border-color'] = `rgb(${c.border.r}, ${c.border.g}, ${c.border.b}, ${c.border.a/255})`;
            b['border-width'] = `${c.border.weight}px`;
            b['outline-color'] = `rgb(${c.outline.r}, ${c.outline.g}, ${c.outline.b}, ${c.outline.a/255})`;
            b['color'] = `rgb(${c.text.r}, ${c.text.g}, ${c.text.b}, ${c.text.a/255})`;
            parent.injectionLogic(a);             
        };
        return;
    };
};

Box.prototype.input = function(callback, defaultText = '', type = 'text', enter = true, alwaysRecord = false){
    let a = this.defineObject('input');
    a.state = true;
    a.alwaysRecord = alwaysRecord;
    a.type = type;
    a.enter = enter;
    a.callback = callback;
    a.defaultText = defaultText;
    return this._();
};
Box.prototype.clearInput = function(text = ''){
    let [a,b] = [this.inputProperties, this.messageProperties];
    if (a?.obj) a.obj.value = text;
    if (b?.obj) b.obj.value = text;
    return this._();
};
Box.prototype.modifyInput = function(type = 'text'){
    setTimeout(()=>{
        let a = this.inputProperties;
        if (a?.obj) a.obj.type = type;
    }, 1)
    return this._();
};
Box.prototype.inputFont = function(size = 12, font = 'monospace'){
    this.defineObject('input');
    let a = this.inputColors;
    a.size = size;
    a.font = font;
    return this;
};
Box.prototype.inputFillColor = function(r=255, g=r, b=r, a=255){
    this.defineObject('input');
    [r, g, b, a] = this.randomColor(r, g, b, a);
    let c = this.inputColors.fill;
    c.r = r;
    c.g = g;
    c.b = b;
    c.a = a;
    return this;
};
Box.prototype.inputTextColor = function(r=0, g=r, b=r, a=255){
    this.defineObject('input');
    [r, g, b, a] = this.randomColor(r, g, b, a);
    let c = this.inputColors.text;
    c.r = r;
    c.g = g;
    c.b = b;
    c.a = a;
    return this;
};
Box.prototype.inputBorderColor = function(r=0, g=r, b=r, a=255, weight=0.1){
    this.defineObject('input');
    [r, g, b, a] = this.randomColor(r, g, b, a);
    let c = this.inputColors.border;
    c.r = r;
    c.g = g;
    c.b = b;
    c.a = a;
    c.weight = weight;
    return this;
};
Box.prototype.inputOutlineColor = function(r=0, g=r, b=r, a=255){
    this.defineObject('input');
    [r, g, b, a] = this.randomColor(r, g, b, a);
    let c = this.inputColors.outline;
    c.r = r;
    c.g = g;
    c.b = b;
    c.a = a;
    return this;
};
Box.prototype.inputInjectCSS = function(key, value){
    let a = this.defineObject('input');
    a.injection.push({
        state: true,
        key: key,
        value: value
    });
    return this;
};
