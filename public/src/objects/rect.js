clientFunc.rect = (parent) => {
    //?Force Button into Being Single Element If Applicable
    if (parent.detectCollision?.state){
        if (parent.boxProperties?.state){
            if (parent.imageProperties?.state){} else {
                parent.boxProperties.state = false;
                parent.buttonProperties.singular = true;
            };
        };
        if (parent.textProperties?.state){
            if (parent.imageProperties?.state){} else {
                parent.textProperties.state = false;
                parent.buttonProperties.singular = true;
            };
        };
    }; 
    //?Visible Box
    if (parent.boxProperties?.state){
        let a = parent.boxProperties;
        parent.changeObj(a);
        parent.createObj(a, () => {
            a.obj = createHTMLElement('rectangle', a);
            a.obj.style['outline'] = 'none';
            a.obj.style['box-shadow'] = 'none';
            a.obj.style['position'] = 'absolute';
        });
        parent.positionLogic(a);
        a.obj.style['z-index'] = `${parent.zIndex+0}`;
        a.obj.style['width'] = `${parent.trueW}px`;
        a.obj.style['height'] = `${parent.trueH}px`;
        a.obj.style['border-radius'] = `${parent.curve/2}${parent.borderRadiusMode} ${parent.curve2/2}${parent.borderRadiusMode} ${parent.curve3/2}${parent.borderRadiusMode} ${parent.curve4/2}${parent.borderRadiusMode}`;
        a.obj.style['background-color'] = `rgb(${parent.r}, ${parent.g}, ${parent.b}, ${parent.a/255})`;
        a.obj.style['border'] = `${parent.stroke.weight}px solid rgb(${parent.stroke.r}, ${parent.stroke.g}, ${parent.stroke.b}, ${parent.stroke.a/255})`;
        if (parent.npolygon?.state){
            a.obj.style['clip-path'] = `polygon(${parent.npolygon.str})`;
        };
        parent.injectionLogic(a);
    };
};
Box.prototype.boxInjectCSS = function(key, value){
    let a = this.defineObject('box');
    a.injection.push({
        state: true,
        key: key,
        value: value
    });
    return this._();
};