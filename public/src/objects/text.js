clientFunc.text = (parent) => {
    if (parent.textProperties?.state && parent.textProperties.content){
        let a = parent.textProperties;
        parent.changeObj(a);
        parent.createObj(a, () => {
            if (a.hyperlink){
                a.obj = createHTMLElement('a', a, {href: a.hyperlink});
            } else {
                a.obj = createHTMLElement('p', a);
            };
            a.obj['textContent'] = `${a.content}`;
            a.obj.style['justify-content'] = 'center';
            a.obj.style['align-items'] = 'center';
            a.obj.style['margin'] = 'auto';
            a.obj.style['overflow'] = 'auto';
            a.obj.style['position'] = 'absolute';
        });
        parent.positionLogic(a, (-a.xOffset + a.horizontalPadding), (-a.yOffset+ a.verticalPadding));
        a.obj['textContent'] = `${a.content}`;
        a.obj.style['white-space'] = `pre-wrap`;
        a.obj.style['z-index'] = `${parent.zIndex+2}`;
        a.obj.style['width'] = `${parent.trueW-a.horizontalPadding*2}px`;
        a.obj.style['height'] = `${parent.trueH}px`;
        parent.textLogic(a);
        parent.injectionLogic(a, 'flex');
    };
};
Box.prototype.text = function(text, size = 10, link = null){
    let a = this.defineObject('text');
    a.state = true;
    if (a.obj && text !== a.content){
        a.obj['textContent'] = text;
    };
    a.content = text;
    this.textSize = size;
    a.hyperlink = link;
    return this;
};
Box.prototype.textOffset = function(xOffset = 0, yOffset = 0){
    let a = this.defineObject('text');
    a.xOffset = xOffset;
    a.yOffset = yOffset;
    return this;
};
Box.prototype.textAlign = function(horizontalAlignment = 'left', horizontalPadding = 10, verticalAlignment = 'center', verticalPadding = 0){
    switch(verticalAlignment){
        case "top": verticalAlignment = 'start'; break;
        case "bottom": verticalAlignment = 'end'; break;
    };
    let a = this.defineObject('text');
    this.textInjectCSS('justify-content', horizontalAlignment);
    this.textInjectCSS('align-items', verticalAlignment);
    a.align = horizontalAlignment;
    a.horizontalPadding = horizontalPadding;
    a.verticalPadding = verticalPadding;
    return this;
};
Box.prototype.textColor = function(r, g = r, b = r, a = 255){
    this.defineObject('text');
    [r, g, b, a] = this.randomColor(r, g, b, a);
    let t = this.textColors;
    t.r = r;
    t.g = g;
    t.b = b;
    t.a = a;
    return this;
};
Box.prototype.textOutline = function(r, g = r, b = r, a = 255, weight = 1){
    this.defineObject('text');
    [r, g, b, a] = this.randomColor(r, g, b, a);
    let t = this.textStroke;
    t.r = r;
    t.g = g;
    t.b = b;
    t.a = a;
    t.weight = weight;
    return this;
};
Box.prototype.textStyle = function(bold = false, italic = false, decType = false, decStyle = "solid", decThickness = 2){
    let a = this.defineObject('text');
    if (bold) a.bold = !a.bold;
    if (italic) a.italic = !a.italic;
    if (decType){
        let d = a.decoration;
        d.state = !d.state;
        d.type = decType;
        d.style = decStyle;
        d.thickness = decThickness;
    };
    return this;
};
Box.prototype.textDecorationColor = function(r = this.textColors.r, g = this.textColors.g, b = this.textColors.b, a = 255){
    let c = this.defineObject('text');
    [r, g, b, a] = this.randomColor(r, g, b, a);
    let d = c.decoration;
    d.r = r;
    d.g = g;
    d.b = b;
    d.a = a;
    return this;
};
Box.prototype.textFont = function(font = 'serif'){
    let a = this.defineObject('text');
    a.font = font;
    return this;
};
Box.prototype.textInjectCSS = function(key, value){
    let a = this.defineObject('text');
    a.injection.push({
        state: true,
        key: key,
        value: value
    });
    return this;
};