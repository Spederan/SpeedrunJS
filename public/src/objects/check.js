clientFunc.check = (parent) => {
    //?Check Box
    if (parent.checkProperties?.state){
        let a = parent.checkProperties;
        parent.changeObj(a);
        parent.createObj(a, () => {
            a.obj = createHTMLElement('input', a, {"type": "checkbox", "checked": a.defaultValue});
            a.value = a.defaultValue;
            parent.w = 12;
            parent.h = 12;
            parent._clearRun();
            parent._run(()=>{
                if (a.callback && a.prevChecked !== a.obj.checked){
                    a.callback(a.obj.checked)
                    a.prevChecked = a.obj.checked;
                };  
            });
        });
        parent.positionLogic(a, -4, -3);
        let [b, c] = [a.text, a.accent];
        a.obj.style['z-index'] = `${parent.zIndex+4}`;
        a.obj.style['color'] = `rgb(${b.r},${b.g},${b.b},${b.a/255})`;
        a.obj.style['accent-color'] = `rgb(${c.r},${c.g},${c.b},${c.a/255})`;
        parent.injectionLogic(a);
        return;
    };
};
Box.prototype.check = function(callback = null, defaultValue = false){
    let a = this.defineObject('check');
    a.state = true;
    a.callback = callback;
    a.defaultValue = defaultValue;
    return this;
};
Box.prototype.checkAccentColor = function(r = 0, g = r, b = r, a = 255){
    let c = (this.defineObject('check')).accent;
    [r, g, b, a] = this.randomColor(r, g, b, a);
    c.r = r;
    c.g = g;
    c.b = b;
    c.a = a;
    return this;
};
Box.prototype.checkLabelColor = function(r = 0, g = r, b = r, a = 255){
    let c = (this.defineObject('check')).text;
    [r, g, b, a] = this.randomColor(r,g,b,a);
    c.r = r;
    c.g = g;
    c.b = b;
    c.a = a;
    return this;
};
Box.prototype.checkInjectCSS = function(key, value){
    let a = this.defineObject('check');
    a.injection.push({
        state: true,
        key: key,
        value: value
    });
    return this;
};