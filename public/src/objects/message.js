clientFunc.message = (parent) => {
    if (parent.messageProperties?.state){
        let a = parent.messageProperties;
        parent.changeObj(a);
        parent.createObj(a, () => {
            a.obj = createHTMLElement('textArea', a, 
            {
            "placeholder": `${a.defaultText}`,
            });
            a.obj.style['position'] = `absolute`;
            if (a.callback){
                parent._clearRun();
                let prevValue = '';
                parent._run(()=>{
                    let value = a.obj.value;
                    if (a.alwaysRecord || value && value != prevValue){
                        prevValue = value;
                        a.callback(value);
                    };
                });
            };   
             //? Proper Tabbing  
             const textarea = a.obj;
             textarea.addEventListener('keydown', (e) => {
             if (e.keyCode === 9) {
                 e.preventDefault()
                 textarea.setRangeText(
                 '    ',
                 textarea.selectionStart,
                 textarea.selectionStart,
                 'end'
                 );
             };
             });  
        });
        parent.positionLogic(a);
        if (parent.curve) a.obj.style['border-radius'] = `${parent.curve/2}${parent.borderRadiusMode} ${parent.curve2/2}${parent.borderRadiusMode} ${parent.curve3/2}${parent.borderRadiusMode} ${parent.curve4/2}${parent.borderRadiusMode}`;
        if (a.obj){
            let [b, c] = [a.obj.style, parent.inputColors];
            a.obj.style['z-index'] = `${parent.zIndex+4}`;
            if (!a.resizable) b['resize'] = 'none';
            b['font-family'] = `${c.font}`;
            b['font-size'] = `${c.size}px`;
            b['width'] = `${parent.trueW-3}px`;
            b['height'] = `${parent.trueH-4}px`;
            b['backgroundColor'] = `rgb(${c.fill.r}, ${c.fill.g}, ${c.fill.b}, ${c.fill.a/255})`;
            b['border-color'] = `rgb(${c.border.r}, ${c.border.g}, ${c.border.b}, ${c.border.a/255})`;
            b['border-width'] = `${c.border.weight}px`;
            b['outline-color'] = `rgb(${c.outline.r}, ${c.outline.g}, ${c.outline.b}, ${c.outline.a/255})`;
            b['color'] = `rgb(${c.text.r}, ${c.text.g}, ${c.text.b}, ${c.text.a/255})`;
            parent.injectionLogic(a);    
            
        };
        return;
    };
};

Box.prototype.message = function(callback = null, defaultText = '', alwaysRecord = false, resizable = false){
    let a = this.defineObject('message');
    a.state = true;
    a.alwaysRecord = alwaysRecord;
    a.callback = callback;
    a.defaultText = defaultText;
    a.resizable = resizable;
    return this;
};
Box.prototype.messageInjectCSS = function(key, value){
    let a = this.defineObject('message');
    a.injection.push({
        state: true,
        key: key,
        value: value
    });
    return this;
};