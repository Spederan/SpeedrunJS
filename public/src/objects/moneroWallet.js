class MoneroWallet{
    constructor(trueW, trueH, color){
        this.wallet = null;
        const openWallet = (seed, restoreHeight, seedOffset) => {
            try {
                const monero = new Monero(u, 1);
                console.log(monero)
                let callback = (wallet) => {
                    const refresh = 5000;
                    monero.sync(wallet, 
                        ((progress) => {if (this.wallet === wallet) {this.progress = progress}}), 
                        ((metadata) => {if (this.wallet === wallet) {this.metadata = metadata;}}),
                        ((received) => {if (this.wallet === wallet) {this.received = received}}), 
                        refresh);
                    this.monero = monero;
                    this.wallet = wallet;
                    
                };
                const privateCallback = (privateData) => {
                    this.privateData = privateData;
                };
                monero.openWallet(seed, restoreHeight, seedOffset, callback, privateCallback);
            } catch (error) {
                console.log("Monero does not exist. Try copying the files from './wallets/monero' to './public'.")
                return;
            };            
        };

        this.data = {loadedProgress: 0};
        this.cryptoWallet = new CryptoWallet(trueW, trueH, "Monero", "XMR", "./src/images/monero.png", true, false, true, this.data, color);
        this.master = this.cryptoWallet.master;
        this.clearMainData = () => {
            this.privateData = {};
            this.metadata = {};
            this.progress = {};
            this.data.mnemonicSeed = undefined;
            this.data.restoreHeight = undefined;
            this.data.seedOffset = "";
            this.data.loadedProgress = 0;
        };
        this.clearWalletData = () => {
            try {
                if (this.wallet) this.monero.close(this.wallet, false);
            } catch (e) {}
            this.wallet = null;
        };
        this.clearClientData = () => {
            this.cryptoWallet.mnemonicSeed = "";
            this.cryptoWallet.restoreHeight = "";
            this.cryptoWallet.seedOffset = "";
            this.cryptoWallet.page3?._seedOffsetInput?.clearInput();
            this.cryptoWallet.page3?._restoreHeight?.clearInput();
            this.cryptoWallet.page3?._mnemonic?.clearInput();
            this.cryptoWallet.page2?._seedOffsetInput?.clearInput();
            this.cryptoWallet.page2?._restoreHeight?.clearInput();
            this.cryptoWallet.page2?._mnemonic?.clearInput();
        };
        this.clearData = () => {
            this.clearWalletData();
            this.clearMainData();
            this.clearClientData();
            this.opened = false;
        };
        this.once = true;
        this.opened = false;
        this.submitted = false;
        this.seedOffsetTimer = 0;
        setInterval(() => {
            if (this.opened){
                if (this.progress?.percentDone){
                    this.data.loadedProgress = this.progress.percentDone;
                } else {
                    if (this.metadata && numExists(this.metadata.balance)){
                        this.data.loadedProgress = 1;
                    } else {
                        if (this.cryptoWallet.walletCreated){
                            if (this.data.loadedProgress < 0.95) this.data.loadedProgress+= (1/10/30);
                        };
                    };
                };
            };
            if (this.cryptoWallet.walletOpened){
                this.opened = true;
                if (this.once){
                    this.clearData();
                    this.once = false;
                };
                if (!this.submitted){
                    this.clearMainData();
                    this.clearWalletData();
                };      
                if (this.cryptoWallet.submitAllMetadata){
                    this.submitted = true;
                    openWallet(this.cryptoWallet.mnemonicSeed, this.cryptoWallet.restoreHeight, this.cryptoWallet.seedOffset)
                    this.cryptoWallet.submitAllMetadata = false;
                    this.cryptoWallet.walletOpened = false;
                    this.cryptoWallet.walletCreated = false;
                };
            };
            if (this.cryptoWallet.atHome){
                this.clearData();
                this.once = true;
                this.opened = false;
                this.cryptoWallet.submitAllMetadata = false;
                this.submitted = false;
                this.cryptoWallet.atHome = false;
                this.cryptoWallet.walletCreated = false;
            };
            if (this.metadata?.address){
                this.data.balance = this.metadata.balance;
                this.data.address = this.metadata.subaddress[0];
            };
            if (this.cryptoWallet.walletCreated){
                this.opened = true;
                if (this.once){
                    this.clearData();        
                    openWallet(u, u);
                    this.cryptoWallet.page2?._restoreHeight?.clearInput('Loading...');
                    this.cryptoWallet.page2?._mnemonic?.clearInput('Loading...');
                    this.once = false;
                };
                if (this.privateData?.seed){
                    this.data.mnemonicSeed = this.privateData.seed;
                    this.data.restoreHeight = this.privateData.restoreHeight;
                };
            };
            //? Send funds
            if (this.cryptoWallet.recipient_Address && this.cryptoWallet.recipient_Amount){
                this.monero.send(this.wallet, this.cryptoWallet.recipient_Address, this.cryptoWallet.recipient_Amount, (tx)=>{
                    this.data.balance-=  this.cryptoWallet.recipient_Amount;
                    this.cryptoWallet.recipient_Address = null;
                    this.cryptoWallet.recipient_Amount = null;
                    this.cryptoWallet.page4.sendAmountInput.clearInput();
                    this.cryptoWallet.page4.sendAddressInput.clearInput();
                })
            };
        }, 100);
    };
};

clientFunc.moneroWallet =  (parent) => {
    let a = parent.moneroWalletProperties;
    if (a?.state){
        parent.changeObj(a);
        if (!a.obj){
            let color = null;
            if (parent.boxProperties?.state){
                color = {r: parent.r, g: parent.g, b: parent.b, a: parent.a};
            };
            a.obj = new MoneroWallet(parent.trueW, parent.trueH, color);
            parent._();
        };
        if (a.obj?.master?.parentElement){
            if (a.parentInit){
                parent.parentElement.appendChild(a.obj.master.parentElement);
                a.parentInit = false;
            };
        } else {
            parent._();
        };
        return;
    };
};

Box.prototype.moneroWallet = function(){
    if (this.w < 300) this.w = 300;
    if (this.h < 500) this.h = 500;
    this.trueValues();
    try {
        let monero = new Monero();
    } catch (error) {
        console.log("Monero does not exist. Try copying the files from './wallets/monero' to './public'.");
        this.text("Monero does not exist. Try copying the files from './wallets/monero' to './public'.")
        return this;
    };  
    let a = this.defineObject('moneroWallet');
    a.state = true;
    return this;
};