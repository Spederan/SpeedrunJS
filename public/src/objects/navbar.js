class Navbar {
    constructor(w, h){
        this.options = [];
        this.master = box()
            .size(w,h)
    };
    loadOptions(arr, color, objInject){
        this.master
            .color(color.navbar.background.r, color.navbar.background.g, color.navbar.background.b, color.navbar.background.a)
            .outline(color.navbar.outline.r, color.navbar.outline.g, color.navbar.outline.b, color.navbar.outline.a)
        let options = [];
        let links = [];
        let callbacks = []
        for (let i in arr){
            if (typeof arr[i] === 'object'){
                if (!arr[i][0]) arr[i][0] = '';
                options.push(arr[i][0]);
                if (!arr[i][1]) arr[i][1] = '';
                links.push(arr[i][1]);
                if (!arr[i][2]) arr[i][2] = '';
                callbacks.push(arr[i][2]);
            } else {
                if (!arr[i]) arr[i] = '';
                options.push(arr[i]);
                links.push('');
                callbacks.push('');
            };
        };
        for (let i in options){
            this.options[i] = box();
            let x = 0;
            let y = 0;
            let w = 0;
            let h = 0;
            if (this.master.w > this.master.h){
                if (i > 0) x = this.options[i-1].x + this.options[i-1].w;
                y = 0;
                w = this.master.w/options.length;
                h = this.master.h;
            } else {
                x = 0;
                if (i > 0) y = this.options[i-1].y + this.options[i-1].h;
                w = this.master.w;
                h = this.master.h/options.length;
            };
            this.options[i]
                .position(x, y)
                .size(w+2, h)
                .color(color.unselected.background.r, color.unselected.background.g, color.unselected.background.b, color.unselected.background.a)
                .outline(color.unselected.outline.r, color.unselected.outline.g, color.unselected.outline.b, color.unselected.outline.a, color.unselected.outline.width)
                .text(options[i], color.size)
                .textFont(color.font)
                .textColor(color.unselected.text.r, color.unselected.text.g, color.unselected.text.b, color.unselected.text.a)
                .textOutline(color.unselected.textOutline.r, color.unselected.textOutline.g, color.unselected.textOutline.b, color.unselected.textOutline.a, color.unselected.textOutline.width)
                .textFont('sans-serif')
            if (this.options[i] && options[i]){
                this.options[i]
                    .detect(()=>{
                        this.options[i]
                            .color(color.selected.background.r, color.selected.background.g, color.selected.background.b, color.selected.background.a)
                            .outline(color.selected.outline.r, color.selected.outline.g, color.selected.outline.b, color.selected.outline.a, color.selected.outline.width)
                            .textColor(color.selected.text.r, color.selected.text.g, color.selected.text.b, color.selected.text.a)
                            .textOutline(color.selected.textOutline.r, color.selected.textOutline.g, color.selected.textOutline.b, color.selected.textOutline.a, color.selected.textOutline.width)
                        if (color.selected.callback) color.selected.callback(this.options[i], options[i], i);
                        }, 'over')
                    .detect(()=>{
                        this.options[i]
                            .color(color.unselected.background.r, color.unselected.background.g, color.unselected.background.b, color.unselected.background.a)
                            .outline(color.unselected.outline.r, color.unselected.outline.g, color.unselected.outline.b, color.unselected.outline.a, color.unselected.outline.width)
                            .textColor(color.unselected.text.r, color.unselected.text.g, color.unselected.text.b, color.unselected.text.a)
                            .textOutline(color.unselected.textOutline.r, color.unselected.textOutline.g, color.unselected.textOutline.b, color.unselected.textOutline.a, color.unselected.textOutline.width)
                        if (color.unselected.callback) color.unselected.callback(this.options[i], options[i], i);
                        }, 'out')
                    .detect(()=>{
                        if (color.pressed.state){
                            this.options[i]
                                .color(color.pressed.background.r, color.pressed.background.g, color.pressed.background.b, color.pressed.background.a)
                                .outline(color.pressed.outline.r, color.pressed.outline.g, color.pressed.outline.b, color.pressed.outline.a, color.pressed.outline.width)
                                .textColor(color.pressed.text.r, color.pressed.text.g, color.pressed.text.b, color.pressed.text.a)
                                .textOutline(color.pressed.textOutline.r, color.pressed.textOutline.g, color.pressed.textOutline.b, color.pressed.textOutline.a, color.pressed.textOutline.width)
                            if (color.pressed.callback) color.pressed.callback(this.options[i], options[i], i);
                        };
                    }, 'press')
                    .detect(()=>{
                        if (color.pressed.state){
                            this.options[i]
                                .color(color.selected.background.r, color.selected.background.g, color.selected.background.b, color.selected.background.a)
                                .outline(color.selected.outline.r, color.selected.outline.g, color.selected.outline.b, color.selected.outline.a, color.selected.outline.width)
                                .textColor(color.selected.text.r, color.selected.text.g, color.selected.text.b, color.selected.text.a)
                                .textOutline(color.selected.textOutline.r, color.selected.textOutline.g, color.selected.textOutline.b, color.selected.textOutline.a, color.selected.textOutline.width)
                        };
                        this.options[i].redirect(links[i], false);
                    }, 'release')
                if (callbacks[i]){
                    callbacks[i](this.options[i], options[i], i);
                };
            };
        };
        this.master.contain(this.options, false);
        if (objInject) objInject(this);
    };
};

clientFunc.navbar = (parent) => {
    if (parent.navbarProperties?.state){
        let a = parent.navbarProperties;
        parent.changeObj(a);
        if (!a.obj){
            a.obj = new Navbar(parent.trueW, parent.trueH);
            parent._();
        };
        if (a.obj?.master?.parentElement){
            if (a.parentInit){
                parent.parentElement.appendChild(a.obj.master.parentElement);
                a.obj.loadOptions(a.options, a.colors, parent.objectInject);
                a.parentInit = false;
            };
        } else {
            parent._();
        };
        return;
    };
};

Box.prototype.navbar = function(options = []){
    let a = this.defineObject('navbar');
    a.state = true;
    a.options = options;
    return this;
};
Box.prototype.navbarFont = function(size = 18, font = 'sans-serif'){
    let c = (this.defineObject('navbar')).colors;
    c.size = size;
    c.font = font;
    return this;
};
Box.prototype.navbarColor = function(r = 0, g = r, b = r, a = 255){
    let c = (this.defineObject('navbar')).colors.navbar.background;
    [r, g, b, a] = this.randomColor(r, g, b, a);
    c.r = r;
    c.g = g;
    c.b = b;
    c.a = a;
    return this;
};
Box.prototype.navbarOutline = function(r = 0, g = r, b = r, a = 255, weight = 1){
    let c = (this.defineObject('navbar')).colors.navbar.outline;
    [r, g, b, a] = this.randomColor(r, g, b, a);
    c.r = r;
    c.g = g;
    c.b = b;
    c.a = a;
    c.weight = weight;
    return this;
};
Box.prototype.navbarBoxOver = function(callback){
    let a = this.defineObject('navbar');
    a.colors.selected.callback = callback;
    return this;
};
Box.prototype.navbarBoxOverColor = function(r = 0, g = r, b = r, a = 255){
    let c = (this.defineObject('navbar')).colors.selected.background;
    [r, g, b, a] = this.randomColor(r, g, b, a);
    c.r = r;
    c.g = g;
    c.b = b;
    c.a = a;
    return this;
};
Box.prototype.navbarTextOverColor = function(r = 0, g = r, b = r, a = 255){
    let c = (this.defineObject('navbar')).colors.selected.text;
    [r, g, b, a] = this.randomColor(r, g, b, a);
    c.r = r;
    c.g = g;
    c.b = b;
    c.a = a;
    return this;
};
Box.prototype.navbarBoxOverOutline = function(r = 0, g = r, b = r, a = 255, weight = 1){
    let c = (this.defineObject('navbar')).colors.selected.outline;
    [r, g, b, a] = this.randomColor(r, g, b, a);
    c.r = r;
    c.g = g;
    c.b = b;
    c.a = a;
    c.weight = weight;
    return this;
};
Box.prototype.navbarTextOverOutline = function(r = 0, g = r, b = r, a = 255, weight = 1){
    let c = (this.defineObject('navbar')).colors.selected.textOutline;
    [r, g, b, a] = this.randomColor(r, g, b, a);
    c.r = r;
    c.g = g;
    c.b = b;
    c.a = a;
    c.weight = weight;
    return this;
};
Box.prototype.navbarBoxOut = function(callback){
    let a = this.defineObject('navbar');
    a.colors.unselected.callback = callback;
    return this;
};
Box.prototype.navbarBoxOutColor = function(r = 0, g = r, b = r, a = 255){
    let c = (this.defineObject('navbar')).colors.unselected.background;
    [r, g, b, a] = this.randomColor(r, g, b, a);
    c.r = r;
    c.g = g;
    c.b = b;
    c.a = a;
    return this;
};
Box.prototype.navbarTextOutColor = function(r = 0, g = r, b = r, a = 255){
    let c = (this.defineObject('navbar')).colors.unselected.text;
    [r, g, b, a] = this.randomColor(r, g, b, a);
    c.r = r;
    c.g = g;
    c.b = b;
    c.a = a;
    return this;
};
Box.prototype.navbarBoxOutOutline = function(r = 0, g = r, b = r, a = 255, weight = 1){
    let c = (this.defineObject('navbar')).colors.unselected.outline;
    [r, g, b, a] = this.randomColor(r, g, b, a);
    c.r = r;
    c.g = g;
    c.b = b;
    c.a = a;
    c.weight = weight;
    return this;
};
Box.prototype.navbarTextOutOutline = function(r = 0, g = r, b = r, a = 255, weight = 1){
    let c = (this.defineObject('navbar')).colors.unselected.textOutline;
    [r, g, b, a] = this.randomColor(r, g, b, a);
    c.r = r;
    c.g = g;
    c.b = b;
    c.a = a;
    c.weight = weight;
    return this;
};
Box.prototype.navbarBoxPress = function(callback){
    let c = (this.defineObject('navbar')).colors.pressed;
    c.state = true;
    c.callback = callback;
    return this;
};
Box.prototype.navbarBoxPressColor = function(r = 0, g = r, b = r, a = 255){
    let c = (this.defineObject('navbar')).colors.pressed;
    [r, g, b, a] = this.randomColor(r, g, b, a);
    c.state = true;
    let d = c.background;
    d.r = r;
    d.g = g;
    d.b = b;
    d.a = a;
    return this;
};
Box.prototype.navbarTextPressColor = function(r = 0, g = r, b = r, a = 255){
    let c = (this.defineObject('navbar')).colors.pressed;
    [r, g, b, a] = this.randomColor(r, g, b, a);
    c.state = true;
    let d = c.text;
    d.r = r;
    d.g = g;
    d.b = b;
    d.a = a;
    return this;
};
Box.prototype.navbarBoxPressOutline = function(r = 0, g = r, b = r, a = 255, weight = 1){
    let c = (this.defineObject('navbar')).colors.pressed;
    [r, g, b, a] = this.randomColor(r, g, b, a);
    c.state = true;
    let d = c.outline;
    d.r = r;
    d.g = g;
    d.b = b;
    d.a = a;
    d.weight = weight;
    return this;
};
Box.prototype.navbarTextPressOutline = function(r = 0, g = r, b = r, a = 255, weight = 1){
    let c = (this.defineObject('navbar')).colors.pressed;
    [r, g, b, a] = this.randomColor(r, g, b, a);
    c.state = true;
    let d = c.textOutline;
    d.r = r;
    d.g = g;
    d.b = b;
    d.a = a;
    d.weight = weight;
    return this;
};