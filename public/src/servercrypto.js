server.send = (onMessage = 'data', data, pow = 'instantpow', prefix = 'http://localhost:3000/') => {
    if (pow){
        hashpuzzle((hash)=>{
            server.send.state = true;
            server.send.onMessage = onMessage;
            server.send.data = {message: data, hashpuzzle: hash};
        }, pow, prefix);
    } else {
        server.send.state = true;
        server.send.onMessage = onMessage;
        server.send.data = {message: data, hashpuzzle: null};
    };  
};