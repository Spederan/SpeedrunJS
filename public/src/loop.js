let pageDivElement;
let windowFlare;
let drawAgain;
let pageDivWidth;
let pageDivHeight;
let smallestX = pageSize.w;
let smallestY = pageSize.h;
let largestX = 0;
let largestY = 0;
let socket;
        
setInterval(() => {
    if (server.state){
    if (!socket) socket = io.connect(server.port);
        for (let i in server.onMessages){
            socket.on(server.onMessages[i], (data) => { 
                if (server.callbacks[i]) server.callbacks[i](data);
            });
        };   
    };
}, 1000);     

const pageDiv = createHTMLElement('div', undefined, undefined, 'pageDiv');
    pageDiv.style['z-index'] = '0';
    pageDiv.style['left'] = '0';
    pageDiv.style['top'] = '0';
    pageDiv.style['position'] = 'relative';
pageDiv.style['visibility'] = 'hidden';
setTimeout(() => {
    pageDiv.style['visibility'] = 'visible';
}, 1);
const animationLoop = () => {
    frameCount++;
    let win = window,
        doc = document,
        docElem = doc.documentElement,
        body = doc.getElementsByTagName('body')[0],
        windowWidth = win.innerWidth || docElem.clientWidth || body.clientWidth,
        windowHeight = win.innerHeight|| docElem.clientHeight|| body.clientHeight;
    if (server?.send?.state){
        socket?.emit(server.send.onMessage, server.send.data);
        server.send.state = false;
    };
    drawAgain = false;
    pageSize.w = windowWidth - pageSize.wOffset;
    pageSize.h = windowHeight - pageSize.hOffset;
    if (pageSize.x !== canvasPrevious.x || pageSize.y !== canvasPrevious.y ||
        pageSize.w !== canvasPrevious.w || pageSize.h !== canvasPrevious.h || 
        pageSize.wOffset !== canvasPrevious.wOffset || pageSize.hOffset !== canvasPrevious.hOffset || resizeWindow.state){
        pageDivWidth = pageSize.w;
        pageDivHeight = pageSize.h;
        if (pageDivWidth > windowWidth) pageDivWidth = windowWidth;
        if (pageDivHeight > windowHeight) pageDivHeight = windowHeight;
        pageDiv.style['width'] = `${pageDivWidth}px`;
        pageDiv.style['height'] = `${pageDivHeight}px`;
        
        if (resizeWindow.state){
            if (!windowFlare){
                windowFlare = new Box();
            };
            if (resizeWindow.w > 0){
                pageDiv.style['overflow-x'] = 'scroll';
            } else {
                pageDiv.style['overflow-x'] = 'hidden';
            };
            if (resizeWindow.h > 0){
                pageDiv.style['overflow-y'] = 'scroll';
            } else {
                pageDiv.style['overflow-y'] = 'hidden';
            };
            windowFlare
                .color(255,255,255,0)
                .size(10,10)
                .position(pageSize.w+resizeWindow.w, pageSize.h+resizeWindow.h);
            resizeWindow.state = false;
        };
        drawAgain = true;
        canvasPrevious.x = pageSize.x;
        canvasPrevious.y = pageSize.y;
        canvasPrevious.w = pageSize.w;
        canvasPrevious.h = pageSize.h;
        canvasPrevious.wOffset = pageSize.wOffset;
        canvasPrevious.hOffset = pageSize.hOffset;
    };
    if (filter_speedrun_global && frameCount % 120 === 0){
        speedrun_global = speedrun_global.filter(function (el) {
            return el != null;
        });
        filter_speedrun_global = false;
    }; 
    for (i in speedrun_global){
        if (speedrun_global[i]){
            if (drawAgain) speedrun_global[i].drawAgain = true;
            if (speedrun_global[i].drawAgain){
                speedrun_global[i].drawAgain = false;
                speedrun_global[i].create();
            };
            speedrun_global[i].sustain();
        };
    };
if (window.requestAnimationFrame){
    requestAnimationFrame(animationLoop);
};
};
if (window.requestAnimationFrame){
    window.requestAnimationFrame(animationLoop)
} else {
    setInterval(animationLoop, 17);
};