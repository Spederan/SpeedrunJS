const deriveSecureKey = (password, callback, rounds = 1000, size = 512, base = "BASE90") => {
    injectWorker(() => {
        return pbkdf2(vars[0], vars[1], vars[2]);
    }, (key) => {
        callback(key)
    }, [password, rounds, size, base]);
};

let sybilShield;
const hashpuzzle = (callback, mode = 'fastpow', prefix = window.location.origin, rounds = undefined, targetLength = undefined, numPuzzles = undefined, timeout = undefined, worker = true) => {
    if (worker){
        injectWorker(() => {
            post = false;
            sybilShield = new SybilShield(vars[0], vars[1], vars[2], vars[3], vars[4], vars[5]);
            sybilShield.proveWork();
            sybilShield.checkWork((hash)=>{
                data = hash;
                post = true;
            });
        }, (hash) => {
            callback(hash);
        }, [mode, prefix, rounds, targetLength, numPuzzles, timeout], true);
    } else {
        setTimeout(() => {
            sybilShield = new SybilShield(mode, prefix, rounds, targetLength, numPuzzles, timeout);
            sybilShield.proveWork();
            sybilShield.checkWork(callback);
        }, 1000);
    };
};