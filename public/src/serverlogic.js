const server = (onMessage = 'data', callback = null, url = 'http://127.0.0.1:3000/') => {
    if (url){
        server.state = true;
        server.url = url;
        if (!server.callbacks){
            server.callbacks = [];
            server.onMessages = [];
        };
        server.onMessages.push(onMessage);
        server.callbacks.push(callback);
    } else {
        console.log("It seems you have either left out a url, or have disabled server tools, and therefore cannot use this.")
    };
};