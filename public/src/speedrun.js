const require = undefined;
function loadScript(pathToScript, callback) {
    try {
        var head = document.getElementsByTagName("head")[0];
        var script = document.createElement("script");
        script.type = "text/javascript";
        script.src = pathToScript + "?t=" + new Date().getTime();
        if (callback) {
            script.onload = callback;
        };
        head.appendChild(script);
    } catch (error) {
        
    }
    
};
if (!path){
    var path = './';
};
let components = [];
const necessaryComponents = [`${path}src/loop.js`];
let scripts = [
    `${path}src/SybilShielder/randomness.js`, `${path}src/clientUtilities.js`, `${path}src/libraries/percent-round.js`, 
    `${path}src/objects/box.js`, `${path}src/objects/div.js`, `${path}src/libraries/qrcode.min.js`, 
    `${path}src/objects/qrcode.js`,`${path}src/objects/rect.js`, 
    `${path}src/objects/text.js`, `${path}src/objects/image.js`, `${path}src/objects/button.js`, `${path}src/objects/polygon.js`, 
    `${path}src/objects/input.js`, `${path}src/objects/message.js`, `${path}src/objects/slider.js`, `${path}src/objects/select.js`, 
    `${path}src/objects/check.js`,`${path}src/objects/radio.js`, `${path}src/objects/colorPicker.js`,
    `${path}src/objects/navbar.js`, `${path}src/objects/graph.js`, `${path}src/objects/tab.js`, 
    `${path}src/objects/login.js`, `${path}src/objects/contact.js`, `${path}src/objects/mediaPlayer.js`, 
    ]; 
    scripts.push(
        `${path}src/SybilShielder/CryptoJS v3.1.2/core.js`, 
        `${path}src/SybilShielder/CryptoJS v3.1.2/x64-core.js`, 
        `${path}src/SybilShielder/CryptoJS v3.1.2/format-hex.js`, 
        `${path}src/SybilShielder/CryptoJS v3.1.2/enc-base64.js`, 
        `${path}src/SybilShielder/CryptoJS v3.1.2/md5.js`, 
        `${path}src/SybilShielder/CryptoJS v3.1.2/evpkdf.js`, 
        `${path}src/SybilShielder/CryptoJS v3.1.2/cipher-core.js`, 
        `${path}src/SybilShielder/CryptoJS v3.1.2/sha256.js`,
        `${path}src/SybilShielder/CryptoJS v3.1.2/sha512.js`,
        `${path}src/SybilShielder/CryptoJS v3.1.2/pbkdf2.js`, 
        `${path}src/SybilShielder/CryptoJS v3.1.2/aes.js`, 
        `${path}src/SybilShielder/javascript-bcrypt-master/bCrypt.js`, 
        `${path}src/SybilShielder/cryptography.js`,
        `${path}src/SybilShielder/SybilShielder.js`, 
        `${path}src/cryptoWorker.js`,
        `${path}src/libraries/socketio.min.js`,
        `${path}src/serverlogic.js`, 
        `${path}src/servercrypto.js`,
        ); 
scripts.push(`${path}src/objects/cryptoAccept.js`, `${path}monero.dist.js`, `${path}src/objects/cryptoWallet.js`, 
    `${path}src/objects/moneroWallet.js`);
scripts.push(`./main.js`, `${path}src/loadUserFiles.js`);  

function load(i) {
    if (i < scripts.length) {
        loadScript(scripts[i], function () {
            load(++i);
        });
    };
};
load(0);